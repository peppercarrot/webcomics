# Episode 32 script: The Battlefield

Low-angle panorama shot on the edge of a hill filled with thick grass and bushes. It's a misty gray day with thick gray clouds. Hundreds of experienced soldiers, archers, horsemen all of them highly equipped with complex armors, helmets, plate, swords are standing lined ready for a Battle. They are experienced in a way we can see it's a band of folks with scars, missing features, and such. Long and large flags on stick are also here to reminds it is not a horde of barbarian or pirates but the army of a small king. At the center, we can recognize two horsemen for their luxurious and bigger armors as well as for their special drapery: probably the King with his Officer on his side. 

Low angle shot on the King and the officer on their high horses surrounded by armored soldiers with spears. The King is around 40, thick beard long hair: the type of man who never had to question anything except the art of war. Probably one of this new rich barbarian king on Hereva. The officer is in contrast a thin intellectual men, shaved with a large forehead who spent probably most of his time managing maps, money, equipment and the diplomacy into the multiple wars of his King. They all look at the horizon carefully, probably where another army faces them.

Extreme long shot; flying above the army 'shoulder' and looking at the other side of the hill; discovering the mini silhouettes of many other soldiers and silhouette of flag shaped differently; their enemies. Their silhouette are more muscular, less armored and we can see skull on stick, silhouette of fur coat and rounded shield. Their civilization has a little something more primitive and careful reader will probably recognize something like an army of orcs.

---

Medium close-up on the King with more details on his sculpted dark armor and face, speaking to someone on his left (we guess it will be the officer). 

	KING
	Officer! Did you recruit a witch like I asked?

Medium close-up on the Officer, this one panic a bit while replying to his superior. He sweat, certainly because the King rules by fear and he is not so sure to have found someone up to his expectations...

	OFFICER
	Yes, my lord! She is standing beside you.

Close-up on the King a bit confused by the tone of the announcement; but still trying to understand why the officer behaves this way while turning head mid-way and keeping eyes on the Officer.

	KING
	...?

Close-up (sequence) on the King, looking at his right very surprised.

	KING
	?!!!

Long panel depicting Pepper with her usual curly hat, socks with red pattern and orange cat. She really brings a lot of contrast appearing here on the battlefield. She tries to be friendly and wave with a big smile to the King. 

	PEPPER
	Hi! My name is Pepp...

Full frontal shot on our three characters. Pepper (framed head only, camera is at the level of the horse of the King) surprised to be cut right in the middle of her speech. The King is red of anger and turned to his officer to shout at him with all his power.  The Officer bends his pose a bit under the screaming as would do a tree under wind. 

	KING
	FOOL!!! Why have you recruited this child?! I need a real battlefield witch!

---

Cowboy Shot on Pepper holding a scroll (the diploma signed from Cayenne, Thyme and Cumin - see ending of episode 31). She is clearly disappointed by how the king evaluated her and jumped into conclusion only by a quick sight. She tries to justify herself.

	PEPPER
	Excuse me! I'm a real Chaosah Witch. I even have a diploma that says...

Close-up on the King, looking at her not convinced.
   
	KING
	Silence!

Large shot of the King while starting to turn his horse to ignore Pepper. Pepper is surprised to be ignored this way. CARROT doesn't really understand why Pepper is not praised.

	KING
	I have no use for children in this army.
	Go home and play with your dolls.
	
	ARMY (various mens around)
	Ha! Ha! Ha!

Wide shot view from behind the line of warrior, they all turn back to the camera from this point of view, and Pepper, still angry is walking away the army followed by Carrot. Walking with an angry attitude.
    
	PEPPER
	Grrrr!!! 

---

Panorama from distance; tiny Pepper silhouette is now a kilometer away from the line, and cross the empty military camp: tents made of old dark fabrics and rounded stick of woods, fire still smoky a bit, old trees (like twisted olive trees) are here and there. We can guess at the center a more luxurious tent (the one of the king); but they are all open, abandoned (everyone joined the war after breakfast).
  
General view on Pepper walking the camp still angry. Carrot stopped a few meters behind her and get distracted by what the warrior were cooking on the fire: ribs with still little piece of meats. No more fire, but a thin smoke and a lot of ashes (proof the fire was big enough for all night and roasting a goat at breakfast). Bowl of wood with a dark mixture (the soup?) are still around the fire.
  
	PEPPER
	I don't believe this!
	I've studied for years, but nobody will take me seriously because...

Climax close-up on Pepper's face; she underlines the problematic of the story here (and the previous panel was her desire) for the one who did not get it with the introduction sequence. So it's important her frustration and anger is easy to read. 

	PEPPER:
	...I don't look experienced enough!

	SOUND FX (behind Pepper, like being hit with powder)
	POOF!

Close-up (sequence) Pepper turns around to look,  attracted by the "POOF" sound.
  
	PEPPER:
	... Carrot?

General view on Pepper alarmed looking at the extinguished fire; a silhouette of Carrot holding something is visible trough a cloud of dusty gray ashes. The audience can see the piece of meat on the ribs has disappeared. 

	PEPPER
	!?!

---

Mannequin panel where we can see Carrot after the cloud of ashes vanished, he is holding a piece of meat and he is happy eating it. But most surprising; his fur is covered with gray  ashes and fur is messy.

Cowboy shot of Pepper, she has mixed feeling between loosing patience and being amused of the appearance of Carrot: problems are really stacking together today so a part of her starts to take things with humor.

    PEPPER
    Really, Carrot? 
    I hope that meal was worth it. 
    You look hideous...

Close-up on Pepper, time freeze in her mind as she repeat her words, mumbling. 

	PEPPER
	...You look...

General view, Pepper has an Eureka moment and start to walk away from Carrot with an idea in mind. Carrot keeps looking at Pepper, questioning why she suddenly changes behavior,  while eating his piece of meat.

	PEPPER
	The appearance! Of course!

Mini panel montage: the hand of Pepper picking the fabric off of a dark tent.

Mini panel montage: the hand of Pepper breaks a twisted branch of one of those dry short trees.

Mini montage sequence: the hand of Pepper takes the clean ribs bones at the feet of Carrot.

Mini montage sequence: fingers of Peppers with a dark sauce, drawing into her arm or leg. A wooden bowl is visible.

--- 

Shot on lines of warriors, we can see their surprised face because of the loud, dark voice behind them, using the Drukaarti font to mimic an ancient accent and black speech-bubble: 

	PEPPER
	HEY! 
	I heard you were looking for a REAL WITCH?!?
	
	VARIOUS WARRIORS
	?!   ?!   ?!

General top-view: the army of warriors turns themselves to the origin of the voice on their back, letting a large path into the crowd.
  
Close up of the King and Officer also turning back surprised.
  
	KING
	?!
	
	Officer
	?!
  
Mannequin view bordering the vertical page: It's Pepper in disguise walking with confidence. She is dressed with dark drapery, collar of bones, the face mystically half hidden under her hood, messy hair, her naked arm and legs appearing through the drapery are tattooed with mystic runes. She also has a wood stick really twisted and match perfectly a stereotype of a dark experienced necromancer. She hold a confident smile. She is followed by a gray messy Carrot, happy to see the strategy of Pepper being effective.

---

General shot from behind, Pepper is now reaching the right side of the King; taking her place. The army, the King and the Officer are still surprised looking at her.

Close up on a cruel smile that Pepper act perfectly, hidden behind the hoodies and messy hair. She knows it is working.
  
Close up on the King, sweating a bit of discomfort in front of this new witch. We understand he struggles how to react.
    
General view of the king mumbling something to the hear of his officer. Pepper, who could listen to this drops her head on her wooden stick, suddenly discouraged. 
  
	KING
	On second thought, call that kid back.
	This one is probably too expensive for our budget.
	
	PEPPER:
	...

	TO BE CONTINUED
  
---