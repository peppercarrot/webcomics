# Transcript of Pepper&Carrot Episode 32 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 32: El campo de batalla

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|True|¡Oficial!
Rey|2|False|¿Habéis reclutado una bruja, como pedí?
Oficial|3|True|¡Sí, mi señor!
Oficial|4|False|Ella está ahí mismo, a vuestro lado.
Rey|5|False|¿...?
Pimienta|6|True|¡Hola!
Pimienta|7|False|Mi nombre es Pimie...
Rey|8|False|¡¡¿?!!
Rey|9|True|¡¡¡IMBÉCIL!!!
Rey|10|True|¡¿Por qué habéis contratado esta niña?!
Rey|11|False|¡Necesito una verdadera hechicera de combate!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡¿Oiga, perdone?!
Pimienta|2|True|Yo soy una auténtica bruja de Chaosah.
Pimienta|3|False|Aquí mismo tengo un diploma que...
Escritura|4|True|Diploma
Escritura|5|True|de
Escritura|6|False|Chaosah
Escritura|8|True|Cayena
Escritura|9|False|Comino
Escritura|7|True|Tomillo
Escritura|10|False|~ para Pimienta ~
Rey|11|False|¡SILENCIO!
Rey|12|True|No pinta nada una niña aquí con este ejército.
Rey|13|False|Vuelve a casa a jugar con tus muñecas.
Sonido|14|False|¡Slap!
Ejército.|15|True|¡JAJAJAJAJA!
Ejército.|16|True|¡JAJAJAJAJA!
Ejército.|17|True|¡JAJAJAJA!
Ejército.|18|False|¡JAJAJAJAJA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡No me lo puedo creer!
Pimienta|2|False|Tantos años de estudio y nadie me toma en serio bajo el pretexto de que...
Pimienta|3|False|...¡que no tengo suficiente experiencia!
Sonido|4|False|¡¡ POF !!
Pimienta|5|False|¡ZANAHORIA!|nowhitespace
Sonido|6|False|¡¡PAF!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¿En serio, Zanahoria?
Pimienta|2|True|Espero que esta comida haya valido la pena.
Pimienta|3|False|Tienes un aspecto horrible...
Pimienta|4|False|...Tienes un aspecto...
Pimienta|5|True|¡Las apariencias!
Pimienta|6|False|¡Eso es!
Sonido|7|False|¡Crac!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡EH!
Pimienta|2|False|He oído que estáis buscando a una AUTÉNTICA HECHICERA...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|True|Pensándolo mejor, llamad de nuevo a la niña.
Rey|2|False|Esta de aquí debe de costar muy cara.
Escritura|3|False|CONTINUARÁ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|5|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|3|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 1121 mecenas!
Pimienta|7|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|6|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|8|False|¡Gracias!
Pimienta|2|True|¿Sabías qué?
Créditos|1|False|31 de marzo de 2020 Dibujo & Guion: David Revoy. Lectores de la versión beta: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Version en castellano Traducción: TheFaico. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.2.9-beta, Inkscape 0.92.3 en Kubuntu 19.10. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
