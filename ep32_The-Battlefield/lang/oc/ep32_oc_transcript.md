# Transcript of Pepper&Carrot Episode 32 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 32 : Lo prat batalhièr

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|True|Oficièr !
Rei|2|False|Avètz logat una masca, coma o vos demandèri ?
Oficièr|3|True|Òc, sénher rei !
Oficièr|4|False|Es aquí, còsta de vos.
Rei|5|False|... ?
Pepper|6|True|Adissiatz !
Pepper|7|False|Me soni Pepp...
Rei|8|False|?!!
Rei|9|True|TRÒÇ D'ASE !!!
Rei|10|True|Perqué avètz logat aquesta drolleta ?!
Rei|11|False|Ai besonh d'una vertadièra masca de combat !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Eu, plèti ?!
Pepper|2|True|Soi una masca de Caosah vertadièra.
Pepper|3|False|A mai, ai un diplòma que...
Escritura|4|True|Diplòma
Escritura|5|True|de
Escritura|6|False|Caosah
Escritura|9|False|Cayenne
Escritura|8|True|Comin
Escritura|7|True|Frigola
Escritura|10|False|~ per Pepper ~
Rei|11|False|SILENCI !
Rei|12|True|Ai pas besonh d'una mainada dins aquesta armada.
Rei|13|False|Torna-te'n a l'ostal per jogar amb tas monacas.
Son|14|False|Slap !
Armada|15|True|HAHA HA HA !
Armada|16|True|HAHA HA HA !
Armada|17|True|HAHA HA HA !
Armada|18|False|HA HA HAHA !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Incredible !
Pepper|2|False|Totas aquelas annadas d'estudis e degun me pren pas al serios jos color que...
Pepper|3|False|...ai pas pro d'experiéncia !
Son|4|False|POF !!
Pepper|5|False|CARRÒT !
Son|6|False|PAF !!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|De vertat, Carròt ?
Pepper|2|True|Espèri qu'aquel repais s'o valiá.
Pepper|3|False|As una mina afrosa...
Pepper|4|False|... Semblas...
Pepper|5|True|Las aparéncias !
Pepper|6|False|Solide !
Son|7|False|Crac !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|E !
Pepper|2|False|Ai entendut a dire que cercàvetz una MASCA VERTADIÈRA ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|True|Fin finala, tornatz sonar la drolleta.
Rei|2|False|Aquesta deu costar los uèlhs del cap.
Escritura|3|False|DE SEGUIR...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vos tanben, podètz venir mecèna de Pepper&Carrot e aver vòstre nom inscriut aquí !
Pepper|3|True|Pepper&Carrot es completament liure, gratuit, open-source e esponsorisat mercés al mecenat de sos lectors.
Pepper|4|False|Aqueste episòdi a recebut lo sosten de 1121 mecènas !
Pepper|7|True|Anatz sus www.peppercarrot.com per mai d'informacions !
Pepper|6|True|Sèm sus Patreon, Tipeee, PayPal, Liberapay ... e d'autres !
Pepper|8|False|Mercé !
Pepper|2|True|O sabiatz ?
Crèdits|1|False|Lo 31 de març de 2020 Art & scenari : David Revoy. Lectors de la version bêta : Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Traduccion occitana (lengadocian) : Aure Séguier. Basat sus l'univèrs d'Hereva Creator : David Revoy. Manteneire màger : Craig Maloney. Redactors : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logicials : Krita 4.2.9-beta, Inkscape 0.92.3 sus Kubuntu 19.10. Licéncia : Creative Commons Attribution 4.0. www.peppercarrot.com
