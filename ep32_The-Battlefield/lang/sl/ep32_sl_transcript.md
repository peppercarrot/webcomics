# Transcript of Pepper&Carrot Episode 32 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 32: Bojišče

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|True|Častnik!
Kralj|2|False|Ste najeli čarovnico, kot sem naročil?
Častnik|3|True|Seveda, gospod!
Častnik|4|False|Poleg vas stoji.
Kralj|5|False|…?
Paprika|6|True|Živjo!
Paprika|7|False|Jaz sem Pap…
Kralj|8|False|?!!
Kralj|9|True|TESLO!!!
Kralj|10|True|Zakaj ste mi pripeljali to punčaro?!
Kralj|11|False|Potrebujem pravo bojno čarovnico!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Pardon!
Paprika|2|True|Saj sem prava kaosaška čarovnica.
Paprika|3|False|Imam celo potrdilo o …
Napis|4|True|Spričevalo
Napis|5|True|Kaosaha
Napis|6|False|-
Napis|8|True|Kajenka
Napis|9|False|Kumina
Napis|7|True|Timijana
Napis|10|False|~ za Papriko ~
Kralj|11|False|JEZIK ZA ZOBE!
Kralj|12|True|V moji vojski ni prostora za mularijo.
Kralj|13|False|Pojdi se domov igrat s punčkami.
Zvok|14|False|Paf!
Vojska|15|True|HAHA HA HA!
Vojska|16|True|HAHA HA HA!
Vojska|17|True|HAHA HA HA!
Vojska|18|False|HA HA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Saj ni res!
Paprika|2|False|Leta in leta izobrazbe, pa me ne jemljejo resno samo zato, ker …
Paprika|3|False|… nisem videti dovolj izkušena!
Zvok|4|False|PUF !!
Paprika|5|False|KORENČEK!
Zvok|6|False|PAF !!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Resno, Korenček?
Paprika|2|True|Upam, da si zdaj zadovoljen.
Paprika|3|False|Videti si obupno …
Paprika|4|False|… videti si …
Paprika|5|True|Seveda!
Paprika|6|False|Videz!
Zvok|7|False|Škrc!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|HEJ!
Paprika|2|False|Slišala sem, da iščete PRAVO ČAROVNICO?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|True|Če pa zdaj pomislim, raje pokličite nazaj tisto punčaro.
Kralj|2|False|Tale tukaj je videti predraga.
Napis|3|False|SE NADALJUJE …

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|3|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 1121 podpornikov!
Paprika|7|True|Obišči www.peppercarrot.com za več informacij!
Paprika|6|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|8|False|Hvalaaaa!
Paprika|2|True|Že veš?
Zasluge|1|False|31. marec 2020 Piše in riše: David Revoy. Testni bralci: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin in Vejvej. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 4.2.9-beta in Inkscape 0.92.3 na Kubuntu 19.10. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
