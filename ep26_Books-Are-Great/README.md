# Episode 26: Books Are Great

![cover of episode 26](https://www.peppercarrot.com/0_sources/ep26_Books-Are-Great/low-res/Pepper-and-Carrot_by-David-Revoy_E26.jpg)

## Comments from the author

About the story; on previous episode 25, we ended on a panel where Pepper had to read stacks of books. A possible interpretation was that "books are boring"...

Episode 26 was created to do away with this faulty interpretation. That's how "Books Are Great" was born. This episode shows (without obviously telling) how Pepper is slowly growing and evolving. This episode also fills an undetermined period of time after the rebuild of the house. So, it will let me jump directly to a new arc (probably related to the exam of Pepper) on the next episode. I hope you'll like this episode with its mix of adventure/comedy.

From [Beta-reading Forum of episode 26](https://framagit.org/peppercarrot/webcomics/-/issues/42)
