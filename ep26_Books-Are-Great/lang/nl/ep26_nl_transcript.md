# Transcript of Pepper&Carrot Episode 26 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 26: Boeken zijn geweldig

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Boeken zijn geweldig!
Pepper|3|False|Dit is van onschatbare waarde!
Pepper|2|True|In dit zeldzame boek beschrijft een avonturier hoe je dit kasteel kunt binnendringen.
Pepper|4|False|Bijvoorbeeld: dit kwade oog op de voordeur schiet vuurballen af tegen ongewenste bezoekers.
Pepper|5|False|Maar als je voorzichtig van de zijkant komt, zodat het je niet ziet …

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|… en het dan bedekt met je hoed …
Pepper|3|False|… dan gaat de deur vanzelf open!
Pepper|4|True|De auteur zegt dat je de hoed beter kunt laten hangen, om straks makkelijk weg te kunnen!
Pepper|5|False|Boeken zijn echt geweldig!
Geschrift|6|False|“Spring meteen over de balustrade van het eerste balkon."
Pepper|7|False|Oké!
Geluid|9|False|BOING
Geluid|10|False|BOING
Geschrift|8|False|“Je ervaart misschien een vallend gevoel. Dit is normaal. Je snijdt zo een stuk af.”
Geschrift|11|False|“Neem gerust de tijd om het verlichte spinnenweb te bekijken. De spinnen zijn fotosynthetisch, en leven van het weerspiegelde licht.”
Geschrift|12|False|“Ik raad aan een dutje te doen om weer helemaal fit te worden.”
Pepper|13|False|Boeken zijn echt perfect!
Geluid|2|False|Poef!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Ik weet dat katten niet graag nat worden.
Pepper|3|True|Geen zorgen!
Pepper|4|False|Daar zijn boeken ook goed voor!
Pepper|5|True|Ik ben hier nog geen tien minuten binnen, en ik zit al in de schatkamer blaadjes van deze zeldzame Waterboom te plukken.
Pepper|6|False|Boeken zijn magisch!
Pepper|1|True|O, Carrot …

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geschrift|3|False|“Omdat je een stuk afgesneden hebt, kom je nu bij de kamer van De Bewaker.”
Pepper|4|True|Hmm …
Pepper|5|True|De grote baas?!?
Pepper|6|False|Waar dan?
Pepper|7|True|AHA …
Pepper|8|False|Gevonden!
Pepper|11|True|Pfff …
Pepper|12|False|Veel te simpel als je zijn zwakke plek kent!
Geschrift|1|False|“Pak onderweg een veer mee.”
Pepper|2|False|Veren zat hier!
Geluid|9|True|kietel
Geluid|10|False|kietel

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Sjkrrkrr!
Pepper|2|False|Aha! Hier is de uitgang!
Geluid|3|False|Wooop!
Pepper|5|True|Ieuw …
Pepper|6|False|Ik weet zeker dat in dit boek staat hoe je dit “ding” los kan krijgen …
Pepper|7|False|Carrot, kun je dat even voorlezen?
Pepper|8|True|Uhm … natuurlijk kun je dat niet …
Pepper|9|False|Meh …
Pepper|4|False|?!
Pepper|12|False|Grrr!!!
Pepper|10|True|IK WIST HET!!!
Pepper|11|True|Het was te mooi om waar te zijn!
Pepper|13|False|Boeken zijn toch niet goed voor álles …
Geluid|14|False|Paf!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|… tenzij
Pepper|2|False|HAAJAAAAA!!!
Geluid|3|False|POK!
Pepper|5|False|… zijn boeken geweldig!
Pepper|4|True|Nee, het is waar; echt in elke situatie …
Verteller|6|False|- EINDE -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Jij kunt ook een patroon van Pepper&Carrot worden op www.patreon.com/davidrevoy
Aftiteling|3|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 1098 patronen:
Aftiteling|2|False|Licentie: Creative Commons Naamsvermelding 4.0. Software: Krita 4.1, Inkscape 0.92.3 op Kubuntu 17.10. Tekeningen & verhaal: David Revoy. Scriptdokter: Craig Maloney. Beta-feedback: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Nederlandstalige versie Vertalers: Willem Sonke, Midgard, Marno van der Maas. Gebaseerd op het universum van Hereva Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28 juli 2018 www.peppercarrot.com
