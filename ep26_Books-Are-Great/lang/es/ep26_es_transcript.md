# Transcript of Pepper&Carrot Episode 26 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 26: Los libros son geniales

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Los libros son geniales!
Pimienta|3|False|¡Es realmente una maravilla!
Pimienta|2|True|Este libro contiene las notas de un aventurero veterano. Describe todo lo que hay en esta mazmorra.
Pimienta|4|False|Por ejemplo, el ojo maléfico de la puerta principal lanza bolas de fuego para rechazar a los intrusos.
Pimienta|5|False|Pero, si nos acercamos por el lado, sin que nos vea...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|...y cubrimos el ojo con un sombrero...
Pimienta|3|False|...¡la puerta se debería abrir por sí sola!
Pimienta|4|True|Aconseja igualmente dejar el sombrero ahí para escapar fácilmente!
Pimienta|5|False|¡Los libros son verdaderamente geniales!
Escritura|6|False|«Salta directamente por encima de la barandilla del primer balcón.»
Pimienta|7|False|¡OK!
Sonido|10|False|BOING
Sonido|11|False|BOING
Escritura|8|True|«Notarás una sensación de caída.»
Escritura|12|False|«Disfruta de la comodidad de la tela iluminada sin ningún riesgo: estas arañas fotosintéticas se alimentan de la luz que se refleja.»
Escritura|13|False|«Recomiendo echar una siesta rápida para recuperar todas las fuerzas. »
Pimienta|14|False|¡Adoro los libros!
Sonido|2|False|¡Puf!
Escritura|9|False|«Es normal. Es un atajo.»

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|2|False|Sé bien cuanto detestáis mojaros los gatos.
Pimienta|3|True|¡No te preocupes!
Pimienta|4|False|¡Los libros son también útiles para eso!
Pimienta|5|True|Y en menos de diez minutos tras entrar a la mazmorra, aquí estoy en la sala del tesoro recogiendo las hojas mágicas del fabuloso Árbol del Agua.
Pimienta|6|False|¡Los libros son mágicos!
Pimienta|1|True|Oh, Zanahoria...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|3|False|« Como has tomado el atajo debes estar llegando a la sala del Guardián.»
Pimienta|4|True|Hmm...
Pimienta|5|True|¡¿El jefe final?!
Pimienta|6|False|¿Pero dónde?
Pimienta|7|True|¡AJÁ!
Pimienta|8|False|¡Te encontré!
Pimienta|11|True|¡Pffff... !
Pimienta|12|False|¡Demasiado fácil cuando ya se conoce el punto débil!
Escritura|1|False|«Llévate una pluma durante el camino.»
Pimienta|2|False|¡No faltan plumas por aquí!
Sonido|9|True|guili
Sonido|10|False|guili

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡Crrrr!
Pimienta|2|False|¡Eso es! ¡Aquí está la salida!
Sonido|3|False|¡Wooop!
Pimienta|5|True|Puaj…
Pimienta|6|False|Estoy segura que tiene que haber instrucciones para quitarme esta «cosa»...
Pimienta|7|False|Zanahoria... ¿me puedes leer esto?
Pimienta|8|True|Emm... por supuesto que no...
Pimienta|9|False|Pff...
Pimienta|4|False|?!
Pimienta|12|False|¡¡Grrrr!!
Pimienta|10|True|¡¡¡LO SABÍA!!!
Pimienta|11|True|¡Era demasiado bonito para ser verdad!
Pimienta|13|False|Puede que los libros no sean la solución para todo, al final...
Sonido|14|False|¡Paf!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|...a menos que...
Pimienta|2|False|¡¡¡YAAAAAAAA!!!
Sonido|3|False|¡PAF!
Pimienta|5|False|...¡los libros son geniales!
Pimienta|4|True|Eh, no, de hecho, para cualquier situación...
Narrador|6|False|- FIN -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
Créditos|3|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 1098 mecenas:
Créditos|2|False|Licencia: Creative Commons Atribución 4.0. Software: Krita 4.1, Inkscape 0.92.3 en Kubuntu 17.10. Arte & guion: David Revoy. Corrector de guion: Craig Maloney. Lectores de la versión beta: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Versión en castellano Traducción: TheFaico. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28 julio 2018 www.peppercarrot.com
