# Transcript of Pepper&Carrot Episode 26 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 26: Bøger er fantastiske

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Bøger er fantastiske!
Pepper|3|False|Den er et rent vindunder!
Pepper|2|True|Denne sjældne bog indeholder noter fra en erfaren eventyrer og beskriver alt om dette borgtårn.
Pepper|4|False|For eksempel kaster det onde øje på hoveddøren ildkugler mod ubudne gæster.
Pepper|5|False|Men hvis man går derhen fra siden, ude af syne...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...og sætter sin hat for øjet…
Pepper|3|False|...så burde døren gå op af sig selv!
Pepper|4|True|De anbefaler endda at lade hatten blive på, for hurtigt at kunne komme ud!
Pepper|5|False|Bøger er så fantastiske!
Skrift|6|False|”Hop med det samme over den første balkon.”
Pepper|7|False|OK!
Lyd|9|False|BOING
Lyd|10|False|BOING
Skrift|8|False|”De vil få en følelse af frit fald. Det er helt normalt. Det er en genvej.”
Skrift|11|False|”Nyd det lysende edderkoppe-spinds komfort uden risiko: disse fotosyntetiske edderkopper lever af lyset, der spejler sig i det.”
Skrift|12|False|”Jeg anbefaler en lur for at få kræfterne tilbage.”
Pepper|13|False|Jeg elsker bøger!
Lyd|2|False|Puf!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Jeg ved, hvor meget katte hader at blive våde.
Pepper|3|True|Bare rolig!
Pepper|4|False|Bøger er også gode paraplyer!
Pepper|5|True|Og mindre end ti minutter efter jeg gik ind i tårnet, er jeg nu i skatkammeret ved at plukke det famøse vandtræs magiske blade.
Pepper|6|False|Bøger er magiske!
Pepper|1|True|Åh, Carrot…

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|3|False|”Nu hvor De har taget genvejen, burde De ankomme til vogterens sal.”
Pepper|4|True|Hmm…
Pepper|5|True|Den endelige Boss?!
Pepper|6|False|Men hvor?
Pepper|7|True|AH-HA!
Pepper|8|False|Fundet!
Pepper|11|True|Pffff...!
Pepper|12|False|Alt for nemt når man kender hans svaghed!
Skrift|1|False|"Saml en fjer undervejs."
Pepper|2|False|Der mangler ikke fjer her!
Lyd|9|True|kilde
Lyd|10|False|kilde

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Krrrr!
Pepper|2|False|Aha! Her er udgangen!
Lyd|3|False|Wooop!
Pepper|5|True|Adr…
Pepper|6|False|Jeg er sikker på, at der står noget om hvordan man fjerner denne ”tingest”…
Pepper|7|False|Carrot… kan du læse det for mig?
Pepper|8|True|Øh… Nåh nej…
Pepper|9|False|Pff…
Pepper|4|False|?!
Pepper|12|False|Grrrr!!!
Pepper|10|True|JEG VIDSTE DET!!!
Pepper|11|True|Det var for godt til at være sandt!
Pepper|13|False|Måske kan bøger alligevel ikke løse alt…
Lyd|14|False|Bum!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|…medmindre…
Pepper|2|False|AAAAAAAAA!!!
Lyd|3|False|Bam!
Pepper|5|False|… Bøger er fantastiske!
Pepper|4|True|Jo det er rigtigt... I alle situationer...
Fortæller|6|False|- SLUT -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
Credits|3|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 1098 tilhængere:
Credits|2|False|Licens: Creative Commons Attribution 4.0. Værktøj: Krita 4.1, Inkscape 0.92.3 sur Kubuntu 17.10. Tegning og manuskript: David Revoy Script doctor: Craig Maloney. Rettelser: Craig Maloney, CalimeroTeknik. Beta-læsere: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke Alapetite, Alexandre Alapetite. Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nartance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28 juli 2018 www.peppercarrot.com
