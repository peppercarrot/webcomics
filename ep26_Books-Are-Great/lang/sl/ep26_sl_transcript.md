# Transcript of Pepper&Carrot Episode 26 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 26: Knjige so krasne

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Knjige so krasne!
Paprika|3|False|Neprecenljiva je!
Paprika|2|True|Tale edinstvena knjiga vsebuje zapise izkušenih pustolovcev o tej temnici.
Paprika|4|False|Na primer, to zlobno kukalo na vhodnih vratih strelja smrtonosne ognjene krogle, da bi odgnalo nezaželene obiskovalce.
Paprika|5|False|A če se mu previdno približam s strani, da me ne vidi …

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|… in ga potem pokrijem s klobukom …
Paprika|3|False|…se bodo vrata samodejno odprla!
Paprika|4|True|Knjiga celo priporoča, naj klobuk pustim na vratih za enostavnejši pobeg!
Paprika|5|False|Knjige so res krasne!
Napis|6|False|„Skočite navzdol s prvega balkona.“
Paprika|7|False|No, prav!
Zvok|9|False|BOINK
Zvok|10|False|BOINK
Napis|8|False|„Zdelo se vam bo, da padate, a ne skrbite. To je bližnjica.“
Napis|11|False|„Brez skrbi se ustavite na svetleči mreži, ki vas bo ujela. Pajki na njej so fotosintetični in se hranijo samo s svetlobo z mreže.“
Napis|12|False|„Priporočam kratek dremež, da si povrnete moči.“
Paprika|13|False|Obožujem knjige!
Zvok|2|False|Puf!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|2|False|saj vem, da se mački ne marate zmočiti.
Paprika|3|True|Brez skrbi!
Paprika|4|False|Knjige so uporabne tudi za to!
Paprika|5|True|In manj kot deset minut po vstopu v temnico že sedim v zakladnici in obiram liste z redkega vodnega drevesa.
Paprika|6|False|Knjige so čarobne!
Paprika|1|True|Ah, Korenček,

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|3|False|„Ker ste šli po bližnjici, boste šele sedaj prispeli v stražarjevo sobo.“
Paprika|4|True|Hmm…
Paprika|5|True|Veliki nasprotnik ?!?
Paprika|6|False|Ampak kje je?
Paprika|7|True|A-ha!
Paprika|8|False|Našla!
Paprika|11|True|Pfff…!
Paprika|12|False|Z vsem tem znanjem je kar prelahko!
Napis|1|False|„Med potjo poberite pero.“
Paprika|2|False|Perja tukaj res ne manjka!
Zvok|9|True|gic
Zvok|10|False|gic

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Škrrkrr!
Paprika|2|False|Aha! Tamle je izhod!
Zvok|3|False|Vuuš!
Paprika|5|True|Bljak!
Paprika|6|False|Zagotovo v knjigi piše, kako se odstrani to gnusobo …
Paprika|7|False|Korenček, a mi lahko prebereš?
Paprika|8|True|A. Jasno, da ne …
Paprika|9|False|Šment …
Paprika|4|False|?!
Paprika|12|False|Grrr!!!
Paprika|10|True|SAJ SEM VEDELA!!!
Paprika|11|True|Bilo je čisto prelepo, da bi bilo res!
Paprika|13|False|Mogoče ti pa knjige le ne morejo pomagati v vseh primerih …
Zvok|14|False|Paf!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Razen če …
Paprika|2|False|HAAAAAAAA!!!
Zvok|3|False|TRESK!
Paprika|5|False|… so knjige krasne!
Paprika|4|True|Ne, bo kar držalo. V prav vseh primerih …
Pripovedovalec|6|False|- KONEC -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Licenca: Creative Commons Priznanje avtorstva 4.0. Programska oprema: Krita 4.1 in Inkscape 0.92.3 na Kubuntu 17.10. Piše in riše: David Revoy. Zdravnik za scenarije: Craig Maloney. Testni bralci: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire in Zeograd. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac, Valvin. Popravki: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28. julij 2018 www.peppercarrot.com
Zasluge|3|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
Zasluge|2|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 1098 bralcev:
