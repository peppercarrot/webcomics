# Transcript of Pepper&Carrot Episode 38 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tytuł|1|False|Odcinek 38: Uzdrowiciel

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Finalnie..
Vinya|2|False|Udało nam się to dokończyć!
Fritz|3|False|Tak, to było trudne. Teraz rozumiemy dlaczego tak wiele przygód tu utknęło.
Fritz|4|False|Co z Brasic?
Fritz|5|False|Brasic? Brasic!
Fritz|6|False|Cholera! Nie zauważyłem, że jesteś tak bardzo zraniony!
Vinya|7|False|Uzdrowicielko!
Vinya|8|False|Potrzebujemy Twojej pomocy,SZYBKO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Odsuńcie się na bok...
Dźwięk|2|False|Dzing
Brasic|3|False|Moje rany zniknęły.
Brasic|4|False|Za każdym razem jest ten sam cud.
Brasic|5|False|Twój talent jest niezwykły, Uzdrowicielko!
Brasic|6|False|Nie zwlekajmy. Skarb czeka na nas!
Fritz|7|False|Tutaj tutaj!
Fritz|8|False|Dzięki Niej, jesteśmy niezwyciężeni!
Fritz|9|False|Chodźmy znaleźć następnego potwora!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Forsa!
Vinya|2|False|Nie, ja! Ja!
Pepper|3|False|?
Cayenne|4|False|Co ty myślisz, że robisz?
Cayenne|5|False|Szukałyśmy Cię miesiącami.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Nie jest to oczywiste?
Pepper|2|False|Postanowiłam rzucić wszystko i zmienić moją karierę.
Pepper|3|False|Od teraz jestem Wielką Uzdrowiecielką Peph'Ra, jak widać.
Cayenne|4|False|...
Pepper|5|False|"Ale dlaczego", możesz zapytać?!
Pepper|6|False|To proste!
Pepper|7|False|Mam tego dość!
Pepper|8|False|Mam dość bycia ściganym, dość bycia wyśmiewanym, a przede wszystkim, mam dość tego, że nie mogę znaleźć pracy po tak długim studiowaniu!
Pepper|9|False|Przynajmniej tutaj jestem potrzebna i doceniana za moje umiejętności!
Brasic|10|False|Uzdrowiecielko!
Brasic|11|False|Nie oddalaj się za daleko!
Brasic|12|False|Vinya dostał paskudny cios.
Dźwięk|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|To wszystko.
Pepper|2|False|Coś jeszcze?
Cayenne|3|False|Tak.
Cayenne|4|False|Imię na liście gończym jest normalne dla wiedźmy Chaosu.
Cayenne|5|False|To nawet oznaka dobrej reputacji.
Cayenne|6|False|Zignoruj malkontentów i tych co-ty, stwórz swoją własną kariere jeśli nikt ci jej nie da.
Cayenne|7|False|A przede wszystkim: Ty nie jesteś Uzdrowicielką.
Cayenne|8|False|Zauważą, prędzej lub później, kiedy skończy Ci się ta niesamowita ilość Łez Feniksa które trzymasz przy sobie, udając, że masz nowe moce.
Pepper|9|False|Co... ?
Pepper|10|False|Jak Ty to...
Pepper|11|False|...zgadłaś.
Cayenne|12|False|Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr!!!
Pepper|2|False|Cóż, jeśli będę potrzebować wiecej to zdobędę więcej!
Pepper|3|False|Ponieważ TO DZIAŁA!
Pepper|4|False|W przeciwieństwie do wszystkich Twoich pokręconych rzeczy Chaosu.
Pepper|5|False|GRRR...!
Brasic|6|False|Uważaj!
Brasic|7|False|Potwór zmienia kształt!
Monster|8|False|CReeeeeeeee
Monster|9|False|eeeeeeeeeeeeeee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|CReeeeeeeeeeeeeeeeee
Monster|2|False|eeeeeeeeeeeeeeeee
Monster|3|False|eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
Dźwięk|4|False|CRRRR! ! !|nowhitespace
Dźwięk|5|False|CRRRR! ! !|nowhitespace
Dźwięk|6|False|CRRRR! ! !|nowhitespace
Dźwięk|7|False|Crack! ! !|nowhitespace
Dźwięk|8|False|Crack! ! !|nowhitespace
Dźwięk|9|False|Crack! ! !|nowhitespace
Dźwięk|10|False|Crack! ! !|nowhitespace
Dźwięk|11|False|Crack! ! !|nowhitespace
Dźwięk|12|False|Crack! ! !|nowhitespace
Monster|13|False|eeeeeeee...
Dźwięk|14|False|Dzeeyoo !!|nowhitespace
Vinya|15|False|Ojej, co za zamieszanie!
Brasic|16|False|Ochraniaj mnie. Idę uciszyć to na dobre!
Brasic|17|False|Słyszałes to, potworze?!
Brasic|18|False|Miej oko na mnie na wszelki wypadek, Uzdrowicielko...
Brasic|19|False|Uzdrowiecielko?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|OOPS!
Pepper|2|False|Nie, nie, nie!
Brasic|3|False|Ho ho ho!
Brasic|4|False|Spójrz na to! Ja myślę, że płacz bestii to było zbyt wiele dla naszej Uzdrowiecielki.
Brasic|5|False|Bła ha ha!
Fritz|6|False|HAHA HA HA!
Fritz|7|False|Nie tylko tamto, ale to jest całe niebieskie!
Vinya|8|False|HAHA HA HA!
Vinya|9|False|I Ty myślisz, że to może być też wyleczone?
Fritz|10|False|Ugh, obrzydliwe!
Fritz|11|False|HAHA HA HA!
Brasic|12|False|BłA HA HA!
Pepper|13|False|...
Fritz|14|False|HEJ!
Brasic|15|False|Poczekaj! My tylko żartowaliśmy!
Vinya|16|False|Uzdrowiecielko!
Cayenne|17|False|Witamy ponownie.
Tytuł|18|False|- Koniec -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Kwiecień 26, 2023 Rysunek i scenariusz: David Revoy. Beta czytelnicy: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Angielskie tłumaczenie: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, CitizenK4te, Craig Maloney, Estefania de Vasconcellos Guimaraes, Frederic Mora, Jelfo, Marno van der Maas. Korekta: H-Games, Menhir. Oparty na wszechświecie Hereva Twórca: David Revoy. Główny opiekun: Craig Maloney. Pisarze: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korektorzy: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Oprogramowanie: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. Licensja: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Czy wiedziałeś?
Pepper|3|False|Pepper&Carrot jest całkowicie darmowy(libre), open-source i jest sponsorowany dzięki patronatowi czytelników.
Pepper|4|False|Za ten odcinek, podziękowania należą się 1084 patronom!
Pepper|5|False|Ty także możesz zostać patronem Pepper&Carrot i otrzymać swoje imię tutaj!
Pepper|6|False|Jesteśmy na Patreon, Tipeee, PayPal, Liberapay ... i innych!
Pepper|7|False|Sprawdź www.peppercarrot.com po więcej informacji!
Pepper|8|False|Dziękuję!
