# Transcript of Pepper&Carrot Episode 38 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka luka tu wan: jan sijelo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Winja|1|False|pini la...
jan Winja|2|False|mi kama moli e ona!
Fritz|3|False|lon. moli ona li wile e pali mute. jan mute li lukin tawa lon nasin ni. tenpo ni la mi sona e ike pi tawa ona.
Fritz|4|False|jan Pasi li lon seme?
Fritz|5|False|jan Pasi o? jan Pasi o!
Fritz|6|False|ike a! mi kama ala lukin e pakala pi sijelo sina!
jan Winja|7|False|jan sijelo o!
jan Winja|8|False|mi wile e pali sina, O KAMA!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|o weka tan ona...
kalama|2|False|Ssuno
jan Pasi|3|False|pakala li weka tan sijelo mi.
jan Pasi|4|False|pakala ale la pona wawa li tan sina.
jan Pasi|5|False|ken sina li wawa suli, jan pi pali sijelo o!
jan Pasi|6|False|mi o awen tawa. mani li lon pini pi nasin mi!
Fritz|7|False|toki pona!
Fritz|8|False|jan ni la, ala li ken pini e mi!
Fritz|9|False|mi wile alasa e ike kama!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pasi|1|False|mi open!
jan Winja|2|False|ala, mi open! mi open a!
jan Pepa|3|False|?
jan Kajen|4|False|pali sina li kama seme a?
jan Kajen|5|False|mi alasa e sina lon tenpo suli.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|sina sona ala anu seme?
jan Pepa|2|False|mi kama pini e nasin sina. mi ante e nasin mi.
jan Pepa|3|False|o lukin. mi kama jan suli wawa Pe Pa.
jan Kajen|4|False|...
jan Pepa|5|False|sina ken wile sona: "ni li tan seme?!"
jan Pepa|6|False|o kute pona!
jan Pepa|7|False|NASIN SINA LI IKE!
jan Pepa|8|False|ike la, jan li alasa e mi. ike la, jan li pilin musi tan mi. ike suli la, mi kama ala pali mani tan nasin sona sina. mi pana e tenpo suli tawa ona a. taso ala li pona.
jan Pepa|9|False|jan poka sin mi li wile e ken pi mi wan a!
jan Pasi|10|False|jan sijelo o!
jan Pasi|11|False|o awen lon poka!
jan Pasi|12|False|jan Winja li kama pakala.
kalama|13|False|Ssuno

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|ni li ni.
jan Pepa|2|False|ijo ante li lon ala lon?
jan Kajen|3|False|lon.
jan Kajen|4|False|jan li alasa e sina la, ni li ike ala a tawa kulupu Pakalaa.
jan Kajen|5|False|lon la ni li ken nasin pi kulupu Pakalaa.
jan Kajen|6|False|jan pi toki ike en jan pi wile ike li suli ala. jan ala li kama e pali e mani tawa sina la, o pali e nasin pali sin sina.
jan Kajen|7|False|a, suli la: sina pali ala e pona sijelo.
jan Kajen|8|False|telo pi waso suno li mute a lon sina. taso ona li kama lili lon tenpo. sina jo e ken sin ala e telo ken taso la kulupu poka sina li kama sona.
jan Pepa|9|False|aa... ?
jan Pepa|10|False|sina sona tan...
jan Pepa|11|False|...seme.
jan Kajen|12|False|pona ala!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|aaaa!!!
jan Pepa|2|False|ona li kama weka la, mi ken jo sin e ona!
jan Pepa|3|False|mi ken KEPEKEN ONA!
jan Pepa|4|False|mi ken ala kepeken nasin Pakalaa pakala sina.
jan Pepa|5|False|aaaaa...!
jan Pasi|6|False|o lukin a!
jan Pasi|7|False|ona li wile kama ante!
kon ike|8|False|WWwiiiiiiiiiiiiii
kon ike|9|False|iiiiiiiiiiiiiiiiiiiiiiiiii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kon ike|1|False|Wwiiiiiiiiiiiiiiiiiiiiiiiiiiiii
kon ike|2|False|iiiiiiiiiiiiiiiiiiiiiiiiiii
kon ike|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
kalama|4|False|KIWEN! ! !|nowhitespace
kalama|5|False|KALAMA! ! !|nowhitespace
kalama|6|False|OPENN! ! !|nowhitespace
kalama|7|False|Pakala! ! !|nowhitespace
kalama|8|False|Pakala! ! !|nowhitespace
kalama|9|False|Pakala! ! !|nowhitespace
kalama|10|False|Pakala! ! !|nowhitespace
kalama|11|False|Pakala! ! !|nowhitespace
kalama|12|False|Pakala! ! !|nowhitespace
kon ike|13|False|iiiiiiiiiiiiiii...
kalama|14|False|Koonnnn !!|nowhitespace
jan Winja|15|False|kalama ike a!
jan Pasi|16|False|o lon poka mi. mi moli e kalama ona e ona!
jan Pasi|17|False|mi moli e sina, ike o!
jan Pasi|18|False|jan sijelo o lukin. o ken awen e mi...
jan Pasi|19|False|jan sijelo o?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|PAKALA !
jan Pepa|2|False|ike, ike, ike!
jan Pasi|3|False|aaa aa aa!
jan Pasi|4|False|o lukin e ona! kalama li ike la ona li pana e telo.
jan Pasi|5|False|aaa aa a!
Fritz|6|False|A A A A!
Fritz|7|False|kin la, telo ale li laso!
jan Winja|8|False|AA A A A!
jan Winja|9|False|sina la telo ni li ken ala ken pona e sijelo?
Fritz|10|False|a, jaki!
Fritz|11|False|AA A A A!
jan Pasi|12|False|AAA AA AA!
jan Pepa|13|False|...
Fritz|14|False|O AWEN!
jan Pasi|15|False|o kama sin! mi musi taso!
jan Winja|16|False|jan pali o!
jan Kajen|17|False|kama pona.
nimi|18|False|- PINI -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|tenpo 2023-04-26 musi sitelen & toki: jan David Revoy. jan ni li pona e toki: jan Arlo James Barnes en jan Bobby Hiltz en jan Craig Maloney en jan Christian Aribaud en jan Estefania de Vasconcellos Guimaraes en jan Frédéric Mora en jan Erik Mondrian en janNicolas Artance en jan Rhombihexahedron en jan Valvin en jan Vinay. toki pona: jan Ke Tami. jan ante li pona e ona. jan ni li pona e toki nimi: jan H-Games en jan Menhir. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nicolas Artance en jan Scribblemaniac en jan Valvin. jan ni li pona e pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson. ilo: ilo Krita 5.0.5 en ilo Inkscape 1.2 li kepeken ilo Fedora 36 KDE Spin. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
jan Pepa|2|False|sina sona ala sona?
jan Pepa|3|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free (libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 1084 li pana e mani la, lipu ni li lon!
jan Pepa|5|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|6|False|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|7|False|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|8|False|sina pona!
