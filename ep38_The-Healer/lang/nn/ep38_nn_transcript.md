# Transcript of Pepper&Carrot Episode 38 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 38: Lækjarheksa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Puh!
Vinya|2|False|Endeleg fekk me has på denne òg!
Fritz|3|False|Ja, det var ikkje lett. Forstår no kvifor så få eventyrarar har klart å koma seg forbi her.
Fritz|4|False|Korleis går det med Brassik?
Fritz|5|False|Brassik? Brassik!
Fritz|6|False|Søren! Eg la ikkje merke til at du var så hardt skada!
Vinya|7|False|Lækjarheks!
Vinya|8|False|Me treng hjelp her – NO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Til sides ...
Lyd|2|False|Dzing
Brassik|3|False|Ikkje ei skramme att!
Brassik|4|False|Eit mirakel kvar gong!
Brassik|5|False|Du har utrulege nådegåver!
Brassik|6|False|Men me får koma oss av garde. Skatten ventar!
Fritz|7|False|Så sant!
Fritz|8|False|Og med lækjarheksa på laget er me uovervinnelege!
Fritz|9|False|Framover til neste monster!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brassik|1|False|Fus!
Vinya|2|False|Nei, eg! Eg!
Pepar|3|False|?
Kajenne|4|False|Og kva er det du trur du held på med?
Kajenne|5|False|I fleire månadar har me leita etter deg!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Ser du ikkje det?
Pepar|2|False|Eg har lagt alt bak meg og bytt leveveg.
Pepar|3|False|No går eg under namnet Den store lækjar-heksa Pep’Ra.
Kajenne|4|False|...
Pepar|5|False|«Kvifor?» lurer du kanskje på?
Pepar|6|False|Ein enkelt grunn!
Pepar|7|False|EG ER LUT LEI!
Pepar|8|False|Eg er lei av å verta jakta på, lei av verta ledd av – og ikkje minst er eg lei av aldri å få meg arbeid, etter å ha studert i så mange år!
Pepar|9|False|Her har dei i det minste bruk for meg og det eg kan!
Brassik|10|False|Lækjarheks!
Brassik|11|False|Kom hit.
Brassik|12|False|Vinya vart stygt såra.
Lyd|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Det er grunnen.
Pepar|2|False|Var det noko meir?
Kajenne|3|False|Ja.
Kajenne|4|False|Å ha namnet sitt på «ettersøkt»-plakatar er heilt vanleg for kaosah-hekser.
Kajenne|5|False|Det er til og med teikn på eit godt rykte.
Kajenne|6|False|Ignorer kritikarane, og skap heller din eigen leveveg dersom ingen tilbyr deg ein.
Kajenne|7|False|Og ikkje minst: Du er ikkje ei lækjarheks!
Kajenne|8|False|Før eller seinare oppdagar dei det – når du går tom for den enorme samlinga fønikstårer du brukar når du lèt som du har nye krefter.
Pepar|9|False|Kva ...?
Pepar|10|False|Korleis fann du ut ...
Pepar|11|False|... det?
Kajenne|12|False|Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Grrr!!!
Pepar|2|False|Vel, om det trengst, hentar eg berre meir!
Pepar|3|False|Fordi DET VERKAR!
Pepar|4|False|I motsetnad til dei forskrudde kaosah-greiene dykkar!
Pepar|5|False|GRRR...!
Brassik|6|False|Pass dykk!
Brassik|7|False|Monsteret er i ferd med å endra form!
Monster|8|False|KRiiiiiiiiiiiiiiiiii
Monster|9|False|iiiiiiiiiiiiiiiiiiiiiiiiiiii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|KRiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Monster|2|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Monster|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Lyd|4|False|KRRRR! ! !|nowhitespace
Lyd|5|False|KRRRR! ! !|nowhitespace
Lyd|6|False|KRRRR! ! !|nowhitespace
Lyd|7|False|Knus! ! !|nowhitespace
Lyd|8|False|Knus! ! !|nowhitespace
Lyd|9|False|Knus! ! !|nowhitespace
Lyd|10|False|Knus! ! !|nowhitespace
Lyd|11|False|Knus! ! !|nowhitespace
Lyd|12|False|Knus! ! !|nowhitespace
Monster|13|False|iiiiiiiiiiiiiiii...
Lyd|14|False|Dzeeyoo!!!
Vinya|15|False|Uff, håpar han syng på siste verset ...
Brassik|16|False|Dekk meg. Eg skal få stoppa kjeften på han for godt!
Brassik|17|False|Høyrde du det, utyske?!
Brassik|18|False|Men pass på meg i alle fall, lækjarheks ...
Brassik|19|False|Lækjarheks?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Oi sann!
Pepar|2|False|Å nei, å nei!
Brassik|3|False|Rhå-hå-hå!
Brassik|4|False|Sjå der! Eg trur hylet frå utysket skremde vatnet av lækjarheksa vår!
Brassik|5|False|Bva-ha-ha!
Fritz|6|False|HA-HA-HA-HA!
Fritz|7|False|Og i tillegg er det heilt blått!
Vinya|8|False|HA-HA-HA-HA!
Vinya|9|False|Har det òg lækjande eigenskapar, kanskje?!
Fritz|10|False|Uff, så ekkelt!
Fritz|11|False|HA-HA-HA-HA!
Brassik|12|False|BVA-HA-HA!
Pepar|13|False|...
Fritz|14|False|HALLO!
Brassik|15|False|Vent! Me tulla berre!
Vinya|16|False|Lækjarheks!
Kajenne|17|False|Velkommen attende.
Tittel|18|False|– SLUTT –

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|26. april 2023 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin og Vinay. Omsetjing til nynorsk Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nicolas Artance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson. Programvare: Krita 5.0.5 og Inkscape 1.2 på Fedora 36 KDE Spin. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|2|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|3|False|Takk til dei 1 084 som støtta denne episoden!
Pepar|4|False|Visste du dette?
Pepar|5|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|6|False|Sjå www.peppercarrot.com for meir informasjon!
Pepar|7|False|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
