# Transcript of Pepper&Carrot Episode 38 [eo]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titolo|1|False|Ĉapitro 38a : La Kuracisto

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinja|1|False|Fine...
Vinja|2|False|Ni sukcesis detrui ĉi tiun !
Fritz|3|False|Jes, malfacilis. Nun ni komprenas, kial tiom multaj aventuristoj ne revenis de tie.
Fritz|4|False|Kiel fartas Brasko ?
Fritz|5|True|Brasko ?
Fritz|6|False|… Brasko !
Fritz|7|False|Damne ! Mi ne rimarkis, ke vi estis tiel ege vundita !
Vinja|8|False|Kuracisto !
Vinja|9|False|Ni bezonas vian helpon, RAPIDE !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Flanken...
Sono|2|False|Dzing
Brasko|3|False|Miaj vundoj malaperis.
Brasko|4|False|Ĉiufoje la sama miraklo.
Brasko|5|False|Via talento nekredeblas, Kuracisto !
Brasko|6|False|Ni ne malfruu. La trezoro atendas nin !
Fritz|7|False|Jes ja !
Fritz|8|False|Danke al ŝi ni estas nevenkeblaj !
Fritz|9|False|Ni iru trovi la sekvan monstron !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasko|1|False|Mi !
Vinja|2|False|Ne, mi ! Mi !
Pipro|3|False|?
Kajeno|4|False|Ĉu vi klarigos al mi, kion vi faras ?
Kajeno|5|False|Ni serĉadis vin dum monatoj.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Ĉu ne evidentas ?
Pipro|2|False|Mi decidis rezigni ĉion kaj ŝanĝi mian karieron.
Pipro|3|False|Ekde nun mi estas la Granda Kuracisto Piprom, kiel vi povas vidi.
Kajeno|4|False|...
Pipro|5|False|« Sed kial », vi eble demandas ?!
Pipro|6|False|Simplas !
Pipro|7|False|JAM TROIS AL MI !
Pipro|8|False|Mi lacegas esti ĉasita, lacegas, ke oni ridu pri mi, kaj precipe mi lacegas, ke mi ne povas trovi laboron studinte tiel longe !
Pipro|9|False|Almenaŭ ĉi tie oni bezonas min kaj oni ŝatas miajn kapablojn !
Brasko|10|False|Kuracisto !
Brasko|11|False|Ne tro malproksimiĝu !
Brasko|12|False|Vinja estis forte frapita.
Sono|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Jen ĉio.
Pipro|2|False|Ĉu ion pli ?
Kajeno|3|False|Jes.
Kajeno|4|False|Ke vi aperu en afiŝoj kiel « serĉata », normalas por Ĥaosaha sorĉistino.
Kajeno|5|False|Tio estas eĉ signo de bona reputacio.
Kajeno|6|False|Ignoru la mokadon kaj kio estos dirita, kaj kreu vian propran karieron, se neniu donas iun al vi.
Kajeno|7|False|Kaj precipe : vi ne estas kuracisto.
Kajeno|8|False|Ili rimarkos, pli aŭ malpli frue, kiam vi elĉerpos tiun grandegan kvanton de Feniksaj Larmoj, kiujn vi portas kun vi, ŝajnigante, ke vi havas novajn povojn.
Pipro|9|False|Kio... ?
Pipro|10|False|Kiel vi...
Pipro|11|False|... divenis.
Kajeno|12|False|Fi !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|Grrr!!!
Pipro|2|False|Nu, se mi bezonos pli, mi iros preni pli !
Pipro|3|False|Ĉar TIO FUNKCIAS !
Pipro|4|False|Malsame ol ĉiuj viaj Ĥaosahaj bizaraĵoj !
Pipro|5|False|GRRR...!
Brasko|6|False|Atentu !
Brasko|7|False|La monstro transformiĝas !
Monstro|8|False|KRiiiiiiiiiiiii
Monstro|9|False|iiiiiiiiiiiiiiii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstro|1|False|KRiiiiiiiiiiiiiiiiiii
Monstro|2|False|iiiiiiiiiiiiiiiiiiii
Monstro|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Sono|4|False|KRRRR ! ! !|nowhitespace
Sono|5|False|KRRRR ! ! !|nowhitespace
Sono|6|False|KRRRR ! ! !|nowhitespace
Sono|7|False|Krak ! ! !|nowhitespace
Sono|8|False|Krack ! ! !|nowhitespace
Sono|9|False|Krak ! ! !|nowhitespace
Sono|10|False|Krak ! ! !|nowhitespace
Sono|11|False|Krak ! ! !|nowhitespace
Sono|12|False|Krak ! ! !|nowhitespace
Monstro|13|False|iiiiiiii...
Sono|14|False|Dzeeĵoo !!|nowhitespace
Vinja|15|False|Ho, ho, kia bruaĉego !
Brasko|16|False|Gardu min. Mi silentigos ĝin por ĉiam !
Brasko|17|False|Ĉu vi aŭdas tion, monstro ?!
Brasko|18|False|Atentu pri mi, por se io misokazus, Kuracisto...
Brasko|19|False|Kuracisto ?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipro|1|False|VE !
Pipro|2|False|Ne, ne, ne !
Brasko|3|False|Ĥo ho ho !
Brasko|4|False|Rigardu ! Mi pensas, ke la krio de la bestaĉo iom tro forte impresis nian kuraciston.
Brasko|5|False|Hha ha ha !
Fritz|6|False|HAHA HA HA !
Fritz|7|False|Ne nur tio, sed ĝi tute bluas !
Vinja|8|False|HAHA HA HA !
Vinja|9|False|Kaj ĉu vi pensas, ke ĝi povas ankaŭ sanigi ?
Fritz|10|False|Fi, abomene !
Fritz|11|False|HAHA HA HA !
Brasko|12|False|ĤA HA HA !
Pipro|13|False|...
Fritz|14|False|HE !
Brasko|15|False|Atendu ! Ni nur ŝercis !
Vinja|16|False|Kuracisto !
Kajeno|17|False|Ree bonvenon.
Titolo|18|False|- FINO -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Atribuintaro|1|False|26a de aprilo, 2023 Arto kaj scenaro : David Revoy. Beta-legado : Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Esperanta traduko : Jorge Maldonado Ventura Korekto : Tirifto Fasono : Navi, Tirifto Bazita sur la universo de Hereva Kreinto : David Revoy. Ĉefa daŭrigonto : Craig Maloney. Redakto : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korekto : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programaro : Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. Permesilo : Creative Commons Attribution 4.0. www.peppercarrot.com
Pipro|2|False|Ĉu vi sciis ?
Pipro|3|False|Pepper&Carrot estas tute libera, senpaga, malfermitkoda kaj subtenata de siaj mecenataj legantoj.
Pipro|4|False|Ĉi tiu ĉapitro ricevis subtenon de 1084 mecenatoj !
Pipro|5|False|Vi ankaŭ povas iĝi mecenato de Pepper&Carrot kaj havi vian nomon skribita tie !
Pipro|6|False|Ni estas en Patreon, Tipeee, PayPal, Liberapay ... kaj aliaj !
Pipro|7|False|Venu al www.peppercarrot.com por pli da informo !
Pipro|8|False|Dankon !
