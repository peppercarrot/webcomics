# Transcript of Pepper&Carrot Episode 38 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 38: Zdravilka

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rozga|1|False|Končno …
Rozga|2|False|Smo ga le premagali!
Fric|3|False|Ni bilo ravno lahko. Ampak zdaj vsaj vemo, zakaj je toliko pustolovcev omahnilo tukaj!
Fric|4|False|Kako gre, Brazik?
Fric|5|False|Brazik? Brazik!
Fric|6|False|Ti vrag! Niti opazila nisva, da si tako hudo ranjen!
Rozga|7|False|Zdravilka!
Rozga|8|False|Tvojo pomoč potrebujemo! BRŽ!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Umaknita se …
Zvok|2|False|Dzing
Brazik|3|False|Moje rane so izginile …
Brazik|4|False|Kot vsakič prej. Pravi čudež.
Brazik|5|False|Tvoj dar je neverjeten, zdravilka!
Brazik|6|False|Ne obirajmo se, zaklad nas čaka!
Fric|7|False|Tako se govori!
Fric|8|False|Po njeni zaslugi smo nepremagljivi!
Fric|9|False|Lotimo se naslednje pošasti!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brazik|1|False|Jaz prvi!
Rozga|2|False|Ne, jaz! Jaz!
Paprika|3|False|?
Kajenka|4|False|Mi pojasniš, kaj se tukaj greš?
Kajenka|5|False|Že mesece te iščemo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Ali ni očitno?
Paprika|2|False|Odločila sem se zamenjati svojo zaposlitev!
Paprika|3|False|Kot ste mogoče opazili, sem zdaj Pop'Ra, vélika zdravilka.
Kajenka|4|False|...
Paprika|5|False|„Ampak zakaj?“ se sprašujete?!
Paprika|6|False|Zelo preprosto!
Paprika|7|False|DOVOLJ IMAM!
Paprika|8|False|Sita sem tega, da me lovijo, sita sem posmehovanja, povrhu vsega pa sem sita tega, da ne najdem službe po tolikih letih šolanja!
Paprika|9|False|Tukaj me vsaj potrebujejo in cenijo zaradi mojih sposobnosti!
Brazik|10|False|Zdravilka!
Brazik|11|False|Vrni se sem!
Brazik|12|False|Rozga je precej zdelana.
Zvok|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|No, tako!
Paprika|2|False|Bi še kaj dodali?
Kajenka|3|False|Da.
Kajenka|4|False|To, da te lovijo oblasti, je normalno za kaosaško čarovnico.
Kajenka|5|False|To samo pomeni, da si na dobrem glasu.
Kajenka|6|False|Ne meni se za kritike in nergače. Če ti nihče ne ponudi kariere, si ustvari svojo.
Kajenka|7|False|Poleg tega pa nisi nobena zdravilka!
Kajenka|8|False|Opazili bodo prej ali slej, in ko ti bo zmanjkalo te neverjetne količine feniksovih solz, ki jih imaš pri sebi, bo jasno, da se samo delaš, da imaš nove sposobnosti.
Paprika|9|False|Kaaaj?
Paprika|10|False|Kako ste …
Paprika|11|False|… uganili.
Kajenka|12|False|Tc!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Grrr!!!
Paprika|2|False|Prav! Če mi jih bo zmanjkalo, bom pa šla po nove!
Paprika|3|False|Zato ker to DELUJE,
Paprika|4|False|da veš! Ne pa vaša kaosaška zmešnjava!
Paprika|5|False|GRRR…!
Brazik|6|False|Pazite!
Brazik|7|False|Pošast spreminja obliko!
Pošast|8|False|KReeeeeeeee
Pošast|9|False|eeeeeeeeeeeeeee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pošast|1|False|KRiiiiiiiiiiiiiiiiiii
Pošast|2|False|iiiiiiiiiiiiiiiiiiiiiiiii
Pošast|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Zvok|4|False|KRRRR ! ! !|nowhitespace
Zvok|5|False|KRRRR ! ! !|nowhitespace
Zvok|6|False|KRRRR ! ! !|nowhitespace
Zvok|7|False|Pok ! ! !|nowhitespace
Zvok|8|False|Pok ! ! !|nowhitespace
Zvok|9|False|Pok ! ! !|nowhitespace
Zvok|10|False|Pok ! ! !|nowhitespace
Zvok|11|False|Pok ! ! !|nowhitespace
Zvok|12|False|Pok ! ! !|nowhitespace
Pošast|13|False|iiiiiiii …
Zvok|14|False|Dziiiooo !!|nowhitespace
Rozga|15|False|Joj, kakšen trušč!
Brazik|16|False|Dovolj! Poslal jo bom rakom žvižgat! Krijte me!
Brazik|17|False|Te kaj srbi, pošast?! Pridi! Te bo striček popraskal!
Brazik|18|False|Za vsak slučaj me čuvaj, zdravilka …
Brazik|19|False|Zdravilka?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|UPS!
Paprika|2|False|Ne, ne, ne!
Brazik|3|False|Ho ho ho!
Brazik|4|False|Poglejta jo! Kaže, da je bil pošastni krik preveč za našo zdravilko!
Brazik|5|False|Hoho ho!
Fric|6|False|HAHA HA HA!
Fric|7|False|Le kaj je jedla, da je zdaj modro!?
Rozga|8|False|HAHA HA HA!
Rozga|9|False|Mislita, da bo morala še sebe pregledati?
Fric|10|False|Pha, groza!
Fric|11|False|HAHA HA HA!
Brazik|12|False|HOHO HO!
Paprika|13|False|...
Fric|14|False|HEEJ!
Brazik|15|False|Počakaj! Samo malo smo se hecali!
Rozga|16|False|Zdravilka!
Kajenka|17|False|Dobrodošla nazaj.
Naslov|18|False|- KONEC -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|26. april 2023 Piše in riše: David Revoy. Testni bralci: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin in Vinay. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nicolas Artance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 5.0.5 in Inkscape 1.2 na Fedora 36 KDE Spin. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
Paprika|2|False|Že veš?
Paprika|3|False|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 1084 podpornikov!
Paprika|5|False|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|6|False|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|7|False|Obišči www.peppercarrot.com za več informacij!
Paprika|8|False|Hvalaaaa!
