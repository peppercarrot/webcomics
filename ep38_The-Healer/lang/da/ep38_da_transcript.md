# Transcript of Pepper&Carrot Episode 38 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 38: Healeren

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinja|1|False|Endelig...
Vinja|2|False|Så gjorde vi det også af med den!
Fritz|3|False|Ja, det var ikke let. Det forklarer hvorfor så mange ikke klarede den…
Fritz|4|False|Er du ok, Brasik?
Fritz|5|False|Brasik? ... Brasik!
Fritz|6|False|Pokkers! Jeg så ikke, at du var så slemt såret!
Vinja|7|False|Healer!
Vinja|8|False|Vi har brug for hjælp. HURTIGT!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Gør plads...
Lyd|2|False|Dzing
Brasik|3|False|Mine sår er forsvundet…
Brasik|4|False|…det er lige mirakuløst hver gang.
Brasik|5|False|Dit talent er utroligt, Healer!
Brasik|6|False|Lad os ikke blive hængende. Skatten venter!
Fritz|7|False|Enig!
Fritz|8|False|Takket være hende, er vi uovervindelige!
Fritz|9|False|Afsted til det næste monster!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasik|1|False|Dibs!
Vinja|2|False|Nej, mig! Mig!
Pepper|3|False|?
Cayenne|4|False|Hvad i alverden har du gang i?
Cayenne|5|False|Vi har ledt efter dig i månedsvis.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Øh, er det ikke ret tydeligt?
Pepper|2|False|Jeg har simpelthen besluttet at opgive det hele og ændre karriere.
Pepper|3|False|Fra nu af er jeg den Mægtige Healer Peph'Ra, som du kan se.
Cayenne|4|False|...
Pepper|5|False|Men "hvorfor?" spørger du måske...?
Pepper|6|False|Tja, det er simpelt
Pepper|7|False|JEG HAR FÅET NOK!
Pepper|8|False|Nok af at være eftersøgt, nok af at man gør grin af mine trylleformularer, og frem for alt: Nok af at jeg ikke kan finde noget job efter at have studeret i så lang tid!
Pepper|9|False|Her er der i det mindste brug for mig, og der bliver sat pris på mine evner!
Brasik|10|False|Healer!
Brasik|11|False|Gå ikke for langt væk!
Brasik|12|False|Vinja er hårdt ramt.
Lyd|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Det var det.
Pepper|2|False|Noget at tilføje?!
Cayenne|3|False|Ja.
Cayenne|4|False|At have sit navn på en efterlysningsplakat er normalt for en Kaosah-heks.
Cayenne|5|False|Det er endda tegn på et godt ry.
Cayenne|6|False|Ignorér de negative bemærkninger, "hvad-vil-folk-sige"-tankerne, og skab dit eget arbejde, hvis ingen giver dig noget.
Cayenne|7|False|Og frem for alt: Du er ikke healer.
Cayenne|8|False|De finder ud af det før eller siden, når du løber tør for den gevaldige mængde Fønikstårer, som du bærer rundt på, og som du bruger til at lade som om, du har nye kræfter.
Pepper|9|False|Hv... ?!
Pepper|10|False|Hvordan har du...
Pepper|11|False|...gættet det.
Cayenne|12|False|Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Arrg!!!
Pepper|2|False|Hvis jeg løber tør, så kan jeg bare hente flere!
Pepper|3|False|For DET VIRKER!
Pepper|4|False|Ikke som alle jeres komplicerede Kaosah-ting!
Pepper|5|False|GRRR...!
Brasik|6|False|Pas på!
Brasik|7|False|Monsteret skifter form!
Monster|8|False|SKRiiiiiiiiiiiii
Monster|9|False|iiiiiiiiiiiiiiii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|SKRiiiiiiiiiiiiiiiiiii
Monster|2|False|iiiiiiiiiiiiiiiiiiiiiiiii
Monster|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Lyd|4|False|KRRRR ! ! !|nowhitespace
Lyd|5|False|KRRRR ! ! !|nowhitespace
Lyd|6|False|KRRRR ! ! !|nowhitespace
Lyd|7|False|Kliir ! ! !|nowhitespace
Lyd|8|False|Kliir ! ! !|nowhitespace
Lyd|9|False|Kliir ! ! !|nowhitespace
Lyd|10|False|Kliir ! ! !|nowhitespace
Lyd|11|False|Kliir ! ! !|nowhitespace
Lyd|12|False|Kliir ! ! !|nowhitespace
Monster|13|False|iiiiiiii...
Lyd|14|False|Dziiiuu !!|nowhitespace
Vinja|15|False|Av, mine ører!
Brasik|16|False|Jeg skal nok bringe den til tavshed!
Brasik|17|False|Fremad!
Brasik|18|False|Hold øje for en sikkerheds skyld, Heal-...
Brasik|19|False|Healer?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|UPS!
Pepper|2|False|Nej, nej nej!
Brasik|3|False|Ha ha ha!
Brasik|4|False|Se lige dér! Jeg tror, at dens skrig var lidt for meget for vores healer!
Brasik|5|False|Ho Ho Ho!
Fritz|6|False|HAHA HA HA!
Fritz|7|False|Og det er helt blåt!
Vinja|8|False|HIHI HI HI!
Vinja|9|False|Tror I at det også kan helbrede?
Fritz|10|False|Adr, ulækkert!
Fritz|11|False|HAHA HA HA!
Brasik|12|False|HOHO HO!
Pepper|13|False|...
Fritz|14|False|HEEEY !
Brasik|15|False|Vent! Det var bare for sjov!
Vinja|16|False|Healer!
Cayenne|17|False|Velkommen tilbage blandt os.
Titel|18|False|- SLUT -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Den 26. april 2023 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite, Troels Bech Gravgaard Møller Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Værktøj: Krita 5.1.5, Inkscape 1.2 på Fedora KDE 37 Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Vidste I det?
Pepper|3|False|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|4|False|Denne episode blev støttet af 1084 tilhængere!
Pepper|5|False|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|6|False|Vi er på Patreon, Tipeee, PayPal, Liberapay ... og andre!
Pepper|7|False|Gå på www.peppercarrot.com og få mere information!
Pepper|8|False|Tak!
