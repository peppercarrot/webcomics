# Transcript of Pepper&Carrot Episode 38 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 38: De genezer

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Eindelijk...
Vinya|2|False|Hebben we deze af kunnen maken!
Fritz|3|False|Ja, het was moeilijk. Nu begrijp ik waarom zo veel avonturiers hier vast zaten.
Fritz|4|False|Gaat het Brasic?
Fritz|5|False|Brasic? Brasic!
Fritz|6|False|Jeetje! Ik had niet gemerkt dat je zo verwond was!
Vinya|7|False|Genezer!
Vinya|8|False|We hebben je nodig, NU!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Aan de kant...
Geluid|2|False|Dzing
Brasic|3|False|Mijn wonden verdwijen.
Brasic|4|False|Elke keer is het een wonder.
Brasic|5|False|Jouw talent is ongelofelijk, genezer!
Brasic|6|False|Maak haast. De schat staat voor ons klaar!
Fritz|7|False|Jazeker!
Fritz|8|False|Dankzij haar zijn we onverslaanbaar!
Fritz|9|False|Op zoek naar het volgende monster!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Mijne!
Vinya|2|False|Nee, de mijne!
Pepper|3|False|?
Cayenne|4|False|Waar denk je dat je mee bezig bent?
Cayenne|5|False|We zijn al maanden naar je op zoek.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Is het niet duidelijk?
Pepper|2|False|Ik geef alles op en verander mijn hele carrière.
Pepper|3|False|Vanaf nu ben ik de grote genezer Peph'Ra, zoals je kunt zien.
Cayenne|4|False|...
Pepper|5|False|"Waarom", vraag je je af?!
Pepper|6|False|Heel simpel!
Pepper|7|False|IK BEN ER KLAAR MEE!
Pepper|8|False|Klaar met achterna gezeten worden, klaar met uitgelachen worden, en vooral klaar met geen werk kunnen vinden na al dat studeren!
Pepper|9|False|Hier word ik tenminste gewaardeerd voor mijn kunde!
Brasic|10|False|Genezer!
Brasic|11|False|Ga niet te ver weg!
Brasic|12|False|Vinya is hard geraakt.
Geluid|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Dat is het.
Pepper|2|False|Is er nog iets?
Cayenne|3|False|Ja.
Cayenne|4|False|Op een poster staan en gezocht worden is gewoon voor een Chaosah heks.
Cayenne|5|False|Het is zelfs een teken van een goede reputatie.
Cayenne|6|False|Negeer de nee-zegers enzovoort, en schep je eigen carrière als niemand die aan je geeft.
Cayenne|7|False|En vooral: jij bent geen genezer.
Cayenne|8|False|Ze zullen op een gegeven moment merken dat je geen genezer bent als je door die grote hoeveelheid fenikstranen heen bent. Daarna kun je niet meer alsof doen...
Pepper|9|False|Wa... ?
Pepper|10|False|Hoe wist je...
Pepper|11|False|...dat.
Cayenne|12|False|Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr!!!
Pepper|2|False|Nou, als ik meer nodig heb dan kan ik het halen!
Pepper|3|False|Omdat DIT LUKT!
Pepper|4|False|In tegenstelling tot jouw betwiste Chaosah gedoe.
Pepper|5|False|GRRR...!
Brasic|6|False|Let op!
Brasic|7|False|Het monster evolueert!
Monster|8|False|KRieeeeeeee
Monster|9|False|ieeeeeeeeeeeeee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|KRieeeeeeeeeeeeeeeee
Monster|2|False|ieeeeeeeeeeeeeeee
Monster|3|False|ieeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
Geluid|4|False|KRRRR! ! !|nowhitespace
Geluid|5|False|KRRRR! ! !|nowhitespace
Geluid|6|False|KRRRR! ! !|nowhitespace
Geluid|7|False|Krak! ! !|nowhitespace
Geluid|8|False|Krak! ! !|nowhitespace
Geluid|9|False|Krak! ! !|nowhitespace
Geluid|10|False|Krak! ! !|nowhitespace
Geluid|11|False|Krak! ! !|nowhitespace
Geluid|12|False|Krak! ! !|nowhitespace
Monster|13|False|ieeeeeee...
Geluid|14|False|Dziejoo !!|nowhitespace
Vinya|15|False|O jee, wat een lawaai!
Brasic|16|False|Dek me. Ik zal het voor goed laten zwijgen!
Brasic|17|False|Hoor je dat, monster?!
Brasic|18|False|Hou me in de gaten voor het geval dat...
Brasic|19|False|Genezer?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|OEPS !
Pepper|2|False|Nee, nee, nee!
Brasic|3|False|Bwa ha ha!
Brasic|4|False|Kijk eens! Ik denk dat het gekrijs van het beest iets te veel was voor onze genezer.
Brasic|5|False|Bwa ha ha!
Fritz|6|False|HAHA HA HA!
Fritz|7|False|Dat niet alleen, het is helemaal blauw!
Vinya|8|False|HAHA HA HA!
Vinya|9|False|Denk je dat dat ook kan genezen?
Fritz|10|False|Bah, vies!
Fritz|11|False|HAHA HA HA!
Brasic|12|False|BWA HA HA!
Pepper|13|False|...
Fritz|14|False|HÈ!
Brasic|15|False|Wacht! Het was maar een grap!
Vinya|16|False|Genezer!
Cayenne|17|False|Welkom terug.
Titel|18|False|- EINDE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|april 26, 2023 Tekeningen & verhaal: David Revoy. Beta lezers: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Nederlandse vertaling: Marno van der Maas. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|2|False|Wist je dat?
Pepper|3|False|Pepper&Carrot is helemaal vrij, open-bron en gesponsoord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik 1084 patronen!
Pepper|5|False|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|6|False|Je kan doneren met Tipeee, PayPal, Liberapay ... en meer!
Pepper|7|False|Kijk op www.peppercarrot.com voor meer info!
Pepper|8|False|Dank je!
