# Transcript of Pepper&Carrot Episode 38 [lt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Antraštė|1|False|38 epizodas: Gydūnė

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinja|1|False|Pagaliau…
Vinja|2|False|Pavyko pribaigti šitą baidyklę!
Fricas|3|False|Tikrai, lengva nebuvo. Dabar aišku, kodėl tiek daug nuotykiautojų čia užstrigdavo.
Fricas|4|False|Kaip laikosi Brasikas?
Fricas|5|False|Brasikai? Brasikai!
Fricas|6|False|Po galais! Nepastebėjau, kad tu taip rimtai sužeistas!
Vinja|7|False|Gydūne!
Vinja|8|False|Reikia pagalbos, SKUBIAI!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipirė|1|False|Pasitraukite…
Garsas|2|False|Dzing
Brasikas|3|False|Mano žaizdos išnyko!
Brasikas|4|False|Kaip ir kaskart, koks stebuklas!
Brasikas|5|False|Tavo talentas neįtikėtinas, Gydūne!
Brasikas|6|False|Nebedelskime. Juk viduje mūsų laukia lobis!
Fricas|7|False|Cha cha!
Fricas|8|False|Jos dėka mes nenugalimi!
Fricas|9|False|Eime ieškoti kitos pabaisos!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasikas|1|False|Čiūrikai!
Vinja|2|False|Ne, aš! Aš noriu!
Pipirė|3|False|?
Kajena|4|False|Kokiais niekais čia užsiimi?
Kajena|5|False|Mes tavęs ieškojome ištisus mėnesius!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipirė|1|False|Argi ne akivaizdu?
Pipirė|2|False|Nusprendžiau viską mesti ir pakeisti savo karjerą.
Pipirė|3|False|Kaip matote, dabar aš esu Didžioji Gydūnė Pip'Ra.
Kajena|4|False|…
Pipirė|5|False|„Bet kodėl?“, klausite?!
Pipirė|6|False|Paprasta!
Pipirė|7|False|MAN JAU PAKAKS!
Pipirė|8|False|Man įkyrėjo būti medžiojamai, įkyrėjo būti išjuokiamai, o labiau už viską, man įkyrėjo būti bedarbei taip ilgai po mokslų baigimo!
Pipirė|9|False|Čia bent jau jaučiuosi reikalinga ir esu vertinama už savo įgūdžius!
Brasikas|10|False|Gydūne!
Brasikas|11|False|Nenuklysk per toli!
Brasikas|12|False|Vinją ką tik stipriai sužeidė.
Garsas|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipirė|1|False|Tai tiek.
Pipirė|2|False|Dar kas nors?
Kajena|3|False|Taip.
Kajena|4|False|Būti ieškomoms už atlygį Chaosijos raganoms yra normalu.
Kajena|5|False|Tai netgi laikoma geros reputacijos ženklu.
Kajena|6|False|Neklausyk jokių mulkių sėjančių dvejones, ir pati susikurk karjerą, jei niekas kitas jos nesuteikia.
Kajena|7|False|Bet svarbiausia: tu nesi gydūnė.
Kajena|8|False|Anksčiau ar vėliau jie tai pastebės, kai išseks tavo didžiulės fenikso ašarų atsargos, kurias nešiojiesi su savimi, apsimesdama, jog turi naujų galių.
Pipirė|9|False|Ką…?
Pipirė|10|False|Kaip jūs…
Pipirė|11|False|…atspėjote.
Kajena|12|False|Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipirė|1|False|Grrr!!!
Pipirė|2|False|Na, jei man jų reikės daugiau, aš ir gausiu daugiau!
Pipirė|3|False|Nes TAI VEIKIA!
Pipirė|4|False|Priešingai nei tie sumauti Chaosijos niekalai.
Pipirė|5|False|GRRR…!
Brasikas|6|False|Atsargiai!
Brasikas|7|False|Pabaisa keičia pavidalą!
Pabaisa|8|False|KRyyyyyyyyy
Pabaisa|9|False|yyyyyyyyyyyy

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pabaisa|1|False|KRyyyyyyyyyyyyyyyyy
Pabaisa|2|False|yyyyyyyyyyyyyyyy
Pabaisa|3|False|yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
Garsas|4|False|TRRRR! ! !|nowhitespace
Garsas|5|False|TRRRR! ! !|nowhitespace
Garsas|6|False|TRRRR! ! !|nowhitespace
Garsas|7|False|Trekšt! ! !|nowhitespace
Garsas|8|False|Trekšt! ! !|nowhitespace
Garsas|9|False|Trekšt! ! !|nowhitespace
Garsas|10|False|Trekšt! ! !|nowhitespace
Garsas|11|False|Trekšt! ! !|nowhitespace
Garsas|12|False|Trekšt! ! !|nowhitespace
Pabaisa|13|False|yyyyyyyy...
Garsas|14|False|Dzyyjūū !!|nowhitespace
Vinja|15|False|O, vargeli, koks triukšmas!
Brasikas|16|False|Pridenkite mane. Nutildysiu ją amžiams!
Brasikas|17|False|Ar girdi, pabaisa?!
Brasikas|18|False|Gydūne, dėl visa ko, stebėk mane…
Brasikas|19|False|Gydūne?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pipirė|1|False|Ooiii!
Pipirė|2|False|Ne, ne, ne!
Brasikas|3|False|Ro cho cho!
Brasikas|4|False|Tik pažiūrėkit! Atrodo, žvėries riksmas buvo per didelis iššūkis gydūnės organizmui.
Brasikas|5|False|Ba cha cha!
Fricas|6|False|CHA CHA CHA!
Fricas|7|False|Maža to, dar ir spalva mėlyna!
Vinja|8|False|CHA CHA CHA!
Vinja|9|False|Kaip manot, gal tai irgi išgydo?
Fricas|10|False|Fui, šlykštu!
Fricas|11|False|CHA CHA CHA!
Brasikas|12|False|BA CHA CHA!
Pipirė|13|False|…
Fricas|14|False|EI!
Brasikas|15|False|Palauk! Juk mes tik juokavome!
Vinja|16|False|Gydūne!
Kajena|17|False|Sveika sugrįžusi.
Antraštė|18|False|- PABAIGA -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kūrėjų sąrašas|1|False|2023 m. balandžio 26 d. Piešiniai ir istorija: David Revoy. Beta skaitytojai: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Lietuviška versija Išvertė iš anglų kalbos: Rimas Kudelis. Paremta „Hereva“ visata Sukūrė: David Revoy. Pagrindinis prižiūrėtojas: Craig Maloney. Rašytojai: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Redaktoriai: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programinė įranga: „Krita 5.0.5“, „Inkscape 1.2“, veikiančios „Fedora 36 KDE Spin“ sistemoje. Licencija: Creative Commons Attribution 4.0. www.peppercarrot.com
Pipirė|2|False|Ar žinojote?
Pipirė|3|False|„Pipirė ir Morkius“ yra visiškai laisvasis ir atvirasis komiksas, remiamas skaitytojų aukomis.
Pipirė|4|False|Už paramą šiam epizodui dėkojame 1084 rėmėjams!
Pipirė|5|False|Jūs taipogi galite tapti „Pipirės ir Morkiaus“ rėmėjais ir įrašyti savo vardą čia!
Pipirė|6|False|Mus paremti galite per „Patreon“, „Tipeee“, „PayPal“, „Liberapay“... ir kitais būdais!
Pipirė|7|False|Apsilankykite www.peppercarrot.com ir sužinokite daugiau!
Pipirė|8|False|Ačiū!
