# Translators notes

From https://framagit.org/peppercarrot/webcomics/-/merge_requests/937

Ngô Ngọc Đức Huy @Huy-Ngo at 2023-05-02:

I'm not sure Vietnamese has a word for "healer", so I'm roughly translate it to "witch that heals" (phù thủy trị thương), but it has two problems:
- it's too long, so cannot be fit in some bubbles
- if I cut it down to "witch" (which I do in this draft), it doesn't highlight the fact that she switched her career

I've considered if some alternatives:
- "bác sĩ" (doctor): sounds a bit anachronistic
- "thầy thuốc" (also means doctor): sounds more time-appropriate, but may be too long
- coming up with a new word, in which case I wasn't able to

Maybe someone who plays RPG games where healer is a class can help? 🤔

I have looked at forums and communities and it looks like they just use the English :D I think I will keep the translation as is
