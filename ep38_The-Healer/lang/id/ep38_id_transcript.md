# Transcript of Pepper&Carrot Episode 38 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 38: Sang Dokter

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Akhirnya...
Vinya|2|False|Kita berhasil membunuh yang ini!
Fritz|3|False|Ya, lumayan susah juga. Sekarang aku ngerti kenapa banyak petualang yang nyangkut di sini
Fritz|4|False|Gimana Brasic?
Fritz|5|False|Brasic? Brasic!
Fritz|6|False|Duh! Aku tidak sadar bahwa kamu terluka parah!
Vinya|7|False|Dokter!
Vinya|8|False|Perlu bantuan di sini, CEPAT!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Minggir...
Suara|2|False|Dzing
Brasic|3|False|Lukaku lenyap.
Brasic|4|False|Tiap kali ini mujizat yang sama.
Brasic|5|False|Bakatmu mengagumkan, Dokter!
Brasic|6|False|Mari kita lanjut. Harta karun sudah menanti!
Fritz|7|False|Dengar semua!
Fritz|8|False|Berkat dia, kita tak terkalahkan!
Fritz|9|False|Ayo kita cari monster berikutnya!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Duluan!
Vinya|2|False|Eh, tidak bisa! Aku duluan!
Pepper|3|False|?
Cayenne|4|False|Kamu pikir lagi apa kamu?
Cayenne|5|False|Kami sudah mencarimu berbulan-bulan.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Masih kurang jelas?
Pepper|2|False|Aku sudah memutuskan untuk pindah haluan.
Pepper|3|False|Mulai sekarang, Aku adalah Dokter Peph'Ra Agung, seperti yang kamu lihat.
Cayenne|4|False|...
Pepper|5|False|"Tapi mengapa", mungkin kamu pikir?!
Pepper|6|False|Simpel!
Pepper|7|False|AKU SUDAH MUAK!
Pepper|8|False|Aku capek diburu terus, capek ditertawai terus, dan di atas semuanya itu, aku capek tidak bisa dapat kerja setelah belajar sekian lama!
Pepper|9|False|Setidaknya di sini aku diperlukan dan kemampuanku dihargai!
Brasic|10|False|Dokter!
Brasic|11|False|Jangan jauh-jauh!
Brasic|12|False|Vinya kena pukul berat.
Suara|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Begitulah.
Pepper|2|False|Ada lagi?
Cayenne|3|False|Ya.
Cayenne|4|False|Ketika kita mulai dicari-cari otoritas itu normal untuk penyihir Chaosah.
Cayenne|5|False|Bahkan itu artinya pertanda bagus.
Cayenne|6|False|Tidak usah pedulikan yang pesimis ato apa, dan buat karirmu sendiri kalau tidak ada yang mau kasih.
Cayenne|7|False|Dan terutama: kamu bukan dokter.
Cayenne|8|False|Suatu saat mereka akan sadar, ketika kamu kehabisan segitu banyak Air Mata Phoenix yang kamu bawa-bawa. Pura-puranya kamu punya kekuatan baru.
Pepper|9|False|Hah... ?
Pepper|10|False|Gimana kamu bisa...
Pepper|11|False|...tebak.
Cayenne|12|False|Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr!!!
Pepper|2|False|Ya, kalau nanti aku perlu lagi ya aku bakal ambil lagi!
Pepper|3|False|Karena INI BERFUNGSI!
Pepper|4|False|Tidak seperti sihir-sihir Chaosahmu itu.
Pepper|5|False|GRRR...!
Brasic|6|False|Awas!
Brasic|7|False|Monsternya berubah wujud!
Monster|8|False|KRiiiiiiiiiii
Monster|9|False|iiiiiiiiiiiiiiiiii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|KRiiiiiiiiiiiiiiiiiiiiiiiiii
Monster|2|False|iiiiiiiiiiiiiiiiiiiiiiiiii
Monster|3|False|iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
Suara|4|False|KRRRR! ! !|nowhitespace
Suara|5|False|KRRRR! ! !|nowhitespace
Suara|6|False|KRRRR! ! !|nowhitespace
Suara|7|False|Krek! ! !|nowhitespace
Suara|8|False|Krek! ! !|nowhitespace
Suara|9|False|Krak! ! !|nowhitespace
Suara|10|False|Krak! ! !|nowhitespace
Suara|11|False|Krak! ! !|nowhitespace
Suara|12|False|Krek! ! !|nowhitespace
Monster|13|False|iiiiiiiiiiiii...
Suara|14|False|Dziiuuu !!|nowhitespace
Vinya|15|False|Ya ampun, berisiknya!
Brasic|16|False|Lindungi aku. Bakal aku bikin diam selama-lamanya!
Brasic|17|False|Denger gak, monster?!
Brasic|18|False|Tolong jaga-jaga juga ya, Dokter...
Brasic|19|False|Dokter?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|UPS !
Pepper|2|False|Tidak, tidak!
Brasic|3|False|Rho ho ho!
Brasic|4|False|Lihatlah! Aku rasa jeritan monstet ini terlalu menyeramkan buat dokter kita ini.
Brasic|5|False|Bwa ha ha!
Fritz|6|False|HAHA HA HA!
Fritz|7|False|Bukan cuma itu, itu biru semua!
Vinya|8|False|HAHA HA HA!
Vinya|9|False|Mungkin itu bisa jadi obat juga?
Fritz|10|False|Ih, jijik!
Fritz|11|False|HAHA HA HA!
Brasic|12|False|BWA HA HA!
Pepper|13|False|...
Fritz|14|False|HEI!
Brasic|15|False|Tunggu! Kami cuma bercanda!
Vinya|16|False|Dokter!
Cayenne|17|False|Selamat datang kembali.
Judul|18|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kredit|1|False|April 26, 2023 Pelukis & skenario: David Revoy. Penguji bacaan cerita: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Versi Bahasa Indonesia Penerjemah: Aldrian Obaja Muis. Berdasarkan alam semesta Hereva Pembuat: David Revoy. Penyunting utama: Craig Maloney. Penulis: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Penyunting: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Peranti lunak: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. Izin (Lisensi): Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Tahukah kamu?
Pepper|3|False|Pepper&Carrot sepenuhnya gratis (bebas), sumber terbuka dan disponsori oleh para pembacanya.
Pepper|4|False|Untuk episode ini, terima kasih kepada 1084 relawan!
Pepper|5|False|Kamu juga bisa menjadi sukarelawan Pepper&Carrot dan namamu akan tercantum di sini!
Pepper|6|False|Kami ada di Patreon, Tipeee, PayPal, Liberapay ... dan lainnya!
Pepper|7|False|Cek www.peppercarrot.com untuk informasi selanjutnya!
Pepper|8|False|Terima kasih!
