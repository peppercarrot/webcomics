# Transcript of Pepper&Carrot Episode 38 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 38: La Curandera

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Por fin...
Vinya|2|False|¡Hemos conseguido derrotarlo!
Fritz|3|False|Sí, ha sido difícil. Ahora ya sabemos por qué tantos aventureros estaban aquí atrapados.
Fritz|4|False|¿Cómo está Brasic?
Fritz|5|False|¿Brasic? ¡Brasic!
Fritz|6|False|¡Maldición! No pensaba que estuvieras tan herido.
Vinya|7|False|¡Curandera!
Vinya|8|False|Necesitamos tu ayuda, ¡RÁPIDO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Apartad...
Sonido|2|False|Dzing
Brasic|3|False|Mis heridas se han curado.
Brasic|4|False|Es siempre el mismo milagro.
Brasic|5|False|¡Tu talento es increíble, Curandera!
Brasic|6|False|No hay tiempo que perder. ¡El tesoro nos espera!
Fritz|7|False|¡Bien dicho!
Fritz|8|False|Gracias a ella, ¡somos invencibles!
Fritz|9|False|¡A por el siguiente monstruo!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|¡Dibs!
Vinya|2|False|No, ¡a mi! ¡a mi!
Pimienta|3|False|¿?
Cayena|4|False|¿Se puede saber que estas haciendo aquí?
Cayena|5|False|Hemos estado meses buscándote.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¿No es evidente?
Pimienta|2|False|He decidido dejarlo todo y cambiar de carrera.
Pimienta|3|False|A partir de ahora, soy Pasiflo'Ra, la Gran Curandera, como puedes ver.
Cayena|4|False|...
Pimienta|5|False|"Pero, ¿por qué?", te preguntarás...
Pimienta|6|False|¡Fácil!
Pimienta|7|False|¡ESTOY HARTA!
Pimienta|8|False|Harta de ser perseguida, de que se rían de mi, y, sobre todo, ¡Estoy harta de no poder encontrar trabajo después de estudiar durante tanto tiempo !
Pimienta|9|False|Al menos aquí me necesitan y aprecian mis dotes.
Brasic|10|False|¡Curandera!
Brasic|11|False|¡No tardes!
Brasic|12|False|Le han dado a Vynia.
Sonido|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Eso es todo.
Pimienta|2|False|¿Alguna otra cosa?
Cayena|3|False|Sí.
Cayena|4|False|Que tu nombre aparezca en un cartel de "se busca"es normal para una bruja Chaosah
Cayena|5|False|Es incluso una señal de buena reputación.
Cayena|6|False|Ignora las bromas y el qué dirán, y crea tu propia carrera si nadie te da una.
Cayena|7|False|Y, sobre todo: no eres una curandera.
Cayena|8|False|Se darán cuenta, tarde o temprano, cuando te quedes sin todas esas Lágrimas de Fénix, que llevas encima, como si tuvieses nuevos poderes.
Pimienta|9|False|¿Qué... ?
Pimienta|10|False|Cómo lo has...
Pimienta|11|False|...sabido.
Cayena|12|False|¡Tsk!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡¡¡Grrr!!!
Pimienta|2|False|Bueno, si necesito más, ¡iré a por más!
Pimienta|3|False|Porque ¡ESTO FUNCIONA!
Pimienta|4|False|No como todas esas cosas de Chaosah.
Pimienta|5|False|¡GRRR...!
Brasic|6|False|¡Cuidado!
Brasic|7|False|¡El monstruo está cambiando de forma!
Monstruo|8|False|CReeeeeeeee
Monstruo|9|False|eeeeeeeeeeeeeee

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstruo|1|False|CReeeeeeeeeeeeeeeeee
Monstruo|2|False|eeeeeeeeeeeeeeeee
Monstruo|3|False|eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
Sonido|4|False|CRRRR! ! !|nowhitespace
Sonido|5|False|CRRRR! ! !|nowhitespace
Sonido|6|False|CRRRR! ! !|nowhitespace
Sonido|7|False|Crack! ! !|nowhitespace
Sonido|8|False|Crack! ! !|nowhitespace
Sonido|9|False|Crack! ! !|nowhitespace
Sonido|10|False|Crack! ! !|nowhitespace
Sonido|11|False|Crack! ! !|nowhitespace
Sonido|12|False|Crack! ! !|nowhitespace
Monstruo|13|False|eeeeeeee...
Sonido|14|False|Dzeeyoo !!|nowhitespace
Vinya|15|False|¡Ah, que molesto!
Brasic|16|False|Cubridme. ¡Voy a hacerle callar!
Brasic|17|False|¡¿Me has oído, monstruo?!
Brasic|18|False|Echáme un ojo por si acaso, Curandera...
Brasic|19|False|¿Curandera?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡UUPS !
Pimienta|2|False|¡No, no, no!
Brasic|3|False|¡Rha ja ja!
Brasic|4|False|¡Mirad eso! Creo que el llanto de la bestia ha sido demasiado para nuestra curandera.
Brasic|5|False|¡Bwa ja ja!
Fritz|6|False|¡JAJA JA JA!
Fritz|7|False|No solo eso, también ¡es azul!
Vinya|8|False|¡JAJA JA JA!
Vinya|9|False|¿Y creéis que también sirve para curarnos?
Fritz|10|False|Ugh, ¡qué asco!
Fritz|11|False|¡JAJA JA JA!
Brasic|12|False|¡BWA JA JA!
Pimienta|13|False|...
Fritz|14|False|¡EY!
Brasic|15|False|¡Espera! Solo estábamos bromeando
Vinya|16|False|¡Curandera!
Cayena|17|False|Bienvenida de vuelta.
Título|18|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|26 de abril de 2023 Dibujo y guion: David Revoy. Lectores de la versión beta: Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Versión en castellano Traducción: Raúl García. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 on Fedora 36 KDE Spin. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
Pimienta|2|False|¿Sabías qué?
Pimienta|3|False|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 1036 mecenas!
Pimienta|5|False|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|6|False|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|7|False|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|8|False|¡Gracias!
