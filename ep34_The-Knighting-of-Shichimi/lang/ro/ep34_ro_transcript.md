# Transcript of Pepper&Carrot Episode 34 [ro]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodul 34: Cavalerul Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|În aceeași noapte...
Hibiscus|2|False|...și iată, în această seară, te primim pe tine, Shichimi, drept cel mai tânăr Cavaler din Ah..
Coriander|3|False|Tot nicio urmă de Pepper?
Saffron|4|True|Încă nu.
Saffron|5|False|Ar face bine să se grăbească, altfel pierde discursul lui Shichimi.
Shichimi|6|True|Mulțumesc.
Shichimi|7|False|Aș vrea să...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|ZiooOOOOO
Sound|2|False|ZiooOOOOO
Pepper|3|True|Vine!
Pepper|4|True|Păzea!
Pepper|5|False|ATENȚIE!!!
Sound|6|False|BAM!
Pepper|7|False|Ups!
Pepper|8|False|E toată lumea OK? Nimic rupt?
Shichimi|9|False|Pepper!
Pepper|10|True|Bună Shichimi!
Pepper|11|True|Scuze pentru intrarea dramatică și pentru întârziere!
Pepper|12|False|Am fost pe fugă întreaga zi, dar asta e o poveste lungă.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|E și ea printre invitații tăi?
Shichimi|2|False|Da. Este prietena mea Pepper. Totul e în regulă.
Pepper|3|True|Carrot, ești bine?
Pepper|4|False|Scuze pentru aterizare, încă nu o stăpânesc prea bine când folosesc hiperviteza.
Pepper|5|True|Și scuze din nou tuturor pentru probleme,
Pepper|6|False|cât despre vestimentația mea...
Shichimi|7|False|hihi
Wasabi|8|True|Shichimi,
Wasabi|9|False|această tânără vrăjitoare nou-venită, este ea sigur o prietenă de-a ta?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Da, Maiestate.
Shichimi|2|False|Numele ei este Pepper, din Școala Chaosah.
Wasabi|3|True|Prezența ei poluează natura sacră a școlii noastre.
Wasabi|4|False|Să dispară din fața mea în acest moment.
Shichimi|5|True|Dar...
Shichimi|6|False|Stăpână Wasabi...
Wasabi|7|True|Dar ce?
Wasabi|8|False|Ai prefera să fii expulzată din școala noastră?
Shichimi|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Scuze Pepper, însă va trebui să pleci.
Shichimi|2|False|Acum.
Pepper|3|True|Poftim?
Pepper|4|False|Hei hei hei, stai un pic! Trebuie să fie o neînțelegere.
Shichimi|5|False|Te rog Pepper, nu complica lucrurile.
Pepper|6|False|Hei! Tu cea de pe tron. Dacă ai o problemă cu mine, vino și spune-mi în față!
Wasabi|7|False|Tsss...
Wasabi|8|True|Shichimi, ai zece secunde...
Wasabi|9|True|nouă...
Wasabi|10|True|opt...
Wasabi|11|False|șapte...
Shichimi|12|False|DESTUL, PEPPER! PLEACĂ!!!
Sound|13|False|ȘRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|9|True|PAF!
Pepper|1|False|Shichimi, te rog calmea...
Sound|2|False|BADuuM!
Shichimi|3|True|DISPARI!!!
Shichimi|4|True|DISPARI!!!
Shichimi|5|False|DISPARI!!!
Sound|6|False|CREEEEE!!!
Pepper|7|True|Auci!
Pepper|8|False|Hei! Asta... n-NU E... Auu... Frumos!
Sound|10|True|POF!
Sound|11|True|POF!
Sound|12|False|PAF!
Coriander|13|False|SHICHIMI! PEPPER! OPRIȚI-VĂ, VĂ ROG!
Saffron|14|False|Așteaptă.
Wasabi|15|False|Hmm!
Pepper|16|True|Grrr!
Pepper|17|False|OK, tu ai vrut-o!
Sound|18|False|BRZOO!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Sound|2|False|ȘKLAK!
Pepper|3|False|Auci!
Sound|4|False|PAF!!
Shichimi|5|False|Nici cea mai puternică vrajă a ta de anulare nu are efect asupra mea!
Shichimi|6|True|Renunță, Pepper, și pleacă!
Shichimi|7|False|Nu mă mai forța să te rănesc!
Pepper|8|False|Oh, vraja mea a funcțonat perfect, doar că nu erai tu ținta.
Shichimi|9|True|Hă?
Shichimi|10|False|Ce vrei să spui?!
Sound|11|True|Psș...
Sound|12|False|Psș...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|EA era ținta!
Pepper|3|False|Tocmai i-am anulat Vraja Splendorii care o menține tânără.
Pepper|4|True|Am observat vraja asta de cum am ajuns aici.
Pepper|5|False|Așa că ți-am dat un mic exemplu din ceea ce meriți pentru că ai făcut-o pe Shichimi să se lupte cu mine!
Wasabi|6|True|OBRĂZNICIE!
Wasabi|7|True|Cum îndrăznești,
Wasabi|8|False|și în fața întregii mele școli!
Pepper|9|True|Consideră-te norocoasă!
Pepper|10|False|Dacă aveam întreaga mea Rea, nu mă opream aici.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|DRZOW!!
Wasabi|2|True|Înțeleg. Ai atins nivelul predecesoarelor tale mult mai repede decât mă așteptam...
Wasabi|3|False|Asta îmi accelerează planurile, dar nu sunt decât vești bune.
Pepper|4|True|Planurile tale?
Pepper|5|True|Deci doar mă testai, și nu are nicio legătură cu faptul că am întârziat?
Pepper|6|False|Chiar că ești sucită!
Wasabi|7|True|he...
Wasabi|8|False|he.
Wasabi|9|True|VOI LA CE VĂ HOLBAȚI?!
Wasabi|10|False|TOCMAI AM FOST ATACATĂ, IAR VOI NU FACEȚI ALTCEVA DECÂT SĂ PRIVIȚI?! PRINDEȚI-O!!!
Wasabi|11|False|O VREAU VIE!
Wasabi|12|False|PRINDEȚI-O!!!
Pepper|13|False|Shichimi, vorbim mai târziu despre asta!
Pepper|14|True|Iartă-mă, Carrot, dar va trebui să folosesc din nou hiperviteza.
Pepper|15|False|Ține-te bine!
Sound|16|False|Tap!
Sound|17|False|Tap!
Sound|18|False|ZiiooOO!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|?!
Wasabi|2|False|PRINDEȚI-O!!!
Pepper|3|False|Oh vai.
Saffron|4|False|Pepper, ia mătura mea!
Sound|5|False|Fizzz!
Pepper|6|True|Oh uau!
Pepper|7|False|Mulțumesc, Saffron!
Sound|8|False|Toc!
Sound|9|False|ziooOOOO!
Narrator|10|False|VA URMA...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|31 martie 2021 Artă & scenariu: David Revoy. Cititori beta: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versiunea in limba română Traducere: Florin Șandru Mulțumiri speciale: lui Nartance pentru caracterul Wasabi din această ficțiune. Modul în care imaginat-o a determinat felul în care a fost prezentată. Bazat pe universul Hereva Creator: David Revoy. Administrator principal: Craig Maloney. Scriitori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Corectori: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.1, Inkscape 1.0.2 on Kubuntu Linux 20.04. Licență: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Știai că..?
Pepper|3|True|Pepper&Carrot este în totalitate gratis, open-source și sponsorizat prin patronajul cititorilor săi.
Pepper|4|False|Mulțumirile pentru acest episod merg la cei 1096 de patroni!
Pepper|5|True|Și tu poți deveni patron al Pepper&Carrot și să ai numele trecut aici!
Pepper|6|True|Ne găsești pe Patreon, Tipeee, PayPal, Liberapay ...și altele!
Pepper|7|False|Află mai multe pe www.peppercarrot.com!
Pepper|8|False|Mulțumim!
