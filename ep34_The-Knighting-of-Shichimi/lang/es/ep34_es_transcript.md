# Transcript of Pepper&Carrot Episode 34 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 34: La investidura de Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Esa misma noche...
Hibiscus|2|False|...así pues, esta noche eres investida, Shichimi, como nuestra más joven Caballera de Ah.
Celandria|3|False|¿Aún no se sabe nada de Pimienta?
Azafrán|4|True|Aún no.
Azafrán|5|False|Más le vale darse prisa o se va a perder el discurso de Shichimi.
Shichimi|6|True|Gracias.
Shichimi|7|False|Quisiera...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|SiooOOOOO
Sonido|2|False|SiooOOOOO
Pimienta|3|True|¡Que aterrizo!
Pimienta|4|True|¡Cuidado!
Pimienta|5|False|¡¡¡CUIDADO!!!
Sonido|6|False|¡CRASH!
Pimienta|7|False|¡Ups!
Pimienta|8|False|¿Estáis todos bien? ¿Nada roto?
Shichimi|9|False|¡Pimienta!
Pimienta|10|True|¡Hola, Shichimi!
Pimienta|11|True|Perdón por esta entrada dramática, ¡y por la tardanza!
Pimienta|12|False|He tenido un día ajetreado, pero es una larga historia.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|¿Es una de tus invitadas?
Shichimi|2|False|Sí. Es mi amiga Pimienta. Tranquila.
Pimienta|3|True|Zanahoria, ¿estás bien?
Pimienta|4|False|Perdona, todavía no controlo bien el aterrizaje cuando uso la hipervelocidad.
Pimienta|5|True|Y perdón de nuevo por todo el jaleo,
Pimienta|6|False|y también por mi vestimenta...
Shichimi|7|False|¡Jijiji!
Wasabi|8|True|Shichimi,
Wasabi|9|False|esta joven bruja que acaba de llegar, ¿es realmente una de tus amigas?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Sí, alteza.
Shichimi|2|False|Su nombre es Pimienta, de la escuela de Chaosah.
Wasabi|3|True|Su presencia aquí contamina la naturaleza sagrada de nuestra escuela.
Wasabi|4|False|Aléjala de mi vista inmediatamente.
Shichimi|5|True|Pero...
Shichimi|6|False|Maestra Wasabi...
Wasabi|7|True|¿Pero qué?
Wasabi|8|False|¿Prefieres ser tú la expulsada de nuestra escuela?
Shichimi|9|False|¡¡¡!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Lo siento, Pimienta, pero tienes que irte.
Shichimi|2|False|Ahora.
Pimienta|3|True|¿Qué?
Pimienta|4|False|¡Ey, ey, ey, un minuto! Estoy segura de que esto es solo un malentendido.
Shichimi|5|False|Por favor, Pimienta, no hagas las cosas más difíciles.
Pimienta|6|False|¡Eh, tú! Tú, la del trono. ¡Si tienes un problema conmigo, baja y me lo dices aquí a la cara!
Wasabi|7|False|Pfff...
Wasabi|8|True|Shichimi, tienes diez segundos...
Wasabi|9|True|nueve...
Wasabi|10|True|ocho...
Wasabi|11|False|siete...
Shichimi|12|False|¡BASTA, PIMIENTA! ¡¡¡FUERA!!!
Sonido|13|False|¡ SHRRIiii!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|9|True|¡PAF!
Pimienta|1|False|Shichimi, por favor, cálm...
Sonido|2|False|BADuuM!
Shichimi|3|True|¡¡¡VETE!!!
Shichimi|4|True|¡¡¡VETE!!!
Shichimi|5|False|¡¡¡VETE!!!
Sonido|6|False|¡¡¡CREEEEE!!!
Pimienta|7|True|¡Ay!
Pimienta|8|False|¡Eh! ¡Esto... n-NO... es... agradable!
Sonido|10|True|¡POF!
Sonido|11|True|¡POF!
Sonido|12|False|¡PAF!
Celandria|13|False|¡SHICHIMI! ¡PIMIENTA! ¡PARAD, POR FAVOR!
Azafrán|14|False|Espera.
Wasabi|15|False|¡Humm!
Pimienta|16|True|¡Grrr!
Pimienta|17|False|OK, ¡tú te lo has buscado!
Sonido|18|False|¡¡BRZOO!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡¡¡CURSUS CANCELLARE MAXIMUS!!!
Sonido|2|False|¡SHKLAK!
Pimienta|3|False|¡Ay!
Sonido|4|False|¡¡PAF!!
Shichimi|5|False|¡Ni siquiera tu mejor hechizo de anulación tiene efecto sobre mí!
Shichimi|6|True|¡Ríndete, Pimienta, y vete!
Shichimi|7|False|¡No me obligues a hacerte más daño!
Pimienta|8|False|Oh, mi hechizo de anulación funcionó perfectamente, solo que no eras tú el objetivo.
Shichimi|9|True|¿Qué?
Shichimi|10|False|¡¿Qué quieres decir?!
Sonido|11|True|Pshh...
Sonido|12|False|Pshh...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|¡¡¿?!!
Pimienta|2|True|¡Mi objetivo, era ella!
Pimienta|3|False|Solamente he anulado su Hechizo de Glamur, que le da una apariencia más joven.
Pimienta|4|True|Lo había notado desde mi llegada.
Pimienta|5|False|¡Y eso no es nada más que un aviso de lo que te mereces por obligar a Shichimi a enfrentarse contra mí!
Wasabi|6|True|¡INSOLENTE!
Wasabi|7|True|¿Cómo te atreves?
Wasabi|8|False|¡Y delante de toda mi escuela!
Pimienta|9|True|¡Date por contenta!
Pimienta|10|False|Si tuviese más Rea, no me habría bastado con eso.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡¡DRZOO!!
Wasabi|2|True|Ya veo. Has alcanzado el nivel de tus predecesores bastante antes de lo que había previsto...
Wasabi|3|False|Esto adelanta mis planes pero son buenas noticias.
Pimienta|4|True|¿Tus planes?
Pimienta|5|True|¿Entonces me estabas poniendo a prueba? ¿Esto no tiene nada que ver con haber llegado tarde?
Pimienta|6|False|¡Tú estás majareta!
Wasabi|7|True|Ja...
Wasabi|8|False|ja.
Wasabi|9|True|¿QUÉ ESTÁIS MIRANDO AHÍ TODAS PARADAS?
Wasabi|10|False|¡¿ME ACABAN DE ATACAR Y NO HACÉIS NADA?! ¡¡¡ATRAPADLA!!!
Wasabi|11|False|¡LA QUIERO VIVA!
Wasabi|12|False|¡¡¡ATRAPADLA!!!
Pimienta|13|False|Shichimi, ¡más tarde hablaremos tú y yo!
Pimienta|14|True|Lo siento, Zanahoria, pero debo salir de aquí en hipervelocidad una vez más.
Pimienta|15|False|¡Agárrate fuerte!
Sonido|16|False|¡Tap!
Sonido|17|False|¡Tap!
Sonido|18|False|¡¡ZiiooOO!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡¿?!
Wasabi|2|False|¡¡¡A POR ELLA!!!
Pimienta|3|False|Mecachis.
Azafrán|4|False|Pimienta, ¡toma mi escoba!
Sonido|5|False|¡Fizzz!
Pimienta|6|True|¡Oh, guay!
Pimienta|7|False|¡Gracias, Azafrán!
Sonido|8|False|¡Toc!
Sonido|9|False|¡ziooOOOO!
Narrador|10|False|CONTINUARÁ...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|31 de marzo de 2021 Dibujo & Guion: David Revoy. Lectores de la versión beta: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Versión en castellano Traducción: TheFaico Agradecimiento especial: a Nicolas Artance por explorar el personaje de Wasabi en sus novelas de fan-fiction. La forma en que él la ha imaginado ha tenido un gran impacto sobre la forma en que la he descrito en este episodio. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.1, Inkscape 1.0.2 en Kubuntu Linux 20.04. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
Pimienta|2|True|¿Sabías qué?
Pimienta|3|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 1096 mecenas!
Pimienta|5|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|6|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|7|False|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|8|False|¡Gracias!
