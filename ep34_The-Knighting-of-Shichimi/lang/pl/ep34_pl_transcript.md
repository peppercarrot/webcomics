# Transcript of Pepper&Carrot Episode 34 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tytuł|1|False|Odcinek 34: Pasowanie Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Tej samej nocy…
Hibiscus|2|False|Dzisiejszej nocy powi-tajmy Shichimi jako najmłodszego Rycerza Ah!
Coriander|3|False|Pepper dalej nie przyle-ciała?
Saffron|4|True|Jeszcze nie.
Saffron|5|False|Jeśli się nie pospie-szy, przegapi prze-mowę Shichimi.
Shichimi|6|True|Dziękuję.
Shichimi|7|False|Chciałabym…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|1|False|wziuuuuuuuu
Dźwięk|2|False|wziuuuuuuuu
Pepper|3|True|Lecę!
Pepper|4|True|Uwaga!
Pepper|5|False|UWAGA!!!
Dźwięk|6|False|TRACH!
Pepper|7|False|Ups!
Pepper|8|False|Wszyscy żyją? Nikogo nie skrzywdziłam?
Shichimi|9|False|Pepper!
Pepper|10|True|Cześć, Shichimi!
Pepper|11|True|Wybacz spóźnienie i to niecodzienne wejście!
Pepper|12|False|Od samego rana jestem w biegu, ale mniejsza z tym.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|Czy to jeden z naszych gości?
Shichimi|2|False|Tak, to Pepper, moja przyjaciółka, więc wszystko w porządku.
Pepper|3|True|Carrot, jes-teś cały?
Pepper|4|False|Wybacz to lądo-wanie, ale nie opa-nowałam jeszcze hiperprędkości.
Pepper|5|True|Przepraszam za mój strój
Pepper|6|False|i za narobienie bałaganu.
Shichimi|7|False|Hihihi.
Wasabi|8|True|Shichimi,
Wasabi|9|False|czy ta młoda wiedźma naprawdę jest twoją przyjaciółką?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Tak, Wasza Wysokość.
Shichimi|2|False|Ma na imię Pepper, jest ze szkoły Chaosu.
Wasabi|3|True|Jej obecność narusza świętość naszej szkoły.
Wasabi|4|False|Zabierz ją sprzed mojego oblicza, ale już.
Shichimi|5|True|Ale…
Shichimi|6|False|Mistrzyni Wasabi…
Wasabi|7|True|Ale co?
Wasabi|8|False|Wolisz sama zostać wyrzucona?
Shichimi|9|False|!!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Wybacz, Pepper, ale musisz odejść.
Shichimi|2|False|Natych-miast.
Pepper|3|True|Hę?
Pepper|4|False|Hola, hola. Jestem pewna, że to jakieś nieporozumienie.
Shichimi|5|False|Pepper, proszę, nie utrudniaj mi tego.
Pepper|6|False|Ej, ty tam, na tronie! Jeśli coś do mnie masz, powiedz mi to w twarz!
Wasabi|7|False|Pffft…
Wasabi|8|True|Shichimi, masz dziesięć sekund.
Wasabi|9|True|Dziewięć,
Wasabi|10|True|osiem,
Wasabi|11|False|siedem…
Shichimi|12|False|DOŚĆ TEGO, PEPPER! WYNOŚ SIĘ!
Dźwięk|13|False|SZRIIII!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|9|True|PAF!
Pepper|1|False|Shichimi, proszę, uspo…
Dźwięk|2|False|BUUuuM!
Shichimi|3|True|ODEJDŹ!!!
Shichimi|4|True|WYNOCHA!
Shichimi|5|False|ALE JUŻ!
Dźwięk|6|False|KRIIIII!!!
Pepper|7|True|Auć!
Pepper|8|False|Hej! To wcale nie jest… miłe!
Dźwięk|10|True|POF!
Dźwięk|11|True|POF!
Dźwięk|12|False|PAF!
Coriander|13|False|SHICHIMI, PEPPER! PZESTAŃCIE!
Saffron|14|False|Chwila.
Wasabi|15|False|Hmm!
Pepper|16|True|Grrr!
Pepper|17|False|Sama się o to prosiłaś!
Dźwięk|18|False|BRZUU!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS!!!
Dźwięk|2|False|SZKLAK!
Pepper|3|False|Ała!
Dźwięk|4|False|PAF!!
Shichimi|5|False|Nawet twoje naj-mocniejsze zaklęcie anulowania na mnie nie działa!
Shichimi|6|True|Poddaj się i odejdź, Pepper!
Shichimi|7|False|Nie chcę cię skrzywdzić!
Pepper|8|False|Moje zaklęcie wcale nie było wycelowane w ciebie!
Shichimi|9|True|Co?
Shichimi|10|False|Co masz na myśli?
Dźwięk|11|True|Pszz...
Dźwięk|12|False|Pszz...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!
Pepper|2|True|Ona była celem!
Pepper|3|False|Przerwałam zaklęcie piękności, dzięki któremu wyglądała młodo.
Pepper|4|True|Zauważyłam je zaraz po lądowaniu.
Pepper|5|False|Dałam ci małą próbkę tego, na co zasługujesz za doprowadzenie nas do walki!
Wasabi|6|True|BEZCZELNA!
Wasabi|7|True|Jak śmiesz?!
Wasabi|8|False|Na dodatek przed całą szkołą!
Pepper|9|True|Powinnaś się cieszyć!
Pepper|10|False|Gdybym miała całą Reę, nie skończyłoby się tylko na tym.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dźwięk|1|False|DRZUU!!
Wasabi|2|True|Rozumiem. Osiągnęłaś poziom swoich porzedniczek o wiele szybciej, niż się spodziewałam.
Wasabi|3|False|Co zasadniczo przyspiesza moje plany.
Pepper|4|True|Twoje plany?
Pepper|5|True|Zatem to był test i nie miał nic wspólnego z moim spóźnieniem?
Pepper|6|False|Jesteś szalona!
Wasabi|7|True|He…
Wasabi|8|False|he…
Wasabi|9|True|NA CO SIĘ GAPICIE?!
Wasabi|10|False|WŁAŚNIE MNIE ZAATAKOWANO, A WY STOICIE, JAKBY NIGDY NIC! BRAĆ JĄ!
Wasabi|11|False|CHCĘ JĄ ŻYWĄ!
Wasabi|12|False|BRAĆ JĄ!!!
Pepper|13|False|Shichimi, mamy do pogadania!
Pepper|14|True|Wybacz, Carrot, ale znowu musimy lecieć hiperprędko.
Pepper|15|False|Trzymaj się!
Dźwięk|16|False|Klep!
Dźwięk|17|False|Klep!
Dźwięk|18|False|WziiuuUU!!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|?!
Wasabi|2|False|ŁAPCIE JĄ!
Pepper|3|False|Ups.
Saffron|4|False|Pepper, weź moją miotłę!
Dźwięk|5|False|Fizzz!
Pepper|6|True|Super!
Pepper|7|False|Dzięki, Saffron!
Dźwięk|8|False|Pac!
Dźwięk|9|False|wziuuUUM!
Narrator|10|False|CIĄG DALSZY NASTĄPI…

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Marzec 31, 2021 Rysunki i scenariusz: David Revoy. Poprawki skryptu: Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Wersja polska Tłumaczenie: Sölve Svartskogen. Korekta i kontrola jakości: Besamir. Specjalne podziękowania: dla Nartance'a za szczegóły na temat postaci Wasabi, pochodzącej z jego fanfików. Jego wyobrażenie na jej temat ukształtowało wydźwięk tego odcinka. Oparto na uniwersum Herevy Autor: David Revoy. Pomocnik: Craig Maloney. Pisarze: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekta: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.4.1, Inkscape 1.0.2 na Kubuntu Linux 20.04. Licencja: Creative Commons Uznanie Autorstwa 4.0. www.peppercarrot.com
Pepper|2|True|Wiedziałeś, że
Pepper|3|True|Pepper&Carrot jest całkowicie darmowy, liberalny, open-source'owy oraz wspierany przez naszych czytelników?
Pepper|4|False|Za ten odcinek dziękujemy 1096 patronom!
Pepper|5|True|Ty również możesz zostać patronem Pepper&Carrot i znaleźć się na tej liście.
Pepper|6|True|Jesteśmy na Patreonie, Tipeee, PayPal, Liberapay i wielu innych!
Pepper|7|False|Wejdź na www.peppercarrot.com, by dowiedzieć się więcej!
Pepper|8|False|Dziękujemy!
