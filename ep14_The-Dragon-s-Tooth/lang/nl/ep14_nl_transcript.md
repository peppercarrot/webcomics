# Transcript of Pepper&Carrot Episode 14 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 14: De drakentand

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komijn|5|True|Goed, goed, goed! De vakantie is voorbij.
Komijn|6|False|Laten we beginnen met een les over antieke toverdrankjes en hun voornaamste ingrediënten.
Komijn|7|True|Mmm... Verdraaid! Er is geen draken-tandpoeder meer.
Komijn|8|False|En het heeft geen zin aan deze les te beginnen zonder dat ingrediënt ...
Geschrift|9|False|Drakentand
Geschrift|4|False|33
Geschrift|3|False|PRIVAAT EIGENDOM
Geschrift|2|True|HEKS
Geschrift|1|True|GEVAAR

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komijn|1|False|Cayenne! Thym! Willen jullie drakentandpoeder voor me zoeken?
Cayenne|2|False|In die kou?! Nee, bedankt!
Thym|3|False|Meh ... Zeg dat wel ...
Pepper|4|False|Geen probleem, ik doe het wel!
Pepper|6|False|Zo terug!
Pepper|7|True|"Die kou", "die kou"!
Pepper|8|False|Ongelofelijk, wat een watjes!
Pepper|9|False|Een goeie nijptang, wat beschermende kleding en tadaa! Drakentanden!
Komijn|5|False|Pepper, wacht!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ha ha!
Pepper|2|False|Oepsie! Dit is ietsiepietsie lastiger dan ik had gedacht!
Pepper|3|True|Pfff!
Pepper|4|False|... ik dacht dat het met een luchtdraak misschien makkelijker zou zijn!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Carrot!
Pepper|2|True|Kom terug ...
Pepper|3|True|Een moerasdraak is veel vriendelijker. Dat zeggen ze tenminste ...
Pepper|4|False|... ík heb dat niet verzonnen ...
Pepper|5|True|Hmm ... Voor de bliksemdraak ...
Pepper|6|False|... weet ik niet zeker of deze tang geschikt is ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oké.
Pepper|2|True|Het is te moeilijk ...
Pepper|3|False|... ik geef het op.
Pepper|5|False|Pepper geeft nooit op!
Pepper|4|True|Nee!
Verteller|6|False|Die ochtend
Komijn|8|False|!!
Vogel|7|False|KUKELEKUUUuuuu ! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|10|False|- EINDE -
Aftiteling|11|False|01/2016 - Tekeningen & verhaal: David Revoy - Vertaling: Midgard & Willem Sonke
Geschrift|6|False|Tandarts|nowhitespace
Geschrift|5|True|voor|nowhitespace
Geschrift|4|True|Draken
Geschrift|7|False|Gratis!
Geluid|3|False|plop!
Pepper|1|True|Zo, dat is indrukwekkend, toch?
Pepper|2|False|Ik heb ten minste honderd drakentanden!
Komijn|8|True|... maar Pepper, drakentand ...
Komijn|9|False|... dat is een plant!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 671 patronen:
Aftiteling|2|True|Licentie: Creative Commons Naamsvermelding 4.0 Bronbestanden: beschikbaar op www.peppercarrot.com Software: deze aflevering is 100% gemaakt met vrije software Krita 2.9.10, Inkscape 0.91 op Linux Mint 17
Aftiteling|3|False|Voor de volgende aflevering kun jij ook een patroon van Pepper&Carrot worden op
Aftiteling|4|False|https://www.patreon.com/davidrevoy
