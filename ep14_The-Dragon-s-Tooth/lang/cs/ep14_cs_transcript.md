# Transcript of Pepper&Carrot Episode 14 [cs]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|14. díl: Dračí zub

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|5|True|Ten čas ale utíká! Prázdniny skončily!
Cumin|6|False|Začneme hodinou o starobylých lektvarech a jejich hlavních přísadách.
Cumin|7|True|No... Safra. Došel nám prášek z Dračího zubu.
Cumin|8|False|Bez něj nemá cenu začínat hodinu...
Writing|9|False|Dračí zub
Writing|4|False|33
Writing|3|False|CARODEJKY
Writing|2|True|POZEMEK
Writing|1|True|VAROVANI

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|Pepříno! Tymiáno! Mohla by mi jedna z vás dojít pro nějaký Dračí zub?
Cayenne|2|False|V téhle zimě?!? Ne, děkuju!
Thyme|3|False|Jo... Přesně...
Pepper|4|False|Buď v klidu, já ho přinesu!
Pepper|6|False|Hned jsem zpátky!
Pepper|7|True|Prý zima!
Pepper|8|False|To jsou ale padavky!
Pepper|9|False|Stačí pořádné kleště a "brnění" pro Karotku a ty Dračí zuby budeme mít raz dva!
Cumin|5|False|Feferonko, počkej!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ha ha!
Pepper|2|False|Jejda! Možná to bude o trošičku složitější, než jsem myslela!
Pepper|3|True|Pfff!
Pepper|4|False|... a to jsem si myslela, že s Větrným drakem to bude jednodušší!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Karotko! ...
Pepper|2|True|Vrať se...
Pepper|3|True|... Močáloví draci jsou známí svou neškodností!
Pepper|4|False|nebo jsem si to aspoň myslela...
Pepper|5|True|Hmm... A na Bleskového draka...
Pepper|6|False|... ty kleště asi nebudou moc dobrý nápad...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Dobře.
Pepper|2|True|Je to na mě moc těžké...
Pepper|3|False|... vzdávám to.
Pepper|5|False|Feferonka se nikdy nevzdává!
Pepper|4|True|Ne!
Bird|7|False|Ky-ky-ry-kýýýýý ! ! !|nowhitespace
Narrator|6|False|Další den...
Cumin|8|False|!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|10|False|- KONEC -
Credits|11|False|01/2016 - Kresba a příběh: David Revoy - Překlad: Kateřina Fleknová
Writing|5|False|zubař
Writing|4|True|Dračí
Writing|6|False|Zdarma!
Sound|3|False|pop !
Pepper|1|True|No? To koukáš, co?
Pepper|2|False|Mám už aspoň sto dračích zubů!
Cumin|7|True|... ale Feferonko, Dračí zub...
Cumin|8|False|...je rostlina!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot je naprosto zdrama, open-source komiks sponzorovaný laskavými fanoušky. Za tuto epizodu děkujeme následujícím 671 fanouškům:
Credits|2|True|Další epizodu Pepper&Carrot můžete podpořit i Vy:
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|4|False|Licence: Creative Commons Attribution 4.0 Zdroj: dostupné na www.peppercarrot.com Software: Tato epizoda byla celá vytvořena za pomoci bezplatných softwarů: Krita 2.9.10, Inkscape 0.91 na Linux Mint 17
