# Transcript of Pepper&Carrot Episode 14 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 14: Zmajev zob

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kumina|5|True|Dobro, dobro! Počitnic je konec!
Kumina|6|False|Začniva s poukom starodavnih napojev in njihovih glavnih sestavin.
Kumina|7|True|Hm… Šment! Ni več zmajevega zoba v prahu.
Kumina|8|False|Brez njega nima smisla začeti.
Napis|9|False|Zmajev zob
Napis|4|False|33
Napis|3|False|ČAROVNICE
Napis|2|True|LASTNINA
Napis|1|True|POZOR

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kumina|1|False|Kajenka! Timijana! Bi lahko katera od vaju šla po zmajev zob?
Kajenka|2|False|V tem mrazu?! Ne, hvala!
Timijana|3|False|Brrr… Enako …
Paprika|4|False|Nič ne skrbite, grem jaz!
Paprika|6|False|Takoj se vrnem!
Paprika|7|True|„Zima je“, „Mraz je“!
Paprika|8|False|Zapečkarice!
Paprika|9|False|Dober par klešč in nekaj zaščite, pa bo zmajev zob najin!
Kumina|5|False|Paprika, počakaj!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Ha ha!
Paprika|2|False|Vaauu! Tole bo mogoče malce težje, kot sem mislila!
Paprika|3|True|Pfff!
Paprika|4|False|Človek bi si mislil, da bo z zračnim zmajem lažje!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Korenček!
Paprika|2|True|Pridi nazaj!
Paprika|3|True|Močvirni zmaji so znani po svoji krotkosti!
Paprika|4|False|Vsaj mislila sem, da so …
Paprika|5|True|Hm. Tale strelasti zmaj …
Paprika|6|False|Nisem prepričana, da imava primerne klešče.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|No, prav.
Paprika|2|True|Pretežko je.
Paprika|3|False|Predam se.
Paprika|5|False|Paprika se nikoli ne preda!
Paprika|4|True|Ne!
Ptica|7|False|Kiii kiii riiii kiiiiiii ! ! !|nowhitespace
Pripovedovalec|6|False|Naslednje jutro …
Kumina|8|False|!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|10|False|- KONEC -
Zasluge|11|False|Januar 2016 - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Napis|6|False|za ZMAJE
Napis|5|True|zdravnica|nowhitespace
Napis|4|True|Zobo-
Napis|7|False|ZASTONJ!
Zvok|3|False|pop!
Paprika|1|True|Sem se kar izkazala, a?
Paprika|2|False|Zdaj imam že vsaj sto zmajevih zob!
Kumina|8|True|Ampak Paprika, zmajev zob …
Kumina|9|False|je rastlina!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 671 bralcec:
Zasluge|2|True|Tudi ti lahko postaneš denarni/a podpornik/ca za naslednjo epizodo stripa:
Zasluge|3|False|https://www.patreon.com/davidrevoy
Zasluge|4|False|Licenca: Creative Commons Priznanje avtorstva 4.0. Izvorne datoteke na voljo na www.peppercarrot.com Programska oprema: ta epizoda je nastala s prostim programjem Krita 2.9.10 in Inkscape 0.91 na Linux Mint 17
