# Transcript of Pepper&Carrot Episode 14 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium XIV: Dens Draconis

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cuminum|5|True|Bene, bene ! Feriae confectae sunt!
Cuminum|6|False|Incipiamus lectionem de potionibus antiquis et primis impensis earum.
Cuminum|7|True|Ah... ecastor ! Dentem draconis nondum habeo...
Cuminum|8|False|Sine illa impensa non possumus lectionem incipere...
Writing|9|False|Dens Draconis
Writing|4|False|33
Writing|3|False|POSSESSIONES PRIVATAE
Writing|2|True|MAGAM
Writing|1|True|CAVE

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cuminum|1|False|Caienna! Thymus! Potestisne ire inventum dentem draconis mihi quaeso?
Caienna|2|False|Cum isto gelu?! Minime, nolo !
Thymus|3|False|Ego quoque... Nolo...
Piper|4|False|Nolite sollicitari faciam perlibenter!
Piper|6|False|Mox reveniam !
Piper|7|True|"Gelu", "gelu"!
Piper|8|False|Quam timidae eae sunt !
Piper|9|False|Cum bono forcipe, bonisque praesidiis, dentes draconis nobis erunt !
Cuminum|5|False|Piper, mane !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|True|Ha ha !
Piper|2|False|Mecastor! Paulo difficilior erit quam putavi !
Piper|3|True|Pfff !
Piper|4|False|...ego credebam cum dracone aetheris hoc facilior fuisse!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|True|Carota !...
Piper|2|True|Reveni ...
Piper|3|True|... dracones paludum noti sunt dociles esse.
Piper|4|False|Nihil excogitavi...
Piper|5|True|Mmm... Ad draconem fulguris...
Piper|6|False|...incerta sum an hic forceps aptatus sit.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|True|Ita.
Piper|2|True|Nimis difficilis...
Piper|3|False|... manus do.
Piper|5|False|Piper numquam manus dat !
Piper|4|True|Minime !
Narrator|6|False|Postridie.
Cuminum|8|False|!!|nowhitespace
Bird|7|False|COCOCOCOOoooo ! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|10|False|- FINIS -
Credits|11|False|M. Januario A. MMXVI - Designum & Fabula : David Revoy - Translatio: Benjamin Touati - Correctio: Valentine Guillocheau
Writing|4|True|Medica
Writing|5|True|dentaria
Writing|6|False|Draconis
Writing|7|False|Gratis !
Sonus|3|False|pop !|nowhitespace
Piper|1|True|Em, mirata, nonne ?
Piper|2|False|Nunc plus minusve centum dentes draconis habeo !
Cuminum|8|True|... Sed Piper, dens draconis...
Cuminum|9|False|... planta est !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Piper&Carota gratis constat, omno apertum fontis est ac juvatur maecenatu lectorum. Gratias ago illis 675 Maecenatibus hujus episodii:
Credits|4|False|Licentia : Creative Commons Attribution 4.0 Fontes : inveniuntur apud www.peppercarrot.com Instrumenta : Hoc episodium omnino designatum est liberis programmatibus Krita 2.9.10, Inkscape 0.91 Linuce Mint 17
Credits|2|True|Tu quoque, potes fieri maecenas Piperis&Carotae sequenti episodio :
Credits|3|False|https://www.patreon.com/davidrevoy
