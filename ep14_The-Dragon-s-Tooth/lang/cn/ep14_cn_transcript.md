# Transcript of Pepper&Carrot Episode 14 [cn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
标题|1|False|第14集：龙牙

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
孜然粉|5|True|好啦，好啦，好啦！ 假期结束啦！
孜然粉|6|False|我们的课程就 从经典魔药和它们的 基础配料开始吧。
孜然粉|7|True|嗯…糟糕！ 没有龙牙粉了。
孜然粉|8|False|没有这个配料， 这课也就没法上了。
书写|9|False|龙牙
书写|4|False|33
书写|3|False|私有领地
书写|2|True|女巫出没
书写|1|True|危险

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
孜然粉|1|False|卡宴椒！百里香！ 你们能不能帮我去找点龙牙来？
卡宴椒|2|False|这天寒地冻的？！ 我不去！
百里香|3|False|说的就是… 我也不去…
小辣椒|4|False|没问题， 包在我身上！
小辣椒|6|False|我去去就来！
小辣椒|7|True|这也算“天寒地冻”？
小辣椒|8|False|着实是一帮 老太婆！
小辣椒|9|False|一把好钳子， 再来些保护装备， 龙牙就是咱们的啦！
孜然粉|5|False|小辣椒 等一下！

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|True|哈哈！
小辣椒|2|False|糟了！事情比 我想象中的要 复杂一些哈！
小辣椒|3|True|唉呦！
小辣椒|4|False|…我还以为 飞龙的牙会 好拔一点呢！

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|True|萝卜头！
小辣椒|2|True|回来呀…
小辣椒|3|True|…沼泽龙 可是号称 最温顺的
小辣椒|4|False|我可不是 瞎说的…
小辣椒|5|True|嗯…话说 对付电龙…
小辣椒|6|False|…用这个钳子 可能未必合适

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
小辣椒|1|True|好吧。
小辣椒|2|True|太难了…
小辣椒|3|False|…我放弃。
小辣椒|5|False|小辣椒是 从来不会放弃的！
小辣椒|4|True|才不会！
旁白|6|False|第二天|nowhitespace
孜然粉|8|False|!!
小鸟|7|False|咯咯咯 ! ! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
旁白|10|False|- 完 -
参与者名单|11|False|2016年1月 - 绘画与脚本 ：David Revoy - 翻译：庄冉
书写|4|True|牙科门诊|nowhitespace
书写|5|True|本日特诊|nowhitespace
书写|6|False|龙
书写|7|False|免费拔牙!
声音|3|False|嘣！!|nowhitespace
小辣椒|1|True|怎么样， 是个好主意吧？
小辣椒|2|False|我快有差不多 一百颗龙牙了！
孜然粉|8|True|…可是小辣椒， 我说的龙牙…
孜然粉|9|False|…是一种 植物啊！

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
参与者名单|1|False|授权许可 : Creative Commons Attribution 4.0 源文件 : 可从www.peppercarrot.com网站获取 软件 : 本集100%使用开源软件绘制 Linux Mint 17环境下Krita 2.9.10, Inkscape 0.91
参与者名单|2|True|您也可以成为下一集小辣椒与萝卜头的捐助者
参与者名单|3|False|小辣椒与萝卜头是完全免费且开源的，项目资金由读者捐助。 感谢本集的 671 位捐助者：
参与者名单|4|False|https://www.patreon.com/davidrevoy
