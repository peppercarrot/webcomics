# Transcript of Pepper&Carrot Episode 36 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 36: The Surprise Attack

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|AN APOLOGY?
Wasabi|2|False|You can't be serious!
Wasabi|3|False|THROW THIS FOOL IN JAIL!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fschh...
Pepper|2|True|Darn!
Pepper|3|False|I can't do anything in here!
Pepper|4|False|Damn magical prison! Grrrr!
Sound|5|False|CLANG!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|How could I be so naive!
Shichimi|2|True|Shhhhh, Pepper!
Shichimi|3|False|Keep it down.
Pepper|4|False|Who's there?!
Shichimi|5|True|Shhhhh! Quiet already!
Shichimi|6|True|Over here.
Shichimi|7|False|I'm here to get you out.
Sound|8|False|Kshiii...
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|I... I am sorry for what happened...
Shichimi|2|False|...you know, with our fight.
Shichimi|3|True|I...
Shichimi|4|False|I had to do it.
Pepper|5|True|Don't worry – I know.
Pepper|6|False|Thanks for coming.
Shichimi|7|False|This magical cell is really strong – they’ve gone all out for you!
Pepper|8|False|Ha ha!
Shichimi|9|False|Keep it down, they'll hear us.
Rat|10|True|LAP
Rat|11|True|LAP
Rat|12|False|LAP
Shichimi|13|True|You know,
Shichimi|14|True|another reason I came is that I was admitted into Wasabi's inner circle right after the ceremony.
Shichimi|15|False|And I learned of her plans...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|16|False|CLANG!
Sound|18|True|CLUNK!
Sound|19|True|CLANG!
Sound|20|True|CLANK!
Sound|21|False|CLANG!
Shichimi|1|False|It's awful, Pepper.
Shichimi|2|False|Wasabi wants to dominate all other schools of magic, pure and simple...
Shichimi|3|False|She will leave at dawn with an army of witches for Qualicity...
Pepper|4|True|Oh, no!
Pepper|5|True|Coriander!
Pepper|6|False|And her kingdom!
Shichimi|7|True|And Zombiah magic!
Shichimi|8|False|We must warn her at once!
Shichimi|9|False|A pilot and dragon await us on the platform, to get us out of here.
Shichimi|10|False|That's it, the lock finally gave way!
Sound|11|False|Ching!
Pepper|12|True|Bravo!
Pepper|13|False|I'll get Carrot and my hat.
Rat|14|False|squeak!
Carrot|15|False|HISSSS!
Rat|17|False|squeeee!
Pepper|22|False|!!!
Shichimi|23|False|!!!
Guard|24|True|GUARD!
Guard|25|False|COME HERE!
Guard|26|False|AN INTRUDER IS FREEING THE PRISONER!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|We were so close...
Pepper|2|False|Yeah, so close.
Pepper|3|False|By the way, do you know why Wasabi has it out for me?
Shichimi|4|False|She's afraid of the Chaosah witches, Pepper...
Shichimi|5|True|Especially of your chain reactions.
Shichimi|6|False|She believes they are a major threat to her plans.
Pepper|7|True|Oh, that, pfff...
Pepper|8|False|No need for her to fret; I've never managed to set off any of those.
Shichimi|9|False|Really?
Pepper|10|False|Yes, really, ha ha ha!
Shichimi|11|True|Hee-hee!
Shichimi|12|False|She's so paranoid...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Officer!
King|2|False|Can you confirm that this is the temple of that witch?
Officer|3|True|Highly likely, my liege!
Officer|4|False|Several of our spies have spotted her here recently.
King|5|True|GRrrr...
King|6|False|So, that threat to our art of war and traditions lives here!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|Let us celebrate our alliance by retaliating and razing this temple from the surface of Hereva.
Sound|3|False|Clap
Enemy|2|False|Well said!
Army|4|True|Yeah!
Army|5|True|Yeah!
Army|6|True|Yeah!
Army|7|True|Yeah!
Army|8|True|Yeehaaa!
Army|9|True|Yarr!
Army|10|True|Yeehaw!
Army|11|True|Yeee-haw!
Army|12|True|Yaaa!
Army|13|False|Hurrah!
King|14|True|CATAPULTS!
King|15|False|FIRE!!!
Sound|16|False|huUWOOOOOOOOOOO!
Sound|17|True|Whooosh!
Sound|18|False|Whooosh!
King|19|False|ATTAAAAAAACK!!!
Pepper|20|True|What's going on?
Pepper|21|False|An attack?!
Sound|22|True|BOOM!
Sound|23|False|BOOOM!
Shichimi|24|False|What?!
Sound|25|True|BOO~OOOM!
Sound|26|False|BOOM!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|cough
Pepper|2|False|cough!!
Pepper|3|False|Shichimi! Are you alright?
Shichimi|4|True|Yes, I'm fine!
Shichimi|5|True|What about you?
Shichimi|6|False|And Carrot?
Pepper|7|False|We're fine.
Pepper|8|True|What the heck...
Pepper|9|False|I... can't... believe it...
Shichimi|10|True|Where did those soldiers come from?!
Shichimi|11|True|And with catapults?!
Shichimi|12|False|What do they want?
Pepper|13|True|I don't know...
Pepper|14|False|But those two I do know.
Shichimi|15|False|?
Shichimi|16|False|!!!
Shichimi|17|False|Torreya, over here!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Spirits be praised, you are safe and sound.
Sound|3|False|Grab
Torreya|4|True|I was so worried when I heard that they had imprisoned you!
Torreya|5|True|And this battle!
Torreya|6|False|What chaos!
Shichimi|7|False|Torreya, it's so good to see you.
Pepper|8|False|Oh, my...
Pepper|9|False|So this dragon pilot is Shichimi's girlfriend...
Pepper|10|False|I can't imagine what would have happened had I gotten rid of her.
Pepper|11|False|And those armies, they must have followed me.
Pepper|12|False|Without them, we would not be free.
Pepper|13|False|It all seems so connected...
Pepper|14|False|...OH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|What's up with her?
Shichimi|2|False|Pepper? Is everything okay?
Pepper|3|True|Yes, I'm fine.
Pepper|4|False|I've just realized something.
Pepper|5|False|Everything that's happened is, directly or indirectly, the result of my actions, my choices...
Pepper|6|False|...in other words, my chain reaction!
Shichimi|7|True|Really?
Shichimi|8|False|You'll have to explain.
Torreya|9|True|Enough chat, we're in the middle of a battlefield.
Torreya|10|True|We can talk about it once we're flying.
Torreya|11|False|Hop on, Pepper!
Shichimi|12|True|Torreya is right.
Shichimi|13|False|We need to get to Qualicity, no matter what.
Pepper|14|False|Hold on a second.
Pepper|15|True|Wasabi's army is about to counter-attack.
Pepper|16|False|We can't let them kill each other like that.
Pepper|17|False|I feel that it's my responsibility to end this battle.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|But how?
Arra|2|True|Yes, how do you plan to do that, witch?
Arra|3|False|You haven't recovered much Rea, I can feel it.
Pepper|4|True|You're absolutely right, but there is a spell of mine that could fix everything.
Pepper|5|False|I just need your Rea to be able to reach everyone.
Arra|6|True|Give energy to a witch?
Arra|7|True|It's forbidden!
Arra|8|False|Most certainly not!
Pepper|9|False|Would you rather watch a carnage unfold?
Torreya|10|True|Please, Arra. Make an exception. Those girls and dragons fighting, that's our school, our family.
Torreya|11|False|And yours too.
Shichimi|12|False|Yes, please, Arra.
Arra|13|True|PFF! Fine!
Arra|14|False|But at her own risk!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|fshhhh!!
Pepper|2|False|WOAAAH!
Pepper|3|False|So that's what a dragon's Rea is like!
Shichimi|4|False|Pepper, quickly! The battle!
Pepper|5|True|Allus...!
Pepper|6|True|Yuus...!
Pepper|7|True|Needum...!
Pepper|8|True|Est...
Pepper|9|False|...LOVIUS!
Sound|10|False|Dzziooo!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Fiizz!
Sound|4|True|Dziing!
Sound|3|True|Hfhii!
Sound|2|True|Schii!
Sound|1|True|Schsss!
Sound|6|True|Fiizz!
Sound|7|True|Dziing!
Sound|9|True|Schii!
Sound|8|True|Hfheee!
Sound|10|False|Schsss!
Pepper|11|False|This spell was the first draft of my anti-war spells.
Pepper|12|False|It converts mortal foes into friends...
Pepper|13|False|…and violence into love and compassion.
Shichimi|14|True|Wow!
Shichimi|15|True|Well... that's terrific, Pepper!
Shichimi|16|False|They've stopped fighting!
Torreya|17|False|It's working!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|But what's going on?
Shichimi|2|False|Some of them are kissing...?!
Torreya|3|True|Uh... That's a lot of new couples!
Torreya|4|False|Was that meant to happen, Pepper?
Pepper|5|False|Oh, no! I think it was the Dragon's Rea that amplified the love in my spell.
Torreya|6|False|Haha, this battle will go straight into the history books.
Shichimi|7|True|Hee-hee,
Shichimi|8|False|definitely!
Pepper|9|False|Ooohhh, how embarrassing!
Writing|10|False|- FIN -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|December 15, 2021 Art & scenario: David Revoy. Beta readers: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. English translation: Arlo James Barnes, Bobby Hiltz, Estefania de Vasconcellos Guimaraes, Frederic Mora, GunChleoc, Karl Ove Hufthammer, Pierre-Antoine Angelini, Zane Waldman. Proofreading: Craig Maloney. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0β, Inkscape 1.1 on Kubuntu Linux 20.04. License: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Did you know?
Pepper|3|False|Pepper&Carrot is entirely free (libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 1036 patrons!
Pepper|5|False|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|6|False|We are on Patreon, Tipeee, PayPal, Liberapay ... and more!
Pepper|7|False|Check www.peppercarrot.com for more info!
Pepper|8|False|Thank you!
