# Transcript of Pepper&Carrot Episode 36 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 36: Der Überraschungsangriff

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wasabi|1|True|EINE ENTSCHULDIGUNG?
Wasabi|2|False|Das kann doch nicht dein Ernst sein!
Wasabi|3|False|WERFT DIESE NÄRRIN INS GEFÄNGNIS!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Fschh...
Pepper|2|True|Verdammt!
Pepper|3|False|Ich kann hier gar nichts machen!
Pepper|4|False|Blödes magisches Gefängnis! Grrrr!
Geräusch|5|False|SCHEPPER!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Wie konnte ich nur so naiv sein!
Shichimi|2|True|Psssst, Pepper!
Shichimi|3|False|Sei leise.
Pepper|4|False|Wer ist da?!
Shichimi|5|True|Pssst! Sei endlich still!
Shichimi|6|True|Hier drüben.
Shichimi|7|False|Ich bin hier, um dich zu befreien.
Geräusch|8|False|Djjjiii...
Pepper|9|False|Shichimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Es... Es tut mir leid, was passiert ist...
Shichimi|2|False|...also, mit unserem Streit.
Shichimi|3|True|Ich...
Shichimi|4|False|Ich hatte keine andere Wahl.
Pepper|5|True|Keine Sorge – ich verstehe.
Pepper|6|False|Danke, dass du hier bist.
Shichimi|7|False|Dieser magische Käfig ist echt stark – sie haben sich für dich richtig angestrengt!
Pepper|8|False|Ha ha!
Shichimi|9|False|Sei leise, sie werden uns hören.
Ratte|10|True|SCHLECK
Ratte|11|True|SCHLECK
Ratte|12|False|SCHLECK
Shichimi|13|True|Übrigens...
Shichimi|14|True|ein anderer Grund, warum ich gekommen bin, ist, weil ich nach der Zeremonie in Wasabis inneren Kreis aufgenommen wurde.
Shichimi|15|False|Und ich erfuhr von ihren Plänen...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|16|False|KRACH!
Geräusch|18|True|POLTER!
Geräusch|19|True|PENG!
Geräusch|20|True|SCHEPPER!
Geräusch|21|False|SCHEPPER!
Shichimi|1|False|Es ist furchtbar, Pepper.
Shichimi|2|False|Wasabi will schlicht und einfach alle anderen Schulen der Magie unterwerfen...
Shichimi|3|False|Sie bricht am Morgengrauen in Richtung Qualicity auf, mit einer Armee von Hexen...
Pepper|4|True|Oh nein!
Pepper|5|True|Coriander!
Pepper|6|False|Und ihr Königreich!
Shichimi|7|True|Und Zombiah-Magie!
Shichimi|8|False|Wir müssen sie sofort warnen!
Shichimi|9|False|Eine Pilotin und ein Drache warten auf uns am Steig, um uns rauszubringen.
Shichimi|10|False|Das war's, das Schloss hat endlich nachgegeben!
Geräusch|11|False|Tsching!
Pepper|12|True|Prima!
Pepper|13|False|Ich schnappe mir Carrot und meinen Hut.
Ratte|14|False|quiek!
Carrot|15|False|FAUCH!
Ratte|17|False|quiiiiek!
Pepper|22|False|!!!
Shichimi|23|False|!!!
Wache|24|True|WACHEN!
Wache|25|False|KOMMT HER!
Wache|26|False|EIN EINDRINGLING BEFREIT DIE GEFANGENE!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Wir hätten es fast geschafft...
Pepper|2|False|Ja, fast.
Pepper|3|False|Nebenbei, weißt du, warum Wasabi es auf mich abgesehen hat?
Shichimi|4|False|Sie hat Angst vor den Hexen von Chaosāh, Pepper...
Shichimi|5|True|Besonders vor deinen Kettenreaktionen.
Shichimi|6|False|Sie glaubt, dass sie eine große Bedrohung für ihre Pläne darstellen.
Pepper|7|True|Ach, das, pfff...
Pepper|8|False|Kein Grund für sie, sich aufzuregen; ich habe es noch nie geschafft, so eine auszulösen.
Shichimi|9|False|Echt nicht?
Pepper|10|False|Ja, echt nicht, ha ha ha!
Shichimi|11|True|Hee-hee!
Shichimi|12|False|Sie ist so paranoid...

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|True|Hauptmann!
König|2|False|Könnt Ihr bestätigen, dass dies der Tempel dieser Hexe ist?
Hauptmann|3|True|Mit höchster Wahrscheinlichkeit, mein Herr!
Hauptmann|4|False|Mehrere unserer Spione haben sie hier beobachtet.
König|5|True|GRrrr...
König|6|False|Die Bedrohung unserer Kunst des Krieges und Traditionen lebt also hier!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
König|1|False|Lasst uns unsere Allianz feiern, indem wir zurückschlagen und diesen Tempel von der Oberfläche Herevas wegputzen!
Geräusch|3|False|Klatsch
Feind|2|False|Wahre Worte!
Armee|4|True|Ja!
Armee|5|True|Ja!
Armee|6|True|Ja!
Armee|7|True|Ja!
Armee|8|True|Hurra!
Armee|9|True|Juhu!
Armee|10|True|Jahwohl!
Armee|11|True|Juhuuu!
Armee|12|True|Jaaa!
Armee|13|False|Hurra!
König|14|True|KATAPULTE!
König|15|False|FEUER!!!
Geräusch|16|False|huUWOOOOOOOOOOO!
Geräusch|17|True|Wuuusch!
Geräusch|18|False|Wuuusch!
König|19|False|ATTACKEEEEEE!!!
Pepper|20|True|Was ist los?
Pepper|21|False|Ein Angriff?!
Geräusch|22|True|BUMM!
Geräusch|23|False|BUMMM!
Shichimi|24|False|Was?!
Geräusch|25|True|BUU~UMMM!
Geräusch|26|False|BUMM!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|hust
Pepper|2|False|hust!!
Pepper|3|False|Shichimi! Geht es dir gut?
Shichimi|4|True|Ja, alles gut!
Shichimi|5|True|Und dir?
Shichimi|6|False|Und Carrot?
Pepper|7|False|Uns geht es gut.
Pepper|8|True|Was zum Geier...
Pepper|9|False|Ich... kann es... nicht fassen...
Shichimi|10|True|Wo kommen diese Soldaten auf einmal her?!
Shichimi|11|True|Sogar mit Katapulten?!
Shichimi|12|False|Was wollen sie?
Pepper|13|True|Keine Ahnung...
Pepper|14|False|Aber die beiden da kenne ich.
Shichimi|15|False|?
Shichimi|16|False|!!!
Shichimi|17|False|Torreya, hier drüben!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|True|Shichimi!
Torreya|2|False|Den Geistern sei Dank, du bist heil und unversehrt.
Geräusch|3|False|greif
Torreya|4|True|Ich war so besorgt, als ich gehört habe, dass sie dich eingesperrt haben
Torreya|5|True|Und diese Schlacht!
Torreya|6|False|So ein Chaos!
Shichimi|7|False|Torreya, es ist so schön, dich zu sehen.
Pepper|8|False|Ach, du meine...
Pepper|9|False|Diese Drachenpilotin ist also Shichimis Geliebte...
Pepper|10|False|Ich kann mir nicht vorstellen, was passiert wäre wenn ich sie losgeworden wäre.
Pepper|11|False|Und diese Armeen müssen mir wohl gefolgt sein.
Pepper|12|False|Ohne sie wären wir nicht frei.
Pepper|13|False|Fast so, als ob alles miteinander verknüpft wäre
Pepper|14|False|...OH!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Was ist denn mit ihr los?
Shichimi|2|False|Pepper? Alles in Ordnung?
Pepper|3|True|Ja, mir geht's gut.
Pepper|4|False|Mir ist nur etwas klargeworden.
Pepper|5|False|Alles, was passiert ist, war direkt oder indirekt eine Auswirkung meiner Taten, meiner Entscheidungen...
Pepper|6|False|...in anderen Worten, meine Kettenreaktion!
Shichimi|7|True|Wirklich?
Shichimi|8|False|Dass musst du erklären.
Torreya|9|True|Genug geplaudert, wir befinden uns mitten in einem Schlachtfeld.
Torreya|10|True|Wir können darüber reden, sobald wir fliegen.
Torreya|11|False|Steig auf, Pepper!
Shichimi|12|True|Torreya hat recht.
Shichimi|13|False|Wir müssen nach Qualicity, egal was passiert.
Pepper|14|False|Wartet kurz.
Pepper|15|True|Wasabis Armee schlägt gerade zum Gegenangriff an.
Pepper|16|False|Wir können sie nicht einfach einander töten lassen.
Pepper|17|False|Ich fühle mich dafür verantwortlich, diesen Kampf zu beenden!

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Torreya|1|False|Aber wie?
Arra|2|True|Ja, wie genau willst du das tun, Hexe?
Arra|3|False|Du hast nicht viel Rea zurück-gewonnen, das kann ich spüren.
Pepper|4|True|Da hast du völlig Recht, aber einer meiner Zaubersprüche könnte alles wieder richten.
Pepper|5|False|Ich brauche nur ein wenig Rea von dir, um alle zu erreichen.
Arra|6|True|Energie einer Hexe geben?
Arra|7|True|Das ist verboten!
Arra|8|False|Auf keinen Fall!
Pepper|9|False|Willst du lieber dabei zusehen, wie ein Gemetzel entsteht?
Torreya|10|True|Bitte, Arra. Mach eine Ausnahme. Die Mädchen und die Drachen, die da kämpfen, das ist unsere Schule, unsere Familie.
Torreya|11|False|Und deine auch.
Shichimi|12|False|Ja, bitte, Arra.
Arra|13|True|PFF! Na gut!
Arra|14|False|Aber auf ihre Verantwortung!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|fschhhh!!
Pepper|2|False|WOOOOW!
Pepper|3|False|So fühlt sich Drachen-Rea also an!
Shichimi|4|False|Pepper, schnell! Die Schlacht!
Pepper|5|True|Allus...!
Pepper|6|True|Yuus...!
Pepper|7|True|Needum...!
Pepper|8|True|Est...
Pepper|9|False|...LOVIUS!
Geräusch|10|False|Tschiumm!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|5|True|Brizz!
Geräusch|4|True|Djiing!
Geräusch|3|True|Wusch!
Geräusch|2|True|Schii!
Geräusch|1|True|Schsss!
Geräusch|6|True|Brizz!
Geräusch|7|True|Djiing!
Geräusch|9|True|Schii!
Geräusch|8|True|Wuusch!
Geräusch|10|False|Schsss!
Pepper|11|False|Dieser Spruch war der erste Entwurf meiner Anti-Kriegs-Zauber.
Pepper|12|False|Er verwandelt bittere Feinde in Freunde...
Pepper|13|False|… und Gewalt in Liebe und Mitgefühl.
Shichimi|14|True|Wow!
Shichimi|15|True|Also... das ist klasse, Pepper!
Shichimi|16|False|Sie haben aufgehört zu kämpfen!
Torreya|17|False|Es funktioniert!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Huch, was ist denn das?
Shichimi|2|False|Manche von ihnen küssen sich...?!
Torreya|3|True|Ähm... Das sind ganz schön viele neue Paare!
Torreya|4|False|War das Absicht, Pepper?
Pepper|5|False|Oh, nein! Ich glaube, das Drachen-Rea hat die Liebe in meinem Zauber verstärkt.
Torreya|6|False|Haha, diese Schlacht geht direkt in die Geschichtsbücher ein.
Shichimi|7|True|Hee-hee,
Shichimi|8|False|definitiv!
Pepper|9|False|Ooohhh, wie peinlich!
Schrift|10|False|- ENDE -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|15. Dezember 2021 Illustration & Handlung: David Revoy. Beta-Leser: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini, Valvin. Deutsche Übersetzung: Ret Samys. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson Software: Krita 5.0β, Inkscape 1.1 auf Kubuntu Linux 20.04. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
Pepper|2|False|Wusstest du schon?
Pepper|3|False|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 1036 Gönnern!
Pepper|5|False|Du kannst auch Gönner von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|6|False|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|7|False|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|8|False|Dankeschön!
