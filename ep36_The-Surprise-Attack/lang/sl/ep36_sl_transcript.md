# Transcript of Pepper&Carrot Episode 36 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 36: Napad presenečenja

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vasabi|1|True|OPRAVIČILO?
Vasabi|2|False|Si padla na glavo?!
Vasabi|3|False|PRI PRIČI JO VRZITE V JEČO!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Fsšš …
Paprika|2|True|Ti vrag!
Paprika|3|False|Čisto brez moči sem!
Paprika|4|False|Blesavi čarobni zapor! Grrr!
Zvok|5|False|KLONG!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Kako sem lahko bila tako lahkoverna!
Šičimi|2|True|Šššššš, Paprika!
Šičimi|3|False|Tišje.
Paprika|4|False|Kdo je tam?!
Šičimi|5|True|Šššš! Ne tako glasno!
Šičimi|6|True|Pridi bližje.
Šičimi|7|False|Prišla sem te osvobodit.
Zvok|8|False|Djjjiii …
Paprika|9|False|Šičimi?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|False|Veš … Žal mi je za vse to …
Šičimi|2|False|Za najin spor.
Šičimi|3|True|Ni…
Šičimi|4|False|Nisem imela izbire.
Paprika|5|True|Ne skrbi, razumem.
Paprika|6|False|Hvala, da si prišla.
Šičimi|7|False|Tale čarobna ječa je res močna. Zate so se pošteno potrudili!
Paprika|8|False|Ha ha!
Šičimi|9|False|Tiho, sicer naju bodo slišali.
Podgana|10|True|SRK
Podgana|11|True|SRK
Podgana|12|False|SRK
Šičimi|13|True|Mimogrede …
Šičimi|14|True|Prišla sem tudi, ker so me po slovesnosti sprejeli v Vasabijin ožji krog.
Šičimi|15|False|Izvedela sem njihove načrte …

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|16|False|ŽVENK!
Zvok|18|True|KLUNG!
Zvok|19|True|PENG!
Zvok|20|True|KLONG!
Zvok|21|False|BONG!
Šičimi|1|False|Strašni so, Paprika.
Šičimi|2|False|Na kratko, Vasabi si hoče podrediti vse ostale šole magije …
Šičimi|3|False|Ob zori bo odrinila proti Kvaliteji z vojsko čarovnic …
Paprika|4|True|O, ne!
Paprika|5|True|Koriandrika!
Paprika|6|False|In njeno kraljestvo!
Šičimi|7|True|In zombijaška čarovnija!
Šičimi|8|False|Takoj jo morava posvariti!
Šičimi|9|False|Letalka in njen zmaj naju že čakata na vzletni ploščadi.
Šičimi|10|False|Tako, ključavnica se je končno vdala!
Zvok|11|False|Čing!
Paprika|12|True|Krasno!
Paprika|13|False|Samo še Korenčka in svoj klobuk vzamem.
Podgana|14|False|Cvil!
Korenček|15|False|Mrjav!
Podgana|17|False|Cviiil!
Paprika|22|False|!!!
Šičimi|23|False|!!!
Straža|24|True|STRAŽA!
Straža|25|False|TAKOJ SEM!
Straža|26|False|VSILJIVKA BO OSVOBODILA JETNICO!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|False|Skoraj bi nama uspelo …
Paprika|2|False|Da, skoraj.
Paprika|3|False|Mimogrede, ali slučajno veš, zakaj se me hoče Vasabi znebiti?
Šičimi|4|False|Boji se kaosaških čarovnic, Paprika.
Šičimi|5|True|Še posebej tvojih verižnih reakcij.
Šičimi|6|False|Verjame, da močno ogrožajo njene načrte.
Paprika|7|True|Aja, to! Pfff…
Paprika|8|False|Tega se ji pa res ni treba bati. Nikoli mi še ni uspelo nobene sprožiti.
Šičimi|9|False|Res ne?
Paprika|10|False|Da, res ne, ha ha ha!
Šičimi|11|True|Hi hi!
Šičimi|12|False|Preganjavica jo daje …

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|True|Častnik!
Kralj|2|False|Ali lahko potrdite, da tista čarovnica živi v tem svetišču?
Častnik|3|True|Prepričan sem, gospod!
Častnik|4|False|Naši vohuni so jo opazili tukaj.
Kralj|5|True|Grrrr…
Kralj|6|False|Tukaj torej živi zasmehovalka naših vojnih veščin in tradicije!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kralj|1|False|Proslaviva najino zavezništvo: maščujva se in izbrišiva to svetišče z obličja Hereve!
Zvok|3|False|Plosk
Sovražnik|2|False|Tako se govori!
Vojska|4|True|Ja!
Vojska|5|True|Ja!
Vojska|6|True|Ja!
Vojska|7|True|Ja!
Vojska|8|True|Hura!
Vojska|9|True|Juhu!
Vojska|10|True|Tako!
Vojska|11|True|Juhuuu!
Vojska|12|True|Jaaa!
Vojska|13|False|Hura!
Kralj|14|True|KATAPULTI!
Kralj|15|False|OGENJ!!!
Zvok|16|False|huUVUUUUUUUUU!
Zvok|17|True|Vuuuš!
Zvok|18|False|Vuuuš!
Kralj|19|False|V NAAPAAAAD!!!
Paprika|20|True|Kaj se dogaja?
Paprika|21|False|Napad?!
Zvok|22|True|BUMM!
Zvok|23|False|BUMMM!
Šičimi|24|False|Kaj?!
Zvok|25|True|BUU~UMMM!
Zvok|26|False|BUMM!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|kah
Paprika|2|False|kah!!
Paprika|3|False|Šičimi! Si v redu?
Šičimi|4|True|Da, dobro sem!
Šičimi|5|True|Pa ti?
Šičimi|6|False|In Korenček?
Paprika|7|False|V redu sva.
Paprika|8|True|Kaj, hudimana?
Paprika|9|False|Ne… morem … verjeti …
Šičimi|10|True|Od kje se je vzela ta vojska?!
Šičimi|11|True|Pa katapulti?!
Šičimi|12|False|Kaj sploh hočejo?
Paprika|13|True|Kdo bi vedel …
Paprika|14|False|Ampak tisti dve vojskovodji poznam.
Šičimi|15|False|?
Šičimi|16|False|!!!
Šičimi|17|False|Toreja, tukaj sva!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Toreja|1|True|Šičimi!
Toreja|2|False|Hvala duhovom, da si živa in zdrava.
Zvok|3|False|zgrab
Toreja|4|True|Hudo me je zaskrbelo, ko sem slišala, da so te zaprli!
Toreja|5|True|In ta spopad!
Toreja|6|False|Kakšen kaos!
Šičimi|7|False|Toreja, tako lepo te je videti.
Paprika|8|False|Sveta nebesa!
Paprika|9|False|Se pravi sta zmajska letalka in Šičimi v razmerju …
Paprika|10|False|Ne morem si predstavljati, kaj bi se zgodilo, če bi se je znebila v tisti jami.
Paprika|11|False|In ta vojska mi je nedvomno sledila sem.
Paprika|12|False|Brez njih zdaj ne bi bili svobodni.
Paprika|13|False|Kot da je vse to med seboj povezano …
Paprika|14|False|O!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Toreja|1|False|Kaj je z njo?
Šičimi|2|False|Paprika, je vse v redu?
Paprika|3|True|Da, v redu sem.
Paprika|4|False|Samo nekaj me je prešinilo.
Paprika|5|False|Vse, kar se je zgodilo, je posredno ali neposredno posledica mojih dejanj in odločitev …
Paprika|6|False|Oziroma moja prava verižna reakcija!
Šičimi|7|True|Kaj res?
Šičimi|8|False|To boš morala razložiti.
Toreja|9|True|Dovolj klepetanja. Sredi bojišča smo.
Toreja|10|True|O tem bomo govorili med poletom.
Toreja|11|False|Prisedi, Paprika!
Šičimi|12|True|Toreja ima prav.
Šičimi|13|False|Za vsako ceno moramo v Kvalitejo.
Paprika|14|False|Samo trenutek.
Paprika|15|True|Vasabijina vojska gre v protinapad.
Paprika|16|False|Ne moremo samo pustiti, da se pobijejo med seboj.
Paprika|17|False|Odgovorna sem za vse to. Bitko moram zaključiti.

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Toreja|1|False|Ampak kako?
Arra|2|True|Zares, čarovnica, kaj vendar kaniš?
Arra|3|False|Čutim, da nimaš dovolj rêsa.
Paprika|4|True|Popolnoma prav imaš, vendar poznam urok, ki lahko vse to uredi.
Paprika|5|False|Potrebujem le nekaj tvojega rêsa, da bo urok segel čez celo bojišče.
Arra|6|True|Izročiti energijo čarovnici?
Arra|7|True|To je prepovedano!
Arra|8|False|NIKOLI! NIKDAR!
Paprika|9|False|Bi raje dovolil, da pride do pokola?
Toreja|10|True|Prosim te, Arra. Naredi izjemo. Dekleta in zmaji, ki se tam borijo, so naša šola, naša družina.
Toreja|11|False|Tudi tvoja.
Šičimi|12|False|Tako je, Arra …
Arra|13|True|PFF! Naj bo!
Arra|14|False|Toda odgovornost bo na njenih plečih!

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|fsššššš!!
Paprika|2|False|VAUU!
Paprika|3|False|Takšen je torej občutek zmajskega rêsa!
Šičimi|4|False|Pepper, hitro! Bitka!
Paprika|5|True|Vsis!
Paprika|6|True|Kontrarus!
Paprika|7|True|Ses!
Paprika|8|True|Imejtae!
Paprika|9|False|RADIUS!
Zvok|10|False|Tšiumm!!

### P14

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|5|True|Brizz!
Zvok|4|True|Dziing!
Zvok|3|True|Vušš!
Zvok|2|True|Šii!
Zvok|1|True|Šsss!
Zvok|6|True|Brizz!
Zvok|7|True|Dziing!
Zvok|9|True|Šii!
Zvok|8|True|Vuušš!
Zvok|10|False|Šsss!
Paprika|11|False|Ta urok je bil moja prva iznajdba protivojne čarovnije.
Paprika|12|False|Zagrenjene sovražnike bo spremenil v prijatelje …
Paprika|13|False|… ter nasilje v ljubezen in sočutje.
Šičimi|14|True|Vau!
Šičimi|15|True|Pa saj to je krasno, Paprika!
Šičimi|16|False|Prenehali so se boriti!
Toreja|17|False|Deluje!

### P15

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|True|Uh, kaj je pa to?
Šičimi|2|False|Nekateri se poljubljajo …
Toreja|3|True|Khm. To je pa veliko novih parčkov.
Toreja|4|False|Je bilo to namerno, Paprika?
Paprika|5|False|O, ne! Mislim, da je zmajev rês ojačal ljubezen v mojem uroku.
Toreja|6|False|Ha ha, ta bitka se bo zapisala v zgodovino.
Šičimi|7|True|Hi hi,
Šičimi|8|False|zagotovo!
Paprika|9|False|Uuuhh, kakšna sramota!
Napis|10|False|- KONEC -

### P16

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|15. december 2021 Piše in riše: David Revoy. Testni bralci: Arlo James Barnes, Bhoren, Bobby Hiltz, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Karl Ove Hufthammer, Nicolas Artance, Pierre-Antoine Angelini in Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nicolas Artance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 5.0beta in Inkscape 1.1 na Kubuntu Linux 20.04 Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
Paprika|2|False|Že veš?
Paprika|3|False|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 1036 podpornikov!
Paprika|5|False|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|6|False|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|7|False|Obišči www.peppercarrot.com za več informacij!
Paprika|8|False|Hvalaaaa!
