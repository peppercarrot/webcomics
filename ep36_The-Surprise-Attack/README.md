# Episode 36: The Surprise Attack

![cover of episode 36](https://www.peppercarrot.com/0_sources/ep36_The-Surprise-Attack/low-res/Pepper-and-Carrot_by-David-Revoy_E36.jpg)

## Comments from the author

Hey! This is the longest episode of Pepper&Carrot, it's twice as long as normal!

It is also the first one designed for a dark theme. A real technical challenge designed to complete and conclude the book 4. This episode heavily refers to many past episodes. Especially since episode 32, "The Battlefield". I imagine that this number of references will confuse some readers and this episode will probably works better in the context of finishing reading the book. But maybe it will encourage to reread previous episodes?

In any case, it contains a lot of new elements, surprises and I confess, I really enjoyed writing it! I hope you'll feel that too while discovering it.

From [Author's blog of episode 36](https://www.davidrevoy.com/article886/episode-36-the-surprise-attack/show#comments)
