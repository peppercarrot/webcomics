# Transcript of Pepper&Carrot Episode 06 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 6: Konkurexey fe Iksir

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|True|Malxanse, mi le xorsomno har bukado janela, ripul...
Pilpil|2|True|... to sen daydenmo ventopul...
Pilpil|3|False|... ji mi abil na oko Komona pas janela keseba?
Pilpil|4|False|KOMONA!
Pilpil|5|False|Konkurexey fe Iksir!
Pilpil|6|False|Mi le dayible xorsomno volekal!
Pilpil|7|True|...mas?
Pilpil|8|False|Mi sen keloka?
Piu|10|False|-ke?|nowhitespace
Piu|9|True|kwa|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|!!!
Pilpil|2|False|Karote! Yu sen daydenmo lutufpul kos na trasporta mi cel Konkurexey!
Pilpil|3|False|O-ti-ma!
Pilpil|4|True|Yu le hata idey na tonporta iksir, misu labas, ji misu topi...
Pilpil|5|False|... nun, yu le tonporta ke iksir?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|KETO?!!
Xaherkef de Komona|3|False|Kom Xaherkef de Komona, mi deklara konkurexey fe iksir... bukado!
Xaherkef de Komona|4|False|Imisu xaher furaha na bonatagi no min kom care jumunyen cel hin unyum okur.
Xaherkef de Komona|5|True|Fe lutuf, am
Xaherkef de Komona|6|True|dayday
Eskrixey|2|False|Konkurexey fe Iksir de Komona
Xaherkef de Komona|7|False|afarin ete:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Juitim|29|False|A-fa-rin
Xaherkef de Komona|1|True|Of teli day Uncudo Dexa de Teknoyen, to sen xerafu, na bonatagi carmepul ji fatminfil
Xaherkef de Komona|3|True|... imi am no wanji imisu hinlokayen, Komona su jumunyen, seli
Xaherkef de Komona|5|True|... Imisu tigayum rolyen ata cel imi of dexa fe infracune candru,
Xaherkef de Komona|7|True|... ji fe fini, imisu finili rolyen, of drevolari Koda de Sinjabu,
Xaherkef de Komona|2|False|Koryandro!
Xaherkef de Komona|4|False|Safran!
Xaherkef de Komona|6|False|Xicimi!
Xaherkef de Komona|8|False|Pilpil!
Xaherkef de Komona|9|True|Konkurexey am xoru!
Xaherkef de Komona|10|False|Tupyo xa okur yon afarinje
Xaherkef de Komona|11|False|Unyum, onexa de Koryandro
Koryandro|13|False|... am no haji befobi morcu, fe xukra fe misu...
Koryandro|14|True|... Iksir fe
Koryandro|15|False|ZOMBIGI!
Juitim|16|True|A-fa-rin
Juitim|17|True|A-fa-rin
Juitim|18|True|A-fa-rin
Juitim|19|True|A-fa-rin
Juitim|20|True|A-fa-rin
Juitim|21|True|A-fa-rin
Juitim|22|True|A-fa-rin
Juitim|23|True|A-fa-rin
Juitim|24|True|A-fa-rin
Juitim|25|True|A-fa-rin
Juitim|26|True|A-fa-rin
Juitim|27|True|A-fa-rin
Juitim|28|True|A-fa-rin
Koryandro|12|False|Senyorlari...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Xaherkef de Komona|1|False|OTIMA!
Juitim|3|True|A-fa-rin
Juitim|4|True|A-fa-rin
Juitim|5|True|A-fa-rin
Juitim|6|True|A-fa-rin
Juitim|7|True|A-fa-rin
Juitim|8|True|A-fa-rin
Juitim|9|True|A-fa-rin
Juitim|10|True|A-fa-rin
Juitim|11|True|A-fa-rin
Juitim|12|True|A-fa-rin
Juitim|13|True|A-fa-rin
Juitim|14|True|A-fa-rin
Juitim|15|True|A-fa-rin
Juitim|16|False|A-fa-rin
Safran|18|True|Koski prehay
Safran|17|True|... mas fe lutuf, am hifazi yusu afarin, demo of Komona!
Safran|22|False|...ji hasedagi ete!
Safran|19|True|MISU
Safran|25|False|LUKSO!
Safran|24|True|... Iksir fe
Safran|23|False|... moy dento sen ible yon jandan aplika fe un solo katru fe misu...
Juitim|26|True|A-fa-rin
Juitim|27|True|A-fa-rin
Juitim|28|True|A-fa-rin
Juitim|29|True|A-fa-rin
Juitim|30|True|A-fa-rin
Juitim|31|True|A-fa-rin
Juitim|32|True|A-fa-rin
Juitim|33|True|A-fa-rin
Juitim|34|True|A-fa-rin
Juitim|35|True|A-fa-rin
Juitim|36|True|A-fa-rin
Juitim|37|True|A-fa-rin
Juitim|38|True|A-fa-rin
Juitim|39|True|A-fa-rin
Juitim|40|False|A-fa-rin
Xaherkef de Komona|42|False|Hin iksir ger abil na pesapulgi total Komona!
Xaherkef de Komona|41|True|Otima! Imanunenible!
Juitim|44|True|A-fa-rin
Juitim|45|True|A-fa-rin
Juitim|46|True|A-fa-rin
Juitim|47|True|A-fa-rin
Juitim|48|True|A-fa-rin
Juitim|49|True|A-fa-rin
Juitim|50|True|A-fa-rin
Juitim|51|True|A-fa-rin
Juitim|52|True|A-fa-rin
Juitim|53|True|A-fa-rin
Juitim|54|True|A-fa-rin
Juitim|55|True|A-fa-rin
Juitim|56|True|A-fa-rin
Juitim|57|True|A-fa-rin
Juitim|58|True|A-fa-rin
Juitim|59|True|A-fa-rin
Juitim|60|False|A-fa-rin
Xaherkef de Komona|2|False|Koryandro tyojan seli morcu yon hin mu-je-za-pul iksir!
Safran|21|True|Den iksir hu moy uyu dupul sati intizar da: dento hu da xa wao yusu jara...
Xaherkef de Komona|43|False|Yusu afarin no abil na sen mal. Koryandro le uje beawjeti.
Safran|20|False|iksir!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Xaherkef de Komona|1|False|Den jaleli onexa nun kwasisen katina tas Xicimi na xekaste!
Xicimi|4|True|NO!
Xicimi|5|True|Mi no abil, to sen godomo hataripul
Xicimi|6|False|ASIF!
Xaherkef de Komona|3|False|... fe lutuf, Xicimi, moyte intizar yu.
Xaherkef de Komona|7|False|To kwasisen, Senyorlari, ki Xicimi teslimu...
Safran|8|False|Am gibe dento!
Safran|9|False|... ji am esto na kwasigi na sen hajul, yu nun sungay xow.
Safran|10|False|Moyte uje jixi ki mi le triunfa konkurexey, nenhar muhimya fe ku yusu iksir fale keto...
Xicimi|11|False|!!!
Soti|12|False|ZZZYUUU
Xicimi|15|False|DAYDAY KAYVUTU!
Xicimi|2|False|Mi... Mi le no jixi ki imi xa musi na fale onexa.
Xicimi|13|True|JUI!!!
Xicimi|14|True|To sen iksir cel

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piu|1|False|KU-KU-Kuuuuuuuuuuuuuu
Soti|2|False|BAM!
Pilpil|3|True|... hmm, suprem!
Pilpil|5|False|... misu iksir xa minimum layeya yusu haha koski...
Pilpil|4|False|Fe folo, kam mi turno fe nunya?
Xaherkef de Komona|6|True|Am awpawbu, boboyen!
Xaherkef de Komona|7|False|Konkurexey sen finido! Am juyuen se!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|2|False|...fe xugwan, moyte awidi preciso denwatu hu imi turno.
Pilpil|1|True|Nun, prehay...
Pilpil|4|True|Minimum, mi hare idey tem ku imi xa abil na fale keto yon yusu "iksir", Karote.
Pilpil|5|False|... imi xa bon ruordenugi moyto hinloka, ji ruidi cel ogar!
Pilpil|7|True|Yu
Pilpil|8|False|gododay luksodo zombi-kanari!
Pilpil|10|False|Kam yu vole na jaribi un finili iksir? ...
Pilpil|11|False|...no sati, kam?
Pilpil|6|False|HEY!
Soti|9|False|KA-SI-RU!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|True|Si, am doxo label, juipul...
Pilpil|2|False|Mi xa no taradudu na lyudon total to per yu eger yu no fori awidi Komona!
Xaherkef de Komona|3|True|Koski te le juyuen imisu xaher denwatu hu to le sen in hatari,
Xaherkef de Komona|4|False|imi jayeza unyum ranko tas Pilpil kos tesu Iksir fe... ??!
Pilpil|7|False|em... fe fato, to no real sen iksir; to sen misu myaw su misalxey fe pixi of tesu jaleli visita cel hewan-dotor.
Pilpil|6|True|... Ha ha! sipul...
Pilpil|8|False|... he nil onexa, kam?...
Narrator|9|False|Mon 6: Konkurexey fe Iksir
Narrator|10|False|FIN
Eskrixey|5|False|50,000 Ko
Credits|11|False|March 2015 - Artwork and story by David Revoy - Translation by Hector Ortega

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the kind patronage of readers. For this episode, thank you to the 245 Patrons:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|You too can become a patron of Pepper&Carrot for the next episode:
Credits|7|False|Tools: This episode was 100% drawn with Free/Libre software Krita on Linux Mint
Credits|6|False|Open-source: all source files with layers and fonts, are available on the official site
Credits|5|False|License: Creative Commons Attribution You can modify, reshare, sell etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
