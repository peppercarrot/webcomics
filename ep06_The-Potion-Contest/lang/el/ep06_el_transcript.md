# Transcript of Pepper&Carrot Episode 06 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tίτλος|1|False|Επεισόδιο 6: Ο Διαγωνισμός Φίλτρου

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|Ωχ φτου... Κοιμήθηκα με ανοιχτό παράθυρο, πάλι...
Πιπεριά|2|True|είναι τόσο...
Πιπεριά|3|False|...και γιατί μπορώ να δω την πόλη Κόμονα από το παράθυρο;
Πιπεριά|4|False|ΚΟΜΟΝΑ!
Πιπεριά|5|True|Ο διαγωνισμός φίλτρου!
Πιπεριά|6|True|Μάλλον... Μάλλον παρακοιμήθηκα καταλάθως...
Πιπεριά|9|True|...αλλά;
Πιπεριά|10|False|Πού βρίσκομαι;!!
Πουλί|12|False|ΚουάΚ;!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|False|!!!
Πιπεριά|2|False|Καροτούλη!!! Πολύ σωστή η σκέψη σου να μας "πετάξεις" ως την Κόμονα!
Πιπεριά|3|False|Υ-πέ-ρο-χα!
Πιπεριά|4|True|Σκέφτηκες ακόμη και να φέρεις ένα φίλτρο, τα ρούχα μου και το κάπέλο μου!
Πιπεριά|5|False|...για να δούμε τι είδους φίλτρο έφερες...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|False|ΤΙΙ;;;
ο Δήμαρχος της Κομόνα|3|False|Ως Δήμαρχος της Κόμονα, ανακοινώνω την έναρξη του Διαγωνισμού Φίλτρου...
ο Δήμαρχος της Κομόνα|4|False|Η πόλη μας υποδέχεται θερμά όχι μια, ούτε δυό, αλλά ΤΕΣΣΕΡΕΙΣ μάγισσες για τον διαγωνισμό...
ο Δήμαρχος της Κομόνα|5|True|Παρακαλώ δώστε ένα
ο Δήμαρχος της Κομόνα|6|True|θερμό
Κείμενο|2|False|Διαγωνισμός Φίλτρου της Κόμονα
ο Δήμαρχος της Κομόνα|7|False|χειροκρότημα για τις:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ακροατήριο|29|False|Κλαπ
ο Δήμαρχος της Κομόνα|1|True|Από την ξακουστή Ένωση Τεχνολόγων, είναι τιμή μας να υποδεχθούμε τη μαγευτική και πανέξυπνη
ο Δήμαρχος της Κομόνα|3|True|χωρίς να ξεχνάμε την δική μας μάγισσα, γέννημα-θρέμα της Κόμονα, την
ο Δήμαρχος της Κομόνα|5|True|...Η τρίτη μας διαγωνιζώμενος έρχεται πέρα μακριά, από τις πεδιάδες των κοιμώμενων Φεγγαριών, η
ο Δήμαρχος της Κομόνα|7|True|...και τέλος, η τελευταία μας διαγωνιζόμενος, από το δάσος της Ουράς του Τρελλού Σκίουρου, η
ο Δήμαρχος της Κομόνα|2|False|Κολιάντρα!
ο Δήμαρχος της Κομόνα|4|False|Σάφρον!
ο Δήμαρχος της Κομόνα|6|False|Σίσσιμι!
ο Δήμαρχος της Κομόνα|8|False|Πιπεριά!
ο Δήμαρχος της Κομόνα|9|True|Ας αρχίσει ο διαγωνισμός!
ο Δήμαρχος της Κομόνα|10|False|Οι ψήφοι θα μετρηθούν με το χειροκρότημα.
ο Δήμαρχος της Κομόνα|11|False|Πρώτα, η επίδειξη της Κολιάντρα...
Κολιάντρα|13|False|...βάλτε τέλος στον θάνατο, χάρη στο...
Κολιάντρα|14|True|φίλτρο
Κολιάντρα|15|False|ΖΟΜΠΟΠΟΙΗΣΗΣ!!!
Ακροατήριο|16|True|Κλαπ
Ακροατήριο|17|True|Κλαπ
Ακροατήριο|18|True|Κλαπ
Ακροατήριο|19|True|Κλαπ
Ακροατήριο|20|True|Κλαπ
Ακροατήριο|21|True|Κλαπ
Ακροατήριο|22|True|Κλαπ
Ακροατήριο|23|True|Κλαπ
Ακροατήριο|24|True|Κλαπ
Ακροατήριο|25|True|Κλαπ
Ακροατήριο|26|True|Κλαπ
Ακροατήριο|27|True|Κλαπ
Ακροατήριο|28|True|Κλαπ
Κολιάντρα|12|False|Κυρίες και κύριοι...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|1|False|ΦΟΒΕΡΟ!!!
Ακροατήριο|3|True|Κλαπ
Ακροατήριο|4|True|Κλαπ
Ακροατήριο|5|True|Κλαπ
Ακροατήριο|6|True|Κλαπ
Ακροατήριο|7|True|Κλαπ
Ακροατήριο|8|True|Κλαπ
Ακροατήριο|9|True|Κλαπ
Ακροατήριο|10|True|Κλαπ
Ακροατήριο|11|True|Κλαπ
Ακροατήριο|12|True|Κλαπ
Ακροατήριο|13|True|Κλαπ
Ακροατήριο|14|True|Κλαπ
Ακροατήριο|15|True|Κλαπ
Ακροατήριο|16|False|Κλαπ
Σάφρον|18|True|Ιδού το
Σάφρον|17|True|...αλλά σας παρακαλώ, πολίτες της Κόμονα, φυλάξτε το χειροκρότημα σας...
Σάφρον|22|False|...και θα τους κάνει να ζηλέψουν!
Σάφρον|19|True|ΔΙΚΟ ΜΟΥ
Σάφρον|25|False|ΨΙΛΟΜΥΤΙΑΣ!!!
Σάφρον|24|True|...φίλτρο της
Σάφρον|23|False|...όλα αυτά γίνονται πραγματικότητα με μία μικρή σταγόνα από το...
Ακροατήριο|26|True|Κλαπ
Ακροατήριο|27|True|Κλαπ
Ακροατήριο|28|True|Κλαπ
Ακροατήριο|29|True|Κλαπ
Ακροατήριο|30|True|Κλαπ
Ακροατήριο|31|True|Κλαπ
Ακροατήριο|32|True|Κλαπ
Ακροατήριο|33|True|Κλαπ
Ακροατήριο|34|True|Κλαπ
Ακροατήριο|35|True|Κλαπ
Ακροατήριο|36|True|Κλαπ
Ακροατήριο|37|True|Κλαπ
Ακροατήριο|38|True|Κλαπ
Ακροατήριο|39|True|Κλαπ
Ακροατήριο|40|False|Κλαπ
ο Δήμαρχος της Κομόνα|42|False|Αυτό το φίλτρο θα μας κάνει όλους πλούσιους!
ο Δήμαρχος της Κομόνα|41|True|Φαντασιτκό! Υπέροχο!
Ακροατήριο|44|True|Κλαπ
Ακροατήριο|45|True|Κλαπ
Ακροατήριο|46|True|Κλαπ
Ακροατήριο|47|True|Κλαπ
Ακροατήριο|48|True|Κλαπ
Ακροατήριο|49|True|Κλαπ
Ακροατήριο|50|True|Κλαπ
Ακροατήριο|51|True|Κλαπ
Ακροατήριο|52|True|Κλαπ
Ακροατήριο|53|True|Κλαπ
Ακροατήριο|54|True|Κλαπ
Ακροατήριο|55|True|Κλαπ
Ακροατήριο|56|True|Κλαπ
Ακροατήριο|57|True|Κλαπ
Ακροατήριο|58|True|Κλαπ
Ακροατήριο|59|True|Κλαπ
Ακροατήριο|60|False|Κλαπ
ο Δήμαρχος της Κομόνα|2|False|Η Κολιάντρα κοροϊδεύει τον ίδιο τον θάνατο μέ αυτό το θαυ-μα-τουρ-γό φίλτρο!
Σάφρον|21|True|Το φίλτρο που ΟΛΟΙ σας περιμένατε: αυτό που θα τυφλώσει τους γείτονές σας...
ο Δήμαρχος της Κομόνα|43|False|Το χειροκρότημα σας δε λέει ψέματα. Η Κολιάντρα έχει ήδη αποκλισθεί.
Σάφρον|20|False|φιλτρο

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|1|False|Η τελευταία παρουσίαση είναι δύσκολο να ανατραπεί από την Σίσσιμι!
Σίσσιμι|4|True|ΟΧΙ!
Σίσσιμι|5|True|Δεν μπορώ, Είναι πολύ επικίνδυνο...
Σίσσιμι|6|False|ΣΥΓΝΩΜΗ!
ο Δήμαρχος της Κομόνα|3|False|...εμπρός Σίσσιμι όλοι περιμένουν με ανυπομονησία...
ο Δήμαρχος της Κομόνα|7|False|Απ' ό,τι φαίνεται, κυρίες και κύριοι, η Σίσσιμι αποσύρεται...
Σάφρον|8|False|Δώσ'το μου αυτό!
Σάφρον|9|False|...και σταμάτα να παριστάνεις την ντροπαλη, μας χαλάς το σόου...
Σάφρον|10|False|Όλοι ξέρουν πως εγώ έχω νικήσει, ό,τι και να κάνει το χαζό σου φίλτρο...
Σίσσιμι|11|False|!!!
Ήχος|12|False|ΓΖΟΟΥΟΥΥ
Σίσσιμι|15|False|ΤΕΡΑΤΟΠΟΙΗΣΗΣ!!!
Σίσσιμι|2|False|Δεν... Δεν ήξερα πως έπρεπε να δώσουμε παρουσίαση...
Σίσσιμι|13|True|ΠΡΟΣΕΧΤΕ!!!
Σίσσιμι|14|True|Είναι φίλτρο

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πουλί|1|False|κοΚΟΟΥ-ΚΟ-κο-ΚοοΥ
Ήχος|2|False|ΜΠΑΜ!
Πιπεριά|3|True|...ααα! Πολύ ωραίο...
Πιπεριά|5|False|το φίλτρο μου τουλάχιστον θα σας κάνει να γελάσετε, διότι...
Πιπεριά|4|False|σείρα μου τώρα, έτσι;
ο Δήμαρχος της Κομόνα|6|True|Τρέχα, μουρλάθηκες;
ο Δήμαρχος της Κομόνα|7|False|Ο διαγωνισμός τελείωσε! ...ο σώζων εαυτώ σωθήτω!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|2|False|όπως πάντα, όλοι φεύγουν όταν έρχεται η σειρά μας...
Πιπεριά|1|True|πάλι τα ίδια...
Πιπεριά|4|True|Τουλάχιστον έχω μια ιδέα με το τι μπορούμε να κάνουμε με το "φίλτρο" μας, Καροτούλη...
Πιπεριά|5|False|...θα σώσουμε τη μέρα και μετά πάμε σπίτι!
Πιπεριά|7|False|Υπερμεγέθες-Ψιλομύτικο-Ζόμπο-Καναρίνι!
Πιπεριά|9|False|Θες να δοκιμάσεις άλλο ένα φίλτρο;;;
Πιπεριά|10|False|...όχι ιδιαίτερα, ε;
Πιπεριά|6|False|ΈΙ ΕΣΥ!
Ήχος|8|False|ΚΡΑΚ!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|Έεετσι, διάβασε την ετικέτα προσεκτικά...
Πιπεριά|2|False|...Δεν θα διστάσω ούτε στιγμή να το ρίξω όλο πάνω σου, αν δεν φύγεις από την Κόμονα αυτή τη στιγμή!
ο Δήμαρχος της Κομόνα|3|True|...Και επειδή έσωσε την πόλη μας όταν βρισκόταν σε κίνδυνο...
ο Δήμαρχος της Κομόνα|4|False|Το πρώτο βραβείο πάει στην Πιπεριά για το φίλτρο...;
Πιπεριά|7|False|...λοιπόν... για την ακρίβεια, δεν είναι "ακριβώς" φίλτρο... είναι δείγμα ούρων της γάτας μου από την τελευταία επίσκεψή μας στον κτηνίατρο!
Πιπεριά|6|True|...Χαχα! Εμμ...
Πιπεριά|8|False|δεν θέλετε παρουσίαση, υποθέτω...;
Αφηγητής|9|False|Επεισόδιο 6 : Ο Διαγωνισμός Φίλτρου
Αφηγητής|10|False|ΤΕΛΟΣ
Κείμενο|5|False|50,000 Ko
Συντελεστές|11|False|Μάρτιος 2015 - Σκίτσο και Σενάριο από David Revoy - Μετάφραση από Γεώργιο Καρέττα

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Συντελεστές|1|False|Η Πιπεριά και Καροτούλης είναι ένα εντελώς δωρεάν και ανοικτού κώδικα κόμικ, που υποστηρίζεται με χορηγίες. Αυτό το επεισόδιο χρηματοδοτήθηκε από 245 χορηγούς:
Συντελεστές|4|False|https://www.patreon.com/davidrevoy
Συντελεστές|3|False|Και εσύ μπορείς να γίνεις χορηγός για το επόμενο επεισόδιο:
Συντελεστές|7|False|Εργαλεία: Το κόμικ δημιουργήθηκε με 100% δωρεάν λογισμικό, όπως το Krita σε Linux Mint
Συντελεστές|6|False|Ακοικτού Κώδικα: Όλα τα εργαλεία βρίσκονται στο site
Συντελεστές|5|False|Άδεια : Creative Commons Attribution μπορείτε να κάνετε αλλαγές, να το επανεκδώσετε, να πουλήσετε προϊόντα σχετικά με το κόμικ κλπ...
Συντελεστές|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
