# Transcript of Pepper&Carrot Episode 06 [ga]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 6 : Lo concors de potinga

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Doblevant, que'm hiquèi au lheit shens barrar la frinèsta...
Pepper|2|True|...totun quin vent !
Pepper|3|False|...e perqué vei a Komonà per la frinèsta?
Pepper|4|False|KOMONÀ !
Pepper|5|False|Lo concors de Potinga !
Pepper|6|True|Que... Que m'adromii shens ac voler !
Pepper|9|True|…mes ?
Pepper|10|False|On soi adara ?!?
Ausèth|12|False|aC ?|nowhitespace
Ausèth|11|True|co|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot ! B'ès berogin d'aver pensat a'm har seguir au concors !
Pepper|3|False|Ma-gni-fic !
Pepper|4|True|Qu'as medish pensat a préner ua potinga, los mens vestissis, e lo men capèu...
Pepper|5|False|….Vam véder la potinga qui as pres...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|QUÉ ?!!
Cònsol de Komona|3|False|Jo, Maire de Komonà que declari lo concors de potinga ubèrt!
Cònsol de Komona|4|False|La nosta vila qu'ei hèra contenta d'arcuélher au mensh quate broishas tad aquera estrea.
Cònsol de Komona|5|True|Mercés de trucar de mans
Cònsol de Komona|6|False|plan hòrt :
Escritura|2|False|Concors de Potinga de Komonà

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audiéncia|29|False|Clap
Cònsol de Komona|1|True|Vienguda deu gran país de l'Union deus Tecnologistas, qu'ei un aunor d'arcuélher l'encantaira e ingeniosa
Cònsol de Komona|3|True|...shens desbrombar la hilha deu país, la broisha hilha de Komonà
Cònsol de Komona|5|True|... la nosta tresau participanta que ns'arriba deu país de las luas cornudas
Cònsol de Komona|7|True|...a la fin finala, la nosta darrèra participanta que ns'arriba deu bòsc deu Cap de l'Esquiròu
Cònsol de Komona|2|False|Coriandre !
Cònsol de Komona|4|False|Safran !
Cònsol de Komona|6|False|Shichimi !
Cònsol de Komona|8|False|Pepper !
Cònsol de Komona|9|True|Que lo concors comence !
Cònsol de Komona|10|False|Lo vòte que's harà a l'aplaudimètre !
Cònsol de Komona|11|False|E tot prumèr, plaça a la demostracion de Coriandre !
Coriandre|12|False|Daunas e Mossurs...
Coriandre|13|True|...Ne hè pas besonh d'aver paur a la mort gràcias a...
Coriandre|14|True|…la mea Potinga de
Coriandre|15|False|ZOMBIFICACION !
Audiéncia|16|True|Clap
Audiéncia|17|True|Clap
Audiéncia|18|True|Clap
Audiéncia|19|True|Clap
Audiéncia|20|True|Clap
Audiéncia|21|True|Clap
Audiéncia|22|True|Clap
Audiéncia|23|True|Clap
Audiéncia|24|True|Clap
Audiéncia|25|True|Clap
Audiéncia|26|True|Clap
Audiéncia|27|True|Clap
Audiéncia|28|True|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|FANTASTIC!
Cònsol de Komona|2|False|Clap
Audiéncia|4|True|Clap
Audiéncia|5|True|Clap
Audiéncia|6|True|Clap
Audiéncia|7|True|Clap
Audiéncia|8|True|Clap
Audiéncia|9|True|Clap
Audiéncia|10|True|Clap
Audiéncia|11|True|Clap
Audiéncia|12|True|Clap
Audiéncia|13|True|Clap
Audiéncia|14|True|Clap
Audiéncia|15|True|Clap
Audiéncia|16|True|Clap
Audiéncia|17|False|ací qu'avetz
Safran|19|True|...mes totun, guardatz los vòstes aplaudiments, pòple de Komonà !
Safran|18|False|La vertadèra potinga qui atendetz tots : la qui pòt impressionar tots los vesins vòstes...
Safran|22|True|...e que'n sian gelós !
Safran|23|False|MEA
Safran|20|True|BORGESIA !
Safran|26|False|…potinga de
Safran|25|True|...açò qu'ei d'ara enlà possible per l'aplicacion d'ua simpla gota de la mea...
Safran|24|True|Clap
Audiéncia|28|True|Clap
Audiéncia|29|True|Clap
Audiéncia|30|True|Clap
Audiéncia|31|True|Clap
Audiéncia|32|True|Clap
Audiéncia|33|True|Clap
Audiéncia|34|True|Clap
Audiéncia|35|True|Clap
Audiéncia|36|True|Clap
Audiéncia|37|True|Clap
Audiéncia|38|True|Clap
Audiéncia|39|True|Clap
Audiéncia|40|True|Clap
Audiéncia|42|False|Clap
Audiéncia|41|True|aquela pocion poiriá rendre tot Komona ric !
Cònsol de Komona|44|False|Fantastic ! Incredible !
Cònsol de Komona|43|True|Clap
Audiéncia|46|True|Clap
Audiéncia|47|True|Clap
Audiéncia|48|True|Clap
Audiéncia|49|True|Clap
Audiéncia|50|True|Clap
Audiéncia|51|True|Clap
Audiéncia|52|True|Clap
Audiéncia|53|True|Clap
Audiéncia|54|True|Clap
Audiéncia|55|True|Clap
Audiéncia|56|True|Clap
Audiéncia|57|True|Clap
Audiéncia|58|True|Clap
Audiéncia|59|True|Clap
Audiéncia|60|False|Clap
Audiéncia|3|True|Clap
Audiéncia|27|True|Los vòstes aplaudiments ne s'i enganan pas ; Coriandre qu'ei dejà eliminada.
Cònsol de Komona|45|False|la potinga
Safran|21|False|Coriandre que desfida la mort era medisha dab aquera potinga mi-ra-clo-sa !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|Adara, que va estar dur de har rampèu per Shichimi !
Shichimi|4|True|NON !
Shichimi|5|True|N'ac poish pas, tròp dangerós.
Shichimi|6|False|PERDON !
Cònsol de Komona|3|False|…anem Shichimi, lo monde que vs'atend.
Cònsol de Komona|7|False|Que semblaré, Daunas e Mossurs, que Shichimi e's declarèsse defautanta.
Safran|8|False|Balha'm aquò !
Safran|9|False|…e de mei dèisha de har la vergonhosa e non guastes pas l'espectacle.
Safran|10|False|Lo monde que saben dejà qu'èi ganhat lo concors lavetz enqüèra que la potinga toa e hasosse…
Shichimi|11|False|!!!
Son|12|False|BZZZIIOO|nowhitespace
Shichimi|15|False|MONSTRE GIGANT !
Shichimi|2|False|Que... non sabí pas que'm calè har ua demostracion.
Shichimi|13|True|Avisa-te !!!
Shichimi|14|True|Qu'ei ua potinga de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ausèth|1|False|CRiCRiCRiiiiiiiiiiiiiiiiiiiiii|nowhitespace
Son|2|False|BAM !
Pepper|3|True|…hèra plan !
Pepper|5|False|…la mea potinga que deveré au mensh aver lo meriti de'vs har arríder per'mor…
Pepper|4|False|Qu'ei donc lo men torn, adara ?
Cònsol de Komona|6|True|Huist, pèga !
Cònsol de Komona|7|False|lo concors qu'ei fenit ! ... sauva't la vita !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|…com a l'acostumat lo monde que se'n van quan ei lo noste torn.
Pepper|1|True|E atau :
Pepper|4|True|En tot cap qu'imagini çò qui vam poder har dab la toa « potinga », Carrot
Pepper|5|False|…hèm drin d'agèrc ací e tornem tà casa !
Pepper|7|True|Tu lo
Pepper|8|False|Piu-piu-gigant-zòmbi-borgés !
Pepper|10|False|E't plaseré d'esprovar la darrèra potinga ?
Pepper|11|False|…pas de rèsta, qué ?
Pepper|6|False|Hòu !
Son|9|False|CRAC !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Quiòc, plan que la me lei l'etiqueta...
Pepper|2|False|…Non premerí a te'n barrejar dessús si ne't tiras pas deu puishèu de tira de Komonà !
Cònsol de Komona|3|True|Per'mor qu'a sauvat la nosta vila d'un desastre,
Cònsol de Komona|4|False|que remetem la prumèra plaça a Pepper per la soa Potinga de… ??!!
Pepper|7|False|…Ee... totun, n'ei pas ua vertadèra potinga ; que son los escantilhs de pish deu men gat tà la soa visita medicau !
Pepper|6|True|…Ah ! òc…
Pepper|8|False|…pas de demò qué ?…
Narrator|9|False|Episòdi 6 : Lo concors de potinga
Narrator|10|False|FIN
Escritura|5|False|50 000 Ko
Crèdits|11|False|Març de 2015 - Dessenh e Scenari : David Revoy , correccions : Aurélien Gâteau

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot qu'ei completament liure, open source, e esponsorizat mercés au mecenat deus lectors. Entad aqueste episòdi, mercé aus 245 mecènas :
Crèdits|4|False|https://www.patreon.com/davidrevoy
Crèdits|3|False|Vos tanben, vadetz mecèna de Pepper&Carrot entà l'episòdi vienent :
Crèdits|7|False|Utís : Aqueste episòdi qu'estó dessenhat a 100% dab logiciaus liures Krita sus Linux Mint
Crèdits|6|False|Open source : totas las sorsas, polissas d'escrituras, fichèrs dab calques que son disponibles suu site oficiau au telecargament.
Crèdits|5|False|Licéncia : Creative Commons Attribution que podetz modificar, tornar partatar, véner, etc...
Crèdits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
