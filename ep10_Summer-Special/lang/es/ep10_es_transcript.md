# Transcript of Pepper&Carrot Episode 10 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 10: Especial de verano

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|- FIN -
Créditos|2|False|Agosto 2015 - Dibujo y Guión: David Revoy - Traducción: TheFaico

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto y patrocinado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 422 mecenas
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|2|True|Tú también puedes ser mecenas de Pepper&Carrot para el próximo episodio:
Créditos|4|False|Licencia: Creative Commons Attribution Ficheros originales disponibles en www.peppercarrot.com Herramientas: Este episodio ha sido creado al 100% con software libre Krita 2.9.6, Inkscape 0.91 en Linux Mint 17
