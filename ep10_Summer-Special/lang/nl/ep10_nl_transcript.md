# Transcript of Pepper&Carrot Episode 10 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 10: Zomerspecial

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|- EINDE -
Aftiteling|2|False|08/2015 - Tekeningen & verhaal: David Revoy - Vertaling: Willem Sonke

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 422 patronen:
Aftiteling|3|False|https://www.patreon.com/davidrevoy
Aftiteling|2|True|Voor de volgende aflevering kun jij ook een patroon van Pepper&Carrot worden:
Aftiteling|4|False|Licentie: Creative Commons Naamsvermelding 4.0 Bronbestanden: beschikbaar op www.peppercarrot.com Software: deze aflevering is 100% gemaakt met vrije software Krita 2.9.6, Inkscape 0.91 op Linux Mint 17
