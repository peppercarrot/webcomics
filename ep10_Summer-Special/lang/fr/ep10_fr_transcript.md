# Transcript of Pepper&Carrot Episode 10 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 10 : Spécial été

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|- FIN -
Crédits|2|False|08/2015 - Dessin & Scénario : David Revoy

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 422 Mécènes :
Crédits|3|False|https://www.patreon.com/davidrevoy
Crédits|2|True|Vous aussi, devenez mécène de Pepper&Carrot pour le prochain épisode sur
Crédits|4|False|Licence : Creative Commons Attribution 4.0 Sources : disponibles sur www.peppercarrot.com Logiciels : cet épisode a été dessiné à 100% avec des logiciels libres Krita 2.9.6, Inkscape 0.91 sur Linux Mint 17
