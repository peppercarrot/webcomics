# Transcript of Pepper&Carrot Episode 11 [ls]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 11: Las Brujas de Chaosah

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pepper, avergüenzas a Chaosah.
Thyme|2|False|Cayenne tiene razón, una bruja de Chaosah, digna de llamarse así debe infundir miedo, obediencia y respeto.
Cumin|3|False|... mientras que tú, tú ofreces té y pasteles, incluso a nuestros demonios*...
Nota|4|False|*Ver episodio 8: La fiesta de cumpleaños de Pepper.
Pepper|5|True|Pero...
Pepper|6|False|...madrinas...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|4|False|¡ SILENCIO !
Cumin|9|False|¡Ta-tán!
Thyme|1|True|Pepper, eres realmente talentosa, pero también eres nuestra única sucesora.
Thyme|2|False|Es nuestro deber hacer de ti una verdadera, bruja malvada de Chaosah.
Pepper|3|False|Pero... ¡No quiero ser malvada! Es... Es lo contrario a tod...
Cayenne|5|True|... ¡O te quitaremos todos tus poderes!
Cayenne|6|False|Y volverás a ser, una vez más, la pequeña huérfana idiota del bajo Squirrel.
Thyme|7|False|Cumin te acompañará de ahora en adelante, y te ayudará a entrenar y nos mantendrá al tanto de tu progreso.
Sonido|8|False|Poof!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|El nuevo Rey de Acren es demasiado amable y gentil con sus súbditos. El pueblo debe de temer a su Rey.
Cumin|2|False|Tu primera misión es encontrar e intimidar al monarca para manipularlo.
Cumin|3|False|¡Una verdadera bruja de Chaosah tiene que influenciar a los que tienen el poder!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|¿El Rey? ...
Pepper|2|False|... ¡¿ tan joven ?!
Pepper|3|False|Debemos tener la misma edad ...
Cumin|4|False|¡¿Qué estás esperando?! ¡Dale! ¡Despiértalo, amenázalo, asústalo!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|Dzzz
Pepper|2|False|Tú y yo nos parecemos ...
Pepper|3|True|joven ...
Pepper|4|True|solitario ...
Pepper|5|False|... prisioneros de nuestro destino.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thyme|6|False|Tenemos mucho trabajo por delante Pepper...
Créditos|8|False|09/2015 - Art & Scenario: David Revoy - Translation: Alex Gryson
Narrador|7|False|- FIN -
Sonido|1|False|Poof!
Pepper|2|False|?!
Sonido|3|False|Floof !
Thyme|4|True|Primera prueba:
Thyme|5|True|¡UN FRACASO TOTAL!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito (libre), de código abierto y patrocinado gracias al apoyo de sus lectores. Por este episodio, gracias a los 502 Patrons:
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|2|True|Tú también puedes convertirte en Patron de Pepper&Carrot para el próximo episodio en
Créditos|4|False|Licencia : Creative Commons Attribution 4.0 Fuente: available at www.peppercarrot.com Software : Este episodio ha sido creado 100% con un software libre. Krita 2.9.7, G'MIC 1.6.5.2, Inkscape 0.91 on Linux Mint 17
