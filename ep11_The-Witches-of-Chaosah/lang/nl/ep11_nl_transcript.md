# Transcript of Pepper&Carrot Episode 11 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 11: De heksen van Chaosah

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pepper, je bent een schande voor Chaosah.
Thym|2|False|Cayenne heeft gelijk, een heks van Chaosah die haar titel waard is moet nederigheid, angst en respect afdwingen.
Komijn|3|False|... terwijl jij koekjes en cakejes zit te eten, zelfs met onze demonen*...
Notitie|4|False|* Zie aflevering 8: Peppers verjaardagsfeest
Pepper|5|True|Maar...
Pepper|6|False|... peettantes...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|4|False|STILTE!
Komijn|9|False|Tadaaa!
Thym|1|True|Pepper, je hebt absoluut talent, maar je bent ook onze enige opvolger.
Thym|2|False|Het is onze plicht om van jou een echte, gemene heks van Chaosah te maken.
Pepper|3|False|Maar... Ik wil helemaal niet gemeen zijn! Dat is nou eenmaal niet mijn kara...
Cayenne|5|True|... of we ontnemen je al je krachten!
Cayenne|6|False|En dan ben je weer die kleine, sullige wees uit het Eikhoornwoud.
Thym|7|False|Cumin zal je schaduwen om te helpen je te trainen en om ons op de hoogte te houden van je voortgang.
Geluid|8|False|Poef!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komijn|1|False|De nieuwe Koning van Acren is te lief en aardig voor zijn onderdanen. De mensen moeten hun Koning vrezen.
Komijn|2|False|Je eerste missie is hem te vinden en te intimideren, zodat we hem kunnen manipuleren.
Komijn|3|False|Een echte heks van Chaosah heeft invloed op de machtigen!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|De Koning...
Pepper|2|False|... zo jong?!
Pepper|3|False|We zijn ongeveer even oud...
Komijn|4|False|Waar wacht je op?! Kom op! Maak hem wakker, laat hem schrikken, maak hem bang!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Dzzz
Pepper|2|False|Jij en ik, we lijken wel wat op elkaar...
Pepper|3|True|jong...
Pepper|4|True|eenzaam...
Pepper|5|False|... gevangen in ons lot.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thym|6|False|We hebben nog een hoop werk te doen, Pepper...
Aftiteling|8|False|09/2015 - Tekeningen & verhaal: David Revoy - Vertaling: Willem Sonke
Verteller|7|False|- EINDE -
Geluid|1|False|Poef!
Pepper|2|False|?!
Geluid|3|False|Pffft!
Thym|4|True|Eerste test:
Thym|5|True|VOLLEDIG MISLUKT!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 502 patronen:
Aftiteling|3|False|https://www.patreon.com/davidrevoy
Aftiteling|2|True|Voor de volgende aflevering kun jij ook een patroon van Pepper&Carrot worden:
Aftiteling|4|False|Licentie: Creative Commons Naamsvermelding 4.0 Bronbestanden: beschikbaar op www.peppercarrot.com Software: deze aflevering is 100% gemaakt met vrije software Krita 2.9.7, G'MIC 1.6.5.2, Inkscape 0.91 op Linux Mint 17
