# Transcript of Pepper&Carrot Episode 11 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 11: Kaosaške čarovnice

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|1|False|Paprika, ti si sramota za Kaosah.
Timijana|2|False|Kajenka ima prav. Vsaka poštena kaosaška čarovnica vzbuja v drugih strahospoštovanje in ponižnost.
Kumina|3|False|Ti pa medtem prirejaš čajanke našim demonom* …
Opomba|4|False|* glej epizodo 8: Paprikina rojstnodnevna zabava
Paprika|5|True|Ampak …
Paprika|6|False|Botre …

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|4|False|MOLČI!
Kumina|9|False|Ta-daaa!
Timijana|1|True|Paprika, saj si nadarjena, vendar si tudi naša edina naslednica.
Timijana|2|False|Naša dolžnost je, da iz tebe napravimo presneto žlehtno kaosaško čarovnico.
Paprika|3|False|Ampak nočem biti žlehtna! To je čisto v nasprotju z moj…
Kajenka|5|True|Ali pa ti bomo odvzele moči!
Kajenka|6|False|Potem boš spet samo trapasta mala sirota iz Veveričjega kota.
Timijana|7|False|Kumina te bo od zdaj naprej spremljala, te učila in nama poročala o tvojem napredku.
Zvok|8|False|Puf!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kumina|1|False|Novi kralj Akrene s svojimi podaniki ravna čisto prelepo. Ljudstvo bi se moralo svojega kralja bati.
Kumina|2|False|Tvoja prva naloga je kralja ustrahovati in si ga podrediti.
Kumina|3|False|Prava kaosaška čarovnica ima oblast nad vplivnimi!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Kralj?
Paprika|2|False|Pa tako mlad?!
Paprika|3|False|Gotovo sva istih let …
Kumina|4|False|Kaj zdaj cincaš? Dajmo! Zbudi ga, ustrahuj ga, prestraši ga!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|ZAP
Paprika|2|False|Veliko imava skupnega …
Paprika|3|True|Mlada …
Paprika|4|True|Osamljena …
Paprika|5|False|Jetnika svojih usod.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timijana|6|False|S tabo bo še kup dela, Paprika …
Zasluge|8|False|September 2015 - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Pripovedovalec|7|False|- KONEC -
Zvok|1|False|Puf!
Paprika|2|False|?!
Zvok|3|False|Fluf!
Timijana|4|True|Prvi preizkus:
Timijana|5|True|POPOLNA POLOMIJA!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek sta podprla 502 bralca:
Zasluge|3|False|https://www.patreon.com/davidrevoy
Zasluge|2|True|Tudi ti lahko postaneš denarni/a podpornik/ca za naslednjo epizodo stripa:
Zasluge|4|False|Licenca: Creative Commons Priznanje avtorstva 4.0. Izvorne datoteke na voljo na www.peppercarrot.com Programska oprema: ta epizoda je nastala s prostim programjem Krita 2.9.7, G'MIC 1.6.5.2 in Inkscape 0.91 na Linux Mint 17
