# Transcript of Pepper&Carrot Episode 11 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 11: Jumunyen de Kaosa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kayena|1|False|Pilpil, yu preporta xyuci cel Kaosa.
Thyme|2|False|Kayena sahiloga, jumunyen of Kaosa hu da sen laye fe titulo musi na ilhamu fobi, etatu ji ehtiram.
Zira|3|False|... Fe kompara fe yu, yu teyan cay ji lilkeke, hata tas imisu demoni*...
Note|4|False|* Am oko Mon 8: Parti fe Xencudin de Pilpil
Pilpil|5|True|Mas...
Pilpil|6|False|...batiza-matre...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kayena|4|False|SOTIKALYA!
Zira|9|False|Pre-cu!
Thyme|1|True|Pilpil, yu yakin sen talentopul, mas yu sen pia imisu sol xayen.
Thyme|2|False|To sen imisu musiya, na sengi yu sati, lutufkal jumunyen of Kaosa.
Pilpil|3|False|Mas... mi no vole na sen lutufkal! To ...to sen antipul fe moyto...
Kayena|5|True|... fe alo kaso, imi xa ruvoka yusu moy kowa!
Kayena|6|False|Ji yu xa ripul sencu lil bobo yatima of Koda de Sinjabu.
Thyme|7|False|Zira xa tonidi yu xorfe nunya cel na sahay na treyna yu, ji te xa reporti tas imi yusu fronkadam.
Soti|8|False|Puf!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zira|1|False|Neo Wangu de Akren sen godomo lutufpul ji narem tas sesu demoyen. Demo musi na befobi sesu Wangu.
Zira|2|False|Yusu unyum misyon sen na ewreka ji yonfobi Wangu cel na maxmo bon manipula te.
Zira|3|False|Sati jumunyen of Kaosa hare efeto supra kowapul ete.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|True|Wangu?...
Pilpil|2|False|...daydenmo juni?!
Pilpil|3|False|Imi xankakal sen fe sama laoje...
Zira|4|False|Yu intizar keto?! Am duregi! Am jagegi te, am yonfobi te, am fobi te!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Soti|1|False|Dzzz
Pilpil|2|False|Yu ji mi, imi sen lilmo sim...
Pilpil|3|True|juni...
Pilpil|4|True|solo...
Pilpil|5|False|...poshuruyen de imisu kismetu.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thyme|6|False|Imi hare multi falebisu ergo, Pilpil...
Credits|8|False|09/2015 - Art & Scenario: David Revoy - Translation: Hector Ortega
Narrator|7|False|- FIN -
Soti|1|False|Puf!
Pilpil|2|False|?!
Soti|3|False|Fluf!
Thyme|4|True|Unyum eksame:
Thyme|5|True|TOTAL POSSUKSES!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 502 Patrons:
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|2|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|4|False|License: Creative Commons Attribution 4.0 Source: available at www.peppercarrot.com Software: this episode was 100% drawn with libre software Krita 2.9.7, G'MIC 1.6.5.2, Inkscape 0.91 on Linux Mint 17
