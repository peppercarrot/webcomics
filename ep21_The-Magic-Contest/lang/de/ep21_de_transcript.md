# Transcript of Pepper&Carrot Episode 21 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 21: Der Zauberwettbewerb

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Schrift|1|False|Aufgrund der großen Nachfrage präsentiert die Stadt Komona ihren Zauberwettbewerb
Schrift|2|False|Hauptpreis: 50 000Ko
Schrift|3|False|Für die eindrucksvollste Zauberpräsentation Eintritt: 150Ko Riesige, speziell gesicherte Area Komona Azarday, 10 AirMoon
Schrift|4|False|EXKLUSIVE EINLADUNG
Pepper|5|False|Ich bin soweit! Komm, Carrot, los geht's!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Und du willst wirklich nicht mitkommen?
Cayenne|2|False|Ganz bestimmt nicht. Chaosāh steht für Zauberei, nicht für Zirkusspektakel!
Schrift|3|False|KOMONA-Anzeiger
Schrift|4|False|NEUE ARENA WIRD HEUTE FEIERLICH ERÖFFNET
Schrift|5|False|BÜRGER BESORGT WEGEN STEUERBELASTUNG
Schrift|6|False|KARTEN AUSVERKAUFT: ERFOLG GARANTIERT!
Schrift|7|False|POLITISCHE SPANNUNGEN IN AH
Schrift|8|False|AH BESETZT DERZEIT 80% DER GEBIETE
Schrift|9|False|IHR SEHVERMÖGEN IST EXZELLENT!
Pepper|10|True|Danke, dass ich mitmachen darf!
Pepper|11|True|Ich gewinne, versprochen!
Pepper|12|False|Bis heute Abend!
Cayenne|14|False|…
Geräusch|13|False|rums!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|2|False|klatsch
Bürgermeister von Komona|1|False|Kraft meines Amtes als Bürgermeister von Komona erkläre ich den Zweiten Großen Zauberwettbewerb für eröffnet!
Bürgermeister von Komona|3|True|Ich weiß, wie gespannt Sie diese Darbietung erwarten!
Bürgermeister von Komona|4|False|Aus allen vier Ecken von Hereva sind Sie angereist, um unsere neue, ultra-sichere Arena zu erleben – herzlichen Dank dafür!
Bürgermeister von Komona|5|False|Aber jetzt zu unseren Teilnehmerinnen!
Carrot|6|True|Z
Carrot|7|True|Z|nowhitespace
Carrot|8|False|Z ...|nowhitespace
Pepper|9|True|Carrot?! Was fällt Dir ein?!
Pepper|10|True|Wach auf! Es geht los!
Pepper|11|False|Alle schauen uns an!
Geräusch|12|False|stups

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|3|False|klatsch
Bürgermeister von Komona|1|True|Wir fühlen uns geehrt, die Adeptin des Wiederbelebungs-Zaubers aus „Zombiah“ zu begrüßen,
Bürgermeister von Komona|2|False|CORIANDER!
Bürgermeister von Komona|4|True|Kein Geheimnis aus Fauna und Flora bleibt dieser Hexe aus „Hippiāh“ verborgen … bitte begrüßen Sie
Bürgermeister von Komona|5|False|KAMILLE!
Bürgermeister von Komona|6|False|Und nun, dank der technischen Kompetenz von Komona, in einer weltweit einzigartigen Darbietung …
Geräusch|7|False|rums!
Bürgermeister von Komona|8|True|… eine Hexe aus wasserreichen Tiefen, welche die eindrucksvolle Magie von „Aquah" beherrscht!
Bürgermeister von Komona|9|False|SPIRULINA!
Pepper|10|False|Aquah?! Hier?! Krass.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|9|False|klatsch
Carrot|1|False|Knurr
Pepper|2|True|Carrot!
Pepper|3|True|Wir sind gleich dran!
Pepper|4|True|Gedulde dich!
Pepper|5|True|Bitte!
Pepper|6|False|Nur heute!
Bürgermeister von Komona|7|True|Freue ich mich, die Hexe der spirituellen und asketischen Magie von „Ah“ bei uns zu begrüßen
Bürgermeister von Komona|8|False|SHICHIMI!
Pepper|10|False|Carrot! NEIN!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|7|False|Ha!
Bürgermeister von Komona|1|False|Und nun kommen wir zu der geheimnisvollen Teilnehmerin, die sich anonym angemeldet hat, für eine Darbietung der extravaganten Magie von „Magmah“ …
Bürgermeister von Komona|3|True|OH! Welch Überraschung! Ja, sie ist es! Die unvergleichliche …
Bürgermeister von Komona|4|False|SAFFRON!
Geräusch|2|False|Klapp!
Bürgermeister von Komona|6|False|Ah, eine Teilnehmerin hat anscheinend schon ein Problem!
Geräusch|5|False|Platsch!
Bürgermeister von Komona|8|False|Junge Dame! Bitte begeben Sie sich wieder auf ihren Platz!
Pepper|9|False|Caaarrott! NEIN!
Geräusch|10|False|Bzing!
Pepper|11|False|?!
Geräusch|12|False|Flatsch!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ha-ha! Danke, Spirulina! 'tschuldigung …
Pepper|2|True|Wo willst du hin!
Pepper|3|False|KOMM SOFORT HER!
Bürgermeister von Komona|4|True|Ähem! Und hier ist die letzte Teilnehmerin, mit der ähm … unbeschreiblichen … Magie von „Chaosāh“ …
Bürgermeister von Komona|5|False|PEPPER!
Bürgermeister von Komona|6|False|Wir haben uns entschlossen, ihr als Gewinnerin des Zaubertrank-Wettbewerbs etwas Besonderes anzubieten.
Pepper|7|False|?!…
Bürgermeister von Komona|8|False|Als solche wird sie …
Bürgermeister von Komona|9|False|… Mitglied unserer Jury!
Geräusch|10|False|Plumps
Publikum|11|True|klatsch
Publikum|12|True|klatsch
Publikum|13|True|klatsch
Publikum|14|True|klatsch
Publikum|15|False|klatsch
Schrift|16|False|Zauberwettbewerb Offizielle Jury
Pepper|17|False|Toll.
Schrift|18|False|Königin Aiel
Schrift|19|False|Pepper
Schrift|20|False|Lord Azeirf
Erzähler|21|False|FORTSETZUNG FOLGT…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|02/2017 - www.peppercarrot.com - Grafik & Handlung: David Revoy - Übersetzung: Alex Gryson – Deutsche Übersetzung: colognella
Impressum|2|False|Ideen: Craig Maloney, Quiralta, Nicolas Artance, Talime, Valvin.
Impressum|3|False|Dialogoptimierung: Craig Maloney, Jookia, Nicolas Artance und Valvin.
Impressum|4|False|Besonderer Dank an das Inkscape-Team und besonders Mc.
Impressum|5|False|Basierend auf der Hereva-Welt von David Revoy, mit Beiträgen von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Impressum|6|False|Software: Krita 3.2.1, Inkscape 0.91 auf Linux Mint 18.1 XFCE
Impressum|7|False|Lizenz: Creative Commons Namensnennung 4.0
Impressum|9|False|Auch Du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
Impressum|8|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 719 Förderer:
