# Transcript of Pepper&Carrot Episode 21 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 21: El concurso de magia

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|1|False|Por petición popular, la Villa de Komona presenta el Concurso de Magia
Escritura|2|False|Gran Premio de 50 000Ko
Escritura|3|False|¡Para la demostración mágica más impresionante! Entrada: 150Ko Nuevo anfiteatro gigante y más protegido Komona Azarday, a las 10 AirMoon
Escritura|4|False|INVITACIÓN ESPECIAL
Pimienta|5|False|¡Estoy lista! ¡Venga, Zanahoria, vamos allá!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¿Sigues empeñada en no querer venir con nosotros?
Cayena|2|False|Por supuesto. ¡En Chaosah enseñamos hechicería, no a dar espectáculo!
Escritura|3|False|EL KOMONERO
Escritura|4|False|INAUGURACIÓN DEL NUEVO ANFITEATRO HOY
Escritura|5|False|LOS HABITANTES PREOCUPADOS POR SUS IMPUESTOS
Escritura|6|False|TAQUILLAS CERRADAS: ¡UN AUTÉNTICO ÉXITO!
Escritura|7|False|TENSIONES POLÍTICAS EN TIERRA DE AH
Escritura|8|False|AH OCUPA YA EL 80% DE LOS TERRITORIOS
Escritura|9|False|¡TIENE USTED BUENA VISTA!
Pimienta|10|True|¡Gracias por dejarme participar!
Pimienta|11|True|¡Ganaré, lo prometo!
Pimienta|12|False|¡Nos vemos esta noche!
Cayena|14|False|...
Sonido|13|False|¡Blam!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Público|2|False|Clap
Alcalde de Komona|1|False|Como alcalde de Komona, declaro inaugurado el segundo gran concurso de magia.
Alcalde de Komona|3|True|¡Sé que esperábais este evento con impaciencia!
Alcalde de Komona|4|False|¡Gracias por haberos desplazado desde todos los rincones de Hereva para contemplar nuestro gran anfiteatro ultra protegido!
Alcalde de Komona|5|False|¡Demos paso ahora a la presentación de las participantes!
Zanahoria|6|True|Z
Zanahoria|7|True|Z|nowhitespace
Zanahoria|8|False|Z ...|nowhitespace
Pimienta|9|True|¡¿Zanahoria?! ¡¿Pero qué estás haciendo?!
Pimienta|10|True|¡Despierta! ¡Que ya empieza!
Pimienta|11|False|¡Y todo el mundo nos mira!
Sonido|12|False|¡Tap!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Público|3|False|Clap
Alcalde de Komona|1|True|Versada en la magia mortalmente vivificadora de "Zombiah", nos honra presentar a...
Alcalde de Komona|2|False|¡CELANDRIA!
Alcalde de Komona|4|True|¡La fauna y la flora no guardan secretos para esta bruja de "Hippiah"! Recibamos con un fuerte aplauso a...
Alcalde de Komona|5|False|¡MANZANILLA!
Alcalde de Komona|6|False|Y ahora, gracias a los logros técnicos de Komona, aquí, en exclusiva mundial...
Sonido|7|False|¡Flop!
Alcalde de Komona|8|True|...una bruja procedente de los fondos marinos, maestra de la poderosa magia de "Aquah"...
Alcalde de Komona|9|False|¡ESPIRULINA!
Pimienta|10|False|¡¿Aquah?! ¡¿Aquí?! ¡Increíble!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Público|9|False|Clap
Zanahoria|1|False|Groooo
Pimienta|2|True|¡Zanahoria!
Pimienta|3|True|¡Ahora viene nuestro turno!
Pimienta|4|True|¡Un poco de seriedad!
Pimienta|5|True|¡Por favor!
Pimienta|6|False|¡Solo por hoy!
Alcalde de Komona|7|True|Hechicera de la magia espiritual y ascética de "Ah", es un placer tener hoy entre nosotros a...
Alcalde de Komona|8|False|¡SHICHIMI!
Pimienta|10|False|¡Zanahoria! ¡¡¡NOOO!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Público|7|False|Jajajajaj
Alcalde de Komona|1|False|Ahora le llega el turno a esta misteriosa participante, inscrita de forma anónima para representar a la flamante magia de "Magmah"...
Alcalde de Komona|3|True|¡OH! ¡Qué sorpresa! ¡Sí, es ella! Recibamos a...
Alcalde de Komona|4|False|¡AZAFRÁN!
Sonido|2|False|¡Flap!
Alcalde de Komona|6|False|¡Ah! Parece que una participante está teniendo problemas...
Sonido|5|False|¡Bluf!
Alcalde de Komona|8|False|¡¡Señorita!! ¡Le rogamos que vuelva a su puesto!
Pimienta|9|False|¡Zanahooria! ¡NO!
Sonido|10|False|¡Dzing!
Pimienta|11|False|?!
Sonido|12|False|¡Splash!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Jaja! ¡Gracias, Espirulina! Y perdón...
Pimienta|2|True|¡¿Dónde vas tú, eh?!
Pimienta|3|False|¡VEN PARA ACÁ!
Alcalde de Komona|4|True|¡Ejem! Aquí tenemos a nuestra última participante, con la magia de la... emm... incalificable "Chaosah"...
Alcalde de Komona|5|False|¡PIMIENTA!
Alcalde de Komona|6|False|¡Como gran ganadora del concurso de pociones, hemos decidido ofrecerle un lugar destacado!
Pimienta|7|False|?!...
Alcalde de Komona|8|False|En efecto, ella será...
Alcalde de Komona|9|False|...¡parte de nuestro jurado!
Sonido|10|False|Bam
Público|11|True|Clap
Público|12|True|Clap
Público|13|True|Clap
Público|14|True|Clap
Público|15|False|Clap
Escritura|16|False|Concurso de Magia Jurado Oficial
Pimienta|17|False|Bieeen.
Escritura|18|False|Reina Aiel
Escritura|19|False|Pimienta
Escritura|20|False|Lord Azeirf
Narrador|21|False|CONTINUARÁ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|02/2017 - www.peppercarrot.com - Dibujo & Guion: David Revoy - Traducción: TheFaico
Créditos|2|False|Lluvia de ideas: Craig Maloney, Quiralta, Nicolas Artance, Talime y Valvin.
Créditos|3|False|Mejora de los diálogos: Craig Maloney, Jookia, Nicolas Artance y Valvin.
Créditos|4|False|Agradecimientos: Al equipo de Inkscape y en especial a Mc.
Créditos|5|False|Basado en el universo de Hereva, creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Créditos|6|False|Software: Krita 3.2.1, Inkscape 0.91 en Linux Mint 18.1 XFCE
Créditos|7|False|Licencia: Creative Commons Attribution 4.0
Créditos|9|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 816 mecenas:
Créditos|8|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
