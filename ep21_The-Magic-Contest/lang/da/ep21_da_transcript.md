# Transcript of Pepper&Carrot Episode 21 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 21: Tryllekonkurrencen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|1|False|På opfordring af borgerne præsenterer Komona by Tryllekonkurrencen
Skrift|2|False|Førstepræmie på 50 000Ko
Skrift|3|False|Til den mest imponerende trylleopvisning! Indgang: 150Ko Ny og sikker kæmpearena Komona Azardag, 10 AirMoon
Skrift|4|False|SÆRLIG INVITATION
Pepper|5|False|Jeg er klar! Kom så, Carrot. Nu tager vi afsted!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Er I stadig sikre på, at I ikke vil med?
Cayenne|2|False|Helt sikre. Vi laver Kaosah-magi, ikke opvisninger!
Skrift|3|False|Den KOMONANSKE
Skrift|4|False|INDVIELSE AF DEN NYE ARENA I DAG
Skrift|5|False|BORGERE BEKYMREDE FOR DERES SKAT
Skrift|6|False|ALLE BILLETTER UDSOLGT: SUCCES I SIGTE!
Skrift|7|False|POLITISKE SPÆNDINGER I AH
Skrift|8|False|AH HAR BESAT 80% AF TERRITORIERNE
Skrift|9|False|DE HAR ET GODT SYN!
Pepper|10|True|Tak fordi I lader mig deltage!
Pepper|11|True|Jeg vinder! Det lover jeg!
Pepper|12|False|Vi ses i aften!
Cayenne|14|False|...
Lyd|13|False|Bam!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|2|False|Klap
Komonas Borgmester|1|False|Som Komonas borgmester erklærer jeg den anden Store Tryllekonkurrence for åben!
Komonas Borgmester|3|True|Jeg ved, at I alle sammen har glædet jer til denne store dag!
Komonas Borgmester|4|False|Tak fordi I er kommet hertil fra Herevas fire verdenshjørner for at se vores nye supersikre arena!
Komonas Borgmester|5|False|Nu vil jeg præsentere vores deltagere!
Carrot|6|True|Z
Carrot|7|True|Z|nowhitespace
Carrot|8|False|Z ...|nowhitespace
Pepper|9|True|Carrot?! Hvad laver du?!
Pepper|10|True|Vågn op! Det starter!
Pepper|11|False|Og alle kigger på os!
Lyd|12|False|Tap!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|3|False|Klap
Komonas Borgmester|1|True|Ekspert i ”Zombiah”-magi, der vækker til live, det er en ære at tage i mod...
Komonas Borgmester|2|False|KORIANDER!
Komonas Borgmester|4|True|Dyre-og plantelivet har ingen hemmeligheder for denne ”Hippiah”-heks! Giv en stor hånd til...
Komonas Borgmester|5|False|KAMILLE!
Komonas Borgmester|6|False|Og nu, takket være vores nye teknologier i Komona, er her en verdenspremiere…
Lyd|7|False|Flap!
Komonas Borgmester|8|True|...en heks fra de dybe havbunde, som bruger den formidable ”Akvah”-magi!
Komonas Borgmester|9|False|SPIRULINA!
Pepper|10|False|Akvah?! Her?! Utroligt!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|9|False|Klap
Carrot|1|False|Rumle
Pepper|2|True|Carrot!
Pepper|3|True|Det er snart vores tur!
Pepper|4|True|Vær lidt seriøs!
Pepper|5|True|Kom nu!
Pepper|6|False|Bare i dag!
Komonas Borgmester|7|True|Det er en glæde at have en heks, der bruger den åndelige og asketiske ”Ah”-magi i blandt os...
Komonas Borgmester|8|False|SHICHIMI!
Pepper|10|False|Carrot! NEJ!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|7|False|Ha!
Komonas Borgmester|1|False|Og nu er turen kommet til denne mystiske deltager, der har skrevet sig på anonymt for at præsentere den flamboyante ”Magmah”-magi...
Komonas Borgmester|3|True|ÅH! Sikke en overraskelse! Ja, det er hende! Ingen andre end...
Komonas Borgmester|4|False|SAFRAN!
Lyd|2|False|Flap!
Komonas Borgmester|6|False|Det ser ud til, at en af vores deltagere allerede er i problemer!
Lyd|5|False|Plump!
Komonas Borgmester|8|False|Frøken!! Vær venlig at gå tilbage til Deres plads!!
Pepper|9|False|Caaarrott! NEJ!
Lyd|10|False|Dzing !
Pepper|11|False|?!
Lyd|12|False|Splash!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Hi-hi! Tak, Spirulina! Og undskyld...
Pepper|2|True|Og hvor skal du hen?!
Pepper|3|False|KOM TILBAGE!
Komonas Borgmester|4|True|Hm hm! Her er så vores sidste deltager af den… øhm… ubeskrivelige "Kaosah"-magi...
Komonas Borgmester|5|False|PEPPER!
Komonas Borgmester|6|False|Som vinderen af den store eliksirkonkurrence, har vi valgt af give hende en helt særlig plads!
Pepper|7|False|?!...
Komonas Borgmester|8|False|Hun skal nemlig...
Komonas Borgmester|9|False|...deltage i vores jury!
Lyd|10|False|Bum
Publikum|11|True|Klap
Publikum|12|True|Klap
Publikum|13|True|Klap
Publikum|14|True|Klap
Publikum|15|False|Klap
Skrift|16|False|Tryllekonkurence Officiel Jury
Pepper|17|False|Jubii.
Skrift|18|False|Dronning Aiel
Skrift|19|False|Pepper
Skrift|20|False|Lord Azeirf
Fortæller|21|False|Fortsættes...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|02/2017 – www.peppercarrot.com – Tegning og manuskript: David Revoy – Dansk oversættelse: Emmiline Alapetite
Credits|2|False|Brainstorming: Craig Maloney, Quiralta, Nicolas Artance, Talime og Valvin.
Credits|3|False|Forbedring af dialoger: Craig Maloney, Jookia, Nicolas Artance og Valvin.
Credits|4|False|Tak til: Inkscapes hold og især Mc.
Credits|5|False|Baseret på Hereva-universet skabt af David Revoy med bidrag af Craig Maloney. Rettelser af Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Credits|6|False|Værktøj: Krita 3.2.1, Inkscape 0.91, Linux Mint 18.1 XFCE
Credits|7|False|Licens: Creative Commons Kreditering 4.0
Credits|9|False|Pepper&Carrot er helt gratis, open-source, og sponsoreret af læsere. Denne episode kunne ikke være blevet til uden støtte fra de 816 tilhængere:
Credits|8|False|Støt næste episode af Pepper&Carrot på www.patreon.com/davidrevoy
