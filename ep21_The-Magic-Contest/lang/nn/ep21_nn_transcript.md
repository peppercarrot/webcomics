# Transcript of Pepper&Carrot Episode 21 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 21: Trolldomskonkurransen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Skrift|2|False|Endeleg kan Komona by kunngjera ein ny trolldomskonkurranse
Skrift|3|True|Fyrstepremie: 50 000 Ko
Skrift|4|False|for den mest imponerande trolldomsoppvisinga! Inngang: 150 Ko Den nye, enorme og sikre arenaen i Komona, Azardag, klokka 10
Skrift|1|False|SPESIALINNBYDING
Pepar|5|False|Eg er klar! Kom igjen, Gulrot, så dreg me!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Er du sikker på at du ikkje vil vera med?
Kajenne|2|False|Heilt sikker. I kaosah driv me med trolldom, ikkje show!
Skrift|3|False|KOMONA-nytt
Skrift|4|False|OPNING AV DEN NYE ARENAEN I DAG
Skrift|5|False|FOLKET UROA OVER AUKA SKATTAR
Skrift|6|False|ALLE BILLETTANE SELDE: GARANTERT SUKSESS!
Skrift|7|False|POLITISK URO I LANDET AH
Skrift|8|False|AH OKKUPERER 80 % AV LANDOMRÅDA
Skrift|9|False|DU HAR PERFEKT SYN!
Pepar|10|True|Takk for at eg får lov til å delta!
Pepar|11|True|Og eg lovar å vinna!
Pepar|12|False|Me snakkast i kveld!
Kajenne|14|False|...
Lyd|13|False|Kleis!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|2|False|Klapp
Ordføraren i Komona|1|False|Som ordførar i Komona erklærer eg med dette den andre store trolldoms-konkurransen for open!
Ordføraren i Komona|3|True|Eg veit de har venta i spenning på denne storhendinga!
Ordføraren i Komona|4|False|Takk for at de har kome frå alle fire verdshjørna av Hereva for å vitja vår nye, storslegne og ultrasikre arena!
Ordføraren i Komona|5|False|La meg no presentera deltakarane!
Gulrot|6|True|Z
Gulrot|7|True|Z|nowhitespace
Gulrot|8|False|Z ...|nowhitespace
Pepar|9|True|Gulrot?! Kva held du på med?!
Pepar|10|True|Vakna! Det startar no!
Pepar|11|False|Alle ser på oss!
Lyd|12|False|Dult!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|3|False|Klapp
Ordføraren i Komona|1|True|Ho er ein meister i gjen-opplivingstrolldommen zombiah. Det er ei ære å kunna ynskja velkomen ...
Ordføraren i Komona|2|False|KORIANDER!
Ordføraren i Komona|4|True|Ho kjenner alle løyn-dommane til både flora og fauna! Ta godt imot hippiah-heksa ...
Ordføraren i Komona|5|False|KAMILLE!
Ordføraren i Komona|6|False|Og no, takka vere den høge tekniske ekspertisen i Komona, viser me for aller fyrste gong ...
Lyd|7|False|Fapp!
Ordføraren i Komona|8|True|... ei heks frå djuphavet – og meister av den eineståande akvah-magien!
Ordføraren i Komona|9|False|SPIRULINA!
Pepar|10|False|Akvah?! Her?! Utruleg!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|9|False|Klapp
Gulrot|1|False|Rumle
Pepar|2|True|Gulrot!
Pepar|3|True|No er det straks vår tur!
Pepar|4|True|Ha tol!
Pepar|5|True|Ver så snill!
Pepar|6|False|Om berre i dag!
Ordføraren i Komona|7|True|Og så ei heks av den åndelege og asketiske ah-trolldommen. Me er glade for å ta imot ...
Ordføraren i Komona|8|False|SHICHIMI!
Pepar|10|False|Gulrot! NEI!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|7|False|Ha!
Ordføraren i Komona|1|False|Og så denne mystiske deltakaren – som har meldt seg på anonymt – og vil visa oss litt frå den raudglødande heite magmah-trolldommen ...
Ordføraren i Komona|3|True|Oi! For ei overrasking! Ja, det er henne – ingen ringare enn ...
Ordføraren i Komona|4|False|SAFRAN!
Lyd|2|False|Fapp!
Ordføraren i Komona|6|False|Oi sann! Det ser ut til at ein deltakar alt er i vanskar!
Lyd|5|False|Plask!
Ordføraren i Komona|8|False|Frøken! Still deg tilbake på plass!!!
Pepar|9|False|Gulrot! Nei!
Lyd|10|False|Dzing!
Pepar|11|False|?!
Lyd|12|False|Plask!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Ha-ha! Takk, Spirulina! Og orsak ...
Pepar|2|True|Kor har du tenkt deg?!
Pepar|3|False|KOM HER!
Ordføraren i Komona|4|True|Kremt! Og her er den siste deltakaren – med den ... æh ... ubeskrivelege kaosah-trolldommen ...
Ordføraren i Komona|5|False|PEPAR!
Ordføraren i Komona|6|False|Og sidan ho vann trylledrikk-konkurransen, får ho æra av ...
Pepar|7|False|?! ...
Ordføraren i Komona|8|False|... å verta ...
Ordføraren i Komona|9|False|... medlem av juryen vår!
Lyd|10|False|Dunk!
Publikum|11|True|Klapp
Publikum|12|True|Klapp
Publikum|13|True|Klapp
Publikum|14|True|Klapp
Publikum|15|False|Klapp
Skrift|16|False|Offisiell jury i trolldomskonkurransen
Pepar|17|False|Jippi.
Skrift|18|False|Dronning Aiel
Skrift|19|False|Pepar
Skrift|20|False|Lord Azeirf
Forteljar|21|False|FRAMHALD FØLGJER ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Februar 2017 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen
Bidragsytarar|2|False|Idédugnad: Craig Maloney, Quiralta, Nicolas Artance, Talime og Valvin.
Bidragsytarar|3|False|Forbetring av dialogen: Craig Maloney, Jookia, Nicolas Artance og Valvin.
Bidragsytarar|4|False|Ein særskild takk til Inkscape-gruppa, spesielt Mc.
Bidragsytarar|5|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Bidragsytarar|6|False|Programvare: Krita 3.2.1 og Inkscape 0.91 på Linux Mint 18.1 XFCE
Bidragsytarar|7|False|Lisens: Creative Commons Attribution 4.0
Bidragsytarar|9|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
Bidragsytarar|8|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 816 som støtta denne episoden:
