# Transcript of Pepper&Carrot Episode 21 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 21: Tekmovanje v čaranju

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|1|False|Zaradi velikega povpraševanja vam mesto Komona predstavlja Tekmovanje v čaranju
Napis|2|False|Nagrada 50 000 kojev
Napis|3|False|za najbolj osupljiv prikaz čarovnije! Vstopnina: 150 kojev V novi ogromni varovani komonski areni V azardeljo, ob 10. luni
Napis|4|False|POSEBNO POVABILO
Paprika|5|False|Pripravljena sem! Pridi, Korenček, pojdiva!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Ste še vedno prepričani, da nočete iti z nama?
Kajenka|2|False|Zagotovo. Kaosah je pravo čarovništvo, ne nek spektakel!
Napis|3|False|KOMONSKI GLAS
Napis|4|False|SLOVESNOST OB OTVORITVI NOVE ARENE DANES
Napis|5|False|PREBIVALCI ZASKRBLJENI ZARADI SVOJIH DAVKOV
Napis|6|False|VSTOPNICE RAZPRODANE: USPEH ZAGOTOVLJEN!
Napis|7|False|POLITIČNE NAPETOSTI NA OZEMLJU AH
Napis|8|False|AH TRENUTNO ZAVZEMA 80% VSEGA TERITORIJA
Napis|9|False|IMATE IZJEMEN VID
Paprika|10|True|Hvala, ker lahko sodelujem!
Paprika|11|True|Obljubim, da bom zmagala!
Paprika|12|False|Se vidimo zvečer!
Kajenka|14|False|...
Zvok|13|False|tresk!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Občinstvo|2|False|Plosk
Župan Komone|1|False|Kot župan Komone razglašam začetek drugega velikega čarovniškega tekmovanja!
Župan Komone|3|True|Vem, da ste nestrpno čakali na ta dogodek!
Župan Komone|4|False|Zahvaljujem se vsem, ki ste prišli s štirih koncev Hereve izkusit našo ogromno ultra-varovano areno!
Župan Komone|5|False|Predstavimo sedaj naše tekmovalke!
Korenček|6|True|Z
Korenček|7|True|Z|nowhitespace
Korenček|8|False|Z …|nowhitespace
Paprika|9|True|Korenček?! Kaj se pa greš?!
Paprika|10|True|Zbudi se! Začelo se je!
Paprika|11|False|Vsi naju gledajo!
Zvok|12|False|Tap!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Občinstvo|3|False|Plosk
Župan Komone|1|True|Predstavljamo strokovnjakinjo za Zombijah, čarovnijo oživitve …
Župan Komone|2|False|KORIANDRIKO!
Župan Komone|4|True|Ta hipijaška čarovnica pozna vse skrivnosti flore in favne. Pozdravite, prosim …
Župan Komone|5|False|KAMILICO!
Župan Komone|6|False|In zdaj, zahvaljujoč tehničnim spretnostim Komone, je prvič na površju …
Zvok|7|False|Flop!
Župan Komone|8|True|…čarovnica iz vodnih globin, ki obvladuje osupljivo akvaško čarovnijo,
Župan Komone|9|False|SPIRULINA!
Paprika|10|False|Akvah?! Tukaj?! Neverjetno!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Občinstvo|9|False|Plosk
Korenček|1|False|Kruuul
Paprika|2|True|Korenček!
Paprika|3|True|Vsak hip bova na vrsti!
Paprika|4|True|Potrpi!
Paprika|5|True|Prosim!
Paprika|6|False|Samo danes!
Župan Komone|7|True|V veselje nam je gostiti čarovnico duhovne in lepotne čarovnije Ah …
Župan Komone|8|False|ŠIČIMI!
Paprika|10|False|Korenček! NE!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Občinstvo|7|False|Ha!
Župan Komone|1|False|Čas je za predstavitev te skrivnostne tekmovalke, ki je prišla prikazat ognjevito čarovnijo Magmah …
Župan Komone|3|True|O! Kakšno presenečenje! To je nihče drugo kot …
Župan Komone|4|False|ŽAFRANKA!
Zvok|2|False|Flap!
Župan Komone|6|False|Ah! Kaže, da ima ena tekmovalka že težave!
Zvok|5|False|Čoof!
Župan Komone|8|False|Gospodična!! Prosim, vrnite se na svoje mesto!!
Paprika|9|False|Korenček! NE!
Zvok|10|False|Dzing!
Paprika|11|False|?!
Zvok|12|False|Pljusk!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Ha ha! Hvala, Spirulina! Oprosti …
Paprika|2|True|Kam zdaj greš?!
Paprika|3|False|TAKOJ NAZAJ!
Župan Komone|4|True|Khm! In še naša zadnja tekmovalka z … uh … neopisljivo čarovnijo Kaosah …
Župan Komone|5|False|PAPRIKA!
Župan Komone|6|False|Ker je zmagala v zadnjem tekmovanju v napojih, smo se odločili, da ji dodelimo posebno vlogo!
Paprika|7|False|?! …
Župan Komone|8|False|In tako bo postala …
Župan Komone|9|False|… del naše žirije!
Zvok|10|False|Bam
Občinstvo|11|True|Plosk
Občinstvo|12|True|Plosk
Občinstvo|13|True|Plosk
Občinstvo|14|True|Plosk
Občinstvo|15|False|Plosk
Napis|16|False|Tekmovanje v čaranju Uradna žirija
Paprika|17|False|Juhu.
Napis|18|False|Kraljica Aiel
Napis|19|False|Paprika
Napis|20|False|Knez Azeirf
Pripovedovalec|21|False|SE NADALJUJE …

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Februar 2017 - www.peppercarrot.com - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Zasluge|2|False|Ideje so prispevali Craig Maloney, Quiralta, Nicolas Artance, Talime in Valvin.
Zasluge|3|False|Izboljšave dialoga: Craig Maloney, Jookia, Nicolas Artance in Valvin.
Zasluge|4|False|Posebna zahvala gre razvijalcem Inkscapea, predvsem Mc.
Zasluge|5|False|Dogaja se v svetu Hereve avtorja Davida Revoyja s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|6|False|Programska oprema: Krita 3.2.1 in Inkscape 0.91 na Linux Mint 18.1 XFCE
Zasluge|7|False|Licenca: Creative Commons Priznanje avtorstva 4.0.
Zasluge|9|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
Zasluge|8|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 816 bralcev:
