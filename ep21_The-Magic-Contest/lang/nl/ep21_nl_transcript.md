# Transcript of Pepper&Carrot Episode 21 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 21: De magiewedstrijd

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geschrift|1|False|Op algemeen verzoek presenteert de Stad Komona de magiewedstrijd
Geschrift|2|False|Hoofdprijs: 50 000 Ko
Geschrift|3|False|Voor de meest indrukwekkende magievertoning! Toegang: 150 Ko Nieuwe, gigantische en beveiligde arena Komona Azerdag, 10 LuchtMaan
Geschrift|4|False|SPECIALE UITNODIGING
Pepper|5|False|Ik ben klaar! Kom, Carrot, we zijn weg!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Zeker dat je niet met ons mee wil komen?
Cayenne|2|False|Zeker. Bij Chaosah doen we aan hekserij, niet aan vertoningen!
Geschrift|3|False|De KOMONER
Geschrift|4|False|VANDAAG PLECHTIGE OPENING NIEUWE ARENA
Geschrift|5|False|INWONERS ONGERUST OVER BELASTINGEN
Geschrift|6|False|TICKETS UITVERKOCHT: SUCCES GEGARANDEERD!
Geschrift|7|False|POLITIEKE ONLUSTEN IN LAND VAN AH
Geschrift|8|False|AH BEZET NU 80% VAN GRONDGEBIED
Geschrift|9|False|U HEEFT GOEDE OGEN!
Pepper|10|True|Bedankt om me te laten meedoen!
Pepper|11|True|Ik ga winnen! Beloofd!
Pepper|12|False|Tot vanavond!
Cayenne|14|False|...
Geluid|13|False|Vlam!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publiek|2|False|Klap
Burgemeester van Komona|1|False|Als burgemeester van Komona, verklaar ik de grote magiewedstrijd geopend!
Burgemeester van Komona|3|True|Jullie hebben vol ongeduld op dit moment gewacht!
Burgemeester van Komona|4|False|Vanuit de verste hoeken van Hereva zijn jullie afgereisd naar onze grote, superbeveiligde arena! Dank jullie wel!
Burgemeester van Komona|5|False|Dan gaan we nu over op de voorstelling van de deelneemsters!
Carrot|6|True|Z
Carrot|7|True|Z|nowhitespace
Carrot|8|False|Z …|nowhitespace
Pepper|9|True|Carrot?! Maar wat doe je nu?!
Pepper|10|True|Word wakker! Het begint zo!
Pepper|11|False|En iedereen kijkt naar ons!
Geluid|12|False|Pok!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publiek|3|False|Klap
Burgemeester van Komona|1|True|We hebben de eer een heks van “Zombiah” te verwelkomen, die de doden kan opwekken! Hier is …
Burgemeester van Komona|2|False|KORIANDER!
Burgemeester van Komona|4|True|De fauna en flora hebben geen enkel geheim voor deze heks van “Hippiah”! Graag uw applaus voor …
Burgemeester van Komona|5|False|KAMILLE!
Burgemeester van Komona|6|False|En dan nu, dankzij het technische vernuft uit Komona, is hier als wereldprimeur …
Geluid|7|False|Flop!
Burgemeester van Komona|8|True|…een heks uit de diepten der zee, die de huiveringwekkende magie van “Aquah” beheerst!
Burgemeester van Komona|9|False|SPIRULINA!
Pepper|10|False|Aquah?! Hier?! Ongelofelijk!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publiek|9|False|Klap
Carrot|1|False|Groeee
Pepper|2|True|Carrot!
Pepper|3|True|In 's hemels naam!
Pepper|4|True|Het is zo aan ons!
Pepper|5|True|Doe normaal!
Pepper|6|False|Alleen vandaag!
Burgemeester van Komona|7|True|Het is ons een genoegen deze heks van de spirituele en ascetische magie van "Ah" te verwelkomen …
Burgemeester van Komona|8|False|SHICHIMI!
Pepper|10|False|Carrot! NEE!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publiek|7|False|Ha!
Burgemeester van Komona|1|False|En dan is het nu aan deze mysterieuze deel-neemster, die zich anoniem ingeschreven heeft om de flamboyante magie van “Magmah” te vertegenwoordigen …
Burgemeester van Komona|3|True|OH! Wat een verrassing! Ja, zij is het! Het is …
Burgemeester van Komona|4|False|SAFFRAAN!
Geluid|2|False|Swoesj!
Burgemeester van Komona|6|False|Ah! Het ziet ernaar uit dat een van de deelneemsters al een probleem heeft!
Geluid|5|False|Ploef!
Burgemeester van Komona|8|False|Juffrouw! Gaat u alstublieft terug naar uw plaats!
Pepper|9|False|Caaarrott! NEE!
Geluid|10|False|Dzing!
Pepper|11|False|?!
Geluid|12|False|Plens!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ha-ha! Dank je, Spirulina! En sorry …
Pepper|2|True|Waar ga jij heen?!
Pepper|3|False|HIER!
Burgemeester van Komona|4|True|Ahum! Dus hier onze laatste deelnemster, die de … uhm … onbeschrijfelijke magie beheerst van … “Chaosah” …
Burgemeester van Komona|5|False|PEPPER!
Burgemeester van Komona|6|False|Als grote winnares van de toverdrankwedstrijd, krijgt ze een speciale plaats aangeboden!
Pepper|7|False|?! …
Burgemeester van Komona|8|False|Zij zal deel uitmaken van …
Burgemeester van Komona|9|False|… onze jury!
Geluid|10|False|Bam
Publiek|11|True|Klap
Publiek|12|True|Klap
Publiek|13|True|Klap
Publiek|14|True|Klap
Publiek|15|False|Klap
Geschrift|16|False|Magiewedstrijd Officiële jury
Pepper|17|False|Yay.
Geschrift|18|False|Koningin Aiel
Geschrift|19|False|Pepper
Geschrift|20|False|Heer Azeerf
Verteller|21|False|WORDT VERVOLGD …

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|02/2017 - www.peppercarrot.com - Tekeningen & verhaal: David Revoy - Vertaling: Midgard en Marno van der Maas.
Aftiteling|2|False|Brainstorming: Craig Maloney, Quiralta, Nicolas Artance, Talime en Valvin.
Aftiteling|3|False|Conversatieverbeteringen: Craig Maloney, Jookia, Nicolas Artance en Valvin.
Aftiteling|4|False|Met dank aan het Inkscape-team, en in het bijzonder Mc.
Aftiteling|5|False|Gebaseerd op het universum van Hereva gecreëerd door David Revoy met bijdragen van Craig Maloney. Verbeteringen door Willem Sonke, Moini, Hali, CGand en Alex Gryson.
Aftiteling|6|False|Software: Krita 3.2.1, Inkscape 0.91 op Linux Mint 18.1 XFCE
Aftiteling|7|False|Licentie: Creative Commons Naamsvermelding 4.0
Aftiteling|9|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 816 patronen:
Aftiteling|8|False|Jij kan ook een patroon van Pepper&Carrot worden op www.patreon.com/davidrevoy
