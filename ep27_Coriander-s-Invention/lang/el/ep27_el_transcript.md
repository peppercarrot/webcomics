# Transcript of Pepper&Carrot Episode 27 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tίτλος|1|False|Επεισόδιο 27: Η Εφεύρεση της Κολιάντρας

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Αφηγητής|1|False|Η Καλόπολη το απόγευμα
Ράφτης|4|False|ΠΑΡΑΚΑΛΩ, ΠΡΙΓΚΙΠΙΣΣΑ ΚΟΛΙΑΝΤΡΑ...
Ράφτης|5|False|ΘΑ ΣΤΑΜΑΤΗΣΕTE ΝΑ ΜΕΤΑΚΙΝΕIΣTE ΤΟΣΟ ΠΟΛΥ;
Κολιάντρα|6|False|ΕΝΤΑΞΕΙ ΕΝΤΑΞΕΙ...
Ράφτης|7|False|ΤΕΛΕΙΩΝΕΙ Ο ΧΡΟΝΟΣ ΚΑΙ Η ΣΤΕΨΗ ΣΑΣ ΑΡΧΙΖΕΙ ΣΕ ΤΡΕΙΣ ΩΡΕΣ.
Κολιάντρα|8|False|...
Κείμενο|2|True|Η ΣΤΕΨΗ ΤΗΣ ΜΕΓΑΛΕΙΟΤΑΤΗΣ
Κείμενο|3|False|ΒΑΣΙΛΙΣΣΑ ΚΟΛΙΑΝΤΡΑ

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Κολιάντρα|1|True|ΞΕΡΕΙΣ...
Κολιάντρα|2|False|ΣΚΕΦΤΟΜΟΥΝ ΤΗΝ ΑΣΦΑΛΕΙΑ ΞΑΝΑ...
Κολιάντρα|3|True|ΤΟΣΟΙ ΝΕΟΙ ΒΑΣΙΛΙΑΔΕΣ ΚΑΙ ΒΑΣΙΛΙΣΣΕΣ ΕΧΟΥΝ ΔΟΛΟΦΟΝΗΘΕΙ ΣΤΗ ΓΙΟΡΤΗ ΤΗΣ ΣΤΕΨΗΣ.
Κολιάντρα|4|False|ΕΧΕΙ ΓΙΝΕΙ ΜΕΡΟΣ ΤΗΣ ΟΙΚΟΓΕΝΕΙΑΚΗΣ ΙΣΤΟΡΙΑΣ ΜΟΥ.
Κολιάντρα|5|True|ΚΑΙ ΤΩΡΑ, ΣΕ ΛΙΓΕΣ ΩΡΕΣ, ΘΑ ΕΡΘΟΥΝ ΧΙΛΙΑΔΕΣ ΑΠΟ ΟΛΟ ΤΟΝ ΚΟΣΜΟ ΚΑΙ ΘΑ ΕΙΜΑΙ ΤΟ ΚΕΝΤΡΟ ΤΗΣ ΠΡΟΣΟΧΗΣ.
Πιπερούλα|7|True|ΜΗΝ ΑΝΗΣΥΧΕΙΣ ΚΟΛΙΑΝΤΡΑ.
Κολιάντρα|6|False|ΠΡΑΓΜΑΤΙΚΑ ΦΟΒΑΜΑΙ!
Πιπερούλα|8|True|ΘΑ ΑΚΟΛΟΥΘΗΣΟΥΜΕ ΤΟ ΣΧΕΔΙΟ ΜΑΣ.
Πιπερούλα|9|False|Η ΣΙΣΣΙΜΙ ΚΑΙ ΕΓΩ ΘΑ ΕΙΜΑΣΤΕ ΟΙ ΣΩΜΑΤΟΦΥΛΑΚΕΣ ΣΟΥ, ΘΥΜΑΣΑΙ;
Σίσσιμι|10|True|ΑΚΡΙΒΩΣ
Σίσσιμι|11|False|ΚΑΝΕ ΔΙΑΛΕΙΜΜΑ ΚΑΙ ΣΚΕΨΟΥ ΚΑΤΙ ΑΛΛΟ ΤΩΡΑ.
Κολιάντρα|12|False|ΕΝΤΑΞΕΙ
Κολιάντρα|13|False|Χέϊ! ΓΙΑΤΙ ΔΕΝ ΜΟΥ ΔΕΙΧΝΕΙΣ ΤΗ ΤΕΛΕΥΤΑΙΑ ΣΟΥ ΕΦΕΥΡΕΣΗ;
Ράφτης|14|False|...
Κολιάντρα|15|True|ΕΙΝΑΙ ΚΑΤΩ ΣΤΟ ΕΡΓΑΣΤΗΡΙ ΜΟΥ.
Κολιάντρα|16|False|ΑΚΟΛΟΥΘΗΣΕ ΜΕ

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Κολιάντρα|1|False|ΣΥΝΤΟΜΑ ΘΑ ΚΑΤΑΛΑΒΕΙΣ ΓΙΑΤΙ ΗΘΕΛΑ ΝΑ ΧΤΙΣΤΕΙ ΑΥΤΟ ΕΔΩ.
Κολιάντρα|2|False|ΕΧΕΙ ΝΑ ΚΑΝΕΙ ΜΕ ΤΙΣ ΑΝΗΣΥΧΙΕΣ ΜΟΥ ΓΙΑ ΤΗ ΣΤΕΨΗ.
Κολιάντρα|3|False|ΝΑ ΣΕ ΣΥΣΤΗΣΩ ΣΤΗ ΝΕΑ ΜΟΥ ΕΦΕΥΡΕΣΗ...
Ρομπότ-Ψυχολόγος|5|False|ΓΕΙΑ ΣΟΥ ΚΟΣΜΕ.
Ρομπότ-Ψυχολόγος|6|False|ΕΙΜΑΙ Ο ΡΟΜΠΟΤ-ΨΥΧΟΛΟΓΟΣ ΜΠΙΠ, ΜΠΙΠ!
Ρομπότ-Ψυχολόγος|7|False|ΠΑΡΑΚΑΛΩ ΞΑΠΛΩΣΕ ΣΤΟΝ ΚΑΝΑΠΕ ΜΟΥ ΓΙΑ ΜΙΑ ΔΩΡΕΑΝ ΕΞΕΤΑΣΗ.
Ήχος|4|False|κλικ!
Ήχος|8|False|Ταπ!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Κολιάντρα|1|True|ΤΙ ΝΟΜΙΖΕΙΣ;
Κολιάντρα|2|False|ΘΕΛΕΙΣ ΝΑ ΤΟ ΔΟΚΙΜΑΣΕΙΣ;
Πιπερούλα|3|True|ΜΠΑΑ.
Πιπερούλα|4|True|ΔΕΝ ΧΡΕΙΑΖΕΤΑΙ.
Πιπερούλα|5|False|ΔΕΝ ΕΧΩ ΚΑΝΕΝΑ ΨΥΧΟΛΟΓΙΚΟ ΘΕΜΑ.
Ήχος|6|True|Μ|nowhitespace
Πιπερούλα|11|False|Χέϊ, ΤΙ ΠΗΓΕ ΛΑΘΟΣ ΜΕ ΤΟ ΡΟΜΠΟΤ;!
Κολιάντρα|12|True|ΣΥΓΝΩΜΗ!
Κολιάντρα|13|True|ΕΧΕΙ ΑΚΟΜΑ ΑΥΤΟ ΤΟ ΣΦΑΛΜΑ ΠΟΥ ΔΕΝ ΜΠΟΡΩ ΝΑ ΛΥΣΩ.
Κολιάντρα|14|False|ΣΟΥ ΔΙΝΕΙ ΗΛΕΚΤΡΙΚΟ ΣΟΚ ΜΟΛΙΣ ΕΝΤΟΠΙΣΕΙ ΕΝΑ ΨΕΜΜΑ.
Πιπερούλα|15|False|ΕΝΑ ΨΕΜΜΑ;!
Πιπερούλα|16|False|ΑΛΛΑ ΔΕΝ ΕΧΩ ΠΕΙ ΚΑΝΕΝΑ ΨΕΜΜΑ ΣΕ ΟΛΟΚΛΗΡΗ ΤΗ ΖΩΗ ΜΟΥ!
Πιπερούλα|22|True|ΑΥΤΟ...
Πιπερούλα|23|True|ΔΕΝ ΕΙΝΑΙ...
Πιπερούλα|24|False|ΑΣΤΕΙΟ!!!
Ήχος|7|True|Π|nowhitespace
Ήχος|8|True|Ζ|nowhitespace
Ήχος|9|True|Ζ|nowhitespace
Ήχος|10|False|!|nowhitespace
Ήχος|17|True|Μ
Ήχος|18|True|Π|nowhitespace
Ήχος|19|True|Ζ|nowhitespace
Ήχος|20|True|Ζ|nowhitespace
Ήχος|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Σίσσιμι|1|False|ΑΣΤΕΙΟ? ΟΧΙ, ΚΑΙ ΒΕΒΑΙΑ ΟΧΙ! ΣΥΓΝΩΜΗ.
Κολιάντρα|2|False|ΧΕ ΧΕ! ΑΛΗΘΕΙΑ! ΚΑΘΟΛΟΥ ΑΣΤΕΙΟ!
Πιπερούλα|8|True|ΧΑ...
Πιπερούλα|9|True|ΧΑ...
Πιπερούλα|10|False|ΧΑ.
Κολιάντρα|11|True|Ο, ΟΧΙ!
Κολιάντρα|12|False|Η ΡΟΜΠΑ ΜΟΥ ΓΙΑ ΤΗ ΓΙΟΡΤΗ ΚΑΤΑΣΤΡΑΦΗΚΕ! Ο ΡΑΦΤΗΣ ΜΟΥ ΘΑ ΓΙΝΕΙ ΕΞΩ ΦΡΕΝΩΝ!
Κολιάντρα|13|True|ΚΑΚΟ ΡΟΜΠΟΤ!
Κολιάντρα|14|False|ΘΑ ΦΤΙΑΞΩ ΤΟ ΣΦΑΛΜΑ ΣΟΥ ΜΟΝΙΜΑ!
Πιπερούλα|15|False|ΠΕΡΙΜΕΝΕ!!!
Ήχος|3|True|Μ
Ήχος|4|True|Π|nowhitespace
Ήχος|5|True|Ζ|nowhitespace
Ήχος|6|True|Ζ|nowhitespace
Ήχος|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπερούλα|1|False|ΕΧΩ ΜΙΑ ΙΔΕΑ ΠΩΣ ΝΑ ΧΡΗΣΙΜΟΠΟΙΗΣΟΥΜΕ ΑΥΤΟ ΤΟ ΣΦΑΛΜΑ.
Ήχος|2|False|Τινκ!
Αφηγητής|3|False|ΤΟ ΒΡΑΔΥ...
Κολιάντρα|11|False|ΠΙΠΕΡΟΥΛΑ, ΕΙΣΑΙ ΙΔΙΟΦΥΪΑ!
Πιπερούλα|12|True|ΟΧΙ, ΕΣΥ ΕΙΣΑΙ
Πιπερούλα|13|False|ΕΙΝΑΙ Η ΕΦΕΥΡΕΣΗ ΣΟΥ!
Ρομπότ-Ψυχολόγος|14|False|ΕΠΟΜΕΝΟΣ, ΠΑΡΑΚΑΛΩ.
Ρομπότ-Ψυχολόγος|15|True|ΓΕΙΑ ΣΑΣ,
Ρομπότ-Ψυχολόγος|16|False|ΣΚΕΔΙΑΖΕΤΕ ΜΙΑ ΔΟΛΟΦΟΝΙΑ ΑΠΟΨΕ;
Αφηγητής|17|False|ΣΥΝΕΧΙΖΕΤΕ..
Ήχος|6|True|Μ
Ήχος|7|True|Π|nowhitespace
Ήχος|8|True|Ζ|nowhitespace
Ήχος|9|True|Ζ|nowhitespace
Ήχος|10|False|!|nowhitespace
Κείμενο|4|True|Η ΣΤΕΨΗ ΤΗΣ ΜΕΓΑΛΕΙΟΤΑΤΗΣ
Κείμενο|5|False|ΒΑΣΙΛΙΣΣΑ ΚΟΛΙΑΝΤΡΑ

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπερούλα|5|True|Μπορείτε και εσεις να συνεισφέρετε στο έργο και να δείτε το όνομά σας εδώ!
Πιπερούλα|3|True|Η Πιπερούλα και Ο Καροτούλης είναι εντελώς δωρεάν (ελεύθερο), ανοιχτού κώδικα και υπάρχει χάρη στην υποστήριξη των αναγνωστών του.
Πιπερούλα|4|False|Για αυτό το επεισόδιο, ευχαριστούμε τους 1060 συνεισφέροντες:
Πιπερούλα|7|True|ΤΣΕΚΑΡΕ www.peppercarrot.com ΓΙΑ ΠΕΡΙΣΣΟΤΕΡΑ!
Πιπερούλα|6|True|Βρισκομαστε σε Patreon, Tipeee, PayPal, Liberapay ...και πολλά αλλα!
Πιπερούλα|8|False|ΕΥΧΑΡΙΣΤΟΥΜΕ!
Πιπερούλα|2|True|ΤΟ ΗΞΕΡΕΣ;
Συντελεστές|1|False|ΟΚΤΩΒΡΙΟΣ 31, 2018 ΣΧΕΔΙΟ & ΣΕΝΑΡΙΟ: David Revoy. ΑΝΑΓΝΩΣΤΕΣ ΠΡΟΕΚΔΟΣΗΣ: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna,Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. ΕΛΛΗΝΙΚΗ ΕΚΔΟΣΗ ΜΕΤΑΦΡΑΣΤΗΣ: ELIAS FOTOMELODIOS. ΒΑΣΙΣΜΕΝΟ ΣΤΟ ΚΟΣΜΟ ΤΟΥ ΕΡΕΒΑ ΔΗΜΙΟΥΡΓΟΣ: David Revoy. ΚΥΡΙΟΣ ΣΥΝΤΗΡΗΤΗΣ: Craig Maloney. ΣΥΓΓΡΑΦΕΙΣ: Craig Maloney, Nartance, Scribblemaniac, Valvin. ΔΙΟΡΘΩΤΕΣ: Willem Sonke, Moini, Hali, CGand, Alex Gryson. ΛΟΓΙΣΜΙΚΟ: Krita 4.2-dev, Inkscape 0.92.3 σε Kubuntu 18.04. ΑΔΕΙΑ: Creative Commons Attribution 4.0. www.peppercarrot.com
