# Transcript of Pepper&Carrot Episode 27 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 27: Coriander's Invention

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Qualicity in the afternoon
Tailor|4|False|Please, Princess Coriander...
Tailor|5|False|Would you stop moving so much?
Coriander|6|False|Alright, alright...
Tailor|7|False|We are running out of time and your coronation ceremony starts in three hours.
Coriander|8|False|...
Writing|2|True|HER MAJESTY'S CORONATION
Writing|3|False|QUEEN CORIANDER

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|You know...
Coriander|2|False|I was thinking about the security again...
Coriander|3|True|So many young kings and queens were assassinated during their coronation ceremony.
Coriander|4|False|It has become part of my family's history.
Coriander|5|True|And now, in a couple of hours, we will receive thousands of guests from all over the world and I'll be at the center of attention.
Pepper|7|True|Don't worry, Coriander.
Coriander|6|False|I'm really, really afraid!
Pepper|8|True|We'll follow our plan.
Pepper|9|False|Shichimi and I will be your bodyguards, remember?
Shichimi|10|True|Exactly.
Shichimi|11|False|You should just take a break and think about something else for now.
Coriander|12|False|OK.
Coriander|13|False|Hey! Why don't I show you my latest invention?
Tailor|14|False|...
Coriander|15|True|It's downstairs in my workshop.
Coriander|16|False|Follow me.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|False|You'll soon understand why I wanted to build this new one.
Coriander|2|False|It has to do with my worries about the coronation.
Coriander|3|False|Let me introduce you to my new invention...
Psychologist-Bot|5|False|Hello world.
Psychologist-Bot|6|False|I'm your PSYCHOLOGIST-BOT bleep, bloop!
Psychologist-Bot|7|False|Please lie down on my couch for a free examination.
Sound|4|False|Click!
Sound|8|False|Tap!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|So, what do you think?
Coriander|2|False|Do you want to try it?
Pepper|3|True|Bah.
Pepper|4|True|No need.
Pepper|5|False|I don't have any psychological issues.
Sound|6|True|B|nowhitespace
Pepper|11|False|HEY, WHAT'S WRONG WITH YOUR ROBOT?!
Coriander|12|True|Sorry!
Coriander|13|True|It still has this bug that I can't solve.
Coriander|14|False|It gives off an electric shock as soon as it detects a lie.
Pepper|15|False|A lie?!
Pepper|16|False|But I've never told a lie in my entire LIFE!
Pepper|22|True|THAT'S...
Pepper|23|True|NOT...
Pepper|24|False|FUNNY!!!
Sound|7|True|Z|nowhitespace
Sound|8|True|Z|nowhitespace
Sound|9|True|Z|nowhitespace
Sound|10|False|!|nowhitespace
Sound|17|True|B
Sound|18|True|Z|nowhitespace
Sound|19|True|Z|nowhitespace
Sound|20|True|Z|nowhitespace
Sound|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Funny? No, of course not! Sorry.
Coriander|2|False|He he! True! Not funny at all!
Pepper|8|True|Ha...
Pepper|9|True|Ha...
Pepper|10|False|Ha.
Coriander|11|True|OH NO!
Coriander|12|False|My robe for the ceremony is ruined! My tailor will be furious!
Coriander|13|True|Bad robot!
Coriander|14|False|I'll fix your bug, permanently!
Pepper|15|False|WAIT!!!
Sound|3|True|B
Sound|4|True|Z|nowhitespace
Sound|5|True|Z|nowhitespace
Sound|6|True|Z|nowhitespace
Sound|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|I have an idea on how we could use this bug.
Sound|2|False|Ting!
Narrator|3|False|In the evening...
Coriander|11|False|Pepper, you're a genius!
Pepper|12|True|No, you are.
Pepper|13|False|It's your invention!
Psychologist-Bot|14|False|Next, please.
Psychologist-Bot|15|True|Hello,
Psychologist-Bot|16|False|are you planning an assassination tonight?
Narrator|17|False|TO BE CONTINUED...
Sound|6|True|B
Sound|7|True|Z|nowhitespace
Sound|8|True|Z|nowhitespace
Sound|9|True|Z|nowhitespace
Sound|10|False|!|nowhitespace
Writing|4|True|HER MAJESTY'S CORONATION
Writing|5|False|QUEEN CORIANDER

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|3|True|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 1060 patrons!
Pepper|7|True|Check www.peppercarrot.com for more info!
Pepper|6|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|8|False|Thank you!
Pepper|2|True|Did you know?
Credits|1|False|October 31, 2018 Art & scenario: David Revoy. Beta readers: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. English version Proofreading: Calimeroteknik, Craig Maloney, Midgard, Martin Disch. Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.2-dev, Inkscape 0.92.3 on Kubuntu 18.04. License: Creative Commons Attribution 4.0. www.peppercarrot.com
