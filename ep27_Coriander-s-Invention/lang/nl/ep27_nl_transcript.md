# Transcript of Pepper&Carrot Episode 27 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 27: Korianders uitvinding

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|Kwalistad, ’s middags
Kleermaker|4|False|Alstublieft, Prinses Koriander …
Kleermaker|5|False|Kunt u even stil blijven staan?
Koriander|6|False|Ja, goed …
Kleermaker|7|False|We hebben niet veel tijd meer; uw kroningsceremonie begint over drie uur.
Koriander|8|False|...
Geschrift|2|True|KRONING VAN HARE MAJESTEIT
Geschrift|3|False|KONINGIN KORIANDER

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Weet je …
Koriander|2|False|Ik was weer aan het nadenken over de beveiliging …
Koriander|3|True|Zo veel jonge koningen en koninginnen zijn vermoord tijdens hun kroning.
Koriander|4|False|Dat is al meer dan eens gebeurd in mijn eigen familie.
Koriander|5|True|Over een paar uur ontvangen we duizenden gasten van over heel de wereld, en ik zal in het middelpunt van de belangstelling staan.
Pepper|7|True|Maak je geen zorgen, Koriander.
Koriander|6|False|Ik ben zo bang!
Pepper|8|True|We volgen ons plan.
Pepper|9|False|Shichimi en ik zullen je lijfwachten zijn, weet je nog?
Shichimi|10|True|Precies.
Shichimi|11|False|Ontspan je even, denk aan iets anders.
Koriander|12|False|Oké.
Koriander|13|False|Zal ik anders eens mijn nieuwste uitvinding laten zien?
Kleermaker|14|False|...
Koriander|15|True|Het is beneden in mijn atelier.
Koriander|16|False|Kom maar mee.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|False|Je zult meteen zien waarom ik deze gemaakt heb.
Koriander|2|False|Het heeft alles te maken met mijn zorgen over de kroning.
Koriander|3|False|Mag ik jullie voorstellen aan mijn uitvinding?
Psycholoog-Bot|5|False|Hello, world.
Psycholoog-Bot|6|False|Ik ben je PSYCHOLOOG-BOT bliep, bliep!
Psycholoog-Bot|7|False|Neem plaats op mijn bank voor een gratis sessie.
Geluid|4|False|Klik!
Geluid|8|False|Tik!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|En, wat denk je?
Koriander|2|False|Wil je het uitproberen?
Pepper|3|True|Goh.
Pepper|4|True|Niet nodig.
Pepper|5|False|Ik heb geen psychologische problemen.
Geluid|6|True|B|nowhitespace
Pepper|11|False|HÉ, WAT IS ER MIS MET DIE ROBOT VAN JOU?!
Koriander|12|True|Sorry!
Koriander|13|True|Er is nog een bug die ik niet kon oplossen.
Koriander|14|False|De bot geeft je een elektrische schok als je liegt.
Pepper|15|False|Als je liegt?!
Pepper|16|False|Maar ik heb nog nooit gelogen in mijn hele leven!
Pepper|22|True|DA’S …
Pepper|23|True|NIET …
Pepper|24|False|GRAPPIG !!!
Geluid|7|True|Z|nowhitespace
Geluid|8|True|Z|nowhitespace
Geluid|9|True|T|nowhitespace
Geluid|10|False|!|nowhitespace
Geluid|17|True|B
Geluid|18|True|Z|nowhitespace
Geluid|19|True|Z|nowhitespace
Geluid|20|True|T|nowhitespace
Geluid|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Grappig? Nee, natuurlijk niet! Sorry.
Koriander|2|False|Hè, hè! Klopt! Helemaal niet grappig!
Pepper|8|True|Ha …
Pepper|9|True|Ha …
Pepper|10|False|Ha.
Koriander|11|True|OH NEE!
Koriander|12|False|Mijn mantel voor de plechtigheid is geruïneerd! Mijn kleermaker zal razend zijn!
Koriander|13|True|Dekselse robot!
Koriander|14|False|Wacht maar, ik zal die bug eens vlug oplossen!
Pepper|15|False|STOP !!!
Geluid|3|True|B
Geluid|4|True|Z|nowhitespace
Geluid|5|True|Z|nowhitespace
Geluid|6|True|T|nowhitespace
Geluid|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ik heb een ideetje hoe die bug van pas zou kunnen komen.
Geluid|2|False|Tonk!
Verteller|3|False|Die avond …
Koriander|11|False|Pepper, je bent een genie!
Pepper|12|True|Nee, jij bent het genie.
Pepper|13|False|Het is jouw uitvinding!
Psycholoog-Bot|14|False|Volgende, graag.
Psycholoog-Bot|15|True|Hallo,
Psycholoog-Bot|16|False|bent u van plan een moord te plegen vanavond?
Verteller|17|False|WORDT VERVOLGD …
Geluid|6|True|B
Geluid|7|True|Z|nowhitespace
Geluid|8|True|Z|nowhitespace
Geluid|9|True|T|nowhitespace
Geluid|10|False|!|nowhitespace
Geschrift|4|True|KRONING VAN HARE MAJESTEIT
Geschrift|5|False|KONINGIN KORIANDER

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|3|True|Pepper&Carrot is helemaal vrij, open-bron en gesponsord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik 1060 patronen!
Pepper|7|True|Kijk op www.peppercarrot.com voor alle info!
Pepper|6|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay … en meer!
Pepper|8|False|Dank je!
Pepper|2|True|Wist je dat?
Aftiteling|1|False|31 oktober 2018 Tekeningen & verhaal: David Revoy. Beta-feedback: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. Nederlandstalige versie Vertaling: Midgard. Proeflezing: Willem Sonke en Marno van der Maas. Gebaseerd op het universum van Hereva Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.2-dev, Inkscape 0.92.3 op Kubuntu 18.04. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
