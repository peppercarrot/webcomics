# Transcript of Pepper&Carrot Episode 27 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 27: El invento de Celandria

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Caliciudad, por la tarde
Sastre|4|False|Por favor, Princesa Celandria...
Sastre|5|False|¿Podríais dejar de moveros tanto?
Celandria|6|False|Vale, está bien...
Sastre|7|False|Nos queda poco tiempo y la coronación empieza dentro de tres horas.
Celandria|8|False|...
Escritura|2|True|CORONACIÓN DE SU MAJESTAD
Escritura|3|False|REINA CELANDRIA

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Celandria|1|True|Sabéis...
Celandria|2|False|He estado pensando en la seguridad de nuevo...
Celandria|3|True|Tantas jóvenes reinas y reyes han sido asesinados durante su ceremonia de coronación.
Celandria|4|False|Forma parte de mi historia familiar.
Celandria|5|True|Y ahora, en unas pocas horas, recibiremos miles de invitados procedentes del mundo entero y yo seré el centro de atención.
Pimienta|7|True|No te preocupes, Celandria.
Celandria|6|False|¡Tengo mucho, mucho miedo!
Pimienta|8|True|Seguiremos nuestro plan.
Pimienta|9|False|Shichimi y yo seremos tus guardaespaldas, ¿recuerdas?
Shichimi|10|True|Exacto.
Shichimi|11|False|Deberías hacer una pausa y pensar en otra cosa por ahora.
Celandria|12|False|OK.
Celandria|13|False|¡Eh! ¿Y si os enseño mi último invento?
Sastre|14|False|...
Celandria|15|True|Está aquí abajo, en mi taller.
Celandria|16|False|Seguidme.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Celandria|1|False|Vais a entender enseguida por qué quería construir esto.
Celandria|2|False|Tiene que ver con mi preocupación por la coronación.
Celandria|3|False|Dejadme presentaros mi último invento...
Robot psicólogo|5|False|¡Hola, mundo!
Robot psicólogo|6|False|Yo soy su ROBOT PSICÓLOGO. ¡Bip, Bip!
Robot psicólogo|7|False|¡Clic!
Sonido|4|False|¡Tap!
Sonido|8|False|Por favor, túmbese sobre el diván para una sesión gratuita.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Celandria|1|True|¿Entonces, qué me decís?
Celandria|2|False|¿Queréis probarlo?
Pimienta|3|True|Bah.
Pimienta|4|True|No lo necesito.
Pimienta|5|False|Yo no tengo ningún problema psicológico.
Sonido|6|True|¡B|nowhitespace
Pimienta|11|False|¡¿EH, QUÉ LE PASA A ESTE TRASTO?!
Celandria|12|True|¡Lo siento!
Celandria|13|True|No consigo corregir ese fallo.
Celandria|14|False|El robot manda una descarga eléctrica en cuanto detecta que le mienten.
Pimienta|15|False|¡¿Mentir?!
Pimienta|16|False|¡Si yo no he mentido en toda mi VIDA!
Pimienta|22|True|¡¡¡NO...
Pimienta|23|True|TIENE...
Pimienta|24|False|GRACIA!!!
Sonido|7|True|Z|nowhitespace
Sonido|8|True|Z|nowhitespace
Sonido|9|True|Z|nowhitespace
Sonido|10|False|!|nowhitespace
Sonido|17|True|¡B
Sonido|18|True|Z|nowhitespace
Sonido|19|True|Z|nowhitespace
Sonido|20|True|Z|nowhitespace
Sonido|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|¿Eh? No, si no me estoy riendo. Lo siento.
Celandria|2|False|¡Jajaja! Es cierto. No tiene ninguna gracia.
Pimienta|8|True|Ja...
Pimienta|9|True|Ja...
Pimienta|10|False|Ja.
Celandria|11|True|¡OH, NO!
Celandria|12|False|¡Mi vestido para la ceremonia está destrozado! ¡Mi sastre se va a cabrear!
Celandria|13|True|¡Maldito robot!
Celandria|14|False|¡Te voy a arreglar el fallo, pero para siempre!
Pimienta|15|False|¡¡¡ESPERA!!!
Sonido|3|True|¡B
Sonido|4|True|Z|nowhitespace
Sonido|5|True|Z|nowhitespace
Sonido|6|True|Z|nowhitespace
Sonido|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Tengo una idea de como podríamos aprovechar ese fallo.
Sonido|2|False|¡Ting!
Narrador|3|False|Por la noche...
Celandria|11|False|¡Pimienta, qué idea genial!
Pimienta|12|True|No, la idea es tuya.
Pimienta|13|False|¡Es tu invento!
Robot psicólogo|14|False|El siguiente, por favor.
Robot psicólogo|15|True|Buenas noches,
Robot psicólogo|16|False|¿Planea usted un asesinato esta noche?
Narrador|17|False|CONTINUARÁ...
Sonido|6|True|¡B
Sonido|7|True|Z|nowhitespace
Sonido|8|True|Z|nowhitespace
Sonido|9|True|Z|nowhitespace
Sonido|10|False|!|nowhitespace
Escritura|4|True|CORONACIÓN DE SU MAJESTAD
Escritura|5|False|REINA CELANDRIA

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|5|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|3|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 1060 mecenas!
Pimienta|7|True|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|6|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|8|False|¡Gracias!
Pimienta|2|True|¿Sabías qué?
Créditos|1|False|31 de octubre de 2018 Arte & guion: David Revoy. Lectores de la versión beta: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. Versión en castellano Traducción: TheFaico. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.2-dev, Inkscape 0.92.3 en Kubuntu 18.04. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
