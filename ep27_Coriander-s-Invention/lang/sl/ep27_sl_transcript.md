# Transcript of Pepper&Carrot Episode 27 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 27: Koriandrikin izum

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|Popoldne v mestu Kvaliteja
Krojač|4|False|Prosim vas, princesa Koriandrika …
Krojač|5|False|Bi se, prosim, nehali toliko premikati?
Koriandrika|6|False|Že prav, že prav …
Krojač|7|False|Zmanjkuje nam časa. Vaše kronanje se bo pričelo čez tri ure!
Koriandrika|8|False|...
Napis|2|True|KRONANJE NJENEGA VELIČANSTVA
Napis|3|False|KRALJICE KORIANDRIKE

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandrika|1|True|No …
Koriandrika|2|False|Spet sem razmišljala o varnostnih ukrepih …
Koriandrika|3|True|Ogromno mladih kraljev in kraljic je umrlo med svojim kronanjem.
Koriandrika|4|False|To je celo postal del moje družinske zgodovine.
Koriandrika|5|True|In čez nekaj ur se bo tukaj trlo obiskovalcev z vsega sveta in jaz bom v središču pozornosti.
Paprika|7|True|Ne skrbi, Koriandrika.
Koriandrika|6|False|Na smrt me je strah!
Paprika|8|True|Saj imamo načrt.
Paprika|9|False|S Šičimi bova tvoji telesni stražarki, se spomniš?
Šičimi|10|True|Tako je.
Šičimi|11|False|Malo se sprosti in razmišljaj o čem drugem.
Koriandrika|12|False|Prav.
Koriandrika|13|False|Hej! Naj vama pokažem svoj najnovejši izum!
Krojač|14|False|...
Koriandrika|15|True|Spodaj v moji delavnici je.
Koriandrika|16|False|Pridita.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandrika|1|False|Kmalu vama bo jasno, čemu je namenjen.
Koriandrika|2|False|Izdelala sem ga zaradi svojih skrbi glede kronanja.
Koriandrika|3|False|Naj vama predstavim svoj novi izum …
Robot-psiholog|5|False|Pozdravljen, svet!
Robot-psiholog|6|False|Sem vaš ROBOT-PSIHOLOG. Blip, blop!
Robot-psiholog|7|False|Prosim, ulezite se na kavč in pričnite z brezplačno terapijo.
Zvok|4|False|Klik!
Zvok|8|False|Tap!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandrika|1|True|No, kako se vama zdi?
Koriandrika|2|False|Ga bosta preizkusili?
Paprika|3|True|Pha.
Paprika|4|True|Ni potrebe.
Paprika|5|False|Nimam psiholoških težav.
Zvok|6|True|B|nowhitespace
Paprika|11|False|HEJ, KAJ JE NAROBE S TVOJIM ROBOTOM?!
Koriandrika|12|True|Oprosti!
Koriandrika|13|True|Robot ima napako, ki je še ne znam rešiti.
Koriandrika|14|False|Kadar zazna laž, sproži električni šok.
Paprika|15|False|Laž?!
Paprika|16|False|Ampak jaz se v ŽIVLJENJU nisem zlagala!
Paprika|22|True|TO
Paprika|23|True|NI
Paprika|24|False|SMEŠNO!!!
Zvok|7|True|Z|nowhitespace
Zvok|8|True|Z|nowhitespace
Zvok|9|True|Z|nowhitespace
Zvok|10|False|!|nowhitespace
Zvok|17|True|B
Zvok|18|True|Z|nowhitespace
Zvok|19|True|Z|nowhitespace
Zvok|20|True|Z|nowhitespace
Zvok|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|False|Smešno? Ne, seveda ni! Oprosti.
Koriandrika|2|False|Hi hi! Res, res, sploh ni smešno!
Paprika|8|True|Ha.
Paprika|9|True|Ha.
Paprika|10|False|Ha.
Koriandrika|11|True|O NE!
Koriandrika|12|False|Moja slavnostna obleka je uničena! Moj krojač bo ob pamet!
Koriandrika|13|True|Ti kup pločevine!
Koriandrika|14|False|Čakaj, da izbijem to napako iz tebe!
Paprika|15|False|STOJ!!!
Zvok|3|True|B
Zvok|4|True|Z|nowhitespace
Zvok|5|True|Z|nowhitespace
Zvok|6|True|Z|nowhitespace
Zvok|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Že vem, kako lahko uporabimo to napako sebi v prid.
Zvok|2|False|Ting!
Pripovedovalec|3|False|Zvečer …
Koriandrika|11|False|Paprika, ti si genij!
Paprika|12|True|Ne, to si ti.
Paprika|13|False|Izum je vendar tvoj!
Robot-psiholog|14|False|Naslednji, prosim.
Robot-psiholog|15|True|Pozdravljeni,
Robot-psiholog|16|False|ste za današnji večer načrtovali atentat?
Pripovedovalec|17|False|SE NADALJUJE …
Zvok|6|True|B
Zvok|7|True|Z|nowhitespace
Zvok|8|True|Z|nowhitespace
Zvok|9|True|Z|nowhitespace
Zvok|10|False|!|nowhitespace
Napis|4|True|KRONANJE NJENEGA VELIČANSTVA
Napis|5|False|KRALJICE KORIANDRIKE

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|3|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 1060 podpornikov!
Paprika|7|True|Obišči www.peppercarrot.com za več informacij!
Paprika|6|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|8|False|Hvalaaaa!
Paprika|2|True|Že veš?
Zasluge|1|False|31. oktober 2018 Piše in riše: David Revoy. Testni bralci: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac, Valvin. Popravki: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programska oprema: Krita 4.2-dev in Inkscape 0.92.3 na Kubuntu 18.04. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
