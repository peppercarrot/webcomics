# Transcript of Pepper&Carrot Episode 18 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 18: De ontmoeting

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Zo, je mag kijken!
Pepper|2|True|TA~DAA~~A !|nowhitespace
Pepper|3|True|De outfit van een heks van Hippiah!
Pepper|4|False|Dit is de enige die ze nog hadden in de winkel, dus in afwachting van mijn nieuwe kleren heb ik hem maar meegenomen!
Pepper|7|False|Nou, doet het je nergens aan denken?
Geschrift|6|False|HIPPIAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geschrift|1|False|School voor Heksen van Hippiah
Geschrift|3|False|KEUKENS
Carrot|2|False|Rommel
Basilic|4|False|Oregano, goed.
Basilic|5|False|Kardemom, goed.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Basilic|1|False|Kaneel, zeer goed.
Basilic|2|False|Kamille, goed.
Basilic|3|False|Pepper?!
Pepper|4|False|Maar ik ...
Basilic|5|True|Alweer verprutst!!!
Basilic|6|False|Ik herinner je eraan dat Hippiah geen magie is om onkruidverdelgers te maken! ...
Oregano|7|False|HA HAHA HA!
Kardemom|8|False|HA HA!
Kaneel|9|False|HA HA!
Kamille|10|False|HA HAHA HA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oregano|1|False|HAHA HA!
Kardemom|2|False|HoHo Ho!
Kaneel|9|False|Pffft!
Kamille|10|False|HAHA HA HA!
Pepper|3|True|Hou OP
Pepper|4|True|!!!
Pepper|5|True|Het is niet mijn fout!!!
Pepper|6|True|Hou toch
Pepper|7|True|OP
Pepper|8|False|!!!
Kardemom|12|False|HAHA HA HA!
Oregano|11|False|HoHo Ho!
Kamille|14|False|HAHA HA HA!
Kaneel|15|False|HAHA HA!!
Pepper|13|False|Grr ... Hou op ... Hou ...
Pepper|16|True|Ik zei...
Pepper|17|False|HOU OP!!!
Geluid|18|False|BAMMM!!!
Geluid|19|False|KRATSJ!!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Ahum ...
Cayenne|2|False|Als u wilt, kunnen wij haar onder onze hoede nemen ...
Cayenne|3|False|We zijn juist op zoek naar een student voor Chaosah ...
Thym|4|False|... en zij daar lijkt me de ideale kandidaat.
Basilic|5|True|Cayenne?!
Basilic|6|True|Thym?! Cumin?!
Basilic|7|False|Maar ik dacht dat jullie allemaal ... dat kan niet!
Geschrift|8|False|KEUKENS

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Roetsj!
Geluid|2|False|Pof!
Pepper|3|False|Hi hi hi!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Volgens mij kunnen wij wel met elkaar opschieten!
Pepper|2|False|... en ik heb al een ideetje hoe ik je ga noemen!
Pepper|3|False|En? Waar doet het je aan denken?!
Carrot|4|False|Groeee?
Geschrift|5|False|HIPPIAH
Verteller|6|False|- EINDE -
Aftiteling|7|False|08/2016 - www.peppercarrot.com - Tekeningen & verhaal: David Revoy - Vertaling: Willem Sonke, Midgard en Marno van der Maas.
Aftiteling|8|False|Scenario geïnspireerd op twee scenarios voorgesteld door Craig Maloney: "You found me, I Choose You" en "Visit from Hippiah".
Aftiteling|9|False|Gebaseerd op het universum van Hereva gecreëerd door David Revoy met bijdragen van Craig Maloney. Verbeteringen door Willem Sonke, Moini, Hali, CGand en Alex Gryson.
Aftiteling|10|False|Licentie: Creative Commons Naamsvermelding 4.0, Software: Krita 3.0, Inkscape 0.91 op Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 720 patronen:
Aftiteling|2|False|Jij kan ook een patroon van Pepper&Carrot worden op https://www.patreon.com/davidrevoy
