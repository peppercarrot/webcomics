# Transcript of Pepper&Carrot Episode 18 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 18: El encuentro

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Vale, ya puedes mirar!
Pimienta|2|True|¡TAA~CHÁAAN!
Pimienta|3|True|¡Un uniforme de bruja de Hippiah!
Pimienta|4|False|¡Solo había esto en la tienda, así que lo he tomado para llevarlo mientras que no tenga mi ropa nueva!
Pimienta|7|False|...¿y qué me dices? ¿No te trae recuerdos?...
Escritura|6|False|HIPPIAH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|1|False|Escuela de Brujas de Hippiah
Escritura|3|False|COCINAS
Zanahoria|2|False|Groo
Basilic|4|False|Orégano, bien.
Basilic|5|False|Cardamomo, bien.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Basilic|1|False|Canela, muy bien.
Basilic|2|False|Manzanilla, bien.
Basilic|3|False|¡¿Pimienta?!
Pimienta|4|False|Pero yo...
Basilic|5|True|¡¡¡Otra vez mal!!!
Basilic|6|False|¡Le recuerdo que la magia de Hippiah no es para hacer herbicidas!...
Orégano|7|False|¡JAJAJAJAJA!
Cardamomo|8|False|¡JAJAJA!
Canela|9|False|¡JAJAJA!
Manzanilla|10|False|¡JAJAJAJAJAJA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Orégano|1|False|¡JAJAJAJA!
Cardamomo|2|False|¡JIJIJIJI!
Canela|9|False|¡Pffffff!
Manzanilla|10|False|¡JAJAJAJA!
Pimienta|3|True|¡¡¡ BASTA
Pimienta|4|True|!!!
Pimienta|5|True|¡¡¡ No es culpa mía !!!
Pimienta|6|True|Pero...
Pimienta|7|True|¡¡¡ PARAD
Pimienta|8|False|!!!
Cardamomo|12|False|¡JAJAJAJAJA!
Orégano|11|False|¡JIJIJIJI!
Manzanilla|14|False|¡JAJAJAJAJA!
Canela|15|False|¡¡JAJAJAJA!!
Pimienta|13|False|Gññ... Basta... Ya vale...
Pimienta|16|True|¡¡¡He dicho ...
Pimienta|17|False|QUE PARÉIS!!!
Sonido|18|False|¡¡¡ BAAAM !!!
Sonido|19|False|¡¡ CRACK !!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|1|True|¡Ejem, ejem!...
Cayena|2|False|Si usted quiere, nos podemos ocupar nosotras de ella...
Cayena|3|False|Precisamente buscamos una alumna en Chaosah...
Tomillo|4|False|...y esta parece ser la candidata ideal.
Basilic|5|True|¡¿Cayena?!
Basilic|6|True|¡¿Tomillo?! ¡¿Comino?!
Basilic|7|False|Creía que estábais... ...¡es imposible!
Escritura|8|False|COCINAS

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡Suiiip!
Sonido|2|False|¡Poc!
Pimienta|3|False|¡Jijijiji!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Algo me dice que nos vamos a entender bien tú y yo!
Pimienta|2|False|...¡y creo que tengo una idea de qué nombre ponerte!
Pimienta|3|False|¿Entonces, qué? ¡¿A qué te recuerda esto?!
Zanahoria|4|False|¿Groo?
Escritura|5|False|HIPPIAH
Narrador|6|False|- FIN -
Créditos|7|False|Agosto 2016 - www.peppercarrot.com - Dibujo & Guión: David Revoy - Traducción: TheFaico
Créditos|8|False|Guion inspirado a partir de dos guiones propuestos por Craig Maloney: "You found me, I Choose You" y "Visit from Hippiah".
Créditos|9|False|Basado en el universo de Hereva, creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Créditos|10|False|Licencia: Creative Commons Attribution 4.0. Software: Krita 3.0 e Inkscape 0.91 en Manjaro XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto y patrocinado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 720 mecenas:
Créditos|2|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
