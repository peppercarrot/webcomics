# Transcript of Pepper&Carrot Episode 28 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 28: Svečanost

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|True|Tiste noči so se vse tri Herevine lune poravnale …
Pripovedovalec|2|False|… in njihova svetloba je poskrbela za veličasten prizor v zombijaški katedrali.
Pripovedovalec|3|False|Pod to čarobno svetlobo je moja prijateljica Koriandrika postala …
Pripovedovalec|4|False|Kraljica Kvaliteje.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|Sledilo je praznovanje.
Pripovedovalec|2|False|Ogromna zabava, na katero so prišle vse čarovniške šole in na stotine imenitnih gostov z vse Hereve.
Paprika|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Ah! Ti si!
Paprika|3|False|Odplavala sem z mislimi.
Korenček|2|False|drg drg
Paprika|4|False|Shladilo se je. Raje grem noter.
Novinar|6|False|Brž, brž! Je že tukaj!
Novinar|5|False|Hitro! Ona je!
Novinar|7|False|Gospodična Žafranka! Lahko poveste nekaj besed za Kvalitejski dnevnik?
Žafranka|8|False|Da, seveda!
Paprika|15|False|...
Paprika|16|False|Vidiš Korenček? Mislim, da sem zato slabe volje.
Zvok|9|False|BLISK
Novinar|13|True|Gospodična Žafranka! Herevina modna revija.
Zvok|10|False|BLISK
Zvok|11|False|BLISK
Zvok|12|False|BLISK
Novinar|14|False|Čigav kroj nosite danes?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Vse moje prijateljice so uspešne. Včasih si želim, da bi imela vsaj delček tega, kar imajo one.
Paprika|2|False|Koriandrika je kraljica.
Paprika|3|False|Žafranka je bogata in slavna.
Paprika|4|False|Celo Šičimi je videti popolna ob svoji šoli.
Paprika|5|False|In jaz? Kaj imam pa jaz?
Paprika|6|False|Tole.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|False|Paprika?
Šičimi|2|False|Pritihotapila sem nekaj hrane. Bi ti tudi malo?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|True|Prav smešno. Skriti se moram samo zato, da lahko nekaj malega pojem.
Šičimi|2|False|Ampak naša učiteljica pravi, da moramo dajati vtis nadnaravnih bitij brez osnovnih človeških potreb.
Žafranka|3|False|A, tukaj sta!
Žafranka|4|True|Končno kotiček brez fotografov.
Žafranka|5|False|Zaradi njih se mi bo še zmešalo.
Koriandrika|6|False|Fotografi?
Koriandrika|7|False|Poskusi se izogniti dolgočasnemu klepetu s politiki, medtem ko ti tole tiči na glavi.
Zvok|8|False|prask prask

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandrika|1|True|Aja, Paprika, končno sem spoznala tvoje botre.
Koriandrika|2|False|Res so nekaj posebnega.
Paprika|3|False|!!!
Koriandrika|4|True|Mislim, da imaš veliko srečo.
Koriandrika|5|False|Dovolijo ti početi karkoli.
Koriandrika|6|False|Suhoparneže lahko pošlješ v tri krasne, ne da bi to imelo diplomatske posledice.
Žafranka|7|False|Lahko plešeš in rajaš, ne da bi te skrbelo za mnenje drugih.
Šičimi|8|False|Ali pa poskusiš vso hrano na mizi brez skrbi, da te bo kdo videl.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|4|False|SE NADALJUJE …
Šičimi|1|False|Šment, Paprika!? Smo te slučajno užalile?
Paprika|2|True|Kje pa!
Paprika|3|False|Hvala vam, prijateljice!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|3|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 960 podpornikov!
Paprika|7|True|Obišči www.peppercarrot.com za več informacij!
Paprika|6|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|8|False|Hvalaaaa!
Paprika|2|True|Že veš?
Zasluge|1|False|Januar 2019 Piše in riše: David Revoy. Testni bralci: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac, Valvin. Popravki: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programska oprema: Krita 4.1.5~appimage in Inkscape 0.92.3 na Kubuntu 18.04.1. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
