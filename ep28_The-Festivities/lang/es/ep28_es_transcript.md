# Transcript of Pepper&Carrot Episode 28 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 28: Los festejos

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|True|Las tres lunas de Hereva estaban alineadas esa noche...
Narrador|2|False|y la luz que reflejaban causaba un verdadero espectáculo en la catedral de Zombiah.
Narrador|3|False|Bajo esta mágica luz fue como Celandria, mi amiga, se convirtió en...
Narrador|4|False|Reina de Caliciudad.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Los festejos comenzaron poco después.
Narrador|2|False|Una gran fiesta con todas las escuelas de magia y cientos de prestigiosos invitados de todos los lugares de Hereva.
Pimienta|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Oh! ¡Eres tú!
Pimienta|3|False|Estaba sumida en mis pensamientos.
Zanahoria|2|False|ras ras
Pimienta|4|False|Voy a entrar. Está refrescando aquí fuera.
Periodista|6|False|¡Corred! ¡Está aquí!
Periodista|5|False|¡Rápido! ¡Es ella!
Periodista|7|False|¡Señorita Azafrán! ¿Nos permite unas palabras para el Diario de Caliciudad?
Azafrán|8|False|¡Sí, por supuesto!
Pimienta|15|False|...
Pimienta|16|False|¿Lo ves, Zanahoria? Creo que entiendo por qué yo no estoy de humor.
Sonido|9|False|FLASH
Periodista|13|True|¡Señorita Azafrán! La Gaceta del Estilo de Hereva.
Sonido|10|False|FLASH
Sonido|11|False|FLASH
Sonido|12|False|FLASH
Periodista|14|False|¿De qué diseñador viste esta noche?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Todas mis amigas tienen mucho éxito y a veces yo desearía tener solo una mínima parte de lo que ellas tienen.
Pimienta|2|False|Celandria es una reina.
Pimienta|3|False|Azafrán es una superestrella rica.
Pimienta|4|False|Hasta Shichimi se ve perfecta con su escuela.
Pimienta|5|False|¿Y yo? ¿Qué tengo?
Pimienta|6|False|Esto.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|¿Pimienta?
Shichimi|2|False|He pillado alguna cosa para comer. ¿Quieres algo?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Parece raro que tenga que ir a escondidas para comer algo.
Shichimi|2|False|Pero nuestra profesora dice que debemos parecer espíritus puros sin necesidades humanas básicas.
Azafrán|3|False|¡Hey! ¡Aquí estáis!
Azafrán|4|True|¡Por fin! Un sitio donde puedo escapar de los fotógrafos.
Azafrán|5|False|Me vuelven loca.
Celandria|6|False|¿Fotógrafos?
Celandria|7|False|Intenta evitar aburridas discusiones con políticos mientras llevas esta cosa en la cabeza.
Sonido|8|False|ras ras

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Celandria|1|True|Por cierto, Pimienta, al fin he conocido a tus madrinas.
Celandria|2|False|Es cierto que son «especiales».
Pimienta|3|False|!!!
Celandria|4|True|Quiero decir... que tienes suerte.
Celandria|5|False|Estoy segura de que te dejan hacer todo lo que quieras.
Celandria|6|False|Como dejar de lado los protocolos y poses oficiales sin preocuparte por la diplomacia.
Azafrán|7|False|O bailar y divertirte sin importar lo que la gente pueda pensar.
Shichimi|8|False|¡O saborear toda la comida del buffet sin temor de que te vean!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|4|False|CONTINUARÁ...
Shichimi|1|False|¡Oh! ¿¡Pimienta!? ¿Hemos dicho algo que te haya molestado?
Pimienta|2|True|¡Para nada!
Pimienta|3|False|¡Gracias, amigas!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|5|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|3|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 960 mecenas!
Pimienta|7|True|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|6|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|8|False|¡Gracias!
Pimienta|2|True|¿Sabías qué?
Créditos|1|False|Enero de 2019 Dibujo & guion: David Revoy. Lectores de la versión beta: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, ValVin. Versión en castellano Traducción: TheFaico. Revisor: LuisR14. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 3.1.5~appimage, Inkscape 0.92.3 en Kubuntu 18.04.1. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
