# Transcript of Pepper&Carrot Episode 28 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 28: An comharrachadh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|True|Cho-thaobhaich na trì gealaichean Theireabha air an oidhche ud,
Neach-aithris|2|False|agus thug an solas sealladh iongantach dhan èisteachd ann an àrd-theampall na Sòmbaidheachd.
Neach-aithris|3|False|B’ ann fon t-solas drùiteach seo a chaidh mo charaid Costag na...
Neach-aithris|4|False|Banrigh Àird Acainn.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|Bha subhachas againn an uairsin.
Neach-aithris|2|False|Cèilidh mhòr ’s na sgoiltean buidseachais uile an làthair ’s ceudan dhe dh’aoighean uasal o fheadh Theireabha.
Peabar|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Ò! ’S tusa a th’ ann!
Peabar|3|False|Bha m’ aire fad air falbh.
Curran|2|False|slìob slìob
Peabar|4|False|Thèid mi a-steach. Tha i a’ fàs fuar a-muigh.
Neach-naidheachd|6|False|Dlùth oirre! Gu sgiobalta!
Neach-naidheachd|5|False|Greas oirbh! Seo ise!
Neach-naidheachd|7|False|A mhaighdeann Chròch, an can sibh facal no dhà do Theachdaire Àird Acainn?
Cròch|8|False|Canaidh gu dearbh!
Peabar|15|False|...
Peabar|16|False|A bheil thu agam, a Churrain? Saoilidh mi gu bheil mi a’ tuigsinn carson nach eil mi ann an deagh thrum a-nochd.
Fuaim|9|False|BOILLSG
Neach-naidheachd|13|True|A mhaighdeann Chròch! Seo Gasaet Stoidhle Theireabha.
Fuaim|10|False|BOILLSG
Fuaim|11|False|BOILLSG
Fuaim|12|False|BOILLSG
Neach-naidheachd|14|False|Cò an dealbhaiche a chuir sibh umaibh a-nochd?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Tha mo charaidean uile cho soirbheachail agus ag amannan bu mhiann leam gun robh beagan dheth agamsa cuideachd.
Peabar|2|False|Tha Costag ’na banrigh
Peabar|3|False|Tha Cròch beartach ’s cliùiteach.
Peabar|4|False|Tha fiù ’s coltas foirfe air Sidimi leis an sgoil aice.
Peabar|5|False|Agus dè mu mo dhèidhinn-sa? Dè th’ agamsa?
Peabar|6|False|Seo.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sidimi|1|False|A Pheabar?
Sidimi|2|False|Shiolp mi cuid bìdh a-mach. A bheil thu ag iarraidh gin dheth?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sidimi|1|True|Tha e a’ faireachdainn neònach gum feum mi dol am falach airson ithe.
Sidimi|2|False|Ach sparraidh an t-oide againn oirnn gum biodh coltas nam spioradan glana gun fheumalachdan daonnail oirnn.
Cròch|3|False|Hà! Sin sibh!
Cròch|4|True|Mu dheireadh thall! Lorg mi àite airson teicheadh on luchd-togail dhealbh.
Cròch|5|False|Tha iad ’gam chur às mo chiall.
Costag|6|False|Luchd-togail dhealbh?
Costag|7|False|Feuch an seachain thu deasbadan ràsanach le luchd poileataigs nuair a bhios an rud seo air do cheann.
Fuaim|8|False|tachais tachais

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Costag|1|True|Eadar dà sgeul, a Pheabar, choinnich mi ris na bana-ghoistidhean agad mu dheireadh thall.
Costag|2|False|Tha iad cianail fhèin sònraichte.
Peabar|3|False|!!!
Costag|4|True|Fhios a’d, tha thu cho fortanach.
Costag|5|False|Leigidh iad leat dèanamh na thogras tu gun teagamh.
Costag|6|False|Can, dol às àicheadh riochd is bleadraigidh fhaoin gun dragh mun dìoplomasaidh.
Cròch|7|False|No dannsa is gabhail tlachd gun diù a’ choin air beachdan chàich ort.
Sidimi|8|False|No feuchainn air a h-uile biadh air a’ bhuffet gun eagal gum faiceadh cuideigin thu!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|4|False|Ri leantainn...
Sidimi|1|False|Ò! A Pheabar!? Na thuirt sinn rudeigin a chuireadh mun cuairt thu?
Peabar|2|True|Cha duirt idir!
Peabar|3|False|Mòran taing, a chàirdean!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|3|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|4|False|Mòran taing dhan 960 pàtran a thug taic dhan eapasod seo!
Peabar|7|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|6|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|8|False|Mòran taing!
Peabar|2|True|An robh fios agad?
Urram|1|False|San Fhaoilleach 2019 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc. Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Bathar-bog: Krita 4.1.5~appimage, Inkscape 0.92.3 air Kubuntu 18.04.1. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
