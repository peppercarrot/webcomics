# Episode 28: The Festivities

![cover of episode 28](https://www.peppercarrot.com/0_sources/ep28_The-Festivities/low-res/Pepper-and-Carrot_by-David-Revoy_E28.jpg)

## Comments from the author

I'm not someone who used to be very active at parties in real life, and I like the bench in the dark, outside, listening to the music and people having fun from a distance. I probably spent myself a lot of time thinking this way during parties. That's a place I like and I hope the one who also like this moments will find that in this episode.

This episode resolves all the jealousy Pepper had about her friends and brings more story background in a subtle way; we meet Shichimi's school (and first appearance of Wasabi, her master and a character I want to reuse later), it gives a glimpse of what Coriander will have to manage, and also introduce the new personality of Saffron-superstar. This episode also erases the last bit of shame Pepper had about her own magic family. Pepper really evolved a lot and now she is ready to be a powerful witch of Chaosah:) I like when resolving of stories happens not after a fight, but when characters just change their point-of-view. 

I also wanted this episode to resolve the jealous relationship Pepper started to develop for Saffron after the magic contest.

The characters does not represents any nationality. They are collage of concept from our world and it's true for Shichimi she wear what I would call Asian ceremonial inspired clothes that are inspired by what a foreigner like me enjoyed aesthetically about Japanese culture. It inspired me exotism and mysterious rituals. The major concept that breaks this stereotype for Shichimi is her bright blond and pink eyes appearance, her nomad habits and very independant nature, and the desertic place (but still green, just not a lot of trees) she live in.

From [Beta-reading Forum of episode 28](https://framagit.org/peppercarrot/webcomics/-/issues/87) and [Author's blog of episode 28](https://www.davidrevoy.com/article660/episode-28-the-festivities/show#comments)
