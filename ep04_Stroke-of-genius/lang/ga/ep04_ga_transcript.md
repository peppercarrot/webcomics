# Transcript of Pepper&Carrot Episode 04 [ga]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 4 : Eslambrec de gèni

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|La vèlha deu concors de potinga, 2 òras deu matin...
Pepper|2|True|Totun... Dab aquera potinga arrés, quitament pas Safran, ne m'abastarà au cavilhar !
Pepper|3|False|Qu'èi sonque besonh de l'esprovar…
Pepper|4|False|CAAAAAAAA~ ~AAARROT !!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|14|False|GAT MIAM
Pepper|1|False|Carrot ?
Pepper|2|False|De mau créder ! Quitament pas devath lo lheit ?
Pepper|4|False|Carrot ?
Pepper|5|False|Carrot ?!
Pepper|6|False|Carrot ?!!!
Pepper|3|False|Carrot !
Pepper|7|False|Ne pensavi pas dever abastar aquiu e utilizar aquera tecnica ...
Pepper|8|False|Carrot !
Son|9|True|Crrr
Son|10|True|Crrr
Son|11|True|Crrr
Son|12|True|Crrr
Son|13|False|Crrr
Son|15|True|Chhh
Son|16|True|Chhh
Son|17|True|Chhh
Son|18|True|Chhh
Son|19|False|Chhh
Son|21|False|Chhh Chhh Chhh Chhh Chhh
Son|20|False|Crrr Crrr Crrr Crrr Crrr
Son|23|False|Crrr Crrr Crrr Crrr Crrr
Son|24|False|Chhh Chhh Chhh Chhh Chhh
Carròt|25|False|Gargblr
Son|26|False|Zoooooooo !
Carròt|22|False|O Sole Miauuuoo !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Òòòh, Carrot ! B'ès berogin d'estar viengut solet !
Pepper|2|False|Just com cau, au moment quan èi besonh deu men assistent estimat.
Pepper|3|True|Taaadaaaa ! Que't presenti lo men cap-d'òbra !
Pepper|4|False|La potinga deus gènis !
Pepper|5|False|Un petit horrup tot dòi…
Pepper|6|False|…e que deverés estar tustat d'un eslambrec de gèni.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Pòc
Pepper|3|False|E ben hòu... Carrot ! Qu'ei fantastic…
Pepper|5|False|qu'… …qu'escrives ?
Pepper|6|False|E ?
Pepper|7|False|Energia ?
Pepper|8|False|Eternau ?
Pepper|9|False|Emocion ?
Son|4|False|Chrrrrrrr
Son|2|False|Chrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Qué ?
Pepper|2|False|Embrumada ?
Pepper|3|False|Emanar ?
Pepper|4|False|Anem Carrot, hè un esfòrç, ne vòu pas díser arren tot aquò !
Pepper|5|False|…ne vòu pas díser arren tot aquò….
Pepper|6|True|Grrrrrrr !!!
Pepper|7|False|Arren ne va dab jo ! !
Pepper|8|True|Bò…
Pepper|9|False|Que'm vaga enqüèra tà n'aprestar ua mei bona.
Son|10|False|CLASChchChchChh !
Pepper|11|False|…bona nueit Carrot…
Narrator|12|False|a seguir...
Crèdits|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo e de mercejaments especials a Amireeti per m'aver ajudat amb las correccions anglesas !
Crèdits|1|False|Pepper&Carrot qu'ei completament liure, open source e esponsorizat mercés au mecenat deus lectors, entad aqueste episòdi, mercé aus 156 mecènas :
Crèdits|6|False|https://www.patreon.com/davidrevoy
Crèdits|5|False|Vos tanben, vadetz mecèna de Pepper&Carrot entà l'episòdi vienent :
Crèdits|9|False|Utís : Aqueste episòdi qu'estó dessenhat a 100% dab logiciaus liures Krita, G'MIC, Blender, GIMP sus Ubuntu Gnome (GNU/Linux)
Crèdits|8|False|Open-source : totas las honts, polissas d'escritura, fichèrs dab calques que son disponibles suu site oficiau au teledescargament.
Crèdits|7|False|Licéncia : Creative Commons Atribucion a 'David Revoy' que podetz modificar, tornar partatjar, véner, etc...
Crèdits|4|False|Mercé !
