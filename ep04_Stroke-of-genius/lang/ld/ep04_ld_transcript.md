# Transcript of Pepper&Carrot Episode 04 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 4: Hiháal Wothethu

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dedidehá|1|False|Bíi ham náal wi. Ham sháa boóya wa. Ham woyahanesherana worashon sháaleya aril.
Loyud|2|True|Doól... Bé thad zhe rawith leth woyahanesh woranawan hi! Íi thad ra Loshesh!
Loyud|3|False|Them durilin beye...
Loyud|4|False|ÁÁÁÁÁÁÁÁÁ~ ~MMEEEED!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thod|14|False|RULANA
Loyud|1|False|Ámed?
Loyud|2|False|Míi le! Rahóo dahaneha yil?
Loyud|4|False|Ámed?
Loyud|5|False|Ámed?!
Loyud|6|False|Ámed?!!!
Loyud|3|False|Ámed!
Loyud|7|False|Bíi edeláad ra le úthú them duth le rashadoneth hi...
Loyud|8|False|Ámed!
Zho|9|True|Zho
Zho|10|True|Zho
Zho|11|True|Zho
Zho|12|True|Zho
Zho|13|False|Zho
Zho|15|True|Aaa-
Zho|16|True|naaa-
Zho|17|True|lom
Zho|18|True|Ana-
Zho|19|False|lom
Zho|21|False|Aaa-naaa-lom Ana-lom
Zho|20|False|Zho Zho Zho Zho Zho
Zho|23|False|Zho Zho Zho Zho Zhor
Zho|24|False|Aaa-naaa-lom Ana-lom
Ámed|25|False|Yidezho
Zho|26|False|Ralóóoolo!
Ámed|22|False|Áanyáa lethi!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|True|Bíi Ámed woméenan worulim wa! Nosháad ne sholanenalehóo...
Loyud|2|False|... úyahú them le woháama wodená lethoth wa!
Loyud|3|True|Bóo láad. Bíi dam le dóhinedal nonede lethu nadim.
Loyud|4|False|Yahanesherana Wothethu
Loyud|5|False|Bóo rilinehel ne
Loyud|6|False|Bere thal be, ébere loláad ne hiháál wothethuth

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zho|1|False|Raheb
Loyud|3|False|Ámed! Míi le wa... wohin wohuzh bem wa...
Loyud|5|False|Báa... Báa thod ne?
Loyud|6|False|E?
Loyud|7|False|Enin?
Loyud|8|False|Ehath?
Loyud|9|False|Ewihi?
Zho|4|False|Wushshshshsh
Zho|2|False|Wushsh

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|Báa thad di hi worazhe wobebáa?
Loyud|2|False|Etham?
Loyud|3|False|Edelin?
Loyud|4|False|Ámed! Thad di ra le medáaneth, i thad en rawith hith wa!
Loyud|5|False|... thad en rawith hith ...
Loyud|6|True|Lhlhlhlhlh!!!
Loyud|7|False|Shóo ra dal únalú néde lhe wa!
Loyud|8|True|Lhlh...
Loyud|9|False|Bíi ham wothen wohath úwanú el le worazhe worana wa
Zho|10|False|WU...LH...ÚDDDD!
Loyud|11|False|Bóo áana ne thalenal, Ámed
Dedidehá|12|False|Bé aril dedideth nádideshub...
Dohiná|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo i áalahóo Amireeti bedim úwáanú den héedan be neth Inelish bedim!
Dohiná|1|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth. Áala withedim 156 wudewan hi:
Dohiná|6|False|https://www.patreon.com/davidrevoy
Dohiná|5|False|Bóo den ne alehalath. Bíi bere den ne losheth nedebe barada, ébere denehal ne Loyud i Ámed beth wa!
Dohiná|9|False|Bodibodedal: Bíi wudeth hi eleshub wodínon wohedenan wa: Krita, G'MIC, Blender Ubuntu Gnome (GNU/Linux) beha
Dohiná|8|False|Wodínon wohel: Bíi meham dadem woho i thoded woho i minidibi...
Dohiná|7|False|Hudi: Creative Commons Attribution, 'David Revoy' bedim Bíi thad el ne zheheleth, shebeleth, nerabutheth, úthú beheb ne, minidibi...
Ámed|4|False|Áala!
