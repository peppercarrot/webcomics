# Transcript of Pepper&Carrot Episode 04 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 4: Golpe de genialidad

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Noche antes del concurso de pociones, 2h00 de la madrugada ...
Pimienta|2|True|¡Por fin! Con esta poción, nadie, ni siquiera Azafrán, podrá demostrar mejores habilidades.
Pimienta|3|False|Solo necesito probarla con alguien ...
Pimienta|4|False|ZAANAAHOO~ ~OORIAAA!!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|14|False|PIENSO
Pimienta|1|False|¿Zanahoria?
Pimienta|2|False|¿Eh ... ni siquiera está bajo la cama?
Pimienta|4|False|¿Zanahoria?
Pimienta|5|False|¡¿Zanahoria?!
Pimienta|6|False|¡¡¡¿Zanahoria?!!!
Pimienta|3|False|¡Zanahoria!
Pimienta|7|False|Nunca imaginé que tendría que utilizar esta técnica ...
Pimienta|8|False|¡Zanahoria!
Sonido|9|True|Crrr
Sonido|10|True|Crrr
Sonido|11|True|Crrr
Sonido|12|True|Crrr
Sonido|13|False|Crrr
Sonido|15|True|Shhh
Sonido|16|True|Shhh
Sonido|17|True|Shhh
Sonido|18|True|Shhh
Sonido|19|False|Shhh
Sonido|21|False|Shhh Shhh Shhh Shhh Shhh
Sonido|20|False|Crrr Crrr Crrr Crrr Crrr
Sonido|23|False|Crrr Crrr Crrr Crrr Crrr
Sonido|24|False|Shhh Shhh Shhh Shhh Shhh
Zanahoria|25|False|Gargblr
Sonido|26|False|¡Fiouuuuumm... !
Zanahoria|22|False|¡O Sole Miaouuuoo!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Oooh, Zanahoria! Has venido a ayudar sin pedírtelo... ¡Eres tan dulce...!
Pimienta|2|False|Justo en el instante en que necesitaba de mi asistente preferido.
Pimienta|3|True|¡Tacháaaan! Te presento mi obra maestra.
Pimienta|4|False|La poción del Genio.
Pimienta|5|False|Solo un traguito ...
Pimienta|6|False|... y si funciona, deberías tener un golpe de genialidad.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|Poc
Pimienta|3|False|¡Guaaau ... Zanahoria! es fantástico...
Pimienta|5|False|... ¿estás... escribiendo?
Pimienta|6|False|¿E?
Pimienta|7|False|¿Energía?
Pimienta|8|False|¿Eternidad?
Pimienta|9|False|¿Emoción?
Sonido|4|False|Shrrrrrrr
Sonido|2|False|Shrr

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¿Quée... ?
Pimienta|2|False|¿Empañar?
Pimienta|3|False|¿Emanar?
Pimienta|4|False|¡Vamos Zanahoria, haz un esfuerzo, eso no tiene ningún sentido!
Pimienta|5|False|... eso no tiene ningún sentido ...
Pimienta|6|True|¡¡¡Grrrrrrr!!!
Pimienta|7|False|¡Nunca me sale nada bien!
Pimienta|8|True|Bah ...
Pimienta|9|False|aún me queda tiempo para preparar otra poción mejor.
Sonido|10|False|CLAShshShshShh !
Pimienta|11|False|... ¡buenas noches Zanahoria!
Narrador|12|False|Continuará ...
Créditos|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156-TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo y agradecimiento especial a Amireeti por ayudarme con correcciones en inglés
Créditos|1|False|Pepper&Carrot es totalmente gratuito, de código abierto, y patrocinado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 156 mecenas:
Créditos|6|False|https://www.patreon.com/davidrevoy
Créditos|5|False|Traducción al castellano: TheFaico Y tú también, puedes ser mecenas de Pepper&Carrot para el próximo episodio:
Créditos|9|False|Herramientas: Este episodio ha sido creado con programas 100% libres Krita, G'MIC, Blender, GIMP en Ubuntu Gnome (GNU/Linux)
Créditos|8|False|Open-source: todas las imágenes, fuentes tipográficas, y demás ficheros están disponibles en la página oficial para su descarga.
Créditos|7|False|Licencia: Creative Commons Attribution a 'David Revoy' puedes modificar, compartir, vender, etc...
Zanahoria|4|False|¡Gracias!
