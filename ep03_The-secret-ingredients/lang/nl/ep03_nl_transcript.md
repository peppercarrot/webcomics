# Transcript of Pepper&Carrot Episode 03 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 3: De geheime ingrediënten

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|Komona-stad; Marktdag
Pepper|2|False|Goedemorgen meneer, mag ik acht pompoensterren, alstublieft?
Verkoper|3|False|Alstublieft, dat is dan 60Ko*.
Pepper|5|False|Oei oei …
Notitie|4|False|* Ko = de munteenheid van Komona

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Uhm, sorry, ik zal het bij vier moeten laten …
Verkoper|2|False|Grrr …
Saffraan|3|False|Gegroet, mijnheer. Geeft u mij twee dozijn van alles. Van de beste kwaliteit, natuurlijk.
Verkoper|4|False|Altijd fijn om u van dienst te kunnen zijn, juffrouw Saffraan.
Saffraan|5|False|Hé, daar hebben we Pepper!
Saffraan|6|False|En, de zaken gaan goed daar op het platteland, zie ik?
Pepper|7|False|...
Saffraan|8|False|Jij bent zeker inkopen aan het doen voor de toverdrankwedstrijd morgen?
Pepper|9|True|… een toverdrank-wedstrijd?
Pepper|10|False|… morgen?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Gelukkig heb ik nog een hele dag om me voor te bereiden!
Pepper|5|False|Dit moet ik winnen!
Geschrift|1|False|Toverdrankwedstrijd van Komona
Geschrift|2|False|Hoofdprijs: 50 000 Ko voor de BESTE TOVERDRANK
Geschrift|3|False|Op Azerdag, om 3 Paarsmaan Grote Markt, Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Oh! Ik heb het!
Pepper|3|False|… dat is precies wat ik moet hebben!
Pepper|4|True|Carrot!
Pepper|5|False|Maak je klaar, we moeten ingrediënten verzamelen.
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Eerst een paar mistdruppels uit die donkere wolken …
Pepper|2|False|… en wat rode bessen uit het behekste oerwoud …

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|… plus de schaal van een feniksei uit de vulkanenvallei …
Pepper|2|False|… en ten slotte een drupje melk van een jonge Drakenkoe.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Zo Carrot, volgens mij heb ik alles wat ik nodig heb.
Pepper|2|True|Pfoe …
Pepper|3|False|… dat ziet er perfect uit.
Pepper|4|True|Mmm …
Pepper|5|True|Nu kan ik de hele nacht doorwerken om een geniale toverdrank te brouwen voor de wedstrijd morgen.
Pepper|6|False|Wordt vervolgd …
Verteller|7|False|Beste … koffie … ooit!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Aftiteling|2|False|Deze aflevering is tot stand gekomen dankzij de steun van 93 patronen:
Aftiteling|1|False|Deze webcomic is geheel vrij en open-bron (Creative Commons Naamsvermelding 3.0 Unported, de bronbestanden zijn vrij te downloaden)
Aftiteling|3|False|https://www.patreon.com/davidrevoy
Aftiteling|6|False|Met speciale dank aan: David Tschumperlé (G'MIC) en het hele Krita-team! Nederlandse vertaling: Willem Sonke en Marno van der Maas
Aftiteling|7|False|Deze aflevering is gemaakt met 100% vrije en open-bron-programma's Krita en G'MIC op Xubuntu (GNU/Linux)
Pepper|5|False|Bedankt!
