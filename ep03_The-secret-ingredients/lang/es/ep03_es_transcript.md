# Transcript of Pepper&Carrot Episode 03 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 3: Los ingredientes secretos

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Villa de Komona, Día de mercado
Pimienta|2|False|Buenos días señor, Querría ocho estrellas de calabaza, por favor.
Vendedor|3|False|Aquí tiene, son 60Ko*.
Pimienta|5|False|... ¡Ups!
Nota|4|False|* Ko = moneda de Komona

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Vaya, lo siento! Creo que me llevaré solamente cuatro.
Vendedor|2|False|Grrr...
Azafrán|3|False|¡Buenos días, tendero! Por favor, prepárame dos docenas de cada. De la mejor calidad, como siempre.
Vendedor|4|False|Siempre es un placer servirla, señorita Azafrán.
Azafrán|5|False|¡Anda, mira! Si está aquí Pimienta...
Azafrán|6|False|A ver, déjame adivinar... ...las cosas van viento en popa en el campo, ¿verdad?
Pimienta|7|False|...
Azafrán|8|False|Supongo que estarás preparando los ingredientes para el concurso de pociones de mañana.
Pimienta|9|True|... ¿un concurso de pociones?
Pimienta|10|False|... ¿mañana?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|4|False|¡Qué suerte! Aún tengo todo el día para prepararme.
Pimienta|5|False|¡Vamos a por el premio!
Escritura|1|False|Concurso de Pociones de Komona
Escritura|2|False|gran premio de 50 000Ko A LA MEJOR POCIÓN
Escritura|3|False|Azarday, 3 Pinkmoon Plaza Mayor de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|2|False|¡Ey ...! ¡Ya sé! ...
Pimienta|3|False|...¡es exactamente lo que necesito!
Pimienta|4|True|¡Zanahoria!
Pimienta|5|False|¡Prepárate! ¡Vamos a cazar juntos los ingredientes!
Pimienta|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Primero, necesito unas perlas de niebla de nubes negras ...
Pimienta|2|False|... y algunas bayas rojas del bosque encantado ...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|... también algunas cáscaras de huevo del Fénix del Valle de los Volcanes ...
Pimienta|2|False|... y para acabar unas gotas de leche de una Vaca-Dragón joven.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Ya está Zanahoria! Diría que lo tenemos todo.
Pimienta|2|True|Creo que está...
Pimienta|3|False|... perfecta.
Pimienta|4|True|Mmm ...
Pimienta|5|True|¡Qué bien sienta ... ... un buen café!
Pimienta|6|False|Es todo lo que necesito para trabajar toda la noche y preparar la mejor poción para el concurso de mañana.
Narrador|7|False|Continuará ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Créditos|2|False|Este episodio no existiría sin el apoyo de 93 mecenas
Créditos|1|False|Este webcomic es completamente libre y de código abierto ( Creative Commons Attribution 3.0, disponibles para descarga los ficheros fuente en alta resolución. )
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|6|False|Agradecimientos especiales a: Amireeti, David Tschumperlé (G'MIC), TheFaico (traducción al español de España) y todo el equipo de Krita.
Créditos|7|False|Este episodio ha sido realizado con herramientas 100% libres y de código abierto Krita y G'MIC en Xubuntu (GNU/Linux)
Pimienta|5|False|¡Muchas gracias!
