# Transcript of Pepper&Carrot Episode 03 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 3: Skrivne sestavine

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|Mesto Komona, na tržnici
Paprika|2|False|Dobro jutro, gospod. Osem bučnih zvezd, prosim.
Prodajalec|3|False|Izvolite, tole bo 60 kojev*.
Paprika|5|False|Ti šment!
Opomba|4|False|* ko = komonska valuta

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Am, oprostite, samo štiri lahko vzamem.
Prodajalec|2|False|Grrr…
Žafranka|3|False|Pozdravljeni, dobri mož. Pripravite mi, prosim, dva ducata vsega. Najboljše kakovosti, kot po navadi.
Prodajalec|4|False|Vedno vam z veseljem ustrežem, cenjena Žafranka.
Žafranka|5|False|O, glej no, če ni to Paprika?
Žafranka|6|False|Posel na podeželju nedvomno cveti, kaj?
Paprika|7|False|...
Žafranka|8|False|Najbrž zbiraš sestavine za jutrišnje tekmovanje v napojih?
Paprika|9|True|Tekmovanje v napojih?
Paprika|10|False|Jutri?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|4|False|Ti sreča, za priprave imam še ves dan!
Paprika|5|False|Seveda grem, pa še zmagala bom!
Napis|1|False|Komonsko tekmovanje v napojih
Napis|2|False|nagrada za najboljši napoj 50 000 kojev
Napis|3|False|V azardeljo, ob 15. luni na komonskem Glavnem trgu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|2|False|Aha! Že vem!
Paprika|3|False|Točno to potrebujem!
Paprika|4|True|Korenček!
Paprika|5|False|Pridi, bova šla skupaj iskat sestavine!
Paprika|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Najprej potrebujem nekaj megličnih biserov iz nevihtnih oblakov …
Paprika|2|False|… in prgišče rdečih jagod iz zaklete džungle.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Pa še lupine feniksovih jajc iz vulkanske doline …
Paprika|2|False|… in za konec kapljico mleka mlade zmaj-krave.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Pa sva, Korenček. Mislim, da imam vse potrebno.
Paprika|2|True|Izgleda …
Paprika|3|False|… popolno.
Paprika|4|True|Mmm!
Paprika|5|True|Kava … in … pol!
Paprika|6|False|Po tem bom lahko vso noč varila najboljši napoj za jutrišnje tekmovanje.
Pripovedovalec|7|False|Se nadaljuje …

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon
Zasluge|2|False|Te epizode ne bi bilo brez 93 podpornikov prek
Zasluge|1|False|Ta spletni strip je popolnoma prost in odprtokoden. (Pod licenco Creative Commons Priznanje avtorstva 4.0. Izvorne datoteke so na voljo za prenos.)
Zasluge|3|False|https://www.patreon.com/davidrevoy
Zasluge|6|False|Posebna zahvala Davidu Tschumperléju (G'MIC) in vsem razvijalcem Krite! Prevedla Andrej Ficko in Gorazd Gorup
Zasluge|7|False|Ta epizoda je bila ustvarjena s 100% prostimi in odprtokodnimi orodji: Krita in G'MIC na Xubuntuju (GNU/Linux)
Paprika|5|False|Hvalaaaa!
