# Transcript of Pepper&Carrot Episode 03 [ga]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 3 : L'ingredient secret

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Nòta|4|False|* Ko = unitat monetària de Komona
Narrator|1|False|Vila de Komonà Dia de mercat
Pepper|2|False|Adishatz Mossur, que volerí ueit cujas-estelas, en vs'ac pregant !
Venedor|3|False|Aquiu que'us avetz, que hè 60Ko*.
Pepper|5|False|zut ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ee, desolada, que'n vau préner sonque quate a la fin finala.
Venedor|2|False|Grrr...
Safran|3|False|Adishatz, car mossur, duas detzèas de cada, que vse'n prègui. De qualitat superiora, com a l'acostumat.
Venedor|4|False|Qu'ei tostemps un plaser de'vs servir, Damisèla Safran.
Safran|5|False|Eh, mes ueratz donc, qu'ei Pepper !
Safran|6|False|Ne'm digas pas, los ahars qu'an endavant, au campèstre ?
Pepper|7|False|...
Safran|8|False|Qu'imagini qu'aprèstas dejà los tons ingredients tau concors de potingas de doman ?
Pepper|9|True|…un concors de potingas ?
Pepper|10|False|…doman ?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|Quina chança aqueth concors ! E qu'èi enqüèra un dia sancèr tà'm preparar.
Pepper|5|False|De segur que vau participar !
Escritura|1|False|Concors de Potingas de Komonà
Escritura|2|False|gran prèmi de 50 000 Ko TÀ LA MEI BONA POTIINGA
Escritura|3|False|Diazard, 3h deu dèxtre Gran' plaça de Komonà

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Òh… ! Qu'ac sèi ! …
Pepper|3|False|…Qu'ei juste çò qui'm cau !
Pepper|4|True|Carrot !
Pepper|5|False|Prepara't, que vam caçar ingredients !
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Prumèr, que'm caleré pèrlas de brumas de crums negres…
Pepper|2|False|…e quauques aujons deu bòsc malefic.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|…puish quauques crescs de ueus de Fènix de la vath deus volcans…
Pepper|2|False|…E a la fin finala quauques gotas de lèit d'ua joena DragonVaca.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Adara que i èm, Carrot, que pensi qu'ac avem tot.
Pepper|2|True|Que'm sembla…
Pepper|3|False|…perfèit.
Pepper|4|True|Mmm ...
Pepper|5|True|Mei bon… cafè… deu monde !
Pepper|6|False|Qu'ei tot çò dont aví besonh abans de machar tota la nueit sus la mei bona potinga tau concors de doman.
Narrator|7|False|a seguir ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Crèdits|2|False|Aqueste episòdi ne poderé pas existir shens lo sostien de 93 mecènas
Crèdits|1|False|Aqueste webcomic qu'ei completament liure e open-source (Creative Commons Attribution 3.0, fichèrs hauta resolucion disponibles au telecargament)
Crèdits|3|False|https://www.patreon.com/davidrevoy
Crèdits|6|False|Mercejament especiau Amireeti, David Tschumperlé (G'MIC) e tota « l'equipa Krita » !
Crèdits|7|False|Aqueste episòdi qu'estó realizat a 100% dab otisses liures, Krita e G'MIC sus Xubuntu (GNU/Linux)
Pepper|5|False|Mila mercés !
