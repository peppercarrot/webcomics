#!/bin/bash
#
#  convert-transcript-to-framapad.sh
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#  SPDX-FileCopyrightText: © 2021 David Revoy <info@davidrevoy.com>
#
#  Script for cleaning the Markdown transcript into a format easier to copy/paste on Framapad
#   $1 Full absolute path to the file with the filename of the transcript
#   $2 Absolute path to output (optional)
#   Usage: ./convert-transcript-to-framapad.sh </absolute/path/to/the/transcript-file.md> <optional/path/to/output>
#
#  Depency required: sed

# Color palette for the output
export Off=$'\e[0m'
export Green=$'\e[1;32m'
export Red=$'\e[1;31m'
export Yellow=$'\e[1;33m'
export Blue=$'\e[1;34m'

echo "${Yellow}--------------------------------------------------------------${Off}"

if [ -f "$1" ]; then
    transcriptfullpath="$1"
    transcriptfile="${transcriptfullpath%%.*}"
    transcriptfile=${transcriptfile##*/}
    transcriptdirectory=${transcriptfullpath%/*}
    txtfile="${transcriptfile}_pad.html"

    if [ -z "$2" ]; then
      echo "${Green}Info:${Off} No output path defined, output directory will be similar to input's one."
    else
      if [ -d "$2" ]; then
        transcriptdirectory=${2}
      else
        echo "${Red}Error:${Off} output directory ${2} doesn't exists for ${1}."
        exit
      fi
    fi

    echo "${Blue}Converting ${transcriptfile} --> ${txtfile} :${Off}"
    echo "Dest. path: ${transcriptdirectory}"
    echo "${Yellow}--------------------------------------------------------------${Off}"

    # Writing the HTML5 UTF8 header for our document.
    echo "<!doctype html>" >> "${transcriptdirectory}/${txtfile}"
    echo "<html>" >> "${transcriptdirectory}/${txtfile}"
    echo "<head>" >> "${transcriptdirectory}/${txtfile}"
    echo "  <link rel=\"stylesheet\" href="styles.css">" >> "${transcriptdirectory}/${txtfile}"
    echo "  <meta charset=\"utf-8\">" >> "${transcriptdirectory}/${txtfile}"
    echo "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" >> "${transcriptdirectory}/${txtfile}"
    echo "</head>" >> "${transcriptdirectory}/${txtfile}"
    echo "<body>" >> "${transcriptdirectory}/${txtfile}"
    echo "<pre>" >> "${transcriptdirectory}/${txtfile}"

    cat ${transcriptfullpath} >> "${transcriptdirectory}/${txtfile}"

    # Start cleaning
    sed -i 's#True##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#<hidden>##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#<unknown>##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#False##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#|[0-9]|##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#|[0-9][0-9]|##g' "${transcriptdirectory}/${txtfile}"

    sed -i 's#----|--------|-----------|----|---------------------##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#Name|Position|Concatenate|Text|Whitespace (Optional)##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#Providing transcripts is optional for translators.##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#The Pepper&Carrot site will work fine without them,##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#but they help for accessibility, screen readers and language learners.##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#Read https\:\/\/www.peppercarrot.com\/xx\/documentation\/062_Transcripts\.html##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#for how they can be generated without retyping them after you are done##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#translating or updating the content of the speechbubbles in the SVG(s)##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's#and more information and documentation about them.##g' "${transcriptdirectory}/${txtfile}"
    sed -i 's%## Notes%%g' "${transcriptdirectory}/${txtfile}"
    sed -i 's%## Pages%%g' "${transcriptdirectory}/${txtfile}"

    sed -i 's%### P%--- Page %g' "${transcriptdirectory}/${txtfile}"
    sed -i 's%[a-zA-Z]*|%%g' "${transcriptdirectory}/${txtfile}"
    sed -i 's%--- Page 00%%g' "${transcriptdirectory}/${txtfile}"
    
    sed -i 's% \.%.%g' "${transcriptdirectory}/${txtfile}"
    sed -i 's% \,%,%g' "${transcriptdirectory}/${txtfile}"

    sed -i '/^$/{:a;N;s/\n$//;ta}' "${transcriptdirectory}/${txtfile}"

    echo " </pre>" >> "${transcriptdirectory}/${txtfile}"
    echo "</body>" >> "${transcriptdirectory}/${txtfile}"
    echo "</html>" >> "${transcriptdirectory}/${txtfile}"

    echo "${Green}Done${Off}"

else
  echo "${Red}Error:${Off} Target transcript not found."
  echo " => Script usage: ./convert-transcript-to-framapad.sh </absolute/path/to/the/transcript-file.md> <optional/path/to/output>"
  echo ""
  exit
fi


