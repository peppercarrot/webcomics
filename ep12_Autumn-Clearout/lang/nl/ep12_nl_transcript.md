# Transcript of Pepper&Carrot Episode 12 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 12: Herfstschoonmaak

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|9|True|Kling
Geluid|10|True|Klang
Geluid|11|False|Klong
Cayenne|1|False|Slappe-lachdrankjes
Cayenne|2|False|Mega-haargroeidrankjes
Cayenne|3|False|Stinkbomtoverdrank
Cayenne|4|False|Het-leven-door-een-roze-bril-drank
Cayenne|5|False|Rook-toverdrank ...
Cayenne|6|False|... om er nog maar een paar te noemen!
Pepper|7|True|Ja, ik weet het:
Pepper|8|False|“Een-echte-heks-van-Chaosah houdt-zich-niet-bezig-met-zulke-drankjes".
Cayenne|13|True|Ik moet nog naar de markt in Komona.
Cayenne|14|False|Doe die troep ondertussen weg, het zou een ramp zijn als kwajongens dit zouden vinden.
Cayenne|12|False|Inderdaad. Zo zou het moeten zijn.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|2|False|Zoooef!
Pepper|3|False|Wat!?
Pepper|4|True|Alles begraven?!
Pepper|5|False|Maar dat zou uren duren! En dan nog de blaren die we eraan zouden overhouden!
Geluid|6|False|Kleng
Pepper|1|False|Die troep wegdoen? Da's gemakkelijk! Joepie!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|“En gebruik zeker geen magie. Een echte heks van Chaosah gebruikt geen magie voor gewone klusjes."
Pepper|2|False|Grrr!
Pepper|3|True|CARROT!
Pepper|4|False|Boek zeven van Chaosah: “Gravitatievelden"... ...en snel een beetje!
Pepper|5|False|Ik zal die oude zaag eens tonen hoe een heks van Chaosah dat écht doet!
Geluid|6|True|Tip
Geluid|7|False|Tip

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|2|False|VROOoooooooooo
Cayenne|3|False|... Een zwart gat van Chaosah?!...
Pepper|4|False|?!
Cayenne|5|False|... Is dat echt wat je verstaat onder "geen magie gebruiken"?
Pepper|6|False|Hé ... Moest jij niet op de markt zijn?
Cayenne|7|True|Natuurlijk niet!
Cayenne|8|False|En ik had groot gelijk; ik kan je geen tien minuten alleen laten!
Pepper|1|False|GURGES ATER !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|wooowoowoowoo
Geluid|7|False|wooowoowoowoo
Geluid|6|False|Bam
Cayenne|8|True|?!
Carrot|10|False|... Kijk nu toch eens.
Cayenne|2|True|Onvoldoende massa!
Cayenne|3|True|Foutieve gravitatieveld-differentiaalvergelijking!
Cayenne|4|True|Zelfs de búítenste stabiele circulaire baan is te klein!
Cayenne|5|False|Een iets grotere gebeurtenissenhorizon en alles was in orde geweest!
Cayenne|9|False|Ziezo: bijna alles zal nu tot een stabiele baan komen of naar de Lagrangepunten gaan en blijven rondcirkelen.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Cayenne|2|False|!!!
Cayenne|3|False|CURSUS ANNULATIONUS MAXIMUS!
Geluid|4|False|KsjKrak!
Verteller|5|False|- EINDE -
Aftiteling|6|False|10/2015 - Tekeningen & verhaal: David Revoy - Vertaling: Midgard

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 575 patronen:
Aftiteling|3|False|https://www.patreon.com/davidrevoy
Aftiteling|2|True|Voor de volgende aflevering kun jij ook een patroon van Pepper&Carrot worden op
Aftiteling|4|False|Licentie: Creative Commons Naamsvermelding 4.0 Bronbestanden: beschikbaar op www.peppercarrot.com Software: deze aflevering is 100% gemaakt met vrije software Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 op Linux Mint 17
