# Transcript of Pepper&Carrot Episode 12 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 12: Limpieza de otoño

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|9|True|Cling
Sonido|10|True|Clang
Sonido|11|False|Clong
Cayena|1|False|Pociones de ataque de risa
Cayena|2|False|Pociones megacrecepelo
Cayena|3|False|Pociones de bombas fétidas
Cayena|4|False|Pociones Qué bello es vivir
Cayena|5|False|Pociones bomba de humo ...
Cayena|6|False|... ¡y por mencionar sólo algunas!
Pimienta|7|True|Sí, ya sé:
Pimienta|8|False|"Una-auténtica-bruja-de-Chaosah no-debe-preparar-ese-tipo-de-pociones".
Cayena|13|True|Ahora tengo que ir a hacer un recado al mercado de Komona.
Cayena|14|False|Haz que todo esto desaparezca durante mi ausencia; no me gustaría que acabe en manos de algún gracioso de turno.
Cayena|12|False|Exacto. Así es como debe ser.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|2|False|¡Ziooum!
Pimienta|3|False|¡La leche!
Pimienta|4|True|¡¿Enterrarlo todo?!
Pimienta|5|False|¡Pero eso nos va a tomar horas, por no hablar de las ampollas que nos saldrían!
Sonido|6|False|Pum
Pimienta|1|False|¿Hacerlo desaparecer? ¡Chupado! ¡Yupii!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|"Y sobre todo no utilices la magia. Una auténtica bruja de Chaosah no utiliza la magia para las tareas cotidianas."
Pimienta|2|False|Grrr !
Pimienta|3|True|¡ZANAHORIA!
Pimienta|4|False|Libro siete de Chaosah: "Los Campos Gravitatorios"... ... ¡y rápido!
Pimienta|5|False|¡Le voy a demostrar que hace una auténtica bruja de Chaosah a esta vieja arpía!
Sonido|6|True|Tam
Sonido|7|False|Tam

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|2|False|VROOoooooooooo
Cayena|3|False|...¿¡Un agujero negro de Chaosah?!...
Pimienta|4|False|?!
Cayena|5|False|... ¿Es esto lo que tú entiendes por: "no utilizar la magia"?
Pimienta|6|False|... Emm... ¿No debería estar en el mercado?
Cayena|7|True|¡Por supuesto que no!
Cayena|8|False|Y he hecho bien; ¡es imposible confiarse y dejarte sin vigilancia!
Pimienta|1|False|¡GURGES ATER!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|wooowoowoowoo
Sonido|7|False|wooowoowoowoo
Sonido|6|False|Bam
Cayena|8|True|Ahí lo tienes: Casi todo va a gravitar en la órbita estable o en los puntos de Lagrange y va a seguir flotando alrededor.
Zanahoria|10|False|?!
Cayena|2|True|...¡y mira esto!
Cayena|3|True|¡Masa insuficiente!
Cayena|4|True|¡Débil campo gravitatorio diferencial!
Cayena|5|False|¡Hasta la última órbita circular estable es demasiado pequeña!
Cayena|9|False|¡Hasta un horizonte de sucesos una chispa más grande habría resultado!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|!!!
Cayena|2|False|!!!
Cayena|3|False|CURSUS CANCELLARE MAXIMUS!
Sonido|4|False|ShKlak!
Narrador|5|False|- FIN -
Créditos|6|False|Octubre 2015 - Dibujo y Guión: David Revoy - Traducción: TheFaico

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto y patrocinado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 575 mecenas:
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|2|True|Tú también puedes ser mecenas de Pepper&Carrot para el próximo episodio:
Créditos|4|False|Licencia: Creative Commons Attribution 4.0 Ficheros originales disponibles en www.peppercarrot.com Herramientas: Este episodio ha sido creado al 100% con software libre Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 on Linux Mint 17.2
