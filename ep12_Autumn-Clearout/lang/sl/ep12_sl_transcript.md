# Transcript of Pepper&Carrot Episode 12 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 12: Jesensko čiščenje

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|9|True|Klang
Zvok|10|True|Kling
Zvok|11|False|Klong
Kajenka|1|False|Napoji krohota,
Kajenka|2|False|napoji razraščanja las,
Kajenka|3|False|napoj smradu,
Kajenka|4|False|napoj prešernosti,
Kajenka|5|False|napoj dima …
Kajenka|6|False|In še ni konca!
Paprika|7|True|Saj vem:
Paprika|8|False|„Taprava-kaosaška-čarovnica-ne-vari-takšnih-napojev.“
Kajenka|13|True|Moram na tržnico v Komono.
Kajenka|14|False|Medtem se znebi vse te navlake. Strašno zoprno bi bilo, če bi jo našla kakšna prefrigana mularija.
Kajenka|12|False|Da. In tako je tudi prav.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|2|False|Švig!
Paprika|3|False|Pišuka!
Paprika|4|True|Vse naj zakopljeva?!
Paprika|5|False|To bo trajalo več ur, da ne omenjam žuljev, ki jih bova imela na koncu!
Zvok|6|False|Paf
Paprika|1|False|Znebim naj se je? Nič lažjega!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|„Pod nobenim pogojem ne čaraj. Prava kaosaška čarovnica ne uporablja čarovnije za vsakdanja opravila.“
Paprika|2|False|Grrr!
Paprika|3|True|KORENČEK!
Paprika|4|False|Prinesi sedmo knjigo Kaosaha, „Težnostna polja“! Pri tej priči!
Paprika|5|False|Bom že pokazala tej stari vrani, kdo je tukaj prava kaosaška čarovnica!
Zvok|6|True|Cap
Zvok|7|False|Cap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|2|False|VRUUuuuuuuuuuu
Kajenka|3|False|Kaosaška črna luknja?!
Paprika|4|False|?!
Kajenka|5|False|Česa nisi razumela pri „Ne čaraj“?
Paprika|6|False|Hej!? Ali niste šli po nakupih?
Kajenka|7|True|Jasno, da ne!
Kajenka|8|False|In prav sem imela! Ni te mogoče pustiti same brez nadzora!
Paprika|1|False|GURGES ATER!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|vuuuvuuvuuuoo
Zvok|7|False|vuuuvuuvuuvuu
Zvok|6|False|Bam
Kajenka|8|True|No, vidiš: Skoraj vse bo šlo proti stabilni krožnici ali proti Lagrangeevim točkam in naprej krožilo okoli luknje.
Korenček|10|False|?!
Kajenka|2|True|Zdaj pa poglej!
Kajenka|3|True|Nezadostna masa!
Kajenka|4|True|Šibka razlika v težnostnem polju!
Kajenka|5|False|Celo najbližja stabilna krožnica je premajhna!
Kajenka|9|False|Že za las večje dogodkovno obzorje bi zadostovalo!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|!!!
Kajenka|2|False|!!!
Kajenka|3|False|UROKUS PREKINITIS NAGLOS!
Zvok|4|False|ŠKlak!
Pripovedovalec|5|False|- KONEC -
Zasluge|6|False|Oktober 2015 - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 575 bralcev:
Zasluge|3|False|https://www.patreon.com/davidrevoy
Zasluge|2|True|Tudi ti lahko postaneš denarni/a podpornik/ca za naslednjo epizodo stripa:
Zasluge|4|False|Licenca: Creative Commons Priznanje avtorstva 4.0. Izvorne datoteke na voljo na www.peppercarrot.com Programska oprema: ta epizoda je nastala s prostim programjem Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14 in G'MIC 1.6.7 na Linux Mint 17.2
