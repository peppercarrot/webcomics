# Episode 24: The Unity Tree

![cover of episode 24](https://www.peppercarrot.com/0_sources/ep24_The-Unity-Tree/low-res/Pepper-and-Carrot_by-David-Revoy_E24.jpg)

## Comments from the author

This episode is a of course a "Special Xmas" episode but because Xmas doesn't exist in the world of Pepper&Carrot, we had to invent a parallel celebration. In English, they celebrate "The Unity" with a decorated "Unity Tree". Background story will be added later on our wiki to define more details about "The Unity".

From [Beta-reading Forum of episode 24](https://framagit.org/peppercarrot/webcomics/-/issues/3)
