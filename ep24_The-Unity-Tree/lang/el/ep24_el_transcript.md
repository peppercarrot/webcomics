# Transcript of Pepper&Carrot Episode 24 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tίτλος|1|False|Επεισόδιο 24: Το Δέντρο της Ενότητας (UNITY)

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|5|False|ΠΑΦ!
Καγιέν|1|True|ΘΕΛΕΙΣ ΤΟ ΔΕΝΤΡΟ ΤΗΣ ΕΝΟΤΗΤΑΣ;
Καγιέν|4|False|ΕΙΜΑΣΤΕ ΜΑΓΙΣΣΕΣ ΤΟΥ ΧΑΟΥΣ! ΔΕΝ ΤΟ ΚΑΝΟΥΜΕ ΑΥΤΟ.
Καγιέν|6|False|ΚΑΛΥΤΕΡΑ ΝΑ ΣΥΓΚΕΝΤΡΩΘΕΙΣ ΔΙΑΒΑΖΟΝΤΑΣ “ΤΑ ΞΟΡΚΙΑ ΤΗΣ ΚΑΤΑΣΤΡΟΦΗΣ”!
Πιπεριά|7|True|ΚΑΤΑΣΤΡΟΦΗΣ;
Πιπεριά|8|True|ΤΩΡΑ;
Πιπεριά|9|False|ΓΙΑΤΙ ΝΑ ΜΗΝ ΜΑΘΟΥΜΕ ΤΟΤΕ ΤΑ ΞΟΡΚΙΑ ΤΗΣ ΔΗΜΙΟΥΡΓΙΑΣ;
Καγιέν|10|False|ΤΟ ΕΠΟΜΕΝΟ ΣΥΜΒΟΥΛΙΟ ΤΩΝ ΤΡΙΩΝ ΦΕΓΓΑΡΙΩΝ ΠΛΗΣΙΑΖΕΙ ΟΠΩΣ ΚΑΙ ΟΙ ΕΞΕΤΑΣΕΙΣ ΣΟΥ. ΜΑΘΕ! ΜΑΘΕ! ΜΑΘΕ!!! ΚΑΙ ΣΤΑΜΑΤΑ ΝΑ ΑΠΟΣΠΑΣ ΤΟΝ ΕΑΥΤΟ ΣΟΥ ΜΕ ΑΥΤΕΣ ΤΙΣ ΣΑΧΛΕΣ ΙΔΕΕΣ!!!
Καγιέν|2|False|ΕΔΩ;!;
Καγιέν|3|True|ΟΧΙ!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|1|False|Σλαμπ!
Πιπεριά|5|True|ΕΝΑΣ ΧΕΙΜΩΝΑΣ ΧΩΡΙΣ ΤΟ ΔΕΝΤΡΟ ΤΗΣ ΕΝΟΤΗΤΑΣ;!
Πιπεριά|7|False|ΓΙΑΤΙ ΔΕΝ ΦΤΙΑΧΝΩ ΤΟ ΔΙΚΟ ΜΟΥ ΔΕΝΤΡΟ ΜΕ ΜΑΓΙΚΑ;!
Πιπεριά|8|True|ΝΑΙ! ΙΣΩΣ ΓΙΑ ΠΡΩΤΗ ΦΟΡΑ ΣΥΝΔΥΑΣΩ ΤΑ ΜΑΓΙΑ ΧΙΠΠΙΔΙΑΣ ΚΑΙ ΧΑΩΔΙΑΣ!
Πιπεριά|9|True|ΕΝΑ ΜΕΓΑΛΟ ΠΡΑΣΙΝΟ ΔΕΝΤΡΟ, ΛΑΜΠΥΡΙΣΜΕΝΟ ΑΠΟ ΑΣΤΕΡΙΑ ΜΕ ΜΙΝΙ ΗΛΙΚΑΚΑ ΣΥΣΤΗΜΑΤΑ ΓΥΡΩ ΤΟΥ!
Πιπεριά|10|False|ΟΥΑΟΥ! ΘΑ ΕΙΝΑΙ ΤΕΛΕΙΟ!
Πιπεριά|11|True|ΚΑΙ ΘΑ ΕΙΝΑΙ ΞΟΡΚΙ
Πιπεριά|12|False|ΔΗΜΙΟΥΡΓΙΚΟ!
Πιπεριά|13|True|ΑΛΛΑ ΔΕΝ ΥΠΑΡΧΕΙ ΤΡΟΠΟΣ ΝΑ ΚΑΝΩ ΤΕΤΟΙΑ ΜΑΓΙΚΑ ΕΔΩ ΠΕΡΑ...
Πιπεριά|14|False|...ΑΛΛΑ ΟΙ ΝΟΝΕΣ ΜΟΥ ΘΑ ΚΑΤΑΛΑΒΟΥΝ ΑΜΕΣΩΣ ΟΤΙ ΔΕΝ ΜΕΛΕΤΑΩ "ΤΑ ΞΟΡΚΙΑ ΤΗΣ ΚΑΤΑΣΤΡΟΦΗΣ".
Καροτούλης|3|False|ΤΑΠ
Ήχος|2|False|Φφφφ
Πιπεριά|6|False|ΠΟΤΕ!
Πιπεριά|4|False|Γκρρρρ...!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|3|False|ΣΡΡΡ
Ήχος|17|False|ΣΙΙΙΝΓΚ
Κείμενο|22|False|The KOMONAN
Πιπεριά|1|True|ΕΚΤΟΣ ΚAI ΑΝ...
Πιπεριά|2|False|ΝΟΜΙΖΩ ΘΥΜΗΘΗΚΑ...
Πιπεριά|4|False|ΝΑΙ! ΣΤΟ ΚΕΦΑΛΑΙΟ “ΞΟΡΚΙΑ ΚΑΤΑΣΤΡΟΦΗΣ”, Η ΘΥΜΑΡΑ ΣΥΝΙΣΤΑ ΝΑ ΔΟΚΙΜΑΣΤΟΥΝ ΣΕ ΜΙΑ ΜΙΚΡΟ-ΔΙΑΣΤΑΣΗ. ΑΛΛΑ ΔΕΣ ΟΛΕΣ ΑΥΤΕΣ ΤΙΣ ΠΡΟΕΙΔΟΠΟΙΗΣΕΙΣ:
Πιπεριά|16|False|ΠΑΜΕ ΝΑ ΤΟ ΚΑΝΟΥΜΕ!
Κουμίν|19|False|Η ΠΙΠΕΡΟΥΛΑ ΜΟΛΙΣ ΔΗΜΙΟΥΡΓΗΣΕ ΜΙΑ ΜΙΚΡΟ-ΔΙΑΣΤΑΣΗ;
Ήχος|18|False|ΣΓΟΥΟΟΜΠ!!
Θυμάρα|20|True|ΩΡΑΙΑ!
Κείμενο|8|False|“ΜΗΝ ΤΑ ΦΕΡΕΙΣ ΣΤΗΝ ΠΡΑΓΜΑΤΙΚΗ ΔΙΑΣΤΑΣΗ, ΓΙΑ ΛΟΓΟΥΣ ΑΣΦΑΛΕΙΑΣ.”
Κείμενο|6|False|“ΜΗΝ ΞΟΔΕΨΕΙΣ ΟΛΟ ΤΟ ΡΕΑ ΣΟΥ ΣΤΗ ΔΗΜΟΥΡΓΙΑ ΤΗΣ ΜΙΚΡΟ-ΔΙΑΣΤΑΣΗΣ.”
Κείμενο|11|True|“ΜΗΝ ΠΕΡΙΜΕΝΕΙΣ ΚΑΜΜΙΑ ΒΟΗΘΕΙΑ ΑΠΕΞΩ ΑΠΟ ΤΗΝ ΜΙΚΡΟ-ΔΙΑΣΤΑΣΗ...
Πιπεριά|7|False|ΦΥΣΙΚΑ.
Πιπεριά|10|False|ΝΑ ΑΥΤΟ ΗΘΕΛΑ!
Πιπεριά|9|False|Πφφ... ΟΛΟΦΑΝΕΡΟ!
Πιπεριά|5|False|Μμμ...
Κείμενο|12|False|...ΚΑΝΕΙΣ ΔΕΝ ΜΠΟΡΕΙ ΝΑ ΔΕΙ ΤΙ ΚΑΝΕΙΣ ΕΚΕΙ!”
Θυμάρα|21|False|ΕΛΠΙΖΩ ΝΑ ΚΑΤΑΛΑΒΕ ΟΛΕΣ ΤΙΣ ΠΡΟΕΙΔΟΠΟΙΗΣΕΙΣ ΠΟΥ ΕΓΡΑΨΑ ΣΤΟ ΚΕΦΑΛΑΙΟ "ΞΟΡΚΙΑ ΚΑΤΑΣΤΡΟΦΗΣ".
Πιπεριά|15|True|ΕΙΝΑΙ ΤΕΛΕΙΟ!
Πιπεριά|13|True|ΚΑΝΕΙΣ;
Πιπεριά|14|True|ΑΛΗΘΕΙΑ!;

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Πιπεριά|1|True|ΤΟΣΟ ΚΟΥΛ!
Πιπεριά|2|True|ΕΙΝΑΙ ΣΑΝ ΚΟΥΒΟΥΚΛΙΟ, ΕΣΩΚΛΕΙΟΝΤΑΣ ΜΟΝΟ ΤΟ ΜΕΡΟΣ ΤΗΣ ΒΙΒΛΙΟΘΗΚΗΣ ΠΟΥ ΧΡΕΙΑΖΟΜΑΙ. ΚΑΙ ΕΙΝΑΙ ΕΝΤΕΛΩΣ ΑΠΟΜΟΝΩΜΕΝΟ ΑΠΟ ΤΗ ΔΙΚΙΑ ΜΑΣ ΠΡΑΓΜΑΤΙΚΟΤΗΤΑ.
Πιπεριά|3|False|ΕΙΝΑΙ Η ΙΔΑΝΙΚΗ ΑΠΟΜΟΝΩΣΗ ΓΙΑ ΝΑ ΔΟΚΙΜΑΖΕΙΣ ΞΟΡΚΙΑ.
Πιπεριά|4|False|ΩΡΑ ΝΑ ΣΥΓΚΕΝΤΡΩΘΩ ΓΙΑ ΤΗΝ ΤΕΛΕΙΑ ΔΗΜΙΟΥΡΓΙΑ ΜΟΥ!
Ήχος|5|False|ΣΙΙΙΝΓΚ
Ήχος|6|False|ΠΛΟΥΥΜΠ!!
Ήχος|8|False|ΝΤΟΟΥΥΦ!!
Ήχος|10|False|ΤΣΙΝΓΚ!!
Πιπεριά|7|False|ΕΧΜ, ΟΧΙ.
Πιπεριά|9|False|ΙΟΥΥ!
Πιπεριά|11|False|ΝΑΙ! ΕΙΝΑΙ ΚΑΛΟ ΑΛΛΑ ΕΙΝΑΙ ΠΟΛΥ ΜΙΚΡΟ...
Πιπεριά|13|False|ΚΑΡΟΤΟΥΛΗ! ΜΗΝ ΤΟ ΑΓΓΙΖΕΙΣ!
Καροτούλης|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|1|False|ΠΛΟΥΜ!!
Πιπεριά|3|True|ΔΕΝ ΒΛΕΠΩ ΚΑΤΙ ΚΑΚΟ ΣΤΟ ΝΑ ΤΟ ΦΕΡΩ ΕΚΤΟΣ ΤΗΣ ΜΙΚΡΟ-ΔΙΑΣΤΑΣΗΣ.
Πιπεριά|2|True|ΝΑ! ΜΕΓΑΛΩΣΕ ΤΟ. ΤΩΡΑ ΕΙΝΑΙ ΤΕΛΕΙΟ!
Πιπεριά|5|True|ΕΝΤΑΞΕΙ,
Πιπεριά|6|True|ΕΙΝΑΙ ΩΡΑ ΝΑ ΞΕΔΙΠΛΩΘΕΙ Η ΜΙΚΡΟ-ΔΙΑΣΤΑΣΗ ΣΤΗΝ ΠΡΑΓΜΑΤΙΚΟΤΗΤΑ ΚΑΙ ΝΑ ΤΟΥΣ ΔΕΙΞΟΥΜΕ ΠΟΣΟ ΔΗΜΙΟΥΡΓΙΚΟ ΤΟ ΧΑΟΣ ΕΙΝΑΙ!
Ήχος|8|False|ΝΤΟΟΟΟΝΓΚ!!
Θυμάρα|9|True|Χμμ... Η ΠΙΠΕΡΟΥΛΑ ΕΙΝΑΙ ΠΙΣΩ
Θυμάρα|10|False|ΜΠΡΑΒΟ, ΚΑΓΙΕΝ. ΟΙ ΜΕΘΟΔΟΙ ΕΚΠΑΙΔΕΥΣΗΣ ΣΟΥ ΕΧΟΥΝ ΕΠΙΤΕΛΟΥΣ ΑΠΟΤΕΛΕΣΜΑ.
Καγιέν|11|False|ΕΥΧΑΡΙΣΤΩ, Κα ΔΙΕΥΘΥΝΤΡΙΑ.
Πιπεριά|7|False|ΑΝΥΠΟΜΩΝΩ ΝΑ ΔΩ ΤΟ ΒΛΕΜΜΑ ΣΤΟ ΠΡΟΣΩΠΟ ΤΟΥΣ!
Κείμενο|12|False|Η Κ Ο Μ Ο Ν Α
Πιπεριά|4|False|ΕΙΝΑΙ ΤΟ ΔΕΝΤΡΟ ΤΗΣ ΕΝΟΤΗΤΑΣ ΔΕΝ ΕΙΝΑΙ ΣΑΝ ΕΝΑ “ΞΟΡΚΙ ΚΑΤΑΣΤΡΟΦΗΣ”.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|5|True|Μπρρρr
Ήχος|6|True|Μπρρ
Ήχος|7|True|ερρρρρ
Ήχος|8|True|Μπρρρr
Ήχος|9|True|Μπρρρr
Ήχος|10|False|Μπρ
Καροτούλης|11|False|?
Καροτούλης|13|False|!!
Ήχος|14|True|Μπρρρ
Ήχος|15|False|Μπρρρρ
Καροτούλης|16|False|!!
Πιπεριά|1|True|ΚΑΡΟΤΟΥΛΗ!
Πιπεριά|2|True|ΘΑ ΕΙΜΑΙ ΠΙΣΩ ΣΕ ΛΙΓΟ ΜΕ ΘΥΜΑΡΑ, ΠΙΠΕΡΑΤΟ ΚΑΙ ΚΥΜΙΝΑ.
Πιπεριά|3|False|ΜΗΝ ΠΕΙΡΑΞΕΙΣ ΤΟ ΔΕΝΤΡΟ ΜΕΧΡΙ ΝΑ ΕΡΘΩ, ΕΝΤΑΞΕΙ;
Πιπεριά|4|False|ΕΝΤΑΞΕΙ, ΘΑ ΓΥΡΙΣΩ ΣΥΝΤΟΜΑ!
Πιπεριά|17|True|ΕΤΟΙΜΑΣΤΕΙΤΕ ΝΑ ΜΕΙΝΕΤΕ ΑΝΑΥΔΟΙ! ΔΕΝ ΕΧΕΤΕ ΞΑΝΑΔΕΙ ΚΑΤΙ ΤΕΤΟΙΟ.
Πιπεριά|18|False|ΠΑΡΑΚΑΛΩ, ΕΛΑΤΕ ΕΠΑΝΩ ΝΑ ΔΕΙΤΕ...
Ήχος|12|False|ΣΡΟΥΥΦ!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Αφηγητής|6|False|- ΤΕΛΟΣ -
Ήχος|1|False|ΣΡΟΥΟΥΦΦ!!
Ήχος|4|False|ΚΡΑΑΣ!!!
Ήχος|3|False|ΜΠΟΟΥΜ!!!
Πιπεριά|2|False|;!!
Πιπεριά|5|False|ΦΑΙΝΕΤΑΙ ΟΤΙ ΜΕΛΕΤΗΣΑ ΤΑ “ΞΟΡΚΙΑ ΚΑΤΑΣΤΡΟΦΗΣ” ΤΕΛΙΚΑ!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Συντελεστές|2|False|12/2017 - www.peppercarrot.com - ΣΧΕΔΙΟ & ΣΕΝΑΡΙΟ: David Revoy
Συντελεστές|3|False|Βελτιώσεις διαλόγου: Craig Maloney, CalimeroTeknik, Jookia.
Συντελεστές|5|False|Βασισμένο στο σύμπαν του Έρεβα που δημιουργήθηκε από τον David Revoy με συνεισφορές του Craig Maloney. Διορθώσεις από τους Willem Sonke, Moini, Hali, CGand και Alex Gryson.
Συντελεστές|6|False|Λογισμικό: Krita 3.2.3, Inkscape 0.92.2 σε Ubuntu 17.10
Συντελεστές|7|False|Άδεια: Creative Commons Attribution 4.0
Συντελεστές|9|False|Μπορείτε και εσεις να συνεισφέρετε στο έργο Η Πιπερούλα και Ο Καροτούλης στο www.patreon.com/davidrevoy
Συντελεστές|8|False|Η Πιπερούλα και Ο Καροτούλης είναι εντελώς δωρεάν (ελεύθερο), ανοιχτού κώδικα και υπάρχει χάρη στην υποστήριξη των αναγνωστών του. Για αυτό το επεισόδιο, ευχαριστούμε τους 810 συνεισφέροντες:
Συντελεστές|4|False|Διορθώσεις/Σχόλια προέκδοσης: Alex Gryson, Nicolas Artance, Ozdamark, Zveryok, Valvin και xHire.
Συντελεστές|1|False|ΕΥΤΥΧΙΣΜΕΝΕΣ ΔΙΑΚΟΠΕΣ!
