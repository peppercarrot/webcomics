# Transcript of Pepper&Carrot Episode 24 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 24: Drevo enotnosti

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|5|False|PAF!
Kajenka|1|True|Drevo enotnosti da hočeš?
Kajenka|4|False|Me smo kaosaške čarovnice! S takimi rečmi se NE ukvarjamo.
Kajenka|6|False|Raje se posveti učenju svojih urokov razdejanja!
Paprika|7|True|Razdejanja?
Paprika|8|True|V tem letnem času?
Paprika|9|False|Zakaj se ne bi raje učila urokov ustvarjanja?
Kajenka|10|False|Naslednji posvet Treh lun se bliža in z njim tudi tvoji izpiti. Uči se! Uči se! UČI SE!!! IN NEHAJ TAVATI Z MISLIMI K TEM TRAPASTIM IDEJAM!!!
Kajenka|2|False|Tukaj?!?
Kajenka|3|True|NE!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Bam!
Paprika|5|True|Zima brez drevesa enotnosti?!
Paprika|7|False|Zakaj si ne bi s pomočjo čarovnije ustvarila svojega drevesa?!
Paprika|8|True|Ha! Prvič bi lahko združila hipijaško in kaosaško čarovnijo!
Paprika|9|True|Veliko zeleno drevo, osvetljeno z zvezdami in drobcenimi osončji!
Paprika|10|False|Oooo! Bilo bi popolno!
Paprika|11|True|In bilo bi
Paprika|12|False|ustvarjalno!
Paprika|13|True|Ampak take sorte čarovnije ne morem izvajati tukaj.
Paprika|14|False|Moje botre bi takoj vedele, da se ne učim urokov razdejanja.
Korenček|3|False|tap
Zvok|2|False|Ffff
Paprika|6|False|Nikoli!
Paprika|4|False|Grrrrr…!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|3|False|Šrr
Zvok|17|False|ŠiiING
Napis|22|False|KOMONSKI GLAS
Paprika|1|True|Razen če …
Paprika|2|False|Samo malo …
Paprika|4|False|Aha! Timijana v svojih zapiskih priporoča, da uroke razdejanja preizkušam v mikrodimenziji. Ampak tukaj je cel kup opozoril.
Paprika|16|False|Pa začniva!
Kumina|19|False|Je Paprika pravkar ustvarila mikrodimenzijo?
Zvok|18|False|ŠŠvoomp!!
Timijana|20|True|Dobro!
Napis|8|False|„Iz varnostnih razlogov NE prinašaj rezultatov nazaj.“
Napis|6|False|„Ne zapravi vsega svojega rêsa za ustvarjanje mikrodimenzije.“
Napis|11|True|„NE pričakuj kakršnekoli pomoči od zunaj,
Paprika|7|False|Smiselno.
Paprika|10|False|Aha, tukaj!
Paprika|9|False|Pff… Očitno!
Paprika|5|False|Mm…
Napis|12|False|ker nihče ne more vedeti, kaj počneš v mikrodimenziji!“
Timijana|21|False|Upam, da je razumela vsa tista svarila, ki sem jih zapisala v buklo o urokih.
Paprika|15|True|To je popolno!
Paprika|13|True|Nihče?
Paprika|14|True|A tako …

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Šponsko!
Paprika|2|True|To je nekakšen prostor, ki vsebuje le tisti del knjižnice, ki ga potrebujeva. Popolnoma je ločen od najine resničnosti.
Paprika|3|False|Idealen peskovnik za preizkušanje urokov.
Paprika|4|False|Lotimo se moje mojstrovine!
Zvok|5|False|ŠiiING
Zvok|6|False|PLOooom!!
Zvok|8|False|Duuuff!!
Zvok|10|False|Čting!!
Paprika|7|False|Heh. Am, ne.
Paprika|9|False|Aahh!
Paprika|11|False|To! Videz ima pravi, samo premajhno je še …
Paprika|13|False|KORENČEK! TAČKE STRAN!
Korenček|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Pluuum!!
Paprika|3|True|Mislim, da ne bo nobene nevarnosti, če drevo preneseva ven iz mikrodimenzije.
Paprika|2|True|Tako, je že večje. Vrhunsko!
Paprika|5|True|No tako,
Paprika|6|True|čas je, da mikrodimenzijo prelijem v resničnost in botram pokažem, kako ustvarjalen je lahko Kaosah!
Zvok|8|False|DOooong!!
Timijana|9|True|Mmm… Paprika se je vrnila.
Timijana|10|False|Bravo, Kajenka. Tvoje učne metode so končno obrodile sadove.
Kajenka|11|False|Hvala, ravnateljica.
Paprika|7|False|Komaj čakam, da vidim, kako bodo ostrmele!
Napis|12|False|KOMONSKI GLAS
Paprika|4|False|Saj je samo drevo enotnosti. Čisto nekaj drugega kot urok razdejanja.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|5|True|BRRrrr
Zvok|6|True|BRRr
Zvok|7|True|BRRrrr
Zvok|8|True|BRRrrr
Zvok|9|True|BRRrrr
Zvok|10|False|BRrr
Korenček|11|False|?
Korenček|13|False|!!
Zvok|14|True|BRRr
Zvok|15|False|BRRrrrr
Korenček|16|False|!!
Paprika|1|True|KORENČEK!
Paprika|2|True|Čez minutko se vrnem s Timijano, Kajenko in Kumino.
Paprika|3|False|Ne dotikaj se drevesa, dokler ne pridem nazaj. A je jasno?
Paprika|4|False|Prima! Takoj se vrnem.
Paprika|17|True|Pripravite se, da boste osuple od presenečenja!
Paprika|18|False|Nikoli še niste videle česa takega. Pridite z menoj gor, da vam pokažem …
Zvok|12|False|Šruuf!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|6|False|- KONEC -
Zvok|1|False|Šruuuuf!!
Zvok|4|False|TRESK!!!
Zvok|3|False|Fjuuuu!!!
Paprika|2|False|?!!
Paprika|5|False|Šment, pa sem se vseeno naučila uroka razdejanja!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|2|False|December 2017 - www.peppercarrot.com - Piše in riše David Revoy - Slovenski prevod: Andrej Ficko in Gorazd Gorup
Zasluge|3|False|Izboljšave dialoga: Craig Maloney, CalimeroTeknik, Jookia.
Zasluge|5|False|Dogaja se v vesolju Hereva avtorja Davida Revoyja s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|6|False|Programska oprema: Krita 3.2.3 in Inkscape 0.92.2 na Ubuntu 17.10
Zasluge|7|False|Licenca: Creative Commons Priznanje avtorstva 4.0.
Zasluge|9|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
Zasluge|8|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 810 bralcev:
Zasluge|4|False|Lektoriranje/Beta povratne informacije: Alex Gryson, Nicolas Artance, Ozdamark, Zveryok, Valvin in xHire.
Zasluge|1|False|Vesele praznike!
