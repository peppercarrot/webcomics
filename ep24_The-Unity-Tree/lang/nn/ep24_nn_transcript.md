# Transcript of Pepper&Carrot Episode 24 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 24: Samhaldstreet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|5|False|POFF!
Kajenne|1|True|Eit samhalds-tre?
Kajenne|4|False|Me er kaosah-hekser, så me DRIV IKKJE med slikt.
Kajenne|6|False|Bruk heller tida på å læra deg øydeleggings-formlane!
Pepar|7|True|Øydelegging?
Pepar|8|True|På denne tida av året?
Pepar|9|False|Ville det ikkje vore betre med skapings-formlar?
Kajenne|10|False|Snart er det tid for neste rådsmøte under dei tre månane – og for eksamen! Så lær! Pugg! ØV!!! OG SLUTT MED Å LA DEG DISTRAHERA AV DESSE TOSKNE INNFALLA!!!
Kajenne|2|False|Her?!?
Kajenne|3|True|NEI!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Kleis!
Pepar|5|True|Vinter utan noko samhaldsstre?!
Pepar|7|False|Eg får laga mitt eige tre – ved hjelp av trolldom!
Pepar|8|False|Ja! Kanskje eg for fyrste gong kan kombinera trolldom frå både hippiah og kaosah!
Pepar|9|False|Eit stort, grønt tre, lyssett av stjerner og mini-solsystem!
Pepar|10|False|Å, det hadde vore heilt nydeleg!
Pepar|11|True|Og ikkje minst
Pepar|12|False|kreativt!
Pepar|13|True|Men eg kan ikkje bruka slik magi her ...
Pepar|14|False|Gudmødrene ville straks oppdaga at eg ikkje pugga øydeleggingsformlar.
Gulrot|3|False|trykk
Lyd|2|False|Ffff
Pepar|6|False|Aldri!
Pepar|4|False|Grrrrr...!!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|3|False|Knnsj
Lyd|17|False|SjiiING
Skrift|22|False|KOMONA-nytt
Pepar|1|True|Med mindre ...
Pepar|2|False|Om eg ikkje hugsar feil ...
Pepar|4|False|Nettopp! I kapittelet om øydeleggingsformlar rår Timian oss til fyrst å prøva dei ut i mikrodimensjonar. Men så mange åtvaringar det er:
Pepar|16|False|Til verket!
Karve|19|False|Var det Pepar som laga ein mikrodimensjon?
Lyd|18|False|Sjvoomp!!
Timian|20|True|Bra!
Skrift|8|False|«Av tryggleiksgrunnar må du ALDRI ta med deg resultatet tilbake.»
Skrift|6|False|«IKKJE bruk opp all reaen på å skapa mikrodimensjonen.»
Skrift|11|True|«IKKJE forvent hjelp utanfrå ...
Pepar|7|False|Seier seg sjølv.
Pepar|10|False|Her er det!
Pepar|9|False|Pff... Sjølvsagt ikkje!
Pepar|5|False|Mm...
Skrift|12|False|... INGEN kan sjå kva du driv med!»
Timian|21|False|Håpar ho fekk med seg alle åtvaringane som eg skreiv i kapittelet om øydeleggingsformlar.
Pepar|15|True|Heilt perfekt!
Pepar|13|True|Ingen?
Pepar|14|True|Verkeleg?!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Dødskult!
Pepar|2|True|Det er ei boble som inneheld berre den delen av biblioteket eg treng – og er heilt isolert frå vår eiga verd.
Pepar|3|False|Ei perfekt leikegrind for utprøving av trylleformlar.
Pepar|4|False|No kan eg konsentrera meg om meisterverket!
Lyd|5|False|SjiiING
Lyd|6|False|PLOooom!!
Lyd|8|False|Dhoooff!!
Lyd|10|False|Tsjting!!
Pepar|7|False|He. Trur ikkje det ...
Pepar|9|False|Æsssj!
Pepar|11|False|Ja! Ser bra ut, men er kanskje litt lite ...
Pepar|13|False|IKKJE RØR DET, GULROT!
Gulrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Plooom!!
Pepar|3|True|Men eg ser ikkje kva skade det kan gjera å ta treet ut av mikrodimensjonen.
Pepar|2|True|Der! Rett storleik. No er det heilt perfekt!
Pepar|5|True|Greitt!
Pepar|6|True|På tide å få flytta denne mikro-dimensjonen inn i røynda, så dei får sjå kor kreativt kaoset kan vera!
Lyd|8|False|DOooong!!
Timian|9|True|Der ja – Pepar er tilbake.
Timian|10|False|Bravo, Kajenne. Pedagogikken din kastar endeleg av seg.
Kajenne|11|False|Takkar, overheks Timian.
Pepar|7|False|Då vert dei nok lange i maska!
Skrift|12|False|KOMONA-nytt
Pepar|4|False|Det er jo berre eit samhaldstre – ikkje ein øydeleggingsformel.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|5|True|BRRrrr
Lyd|6|True|BRRr
Lyd|7|True|BRRrrr
Lyd|8|True|BRRrrr
Lyd|9|True|BRRrrr
Lyd|10|False|BRrr
Gulrot|11|False|?
Gulrot|13|False|!!
Lyd|14|True|BRRr
Lyd|15|False|BRRrrrr
Gulrot|16|False|!!
Pepar|1|True|GULROT!
Pepar|2|True|Eg skal berre ned og henta Timian, Kajenne og Karve.
Pepar|3|False|Og ikkje rør noko i mellomtida, forstått?!
Pepar|4|False|Bra. Eg er straks tilbake!
Pepar|17|True|De kjem til å verta heilt himmelfalne! Tippar de aldri har sett noko liknande!
Pepar|18|False|Berre følg med opp ...
Lyd|12|False|Sjroof!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|6|False|– SLUTT –
Lyd|1|False|Sjroooof!!
Lyd|4|False|KRASJ!!!
Lyd|3|False|Fooom!!!
Pepar|2|False|?!!
Pepar|5|False|Ser visst ut til at eg har studert øydeleggings-formlar likevel ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|2|False|Desember 2017 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen
Bidragsytarar|3|False|Forbetring av dialogen: Craig Maloney, CalimeroTeknik og Jookia.
Bidragsytarar|5|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Bidragsytarar|6|False|Programvare: Krita 3.2.3 og Inkscape 0.92.2 på Ubuntu 17.10
Bidragsytarar|7|False|Licens: Creative Commons Attribution 4.0
Bidragsytarar|9|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
Bidragsytarar|8|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 810 som støtta denne episoden:
Bidragsytarar|4|False|Korrekturlesing og tidleg tilbakemelding: Alex Gryson, Nicolas Artance, Ozdamark, Zveryok, Valvin og xHire.
Bidragsytarar|1|False|Gledeleg jul!
