# Episode 31: The Fight

![cover of episode 31](https://www.peppercarrot.com/0_sources/ep31_The-Fight/low-res/Pepper-and-Carrot_by-David-Revoy_E31.jpg)

## Comments from the author

For the violence, maybe this episode is a good example of how far I'll go (flirting with the limit). Exploding things, fire, projections but I'll never show too much body damages. I might suggest it but I'll never show to respect the wide audience of Pepper&Carrot (I know very young kids follow also the serie)!

I tried to not really mimic fire effect in this episode but plasmas with wavy and electromagnetic patterns. It worked most of the time, but I admit when I finished the episode I was a bit disapointed of the 'disco' colorful appearance because I had something probably a bit more serious in mind with probably a palette a bit more limited. One thing is sure; I like the deep violet as a signature color for the raw Chaosah spells and I like Pepper to colorize her spell in Pink or Green as for colored fireworks (a fancy detail, because Pepper is all about this little extra fun touch). 

The panel is part of the webcomic, and a prank I do to the reader: the reality of Cayenne disapears under too much compression leaving a pure void and I translated that void by the image missing in the web-browser.

Haha, one of the joke of Episode 31 revealed to be like a prank directly to a lot of non-technician readers suddently thinking their browser had an issue or the webcomic was broken and pressing Refresh/F5. I had probably 10 publics feedback about it and almost the same in private. it's obvious this joke doesn't work for everyone and I want to work about fixing it a bit.

From [Author's blog of episode 31](https://www.davidrevoy.com/article749/episode-31-the-fight/show#comments)
