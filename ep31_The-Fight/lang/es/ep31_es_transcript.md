# Transcript of Pepper&Carrot Episode 31 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 31: El combate

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|Tenebruma, Colina sagrada de Chaosah.
Sonido|2|False|¡BRooom!
Pimienta|3|False|¡Tss!
Sonido|4|False|BRoooom
Sonido|5|False|¡Bzooo!
Pimienta|6|False|¡Toma esta!
Sonido|8|False|¡Schh!
Cayena|9|False|¡Pipiola!
Pimienta|10|False|¡¡!!
Sonido|11|False|¡BLAM!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡Drssioooo!
Sonido|2|False|¡¡¡CRAC!!!
Sonido|3|False|¡¡¡CRAAC!!!
Pimienta|4|False|¡¡No tan rápido!!
Pimienta|5|False|¡ESCUDUS GRAVITATIONALIS!
Sonido|6|False|¡¡DSSsioo!!
Sonido|7|False|¡¡Tchkshkk!!
Sonido|8|False|¡¡Tchkshkk!!
Sonido|9|False|¡Toc!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡ZANAHORIA!
Pimienta|2|False|¡Plan 7-B!
Pimienta|4|False|¡JPEGUS QUALITIS!
Sonido|5|False|¡Bsiooo!
Sonido|3|False|Zooom
Cayena|6|False|¡¿?!
Cayena|10|False|¡¡Argh!!
Sonido|7|True|G
Sonido|8|True|Z|nowhitespace
Sonido|9|False|Z|nowhitespace
Pimienta|11|False|¡QUALITIS MINIMALIS!
Cayena|12|False|¡¡!!
Cayena|13|False|Grr...
Escritura|14|False|2019-11-20-E31P03_V15-final.jpg
Escritura|15|False|Error: No se ha podido cargar
Sonido|16|False|¡ ¡ ¡ CRASH !!!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|1|False|¡PYRO BOMBA ATOMICUS!
Sonido|2|False|¡Frrrooou!
Sonido|3|True|K
Sonido|4|True|A|nowhitespace
Sonido|5|True|B|nowhitespace
Sonido|6|True|O|nowhitespace
Sonido|7|True|O|nowhitespace
Sonido|8|True|M|nowhitespace
Sonido|9|True|!|nowhitespace
Sonido|10|False|!|nowhitespace
Sonido|11|True|BRRR
Sonido|12|False|BRRR
Sonido|13|False|Schhhhh...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|1|False|Pfff...
Cayena|2|True|¿Huyendo a una microdimensión a tu edad?
Cayena|3|False|Qué derrota tan patética...
Pimienta|4|True|¡Incorrecto!
Pimienta|5|False|¡Un agujero de gusano!
Sonido|6|False|¡¡Bzz!!
Pimienta|7|False|¡Gracias por hacer la salida, Zanahoria!
Pimienta|8|False|¡Jaque mate, maestra Cayena!
Pimienta|9|False|¡GURGES...
Pimienta|10|False|...ATER!
Sonido|12|False|¡¡¡Swwwwwwiiiiiiiipppp!!!
Sonido|11|False|¡¡¡BROOOM!!!
Tomillo|13|False|¡¡¡STOP!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tomillo|1|True|¡He dicho «STOP»!
Tomillo|2|False|¡Es suficiente!
Sonido|3|False|¡Chas!
Sonido|4|False|¡CHAC!
Sonido|5|False|¡TSHK!
Tomillo|6|True|¡Vosotras dos!
Tomillo|7|True|¡AQUÍ!
Tomillo|8|False|¡Inmediatamente!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡Toc!
Sonido|2|False|¡Toc!
Tomillo|3|True|Llevas tres años diciéndonos «una última prueba más»...
Tomillo|4|False|...¡no vamos a pasar aquí toda la noche!
Tomillo|5|False|¿Entonces... ?
Pimienta|6|False|...
Cayena|7|True|OK...
Cayena|8|True|Ella puede tener su diploma...
Cayena|9|False|...pero solo con una nota «aceptable».
Escritura|10|True|Diploma
Escritura|11|True|de
Escritura|12|False|Chaosah
Escritura|14|False|Cayena
Escritura|13|False|Comino
Escritura|15|False|Tomillo
Escritura|16|False|~ para Pimienta ~
Escritura|17|False|Bruja oficial
Narrador|18|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|5|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|3|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 971 mecenas!
Pimienta|7|True|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|6|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|8|False|¡Gracias!
Pimienta|2|True|¿Sabías qué?
Créditos|1|False|El 20 de diciembre de 2019 Dibujo & guion: David Revoy. Lectores de la versión beta: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Versión en castellano Traducción: TheFaico. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.2.6appimage, Inkscape 0.92.3 en Kubuntu 18.04-LTS. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
