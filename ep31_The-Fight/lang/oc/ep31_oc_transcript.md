# Transcript of Pepper&Carrot Episode 31 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 31 : La batèsta

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Tenebruma, Puèg sacrat de Caosah.
Son|2|False|VRiuuu !
Pepper|3|False|Tss !
Son|4|False|VRiuuuuu
Son|5|False|Bziuuu !
Pepper|6|False|Manja-te aquò !
Son|8|False|Schh !
Cayenne|9|False|Amatritz !
Pepper|10|False|!!
Son|11|False|FLAU !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Dzzziuuuu !
Son|2|False|CRAC !!!
Son|3|False|CRAAC !!!
Pepper|4|False|Espèra un pauc !!!
Pepper|5|False|BOUCLIUS GRAVITATIONAS !
Son|6|False|DZZziuu !!
Son|7|False|Tchkchkk !!
Son|8|False|Tchkchkk !!
Son|9|False|Tòc !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|CARRÒT !
Pepper|2|False|Plan 7-B !
Pepper|4|False|JPEGUS QUALITIS !
Son|5|False|Bziuuu !
Son|3|False|Zoooo
Cayenne|6|False|?!!
Cayenne|10|False|Argh !!
Son|7|True|G
Son|8|True|Z|nowhitespace
Son|9|False|Z|nowhitespace
Pepper|11|False|QUALITIS MINIMALIS !
Cayenne|12|False|!!
Cayenne|13|False|Grr...
Escritura|14|False|2019-12-20-E31P03_V15-final.jpg
Escritura|15|False|Error pendent lo cargament
Son|16|False|CRACH !!!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|PYRO BOMBA ATOMICUS !
Son|2|False|Frrroooo !
Son|3|True|K
Son|4|True|A|nowhitespace
Son|5|True|B|nowhitespace
Son|6|True|O|nowhitespace
Son|7|True|O|nowhitespace
Son|8|True|M|nowhitespace
Son|9|True|!|nowhitespace
Son|10|False|!|nowhitespace
Son|11|True|BRRR
Son|12|False|BRRR
Son|13|False|Schhhhh...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pfff...
Cayenne|2|True|Fugir dins una microdimension a ton atge ?
Cayenne|3|False|Quala desfaita patetica...
Pepper|4|True|Mancat !
Pepper|5|False|Un trauc de vèrm !
Son|6|False|Bzz !!
Pepper|7|False|Mercés Carròt per la sortida !
Pepper|8|False|Escac e mat, mèstra Cayenne !
Pepper|9|False|GURGES...
Pepper|10|False|...ATER !
Son|12|False|Swwwwwwiiiiiiiipppp !!!
Son|11|False|VROOO !!!
Frigola|13|False|BASTA !!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Frigola|1|True|Ai dit : BASTA !
Frigola|2|False|Es pron !
Son|3|False|Clac !
Son|4|False|PCHAC !
Son|5|False|TCHK !
Frigola|6|True|Vosautras doas !
Frigola|7|True|AQUÍ !
Frigola|8|False|Sul pic !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Tòc !
Son|2|False|Tòc !
Frigola|3|True|Fa tres ans que nos fas l'istòria d'una « verificacion de darrièra minuta »...
Frigola|4|False|... I anam pas passar la nuèit !
Frigola|5|False|Alara... ?
Pepper|6|False|...
Cayenne|7|True|D'acòrdi...
Cayenne|8|True|... Pòt aver son diplòme...
Cayenne|9|False|... mas mencion « passable ».
Escritura|10|True|Diplòme
Escritura|11|True|de
Escritura|12|False|Caosah
Escritura|14|False|Cayenne
Escritura|13|False|Comin
Escritura|15|False|Frigola
Escritura|16|False|~ per Pepper ~
Escritura|17|False|Masca oficiala
Narrator|18|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Podètz tanben venir mecèna de Pepper&Carrot et aver vòstre nom marcat aquí !
Pepper|3|True|Pepper&Carrot es completament liure, gratuit, open-source e esponsorizat mercés au mecenat de sos lectors.
Pepper|4|False|Aqueste episòdi recebèt lo sosten de 971 mecènas !
Pepper|7|True|Anatz sus www.peppercarrot.com per mai d'informacions !
Pepper|6|True|Sèm sus Patreon, Tipeee, PayPal, Liberapay …e d'autres !
Pepper|8|False|Mercé !
Pepper|2|True|O sabiatz ?
Crèdits|1|False|Lo 20 de decembre de 2019 Art & scenari : David Revoy. Lectors de la version beta : Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Version occitana (lengadocian) Traduccion : Aure Séguier. Basat sus l'univèrs d'Hereva Creator : David Revoy. Manteneire principal : Craig Maloney. Redactors : Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logicials : Krita 4.2.6appimage, Inkscape 0.92.3 sur Kubuntu 18.04-LTS. Licéncia : Creative Commons Attribution 4.0. www.peppercarrot.com
