# Transcript of Pepper&Carrot Episode 20 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 20: Piknik

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|- KONEC -
Zasluge|2|False|December 2016 - www.peppercarrot.com - Piše in riše David Revoy
Zasluge|3|False|Dogaja se v vesolju Hereve avtorja Davida Revoyja s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|4|False|Licenca: Creative Commons Priznanje avtorstva 4.0 - Programska oprema: Krita 3.1, Inkscape 0.91 na Manjaru XFCE

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 825 bralcev:
Zasluge|2|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
