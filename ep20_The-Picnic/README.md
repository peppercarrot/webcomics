# Episode 20: The Picnic

![cover of episode 20](https://www.peppercarrot.com/0_sources/ep20_The-Picnic/low-res/Pepper-and-Carrot_by-David-Revoy_E20.jpg)

## Comments from the author

Horses of Camargue always fascinated me (on TV documentaries when I was kids). It probably influenced my imagination about I picture wild horses and so, unicorns. :)

From [Author's blog of episode 20](https://www.davidrevoy.com/article593/episode-20-the-picnic/show#comments)
