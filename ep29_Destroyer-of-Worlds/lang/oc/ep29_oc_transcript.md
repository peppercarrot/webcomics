# Transcript of Pepper&Carrot Episode 29 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 29 : Lo Destructor de Mondes

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mostre|2|True|…Enfin !
Mostre|3|False|Una esquinçada interdimensionala !
Mostre|4|False|Causada, probable, per un alinhament d'astre o un eveniment grand !
Mostre|5|True|Creis… Creis, petita falha !
Mostre|6|False|E desvela-me aquel monde novèl a ASSERVIR E DOMINAR !
Mostre|7|True|Tè…
Mostre|8|False|Aquí quicòm de plan interessant.
Mostre|9|False|Podèm quitament díser que soi plan astruc…
Narrator|1|False|Al meteis moment, dins una autra dimension…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mostre|1|False|Una dimension poblada de creaturas intelligentas !
Mostre|2|False|Creis, petita falha !
Mostre|3|False|Creis… Mohaha hahaha !
Pepper|4|True|Of !
Pepper|5|False|Mas quala serada amigas !
Coriandre|6|False|Tè Pepper, èrem a nos mainar, amb Safran, qu'aviam pas cap d'idèa de cossí foncionava ta magia, Caosah.
Safran|7|False|Es plan misteriós, ne nos poiriás díser mai ?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Haha, serà un pauc complicat !
Pepper|2|True|Mas bon, se pòt díser qu'es mai que mai basat sus la compreneson de las leis darrièr totes los sistèmas caotics…
Pepper|3|False|…dels mai petits als mai grands.
Coriandre|4|False|Solide que ditz coma aquó, sul pic, es mai clar…
Pepper|7|False|Pff ! Esperatz, vos vau far un exemple tot simple.
Pepper|8|False|Daissatz-me sonque una minuta, amb un pauc de jutjament de Caosah…
Pepper|9|False|Trobat !
Son|5|True|GRAT
Son|6|False|GRAT
Pepper|10|True|Gaitatz aqueste pic de boleta qu'èra cunhat entre doas caladas.
Pepper|11|False|Lo fait de l'aver trobat e amassat empacharà segurament a qualqu'un de lo s'empegar dins los pès.
Pepper|12|True|Un petit cambiament positiu dins le grand sistèma caotic de l'existéncia, que pòt aver de consequéncias grandassas.
Pepper|13|False|Es aquò Caosah !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Desgostant !
Coriandre|2|False|Bèèè ! Aquò èra dins la boca de qualqu'un !
Safran|3|False|Haha ! Impressionant, coma a cada còp, Pepper !
Coriandre|4|False|Bon, mercé Pepper per aquela… « explicacion ».
Coriandre|5|True|Es ora d'anar dormir cresètz pas ?
Coriandre|6|False|E de se lavar las mans per d'unas.
Pepper|7|False|Eu ! Mas esperatz-me !
Son|8|False|Pòc !
Son|9|True|Pòc !
Son|10|False|Pòc !
Son|11|False|PAF !
Carròt|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Ssss !
Son|2|False|Pòc !
Son|3|False|Tòc !
Son|4|False|CLING !
Son|5|False|Frrfrf…

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mostre|7|False|A L'ATACA !!!
Mostre|6|True|MoahahaHAHA ! Enfin !
Mostre|9|False|!?
Escritura|1|True|ENTREPAUS
Escritura|2|False|FUÒCS D'ARTIFICI
Son|3|False|FrrOof !!
Son|4|False|Frr !!
Son|5|False|BOM !
Son|8|False|Zuiff !!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|3|False|BoM !
Son|2|False|BOM!
Son|1|False|PAF !
Son|4|False|Zuiff !!
Son|5|False|PoF !
Pepper|6|True|Te'n fagas pas, Carròt.
Pepper|7|False|Probable qu'es sonque de monde que festeja encara…
Pepper|8|False|Anem, al lièit !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mostre|5|False|...
Comin|1|False|Vertat ? … Auriá capitat une reaccion en cadena de tan granda escala, e sens se'n mainar pas ?
Cayenne|2|False|Aquò es segur.
Frigola|3|False|Dònas, cresi que nòstra Pepper es prèsta, enfin !
Son|6|False|Plòp !
Son|4|False|Pchhh !!
Narrator|7|False|- TRILOGIA DEL SACRE DE CORIANDRE, FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vos tanben, podètz venir mecèna de Pepper&Carrot e aver vòstre nom marcat aquí !
Pepper|3|True|Pepper&Carrot es completament liure, gratuit, open-source e esponsorizat mercés al mecenat de sos lectors.
Pepper|4|False|Aqueste episòdi recebèt lo sosten de 960 mecènas !
Pepper|7|True|Anatz sus www.peppercarrot.com per mai d'informacions !
Pepper|6|True|Sèm sus Patreon, Tipeee, PayPal, Liberapay …e d'autres !
Pepper|8|False|Mercé !
Pepper|2|True|O sabiatz ?
Crèdits|1|False|lo 25 d'abril de 2019 Art & scenari : David Revoy. Lectors de la version beta : CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Version occitana Traduccion : Aure Séguier. Basat sus l'univèrs d'Hereva Creator : David Revoy. Mainteneire màger : Craig Maloney. Redactors : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logicials : Krita 4.1.5~appimage, Inkscape 0.92.3 sus Kubuntu 18.04.1. Licéncia : Creative Commons Attribution 4.0. www.peppercarrot.com
