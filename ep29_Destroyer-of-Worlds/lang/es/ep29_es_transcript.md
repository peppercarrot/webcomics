# Transcript of Pepper&Carrot Episode 29 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 29: El Destructor de Mundos

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstruo|2|True|¡Por fin!
Monstruo|3|False|¡Una ruptura interdimensional!
Monstruo|4|False|¡Probablemente causada por un gran acontecimiento cósmico!
Monstruo|5|True|¡Crece! ¡Crece, pequeña abertura!
Monstruo|6|False|¡Y desvélame este nuevo mundo que DOBLEGAR Y DOMINAR!
Monstruo|7|True|¡Oooh!
Monstruo|8|False|Esto sí que es interesante...
Monstruo|9|False|Parece que es mi día de suerte...
Narrador|1|False|Mientras tanto, en otra dimensión...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstruo|1|False|¡Una dimensión habitada por criaturas inteligentes!
Monstruo|2|False|¡Más deprisa, pequeña brecha!
Monstruo|3|False|¡Crece, muajaja jajaja!
Pimienta|4|True|¡Uf!
Pimienta|5|False|¡Qué gran fiesta, amigas!
Celandria|6|False|Oye, Pimienta, Azafrán y yo hemos notado que no tenemos ni idea de cómo funciona la magia de Chaosah.
Azafrán|7|False|Es muy misteriosa, ¿podrías contarnos algo más sobre ella?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Jaja, ¡es un poco difícil de explicar!
Pimienta|2|True|Pero bueno, se podría decir que está basado en la comprensión de las leyes que están detrás de los sistemas caóticos...
Pimienta|3|False|...del más pequeño al más grande.
Celandria|4|False|Bueno, es cierto que así ha quedado más claro...
Pimienta|7|False|Hum, esperad, os voy a mostrar un ejemplo fácil.
Pimienta|8|False|Dejadme sólo un segundo que me inspire con la sabiduría de Chaosah...
Pimienta|9|False|¡Lo tengo!
Sonido|5|True|RAS
Sonido|6|False|RAS
Pimienta|10|True|Mirad este pincho de brocheta que estaba atrapado entre esos adoquines.
Pimienta|11|False|El hecho de haberlo encontrado y retirado ha evitado con seguridad que alguien lo pisase.
Pimienta|12|True|Un pequeño cambio positivo en un gran sistema caótico de la existencia puede tener enormes consecuencias.
Pimienta|13|False|¡Así funciona Chaosah!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|¡Oh, aj!
Celandria|2|False|¡Puaj! ¡Esa cosa estaba en la boca de alguien!
Azafrán|3|False|¡Jaja! ¡Impresionante como siempre, Pimienta!
Celandria|4|False|Bien, Pimienta, gracias por esta... «explicación».
Celandria|5|True|Ya es hora de dormir, ¿no?
Celandria|6|False|Y hora de lavarse las manos para algunos.
Pimienta|7|False|¡Eh! Pero... ¡esperadme!
Sonido|8|False|¡Poc!
Sonido|9|True|¡Poc!
Sonido|10|False|¡Poc!
Sonido|11|False|¡PAF!
Zanahoria|12|False|¿?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡Ssssh!
Sonido|2|False|¡Poc!
Sonido|3|False|¡Toc!
Sonido|4|False|¡CLING!
Sonido|5|False|Fluuoosh...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstruo|7|False|¡¡¡AL ATAQUE!!!
Monstruo|6|True|¡MuajajaJAJA! ¡Por fin!
Monstruo|9|False|¿¡!?
Escritura|1|True|ALMACÉN
Escritura|2|False|PIROTÉCNICO
Sonido|3|False|¡¡FluOushh!!
Sonido|4|False|¡¡Flossh!!
Sonido|5|False|¡BUM!
Sonido|8|False|¡¡Fiiiiiiu!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|3|False|¡BUM!
Sonido|2|False|¡BUM!
Sonido|1|False|¡PAF!
Sonido|4|False|¡¡Fiiiiiiu!!
Sonido|5|False|¡PUF!
Pimienta|6|True|No te preocupes, Zanahoria.
Pimienta|7|False|Parece que quedan algunos que siguen de fiesta...
Pimienta|8|False|¡Vamos, a dormir!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstruo|5|False|...
Cumino|1|False|¿En serio? ¿Ella ha conseguido una reacción en cadena a esa escala sin darse ni cuenta de ello?
Cayena|2|False|Sin duda.
Tomillo|3|False|Señoras, ¡creo que nuestra Pimienta por fin está lista!
Sonido|6|False|¡Fuip!
Sonido|4|False|¡¡Fsss !!
Narrador|7|False|- TRILOGÍA DE LA CORONACIÓN DE CELANDRIA, FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|5|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|3|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|¡Este episodio ha recibido el apoyo de 960 mecenas!
Pimienta|7|True|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|6|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|8|False|¡Gracias!
Pimienta|2|True|¿Sabias qué?
Créditos|1|False|Abril de 2019 Dibujo & guion: David Revoy. Lectores de la versión beta: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Versión en castellano Traducción: TheFaico. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.1.5~appimage, Inkscape 0.92.3 en Kubuntu 18.04.1. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
