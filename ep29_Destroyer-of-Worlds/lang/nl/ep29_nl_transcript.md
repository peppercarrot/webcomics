# Transcript of Pepper&Carrot Episode 29 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 29: Verwoester der werelden

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|2|True|…Eindelijk !
Monster|3|False|Een interdimensionale scheur!
Monster|4|False|Zonder twijfel veroorzaakt door een kosmische gebeurtenis.
Monster|5|True|Groei… Groei, mijn scheurtje!
Monster|6|False|En toon mij deze nieuwe wereld die ik kan ONDERWERPEN EN OVERHEERSEN !
Monster|7|True|Nou, nou…
Monster|8|False|Dat is nu eens interessant.
Monster|9|False|Ik zou zelfs durven zeggen dat het mijn geluksdag is…
Verteller|1|False|Op dat moment, in een andere dimensie…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|Een dimensie bewoond door intelligente wezens!
Monster|2|False|Groei, mijn scheur!
Monster|3|False|Groei… Mwoehaha hahaha!
Pepper|4|True|Wauw!
Pepper|5|False|Wat een fantastisch feest was dat!
Koriander|6|False|Zeg Pepper, Saffraan en ik beseften net dat we geen idee hebben hoe jouw Chaosah-magie werkt.
Saffraan|7|False|Het is nogal mysterieus, kun je het ons niet even uitleggen?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Haha, dat is niet zo eenvoudig.
Pepper|2|True|Je zou kunnen zeggen dat het in principe gebaseerd is op een begrip van de fysische wetten van chaotische systemen…
Pepper|3|False|…van de kleinste tot de grootste.
Koriander|4|False|Aha. Nu is alles opeens veel duidelijker.
Pepper|7|False|Boeh! Wacht, ik geef jullie een eenvoudig voorbeeld.
Pepper|8|False|Ik gebruik even mijn Chaosah-buikgevoel…
Pepper|9|False|Daar!
Geluid|5|True|SKRR
Geluid|6|False|SKRR
Pepper|10|True|Ziehier een prikkertje dat tussen twee klinkers vastzat.
Pepper|11|False|Door het op te rapen, heb ik er sowieso voor gezorgd dat iemand er nu niet op zal stappen.
Pepper|12|True|Kleine, positieve wijzigingen in het chaotische systeem van ons bestaan kunnen grootse gevolgen hebben.
Pepper|13|False|Dat is Chaosah!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Oh bah!
Koriander|2|False|Ieuw! Dat ding heeft in iemands mond gezeten!
Saffraan|3|False|Haha! Zoals altijd erg indrukwekkend, Pepper!
Koriander|4|False|Goed. Pepper, bedankt voor deze… “uitleg”.
Koriander|5|True|Ik denk dat het nu tijd is om te gaan slapen.
Koriander|6|False|En iemand wast beter eerst haar handen.
Pepper|7|False|Hé! Wacht op mij!
Geluid|8|False|Toink!
Geluid|9|True|Pok!
Geluid|10|False|Pok!
Geluid|11|False|WAM!
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Zzzzz!
Geluid|2|False|Pok!
Geluid|3|False|Tok!
Geluid|4|False|KRASJ!
Geluid|5|False|Srrrrr…

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|7|False|TEN AANVAL!!!
Monster|6|True|MwoehahaHAHA ! Eindelijk!
Monster|9|False|!?
Geschrift|1|True|MAGAZIJN
Geschrift|2|False|VUURWERK
Geluid|3|False|Srrwoef!!
Geluid|4|False|Srr!!
Geluid|5|False|BOEM!
Geluid|8|False|Fzwiee!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|3|False|Fwuuu!
Geluid|2|False|BOEM!
Geluid|1|False|KRAK!
Geluid|4|False|Fwiee!!
Geluid|5|False|POEF!
Pepper|6|True|Geen zorgen, Carrot.
Pepper|7|False|Dat zijn gewoon mensen die nog aan het feesten zijn…
Pepper|8|False|Hup, oogjes toe!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|5|False|...
Komijn|1|False|Echt?… Zou ze een ketting-reactie van die omvang in gang gezet hebben zonder zich er zelfs maar bewust van te zijn?
Cayenne|2|False|Geen twijfel mogelijk.
Thym|3|False|Dames, ik denk dat onze Pepper eindelijk klaar is!
Geluid|6|False|Fwip!
Geluid|4|False|Fshhh!!
Verteller|7|False|– TRILOGIE VAN DE KRONING VAN KORIANDER, EINDE –

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|25 april 2019 Tekeningen & verhaal: David Revoy. Bèta-feedback: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Nederlandstalige versie Vertaling: Midgard. Proeflezing: Marno van der Maas. Gebaseerd op het universum van Hereva Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.1.5~appimage, Inkscape 0.92.3 op Kubuntu 18.04.1. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|3|True|Pepper&Carrot is helemaal vrij, open-bron en gesponsord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik de 960 patronen!
Pepper|7|True|Wist je dat?
Pepper|6|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|8|False|Kijk op www.peppercarrot.com voor alle info!
Pepper|2|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay … en meer!
Aftiteling|1|False|Dank je!
