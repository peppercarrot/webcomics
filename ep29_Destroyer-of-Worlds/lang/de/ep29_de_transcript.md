# Transcript of Pepper&Carrot Episode 29 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 29: Der Weltenzerstörer

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|2|True|Endlich!
Monster|3|False|Ein Riss zwischen den Dimensionen!
Monster|4|False|Wahrscheinlich verursacht durch ein kosmisches Ereignis!
Monster|5|True|Wachs! Wachs, kleiner Spalt!
Monster|6|False|Und gib mir eine neue Welt zu VERSKLAVEN und BEHERRSCHEN!
Monster|7|True|Nanu…
Monster|8|False|Das ist ja interessant.
Monster|9|False|Heute ist wohl mein Glückstag…
Erzähler|1|False|Währenddessen, in einer anderen Dimension…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|Eine Dimension mit intelligentem Leben!
Monster|2|False|Schneller, kleiner Spalt!
Monster|3|False|Wachs! Muhaha hahaha!
Pepper|4|True|Uff!
Pepper|5|False|Was für eine gross-artige Party!
Coriander|6|False|Hey Pepper, Saffron und ich dachten gerade, dass wir gar nicht wissen, wie deine Chaosāh-Magie funktioniert.
Saffron|7|False|Sie ist so geheimnisvoll. Kannst du uns mehr davon erzählen?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Haha, das ist etwas schwierig zu erklären.
Pepper|2|True|Nun, man könnte sagen, sie basiert auf dem Verständnis der grund-legenden Gesetze von chaotischen Systemen…
Pepper|3|False|…von den kleinsten zu den größten.
Coriander|4|False|Viel schlauer bin ich daraus nicht geworden.
Pepper|7|False|Wartet, lasst mich ein einfaches Beispiel machen.
Pepper|8|False|Ich brauche nur einen Moment mit meiner Chaosāh-Intuition…
Pepper|9|False|Da!
Geräusch|5|True|KRATZ
Geräusch|6|False|KRATZ
Pepper|10|True|Seht ihr diesen Spieß, der zwischen den Pflastersteinen steckte?
Pepper|11|False|Indem ich ihn aufgehoben habe, verhindere ich, dass jemand darauf tritt.
Pepper|12|True|Eine kleine, positive Veränderung im großen, chaotischen System der Existenz kann bedeutende Konsequenzen haben.
Pepper|13|False|Darum geht es bei Chaosāh!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Ekelhaft!
Coriander|2|False|Igitt! Jemand hatte den im Mund!
Saffron|3|False|Haha! Beeindruckend wie immer, Pepper!
Coriander|4|False|Nun Pepper, danke für diese "Erklärung".
Coriander|5|True|Es ist wohl Zeit, ins Bett zu gehen, nicht?
Coriander|6|False|Und sich die Hände zu waschen, für jemanden.
Pepper|7|False|Hey! Wartet!
Geräusch|8|False|Schnipps!
Geräusch|9|True|Tock!
Geräusch|10|False|Tock!
Geräusch|11|False|BÄM!
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|Saus!
Geräusch|2|False|Tock!
Geräusch|3|False|Klack!
Geräusch|4|False|KRACH!
Geräusch|5|False|Zisch…

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|7|False|ANGRIFF!!!
Monster|6|True|MuhahaHAHA! Endlich!
Monster|9|False|!?
Schrift|1|True|FEUERWERK
Schrift|2|False|LAGERHAUS
Geräusch|3|False|Fauch!!
Geräusch|4|False|Wusch!!
Geräusch|5|False|BUMM!
Geräusch|8|False|Zisch!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|3|False|BUMM!
Geräusch|2|False|BUMM!
Geräusch|1|False|KRACH!
Geräusch|4|False|Zisch!!
Geräusch|5|False|BÄNG!
Pepper|6|True|Keine Angst, Carrot.
Pepper|7|False|Da feiert bestimmt noch jemand.
Pepper|8|False|Gute Nacht!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|5|False|...
Kümmel|1|False|Wirklich? Sie hat es geschafft, eine so große Kettenreaktion auszulösen, ohne es zu bemerken?
Cayenne|2|False|Zweifellos.
Thymian|3|False|Meine Damen, ich denke unsere Pepper ist endlich bereit!
Geräusch|6|False|Plopp!
Geräusch|4|False|Zisch!!
Erzähler|7|False|- TRILOGIE DIE KRÖNUNG VON CORIANDER, ENDE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Du kannst auch Spender von Pepper&Carrot werden und deinen Namen hier lesen!
Pepper|3|True|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 960 Spendern!
Pepper|7|True|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|6|True|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|8|False|Dankeschön!
Pepper|2|True|Wusstest du schon?
Impressum|1|False|25. April, 2019 Illustration & Handlung: David Revoy. Beta-Leser: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Deutsche Version Übersetzung: Martin Disch. Korrektur: Alina The Hedgehog. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.1.5~appimage, Inkscape 0.92.3 auf Kubuntu 18.04.1. Lizenz: Creative Commons Namensnennung 4.0. www.peppercarrot.com
