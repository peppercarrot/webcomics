# Transcript of Pepper&Carrot Episode 29 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 29: Milltear nan saoghal

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|¯
<hidden>|0|False|¯
<hidden>|0|False|¯
<hidden>|0|False|¯
Uilebheist|2|True|Mu deıŗead tall!
Uilebheist|3|False|Ŝgàınead eadar ŝaoġalan!
Uilebheist|4|False|Feumaıd gun do d’adbaraıċ taċartaŝ coŝmaċ šeo!
Uilebheist|5|True|Fàŝ! Fàŝ, a brıšıd bıg!
Uilebheist|7|True|A saoġaıl…
Uilebheist|8|False|Ta šeo ıntıneaċ.
Uilebheist|9|False|Naċ buıde dom…
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
Uilebheist|6|False|Iŝ noċd an ŝaoġal ùr šeo dom a ċùm TRÀILLEAĊAID AGUŜ AN-BRUID
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
Neach-aithris|1|False|San eadar-àm, ann an saoghal eile…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Uilebheist|1|False|Šeo ŝaoġal far a beıl creutaıŗean tuıgšeaċ a’ còmnaıd!
Uilebheist|2|False|Greaŝ ort, a brıšıd bıg!
Uilebheist|3|False|Fàŝ! Mùhaha hahahà!
<hidden>|0|True|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
Peabar|4|True|Tha mi claoidhte!
Peabar|5|False|Abair cèilidh, a chàirdean!
Costag|6|False|Èist, a Pheabar, thàinig e a-steach orm ’s air Cròch nach eil spùt againn air mar a dh’obraicheas draoidheachd na Dubh-choimeasgachd agad.
Cròch|7|False|Tha e ’na rùn-dìomhair dhuinn. An innis thu dhuinn rud m’ a dèidhinn?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Hàhà, chan eil i furasta mìneachadh.
Peabar|2|True|Uill, chanainn gu bheil i stèidhichte air tuigse nan laghan a tha aig bonn shiostaman dubh-choimeasgach…
Peabar|3|False|…on fhear as lugha dhan fhear as motha.
Costag|4|False|Uill, tha seo cho soilleir ri làr a' mheadhain-oidhche.
Peabar|7|False|Socair oirbh, seallaidh mi ball-eisimpleir sìmplidh dhuibh.
Peabar|8|False|Fuirichibh diog ’s le seann-ghliocas na Dubh-choimeasgachd…
Peabar|9|False|Seo a-niste!
Fuaim|5|True|SGRÌOB
Fuaim|6|False|SGRÌOB
Peabar|10|True|Am faic sibh am bior seo a bha stuicte eadar na clachan-càsaidh?
Peabar|11|False|Le thogail, shàbhail mi cuideigin o cheumadh air gun teagamh.
Peabar|12|True|Seo atharrachadh beag math ann am mòr-shiostam dubh-choimeasgach a’ bhith le buaidh mhòr aige ma dh’fhaoidte.
Peabar|13|False|Seo na tha aig cridhe na Dubh-choimeasgachd!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sidimi|1|False|A dhiamh mhòir!
Costag|2|False|Ceachhh! Bha an rud seo ann am beul cuideigin!
Cròch|3|False|Hà hà! Iongantach mar as àbhaist, a Pheabar!
Costag|4|False|Ceart ma-thà, a Pheabar, mòran taing airson a’ “mhìneachaidh” seo.
Costag|5|True|Saoil an dèid sinn innte a-nis?
Costag|6|False|’S gun nigh cuideigin a làmh.
Peabar|7|False|Hoigh! Fuirichibh orm!
Fuaim|8|False|Caith!
Fuaim|9|True|Boc!
Fuaim|10|False|Boc!
Fuaim|11|False|BAS!
Curran|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Ssss!
Fuaim|2|False|Sgleog!
Fuaim|3|False|Poc!
Fuaim|4|False|GLUING!
Fuaim|5|False|FrruuiS…

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Uilebheist|7|False|IONŜAIĠ!!!
Uilebheist|6|True|MùhahaHAHÀ! Mu deıŗead tall!
Uilebheist|9|False|!?
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|.
<hidden>|0|False|-
<hidden>|0|False|-
Sgrìobhte|1|True|STÒRAS
Sgrìobhte|2|False|CHLEASAN-TEINE
Fuaim|3|False|FrrUuiSs!!
Fuaim|4|False|Frr!!
Fuaim|5|False|BEUM!
Fuaim|8|False|Fisss!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|3|False|BeUM!
Fuaim|2|False|BEUM!
Fuaim|1|False|BRAG!
Fuaim|4|False|Fisss!!
Fuaim|5|False|SrAD!
Peabar|7|False|Feumaidh gu bheil cuid a dhaoine ’ga chomharrachadh fhathast.
Peabar|8|False|Cadal sèimh, a sheòid!
Peabar|6|True|Na biodh an t-eagal ort, a Churrain.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Uilebheist|5|False|...
Carabhaidh|1|False|Ann an da-rìribh? An deach leatha iom-obrachadh cho mòr sin adhbharachadh gun sgot aice air?
Cìob|2|False|Chaidh gun teagamh sam bith.
Tìom|3|False|A mhnathan-uasal, saoilidh mi gu bheil am Peabar againne ullamh mu dheireadh thall!
Fuaim|6|False|Bhiiib!
Fuaim|4|False|Fissss!!
Neach-aithris|7|False|- Deireadh trì-sgeulachd crùnadh Chostag -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|5|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd is chì thu d’ àinm an-seo!
Peabar|3|True|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean.
Peabar|4|False|Mòran taing dhan 960 pàtran a thug taic dhan eapasod seo!
Peabar|7|True|Tadhail air www.peppercarrot.com airson barrachd fiosrachaidh!
Peabar|6|True|Tha sinn air Patreon, Tipeee, PayPal, Liberapay ...’s a bharrachd!
Peabar|8|False|Mòran taing!
Peabar|2|True|An robh fios agad?
Urram|1|False|25mh dhen Ghiblean 2019 Obair-ealain ⁊ sgeulachd: David Revoy. Leughadairean Beta: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc. Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Bathar-bog: Krita 4.1.5~appimage, Inkscape 0.92.3 air Kubuntu 18.04.1. Ceadachas: Creative Commons Attribution 4.0. www.peppercarrot.com
