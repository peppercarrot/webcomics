# Transcript of Pepper&Carrot Episode 29 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 29: Uničevalec svetov

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pošast|2|True|Končno!
Pošast|3|False|Razpoka med dimenzijami!
Pošast|4|False|Bržda jo je porodil nek večji kozmični pripetljaj!
Pošast|5|True|Rasti! Rasti, drobna vrzel!
Pošast|6|False|Razgali mi ta novi svet, da ga ZASUŽNJIM in PODREDIM!
Pošast|7|True|Oooo…
Pošast|8|False|To je vendar zanimivo.
Pošast|9|False|Napočil je moj srečen dan …
Pripovedovalec|1|False|Medtem v drugi dimenziji …

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pošast|1|False|Dimenzija, ki jo poseljuje inteligentni živelj!
Pošast|2|False|Urneje, mala špranja!
Pošast|3|False|Rasti! Muahaha hahaha!
Paprika|4|True|Ha!
Paprika|5|False|To pa je bila zabava, kot se šika!
Koriandrika|6|False|Paprika, z Žafranko sva pravkar ugotovili, da se nama ne sanja, kako deluje tvoja kaosaška čarovnija.
Žafranka|7|False|Sama radovednost naju je. Bi nama lahko razložila?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Ha ha, malce težko razložim.
Paprika|2|True|Lahko bi rekli, da temelji na razumevanju osnovnih zakonov kaotičnega sistema …
Paprika|3|False|…od najmanjšega do največjega.
Koriandrika|4|False|Da, zdaj je pa res vse jasno.
Paprika|7|False|Hm, potem pa naj vam pokažem enostaven primer.
Paprika|8|False|Samo hipec, z dobro staro kaosaško modrostjo lahko …
Paprika|9|False|Že vem!
Zvok|5|True|PRASK
Zvok|6|False|PRASK
Paprika|10|True|Vidite tale zobotrebec, ki sem ga našla med tlakovci?
Paprika|11|False|S tem, ko sem ga pobrala, sem preprečila, da bi kdo stopil nanj.
Paprika|12|True|Majhna dobrodejna sprememba v velikem kaotičnem sistemu obstoja ima lahko velike posledice.
Paprika|13|False|To je bistvo Kaosaha!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Šičimi|1|False|Ogabno!
Koriandrika|2|False|Fuj! To reč je imel nekdo v ustih!
Žafranka|3|False|Ha ha! Nikoli ne razočaraš, Paprika!
Koriandrika|4|False|Dobro, Paprika, hvala za to … ‘razlago’.
Koriandrika|5|True|Čas je, da gremo spat.
Koriandrika|6|False|In da si nekdo umije roke.
Paprika|7|False|Hej! Čakajte!
Zvok|8|False|Flik!
Zvok|9|True|Tuk!
Zvok|10|False|Tuk!
Zvok|11|False|BAM!
Korenček|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|Ssss!
Zvok|2|False|Puf!
Zvok|3|False|Tok!
Zvok|4|False|TRESK!
Zvok|5|False|Šrruuf…

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pošast|7|False|V NAPAD!!!
Pošast|6|True|MuahahaHAHA! Navsezadnje!
Pošast|9|False|!?
Napis|1|True|SKLADIŠČE ZA
Napis|2|False|OGNJEMET
Zvok|3|False|ŠrrUuf!!
Zvok|4|False|Šrr!!
Zvok|5|False|BUM!
Zvok|8|False|Fiijuuu!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|3|False|BUM!
Zvok|2|False|BUM!
Zvok|1|False|Tresk!
Zvok|4|False|Fizzz!!
Zvok|5|False|PUF!
Paprika|6|True|Ne boj se, Korenček.
Paprika|7|False|Najbrž nekaj ljudi še vedno praznuje.
Paprika|8|False|Lahko noč!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pošast|5|False|...
Kumina|1|False|Ji je resnično uspelo sprožiti verižno reakcijo tolikšnega obsega, ne da bi se tega zavedala?
Kajenka|2|False|Brez dvoma.
Timijana|3|False|Dami, mislim, da je naša Paprika končno pripravljena!
Zvok|6|False|Fijup!
Zvok|4|False|Fšššš!!
Pripovedovalec|7|False|- TRILOGIJA KORIANDRIKINEGA KRONANJA, KONEC -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|3|True|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|4|False|Za to epizodo je zaslužnih 960 podpornikov!
Paprika|7|True|Obišči www.peppercarrot.com za več informacij!
Paprika|6|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|8|False|Hvalaaaa!
Paprika|2|True|Že veš?
Zasluge|1|False|25. april 2019 Piše in riše: David Revoy. Testni bralci: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac, Valvin. Popravki: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Programska oprema: Krita 4.1.5~appimage in Inkscape 0.92.3 na Kubuntu 18.04.1. Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
