# Episode 29: Destroyer of Worlds

![cover of episode 29](https://www.peppercarrot.com/0_sources/ep29_Destroyer-of-Worlds/low-res/Pepper-and-Carrot_by-David-Revoy_E29.jpg)

## Comments from the author

For this new episode, I tried an original take on the classic "a Heros saves the world from the attack of a large creature". This chapter closes the trilogy about Coriander's coronation and now Pepper is offially accepted and part of Chaosah. This is also the end of a sort of "Season 3" in my mind. Under the story of this episode hides a deeper thinking about our small daily actions to make this world a better place. Sometime we don't notice any direct effects but being part of a 'all' they certainly can have hidden very large effects. Maybe we all already saved the world twice today without even noticing it!

I'm sure the chain reaction of positive events started by Pepper made that man (that stepped on something pointed and (unintentionally) set fire to the fireworks depository) hide in his house and not be tracked for the fire, meet an old nurse to bandage his foot and fall in love as a teenager again, get a renovation of his house funded by Queen Coriander (who decided to move the Fireworks Storage outside on the edge of the city, in industrial area). Life is not cruel in Pepper & Carrot!

From [Author's blog of episode 29](https://www.davidrevoy.com/article717/episode-29-destroyer-of-worlds/show#comments)
