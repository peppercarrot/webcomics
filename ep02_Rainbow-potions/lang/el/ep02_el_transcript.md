# Transcript of Pepper&Carrot Episode 02 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tίτλος|1|False|Επεισόδιο 2: Τα φίλτρα του ουράνιου τόξου

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|5|True|Γκλούπ
Ήχος|6|True|Γκλούπ
Ήχος|7|False|Γκλούπ
Κείμενο|1|True|ΠΡΟΣΟΧΗ!
Κείμενο|2|True|ΜΑΓΙΣΣΑ
Κείμενο|3|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|7|False|τικ|nowhitespace
Κείμενο|1|False|FIRE D
Κείμενο|2|False|DEEP OCEAN
Κείμενο|3|False|VIOLE(N)T
Κείμενο|4|False|ULTRA BLUE
Κείμενο|5|False|PIN
Κείμενο|6|False|MSON
Κείμενο|8|False|NATURE
Κείμενο|9|False|YELLOW
Κείμενο|10|False|ORANGE TOP
Κείμενο|11|False|FIRE DANCE
Κείμενο|12|False|DEEP OCEAN
Κείμενο|13|False|VIOLE(N)T
Κείμενο|14|False|ULTRA BLUE
Κείμενο|15|False|META PINK
Κείμενο|16|False|MAGENTA X
Κείμενο|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|1|True|Γκλούπ
Ήχος|2|True|Γκλούπ
Ήχος|3|False|Γκλούπ

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|1|True|Γκλούκ
Ήχος|2|False|Γκλούκ
Ήχος|3|True|Γκλούκ
Ήχος|4|False|Γκλούκ
Ήχος|5|True|Γκλούκ
Ήχος|6|False|Γκλούκ
Ήχος|21|False|... !|nowhitespace
Ήχος|20|True|μ|nowhitespace
Ήχος|19|True|μ|nowhitespace
Ήχος|18|True|M|nowhitespace
Ήχος|8|True|Μ|nowhitespace
Ήχος|9|True|π|nowhitespace
Ήχος|10|True|λ|nowhitespace
Ήχος|11|False|ιάχ !|nowhitespace
Ήχος|12|True|Σ|nowhitespace
Ήχος|13|True|π|nowhitespace
Ήχος|14|True|λ|nowhitespace
Ήχος|15|True|ά|nowhitespace
Ήχος|16|True|τ|nowhitespace
Ήχος|17|False|ς|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ήχος|1|True|Μπ
Ήχος|2|True|λούπ|nowhitespace
Ήχος|3|True|Μπ
Ήχος|4|False|λούπ|nowhitespace
Ήχος|7|True|τς|nowhitespace
Ήχος|6|True|λά|nowhitespace
Ήχος|5|True|π
Ήχος|10|True|τς|nowhitespace
Ήχος|9|True|λά|nowhitespace
Ήχος|8|True|π
Ήχος|13|False|τς|nowhitespace
Ήχος|12|True|λά|nowhitespace
Ήχος|11|True|π

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Συντελεστές|1|True|Αυτό το κόμικ είναι ανοικτού κώδικα και πραγματοποιήθηκε χάρη στους 21 χρηματοδότες μου!
Συντελεστές|2|False|www.patreon.com/davidrevoy
Συντελεστές|3|False|Πολλά ευχαριστώ στους:
Συντελεστές|4|False|φτιάχθηκε με Krita στο GNU/Linux
