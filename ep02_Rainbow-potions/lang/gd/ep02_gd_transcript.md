# Transcript of Pepper&Carrot Episode 02 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 2: Deochan bogha-froise

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|6|True|Glug
Fuaim|7|True|Glug
Fuaim|8|False|Glug
Sgrìobhte|1|True|RABHADH
Sgrìobhte|3|True|BANA-
Sgrìobhte|2|True|TAIGH
Sgrìobhte|4|False|BHUIDSICH|nowhitespace
Sgrìobhte|5|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|7|False|gliong|nowhitespace
Sgrìobhte|1|False|Teine-r
Sgrìobhte|2|False|Gorm na mara
Sgrìobhte|3|False|Ròs-rocach
Sgrìobhte|4|False|Corcar
Sgrìobhte|5|False|Fan
Sgrìobhte|6|False|uadh
Sgrìobhte|8|False|Uaine
Sgrìobhte|9|False|Buidhe
Sgrìobhte|10|False|Dearg-bhuidhe
Sgrìobhte|11|False|Teine-ruadh
Sgrìobhte|12|False|Gorm na mara
Sgrìobhte|13|False|Ròs-rocach
Sgrìobhte|14|False|Corcar
Sgrìobhte|15|False|Fann-dhearg
Sgrìobhte|16|False|Bàn-dhearg
Sgrìobhte|17|False|Ruadh

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|True|Glug
Fuaim|2|True|Glug
Fuaim|3|False|Glug

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|True|Slug
Fuaim|2|False|Slug
Fuaim|3|True|Slug
Fuaim|4|False|Slug
Fuaim|5|True|Slug
Fuaim|6|False|Slug
Fuaim|19|False|m|nowhitespace
Fuaim|18|True|m|nowhitespace
Fuaim|17|True|M|nowhitespace
Fuaim|7|True|S|nowhitespace
Fuaim|8|True|m|nowhitespace
Fuaim|10|False|g !|nowhitespace
Fuaim|11|True|G|nowhitespace
Fuaim|12|True|h|nowhitespace
Fuaim|13|True|i|nowhitespace
Fuaim|14|True|u|nowhitespace
Fuaim|15|True|p|nowhitespace
Fuaim|16|False|!|nowhitespace
Fuaim|9|True|u|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|True|P
Fuaim|2|True|lub|nowhitespace
Fuaim|3|True|P
Fuaim|4|False|lab|nowhitespace
Fuaim|7|True|uis|nowhitespace
Fuaim|6|True|gl|nowhitespace
Fuaim|5|True|s
Fuaim|10|True|uis|nowhitespace
Fuaim|9|True|gl|nowhitespace
Fuaim|8|True|s
Fuaim|13|False|uis|nowhitespace
Fuaim|12|True|gl|nowhitespace
Fuaim|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|True|Tha bun-tùs fosgailte aig an dealbh-èibhinn-lìn seo 's chaidh an t-eapasod seo a mhaoineachadh le 21 pàtranach air
Urram|2|False|www.patreon.com/davidrevoy
Urram|3|False|Mòran taing do
Urram|4|False|chaidh a dhèanamh le Krita air GNU/Linux
