# Transcript of Pepper&Carrot Episode 02 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium II : Potiones multicolores

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|5|True|Glut
Sonus|6|True|Glut
Sonus|7|False|Glut
Writing|1|True|PERICULOSA
Writing|3|False|NOLITE INTROIRE
Writing|2|True|MAGA
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|7|False|toc|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|1|True|Glut
Sonus|2|True|Glut
Sonus|3|False|Glut

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|1|True|Glut
Sonus|2|False|Glut
Sonus|3|True|Glut
Sonus|4|False|Glut
Sonus|5|True|Glut
Sonus|6|False|Glut
Sonus|20|False|m|nowhitespace
Sonus|19|True|m|nowhitespace
Sonus|18|True|M|nowhitespace
Sonus|7|True|S|nowhitespace
Sonus|8|True|P|nowhitespace
Sonus|9|True|l|nowhitespace
Sonus|10|False|urp !|nowhitespace
Sonus|11|True|S|nowhitespace
Sonus|12|True|S|nowhitespace
Sonus|13|True|S|nowhitespace
Sonus|14|True|P|nowhitespace
Sonus|15|True|l|nowhitespace
Sonus|16|True|o|nowhitespace
Sonus|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|1|True|B
Sonus|2|True|lup|nowhitespace
Sonus|3|True|B
Sonus|4|False|lup|nowhitespace
Sonus|7|True|uts|nowhitespace
Sonus|6|True|l|nowhitespace
Sonus|5|True|p
Sonus|10|True|ts|nowhitespace
Sonus|9|True|lu|nowhitespace
Sonus|8|True|p
Sonus|13|False|ts|nowhitespace
Sonus|12|True|lu|nowhitespace
Sonus|11|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Haec fabula imaginalis est aperta fontis et hoc episodium juverunt in
Credits|2|True|www.patreon.com/davidrevoy
Credits|3|False|21 maecenates. Gratias ago illis :
Credits|4|False|confectus Krita in GNU/Linux
