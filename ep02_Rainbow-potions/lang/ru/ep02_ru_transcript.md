# Transcript of Pepper&Carrot Episode 02 [ru]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Эпизод 2: Радужные зелья

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Звук|5|True|Бульк
Звук|6|True|Бульк
Звук|7|False|Бульк
Writing|1|True|WARNING
Writing|3|False|PROPERTY
Writing|2|True|WITCH
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Звук|7|False|дзынь!|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Звук|1|True|Бульк
Звук|2|True|Бульк
Звук|3|False|Бульк

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Звук|1|True|Бульк
Звук|2|False|Бульк
Звук|3|True|Бульк
Звук|4|False|Бульк
Звук|5|True|Бульк
Звук|6|False|Бульк
Звук|20|False|м|nowhitespace
Звук|19|True|м|nowhitespace
Звук|18|True|M
Звук|7|True|П
Звук|8|True|ф|nowhitespace
Звук|9|True|ф|nowhitespace
Звук|10|False|ффф !|nowhitespace
Звук|11|True|Х
Звук|12|True|х|nowhitespace
Звук|13|True|х|nowhitespace
Звук|14|True|л|nowhitespace
Звук|15|True|ю|nowhitespace
Звук|16|False|п|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Звук|1|True|Х
Звук|2|True|люп|nowhitespace
Звук|3|True|Х
Звук|4|False|люп|nowhitespace
Звук|7|True|ёп|nowhitespace
Звук|6|True|л|nowhitespace
Звук|5|True|ш
Звук|10|True|ёп|nowhitespace
Звук|9|True|л|nowhitespace
Звук|8|True|ш
Звук|13|False|ёп|nowhitespace
Звук|12|True|л|nowhitespace
Звук|11|True|ш

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|This webcomic is open-source and this episode was funded by my 21 patrons on
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Many thanks to
Credits|4|False|made with Krita on GNU/Linux
