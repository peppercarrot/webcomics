# Webcomics: Mini Fantasy Theater

This repository includes all the necessary files for translating the miniFantasyTheater webcomics.

## License: CC BY-SA

The miniFantasyTheater is licensed under CC BY-SA. It's a difference from the episodes of Pepper&Carrot licensed under CC BY. You can view the license [here](https://creativecommons.org/licenses/by-sa/4.0/deed.en).

## Files

This directory contains only JSON and Inkscape SVG files.

Please refrain from using the JPG files in this repository for rendering printed versions, as they are intended solely as previews for editing the SVGs.

For high-quality images, we recommend downloading them from the official website. This repository does not include any Krita artwork files, as they are too large to be managed within the versioning system. You can find these files hosted at https://www.peppercarrot.com/0_sources/miniFantasyTheater/

## Code of Conduct

You agree to follow the rules of [our Code of Conduct](https://www.peppercarrot.com/en/documentation/409_Code_of_Conduct.html) if you wish to participate in the miniFantasyTheater webcomic, Pepper&Carrot, and also misc projects on this repository.

## Credits

Each SVG file in the repository is accompanied by a corresponding meta file descriptor (ending with _info.json). These files provide information on credits and attributions. For more details about the file structure, please refer to the [documentation](https://www.peppercarrot.com/en/static14/documentation&page=081_info.json).

## Chat

We’d love to hear from you! Feel free to ask any questions in our [chat room](https://matrix.to/#/#peppercarrot:matrix.org).
