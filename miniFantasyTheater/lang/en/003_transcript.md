# My treasure

A webcomic in four panels:

Panel 1: In a cave containing a treasure with a lot of gold, a young adventurer is chased by a monster: a treasure with big teeth. She drops all the gold items she had on her escape.

Panel 2: While climbing a mountain of bones and skulls to reach the exit of the cave, the adventurer throws a bone to the monster. It is startled.

Panel 3: Like a brave dog, the monster returns the bone to the adventurer, happy to start over. The adventurer has a think about it.

Panel 4: Later, in a city, the adventurer walks proudly and happily with her new "dog" (the monster's treasure) on a leash. This one is a happy one. 
