# Accept Yourself

A four panel wordless webcomic:

Panel 1: A light shines on a large silk cocoon lying on a bed in a bedroom. A long crack on its surface indicates that it's about to open.

Panel 2: A young butterfly with black wings emerges from the cocoon. She sits up, still partially covered in silk threads. A little lost, she touches her face.

Panel 3: She looks at herself in the mirror, disappointed and saddened by her large, dark wings: she holds a colored pencil drawing of the wings she wishes she had.

Panel 4: When she steps outside, not so sure of herself, she is surprised to find that her unique appearance has fascinated a young fairy whose wings are still small.
