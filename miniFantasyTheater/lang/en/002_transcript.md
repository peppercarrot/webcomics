# Monster

A webcomic in four panels:

Panel 1: A cute, dark sorcerer is kneeling in front of a circle of candles and a magic lamp. The magic lamp begins to emit magic smoke.
> Cute Sorcerer: O, Dark Djinn, Lord of Wishes, I summon thee!

Panel 2. The smoke materialized into a dark elemental creature with horns and red eyes, the Dark Djinn.
> Dark Djinn: What is your wish, O human being?
> Cute Sorcerer: I want a monster without morals, with lethal precision, night vision, and sharp claws.

Panel 3. Close-up of the djinn's face. He casts a glow spell. 
> Dark Djinn: Your wish is my command.

Panel 4. The young sorcerer is astonished at the result: he has received a young cat. The djinn vanishes.
