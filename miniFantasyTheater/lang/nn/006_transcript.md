# Kundestøtte

Ei teikneseriestripe med fire ruter:

Rute 1. Ein ung krigar med delvis svidde klede går med sinte og faste steg mot ein sverdhandel. Han held eit øydelagt sverd i handa.

Rute 2. Inne i sverdhandelen er krigaren tydeleg sint. Han har lagt dei to delane av sverdet, sverdbladet og skaftet, på disken framfor sverdsmeden. Smeden granskar sverddelane.

Rute 3. Smeden held opp delane og undersøkjer dei grundigare. Krigaren ser nysgjerrig på.

Rute 4. Smeden har lagt sverdbladet og skaftet tilbake på disken. Krigaren ser sur og oppgjeven ut.
> Sverdsmed: Har du prøvd å skru det av og på att?
