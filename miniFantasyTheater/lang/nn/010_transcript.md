# Godta deg sjølv

Ei ordlaus teikneseriestripe med fire ruter:

Rute 1: Lyset skin inn på ein stor silkekokong på ei seng. Ein stor sprekk i kokongen tyder på at han er i ferd med å bresta.

Rute 2: Ei ung sommarfugljente med svarte vengjer stig ut av kokongen. Ho reiser seg, framleis delvis dekt av silketrådar. Ho verkar litt øren og kjenner forsiktig på ansiktet.

Rute 3: Ho ser på seg sjølv i spegelen, men er skuffa og lei seg over dei store, svarte vengjene ho har fått. Ho held fram ei teikning av dei fargerike vengjene ho ynskjer ho hadde.

Rute 4: Ho er no utandørs, men verkar usikker på seg sjølv. Men så vert ho overraska då ei ung fe med små vengjer ser med ovundring i blikket på henne og den unike utsjånaden hennar.
