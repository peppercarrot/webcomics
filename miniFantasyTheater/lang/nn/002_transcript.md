# Monsteret

Ei teikneseriestripe med fire ruter:

Rute 1: Ein ung, søt, mørk trollkar kneler framom ein sirkel av stearinlys. I midten av sirkelen er det ei magisk lampe som det byrjar å stiga røyk frå.
> Søt trollkar: Å, mørke ånd, du som oppfyller ynske, eg påkallar deg!

Rute 2: Røyken vert til eit mørkt åndevesen med horn og raude auge, den mørke ånda.
> Mørk ånd: Sei kva du ynskjer, vesle menneske!
> Søt trollkar: Eg vil ha eit monster utan moral, med dødeleg presisjon, nattsyn og skarpe klør.

Rute 3: Nærbilete av fjeset til ånda. Det gløder av trolldomen ho utfører.
> Mørk ånd: Som du ynskjer!

Rute 4: Den unge trollkaren er forbløffa over resultatet: Han har fått ein katt. Ånda vert borte i ei sky av røyk.
