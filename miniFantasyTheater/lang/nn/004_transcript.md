# Det vesle ekstra

Ei teikneseriestripe med fire ruter:

Rute 1. Ei ung, mørk dronning stiller eit spørsmål til ein trollspegel på veggen.
> Mørk dronning: Spegel, spegel på veggen der, kven er den vakraste i landet her?

Rute 2. Spegelen viser biletet av ei ung kvinne.
> Spegel: Ho her.

Rute 3. Dronninga vender avvisande ryggen til spegelen, som om ho skal forlata rommet.
> Mørk dronning: Du meiner eg manglar det vesle ekstra? Pytt, det kan eg lett ordna.
> Spegel: Tvilar på at det hjelper …

Rute 4. Den mørke dronninga er tilbake med ei slegge og smiler. Spegelen sveittar og ristar medan han viser biletet av dronninga sjølv.
> Spegel: Å! **Det** vesle ekstra! **Veldig** overtydande!
