# Skatten min

Ei teikneseriestripe med fire ruter:

Rute 1: I ei grotte fylt med gullskattar ser me ei eventyrarjente flykta frå eit monster – ei skattkiste som glefsar med store tenner. For å sleppa unna kastar ho frå seg alle skattane ho har i hendene.

Rute 2: Jenta er på veg opp eit berg av bein og hovudskallar som leiar mot grotteutgangen. Ho overraskar kista ved å dryla eit bein mot henne.

Rute 3: Som ein flittig hund leverer monsterkista beinet tilbake til jenta, slik at ho kan kasta det på nytt. Jenta står og grundar litt.

Rute 4: Seinare ser me jenta gå tur i byen med den nye «hunden» sin (monsterkista) i band. Begge ser glade ut, og folka rundt ser forskrekka ut.
