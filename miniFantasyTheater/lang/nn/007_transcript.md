# Dunning–Kruger-effekten

Ei teikneseriestripe med fire ruter:

Rute 1: Ein skoddefull kveld. Ein unggut av ein riddar, dårleg utrusta med for store bukser, berr overkropp, tresverd, treskjold og rusten hjelm, kaukar i overmot utanfor ei hòle.
> Riddar: Herskar av drakane! Eg utfordrar deg til duell mot MEG, den største riddaren i heile kongeriket!!! Ingen slår meg i fektekunst!!!

Rute 2: Draken kjem roleg ut av hòla. Berre den store snuten er synleg, medan resten av hovudet framleis er i skuggen. Han er gigantisk, men ikkje rasande, berre lei av å verta uroa slik. Riddaren held fram:
> Riddar: Du er vitskremd, ikkje sant?! Det bør du vera! Eg er skremmande! Kom fram!!! Feiging!!!

Rute 3: Draken har kome ut, og han er uhorveleg stor. Riddaren, som til samanlikning vert som ein liten maur, går til åtak på tåneglene hans. Draken står i lyset frå ein episk fullmåne og ensar knapt åtaka.
> Riddar: Ta den! Og den!
> Partnaren til draken (ute av syne, frå inne i hòla): Kven er det, kjære?

Rute 4: Nærbilete av draken som snur seg mot hòla for å svara partnaren.
> Herskar av drakane: Ikkje nokon viktig. Som vanleg berre ein altfor sjølvsikker fersking som overvurderer eigne evner. Ein klassisk menneskeleg feil.
> Riddar (utanfor biletet): Og den! Og den!
