# Morgonrutinen

Ei teikneseriestripe med fire ruter:

Rute 1: Pepar ligg halvvaken i senga med bustete hår. Det er tidleg på morgonen, og ho ser litt irritert ut. Rundt henne er trolldomsbøker og ei vekkjarklokke. Katten hennar, Gulrot, ligg òg på senga.
> Lydeffekt (vekkjarklokka): Ring! Ring! Ring!

Rute 2: Pepar går ned trappa i halvsøvne, som ein zombie. Ho har nattkjole på, og ei sky av lukt frå underetasjen fører henne på rett veg.

Rute 3: Pepar er framleis omtåka, men går ut på eit heimelaga stupebrett over noko som ser ut som eit basseng. Mange bøker er stabla opp og fungerer som ei trapp opp til stupebrettet. Skya av lukta kjem no rett nedanfrå, frå bassenget.

Rute 4: Ho landar med fjeset ned i ein kjempestor kopp med kaffi. Gulrot kikar overraska ned på henne.
> Pepar: Blubb, blubb, blubb.
