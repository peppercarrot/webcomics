# Kjøtetande planter

Ei ordlaus teikneseriestripe med fire ruter:

Rute 1: Ei krigarkvinne går ned ei vindeltrapp av stein. Ho kjem til ei bogeforma døropning inn til eit merkeleg rom laga av murstein, litt som eit typisk fangehòl under ei borg. Ho vert overraska over det ho ser: På veggane slyngjer det seg grøne, tornete vekstar som tyder på at rommet er infisert av kjøtetande planter. I framgrunnen kan me delvis skimta eit offer.

Rute 2: Krigaren stormar til for å hjelpa offeret – ein ung, kvinneleg eventyrar som har hovudet fanga i ein stor, raud, klokkeforma blome.

Rute 3: I det krigaren løftar sverdet sitt for å hogga laus på planta blomen veks på, overraskar offeret henne ved å veiva med hendene, som for å be henne stoppa.

Rute 4: Blomen slepper taket i hovudet til offeret, som viser seg å vera ei ung alvekvinne som no har perfekt, krølla hår. Alvekvinna ser oppglødd på den nye frisyren sin i ein handspegel. Krigaren ser tankefull ut.
