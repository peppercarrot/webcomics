# Altfor ofte

Ei teikneseriestripe med fire ruter:

Rute 1. Ein kamp mellom ein illsint drake og ein krigar med eit magisk eldsverd. Me er i ei glenne i ein tett skog – bada i det varme lyset frå ettermiddagssola.

Rute 2. Nærbilete av sverdet som parerer eit åtak frå klørne til draken. Men krigaren vert distrahert: Sverdet snakkar til han!
> Sverd: Me har ei oppdatering til deg. **Installerer!**
> Krigar: Uæh!!!

Rute 3. Krigaren trekkjer seg litt unna og ser panikkslagen på sverdet. Rundt det krinsar magiske strålar som ser ut til å koma frå ein annan stad. Strålane glødar i blått. Draken forstår ikkje heilt situasjonen, men ser undrande på det heile.
> Sverd: 80 % … 90 % …
> Krigar: Nei, nei, nei, nei, NEI!

Rute 4. Krigaren ser på resultatet med tårer i auga: Sverdet er brote i to. Sverdbladet ligg på bakken. Draken forstår at han har vunne. Han får eit olmt uttrykk, og auga lyser raudt medan han byggjer opp eld i den glisande drakekjeften. Sverdet held fram med å tala, men orda er forvrengde.
> Sverd: Oppdatering fullført. Ver så god!
