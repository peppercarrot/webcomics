# Ein sporv i tranedans

Ei ordlaus teikneseriestripe med fire ruter:

Rute 1: Ei ung fe med brunt hår og ordinære, gjennomsiktige vengjer (litt som på ein augestikkar, men ikkje så vakre) ser ei sjølvsikker, yndefull fe med fargerike sommarfuglvengjer. Ho vert fjetra av synet.

Rute 2: Det er kveld, og den unge fea står framfor spegelen heime. Ho ser på seg sjølv og dei vesle vengjene sine og verkar trist.

Rute 3: Det er natt, og fea ligg i fosterstilling i senga og græt.

Rute 4: Morgon dagen etter. På den same senga ligg det no ein stor silkekokong i staden for ei fe. Morgonlyset strøymer inn på han, som eit løfte om håp og eit varsel om ei framtidig forvandling.
