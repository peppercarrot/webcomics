# Accepte-toi

Un webcomic sans dialogue en quatre cases :

Case 1 : Une lumière brille sur un large cocon de soie, couché sur un lit dans une chambre. Une longue fissure sur sa surface indique qu'il s'apprête à s'ouvrir. 

Case 2 : Une jeune fée avec des ailes de papillon noires sort du cocon. Elle s'assoit, partiellement recouverte de fils de soie. Un peu perdue, elle se touche le visage.

Case 3 : Elle se regarde dans le miroir, déçue et attristée par ses grandes ailes noires : elle tient dans ses mains un dessin des ailes colorées qu'elle aurait souhaité avoir.

Case 4 : Lorsqu'elle sort dehors, peu confiante, elle est surprise de constater que sa nouvelle apparence unique fascine une autre jeune fée dont les ailes sont encore minuscules. 
