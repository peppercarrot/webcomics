# Accessoires

Un webcomic en 4 cases :

Case 1 : Une jeune et méchante reine interroge son miroir magique sur le mur.
> Méchante reine : Miroir, mon beau miroir, qui est la plus belle d'entre toutes ?

Case 2 : Le miroir montre l'image d'une jeune fille.
> Miroir : Elle.

Case 3 : La reine, un peu frustrée, tourne le dos au miroir, s'apprêtant à quitter la pièce.
> Méchante reine : Peut-être qu'avec quelques accessoires, je pourrais te convaincre que c'est plutôt moi !
> Miroir : Ça, j'en doute...

Case 4 : La reine est de retour avec un grand marteau, elle sourit. Le miroir, en sueur, panique et tremble, tandis qu'il affiche l'image de la reine.
> Miroir : Oh ! **Ce** genre d'accessoire ! **Très** convaincant !
