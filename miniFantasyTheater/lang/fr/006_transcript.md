# Service d'assistance

Un webcomic en 4 cases :

Case 1 : Un jeune guerrier, avec le dos de sa tenue de combat partiellement brûlée, marche furieusement vers la boutique d'épées, tenant son épée brisée à la main.

Case 2 : À l'intérieur, le guerrier manifeste sa colère, la lame et la garde ayant été placées sur le comptoir pour montrer l'étendue évidente des dégâts. Le forgeron examine attentivement l'épée brisée.

Case 3 : Le forgeron examine l'épée encore plus attentivement, avec expertise, en la manipulant. Le guerrier est curieux.

Case 4 : Le forgeron repose la garde et la lame sur le comptoir. Le guerrier est exaspéré.
> Forgeron : Et vous avez essayé de l'éteindre et de la rallumer ?
