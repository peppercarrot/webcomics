# Trop souvent

Un webcomic en 4 cases :

Case 1 : Un combat fait rage entre un féroce dragon et un guerrier tenant une épée magique enflammée. La scène se déroule dans une forêt de pins dense, plongée dans la lumière dorée d'une fin d'après-midi, avant le crépuscule. 

Case 2 : Gros plan sur l'épée en action, bloquant un coup de griffes du dragon. Mais le guerrier est soudainement distrait : son épée lui parle pour lui transmettre un message !
> Épée : Mise à jour disponible. **Installation !**
> Guerrier : Argh !

Case 3 : Le guerrier fait un pas en arrière, regardant son épée en panique. Elle est frappée et entourée de rayons magiques venant d'ailleurs. Ils illuminent la scène de lumière bleue. Le dragon ne comprend pas immédiatement la situation, il est fasciné par ce qu'il se passe.
> Épée : 80%... 90%...
> Guerrier : Non non non non non !

Case 4 : Le guerrier regarde le résultat avec les larmes aux yeux : son épée est brisée. La lame repose par terre, inutilisable. Son destin est scellé. Le dragon comprend qu'il a gagné, et lance un regard féroce avec des yeux rouges, préparant une boule de feu dans sa gueule. L'épée continue de parler, le texte est distordu.
> Épée : Mise à jour terminée. Amusez-vous bien !
