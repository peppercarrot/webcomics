# L'effet Dunning–Kruger

Un webcomic en 4 cases :

Case 1 : Un très jeune chevalier, faiblement équipé (pantalon trop grand, épée et bouclier en bois, casque rouillée), crie avec
une confiance aveugle devant une grotte, par une nuit brumeuse. 
> Chevalier : Maître des dragons ! MOI, le plus grand chevalier que le royaume ait connu, je te défie en duel !!! Ma connaissance de l'escrime est inégalée !!!

Case 2 : Le dragon sort calmement de la grotte, son grand nez à peine visible tandis que sa tête est encore dans l'ombre. Aucun regard féroce, mais sa taille est imposante. Il est juste épuisé par ce genre d'interruption. Le chevalier continue :
> Chevalier : Tu es terrifié, hein ?! Tu as bien raison de l'être ! Je suis terrifiant ! Montre-toi !!! Lâche !!!  

Case 3 : La caméra montre un dragon gigantesque. Le chevalier, qui a la taille d'un moustique en comparaison, attaque l'orteil du dragon. Ce dernier observe la situation d'un air las. Un clair de lune épique vient illuminer la scène.
> Chevalier : Gna ! Gna !
> Partenaire du dragon (hors-champ, depuis la grotte) : Chéri, que se passe-t-il ?

Case 4 : Gros plan sur le dragon, tournant la tête vers la grotte pour répondre à sa partenaire : 
> Maître des dragons : Rien d'important. Un débutant trop sûr de lui, surestimant ses propres capacités, comme d'habitude. Un biais classique chez les humains.
> Chevalier (hors-champ) : Gna ! Gna !
