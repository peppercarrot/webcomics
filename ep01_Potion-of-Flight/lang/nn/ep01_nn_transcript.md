# Transcript of Pepper&Carrot Episode 01 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 1: Flygetrylledrikken

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|... og siste hand på verket.
Pepar|4|False|Hm. Sikkert ikkje sterk nok.
Lyd|2|True|Sjusj
Lyd|3|False|Sjusj
Lyd|5|True|PLOPP
Lyd|6|False|PLOPP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Ja! Perfekt!
Pepar|2|False|NEI! Ikkje prøv deg!
Lyd|3|False|PLASK

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Nøgd no?
Bidragsytarar|2|False|WWW.PEPPERCARROT.COM 05/2014
