# Transcript of Pepper&Carrot Episode 01 [ga]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 1 : Potinga d'envolada

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...e lo darrèr ingredient...
Pepper|4|False|...mmm dilhèu pas pro.
Son|2|True|CHH
Son|3|False|CHH
Son|5|True|PLOP
Son|6|False|PLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|A… perfèit !
Pepper|2|False|NON ! Hòra question !
Son|3|False|FLAC

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Content ?
Crèdits|2|False|WWW.PEPPERCARROT.COM 05/2014
