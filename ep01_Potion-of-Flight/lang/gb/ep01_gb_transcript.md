# Transcript of Pepper&Carrot Episode 01 [gb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulo|1|False|Mon 1: Iksir fe Fley

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|...ji finili detale.
Pilpil|4|False|...mmm, dayible, to no sen kufimo bala.
Soti|2|True|XX
Soti|3|False|XX
Soti|5|True|PLOP
Soti|6|False|PLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|A... perfeto!
Pilpil|2|False|NO! Am no hata fikir to!
Soti|3|False|PLAX

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pilpil|1|False|Kam truti?!
Credits|2|False|WWW.PEPPERCARROT.COM 05/2014
