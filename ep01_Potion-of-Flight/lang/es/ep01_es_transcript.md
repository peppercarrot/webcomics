# Transcript of Pepper&Carrot Episode 01 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 1: Poción de vuelo

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|...y el toque final.
Pimienta|4|False|...mmm quizás no sea suficiente.
Sonido|2|True|SHH
Sonido|3|False|SHH
Sonido|5|True|PLOP
Sonido|6|False|PLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|...¡ah! Perfecto.
Pimienta|2|False|¡NO! Ni se te ocurra.
Sonido|3|False|SPLASH

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¿Contento?
Créditos|2|False|WWW.PEPPERCARROT.COM 05/2014
