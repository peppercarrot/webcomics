﻿# Episode 15: The Crystal Ball

![cover of episode 15](https://www.peppercarrot.com/0_sources/ep15_The-Crystal-Ball/low-res/Pepper-and-Carrot_by-David-Revoy_E15.jpg)

## Comments from the author

It's the fourth story of the "witches of Chaosah". Ep12 was about Cayenne, ep13 was about Cumin and now ep15 is a bit about Thyme. This episode introduce a new 'technology' in the little world of Pepper&Carrot and the real theme are: video, erotism and parental control, video-games, bugs, blue-screen-of-death, coding/fixing, network and the World Wide Web. It was a little challenge to insert this elements in the fantasy world of Hereva.

About what I experimented: I'm back to storytelling with wide panel only, it's easier to read, but it add more pages to draw ( it's a 8 pages long story, I usually draw around 6 ). I also experimented with framing and cropping to be more cinematic and I try to treat light and color as an actor. It's also a rare time where I use the 'parody' humor form in a episode, doing many reference to internet, computers and our world.

From [Author's blog of episode 15](https://www.davidrevoy.com/article564/episode-15-the-crystal-ball/show#comments)
