# Transcript of Pepper&Carrot Episode 15 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 15: La bola de cristal

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|1|False|- FIN -
Créditos|2|False|Marzo 2016 - Dibujo & Guión: David Revoy - Traducción: TheFaico

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Tú también puedes ser mecenas de Pepper&Carrot para el próximo episodio:
Créditos|2|True|https://www.patreon.com/davidrevoy
Créditos|3|False|Pepper&Carrot es completamente gratuito, de código abierto, y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 686 mecenas:
Créditos|4|False|Licencia: Creative Commons Attribution 4.0 Ficheros originales disponibles en www.peppercarrot.com Herramientas: Este episodio ha sido creado al 100% con software libre Krita 2.9.11, Inkscape 0.91 en Linux Mint 17
