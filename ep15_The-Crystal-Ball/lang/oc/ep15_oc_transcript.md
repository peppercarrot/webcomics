# Transcript of Pepper&Carrot Episode 15 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 15 : La Bòla de Cristal

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FIN -
Crèdits|2|False|03/2016 - Dessenh & Scenari : David Revoy

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|2|True|Vos tanben, venètz mecèna de Pepper&Carrot sus la pagina :
Crèdits|3|False|https://www.patreon.com/davidrevoy
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 686 mecènas :
Crèdits|4|False|Licéncia : Creative Commons Attribution 4.0 Sorsas : disponiblas sus www.peppercarrot.com Logicials : aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita 2.9.11, Inkscape 0.91 sus Linux Mint 17
