# Transcript of Pepper&Carrot Episode 15 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 15: Kristalna krogla

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|- KONEC -
Zasluge|2|False|Marec 2016 - Piše in riše David Revoy

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 686 bralcev:
Zasluge|2|True|Tudi ti lahko postaneš denarni/a podpornik/ca za naslednjo epizodo stripa:
Zasluge|3|False|https://www.patreon.com/davidrevoy
Zasluge|4|False|Licenca: Creative Commons Priznanje avtorstva 4.0. Izvorne datoteke na voljo na www.peppercarrot.com Programska oprema: ta epizoda je nastala s prostim programjem Krita 2.9.11 in Inkscape 0.91 na Linux Mint 17
