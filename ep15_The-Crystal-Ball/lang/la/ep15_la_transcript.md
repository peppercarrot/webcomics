# Transcript of Pepper&Carrot Episode 15 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium XV : Sphaera Crystallina

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FINIS -
Credits|2|False|Mense martis A. MMXVI - Designum et fabula : David Revoy, lingua latina translatio: Benjamin Touati, correctio: Valentine Guillocheau

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|2|True|Tu quoque, potes fieri maecenas Piperis&Carotae sequenti episodio :
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|1|False|Piper&Carota gratis constat, omnno apertum fontis est ac juvatur maecenatu lectorum ; in hoc episodium, gratias ago illis 686 Maecenatibus :
Credits|4|False|Licentia : Creative Commons Attribution 4.0 Fontes : inveniuntur apud www.peppercarrot.com Instrumenta : Hoc episodium omnino designatum est liberis programmatibus Krita 2.9.11, Inkscape 0.91 in Linux Mint 17
