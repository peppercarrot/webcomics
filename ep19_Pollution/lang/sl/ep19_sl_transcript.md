# Transcript of Pepper&Carrot Episode 19 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 19: Onesnaževanje

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|1|True|Noter grem.
Kajenka|2|True|Nima te smisla gledati kopati …
Kajenka|3|False|Zakoplji mi vse te spodletele napoje in se potem vrni noter počivat.
Kajenka|4|False|Pri tem tempu bo čudež že, če ti uspe do jutri.
Paprika|5|True|Saj hitim, no!
Paprika|6|True|Pa sploh, zakaj moramo zmeraj čisto vse zakopati?
Paprika|7|False|Ne bi bilo bolje, če …
Kajenka|8|False|Če KAJ?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Em…
Paprika|2|True|Nisem neka strokovnjakinja,
Paprika|3|True|ampak letos se zelenjavna greda res čudno obnaša.
Paprika|4|False|In vse druge rastline okoli hiše tudi.
Napis|5|False|Paradižniki
Napis|6|False|Jajčevci
Paprika|7|False|Enako je z mravljami. Zares čudne reči počnejo.
Paprika|8|True|Tako da …
Paprika|9|False|Dozdeva se mi, da imamo rahle težave z onesnaževanjem in bi bilo mogoče pametno razmisliti o kakšnem čiščenju za seb…
Kajenka|10|True|Veš, kaj, gospodična „Vse-svoje-napoje-pokvarim“,
Kajenka|11|False|tvoje hipijaške cote so ti brez dvoma stopile v glavo!
Kajenka|12|True|Kaosaške čarovnice vse svoje spodrsljaje zakopljemo!
Kajenka|13|True|To je naša tradicija že od pamtiveka in eno figo me briga, kaj si MATI NARAVA misli o tem!
Kajenka|14|False|Zato molči in KOPLJI!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|vse svoje spodrsljaje zakopljemo
Paprika|2|False|tradicija
Paprika|3|False|od pamtiveka
Paprika|4|False|JA, SEVEDA!
Korenček|5|False|Zzzz
Paprika|6|False|Dajmo, Korenček, hitreje!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|6|False|Kajenkin osebni dnevnik
Paprika|1|False|„O, bojevnik ti postaven, svetel, zlato oklepan!“
Paprika|2|False|„Ti, ki kaos v meni prebudiš!“
Paprika|3|False|„Ti v zarji moji entropijo povzročiš.“
Paprika|4|True|Lepa pesnitev, botra Kajenka!
Paprika|5|False|Neverjetno, kaj vse izveš o čarovnicah, ki vse svoje polomije zakopljejo!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|1|True|Vse smo delale napake.
Kajenka|2|True|In nisem več tista Kajenka, ki je to napisala.
Kajenka|3|False|Dnevnik sem zakopala z razlogom.
Paprika|4|False|...
Paprika|5|True|Prav!
Paprika|6|False|Kaj pa to?
Napis|7|False|KAOSAH SUTRA Timijana
Timijana|8|False|Tole je pa pregreha iz moje mladosti, ki poleg tega še ni primerna za tvojo starost!
Paprika|9|True|Hmm…
Paprika|10|False|Že vidim …
Paprika|11|False|Teh reči vas ni čisto nič sram …

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Ampak okolje! Narava!
Paprika|2|False|Ne moremo večno onesnaževati vsega brez kakršnihkoli posledic!!!
Kajenka|3|True|Do zdaj nam ni povzročalo težav!
Kajenka|4|True|Me smo kaosaške čarovnice in svoje polomije zakopljemo
Kajenka|5|True|GLOBOKO!
Kajenka|6|False|O tradicijah ne bomo razpravljale!
Kumina|7|True|Glej no, glej, kaj sem pravkar našla!
Kumina|8|False|Saj ne morem verjeti!
Kumina|9|False|Kako se je pa to znašlo tukaj?
Kumina|10|False|Treba jo bo malo uglasiti, sicer pa zveni prav fino.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kumina|1|False|Kako je že šlo? Ka~ka ka, Kaosaaah!
Kumina|2|True|Ha ha, besedilo sem že pozabila.
Kumina|3|False|Mogoče je moja pesmarica kje tukaj …
Kajenka|4|True|Torej se vse strinjamo. Sprememba v kaosaškem pravilniku:
Kajenka|5|True|Od zdaj naprej
Kajenka|6|True|ločimo,
Kajenka|7|True|zmeljemo
Kajenka|8|True|in recikliramo vse!
Kajenka|9|False|VSE!!
Napis|10|False|steklo
Napis|11|False|kovina
Pripovedovalec|12|False|- KONEC -
Zasluge|13|False|September 2016 - www.peppercarrot.com - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Zasluge|14|False|Zdravnik za scenarij: Craig Maloney. Lektoriranje in pomoč pri dialogu: Valvin, Seblediacre in Alex Gryson. Navdih: "The Book of Secrets" Juana Joséja Segure
Zasluge|15|False|Strip se dogaja v svetu Hereve avtorja Davida Revoya s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|16|False|Licenca: Creative Commons Priznanje avtorstva 4.0 - Programska oprema: Krita 3.0.1, Inkscape 0.91 na Arch Linuxu XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 755 bralcev:
Zasluge|2|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
