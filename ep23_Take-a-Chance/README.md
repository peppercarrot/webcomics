﻿# Episode 23: Take a Chance

![cover of episode 23](https://www.peppercarrot.com/0_sources/ep23_Take-a-Chance/low-res/Pepper-and-Carrot_by-David-Revoy_E23.jpg)

## Comments from the author

This episode follows previous episode of the magic challenge and focus on the result one week later. It's the last part of the trilogy. It splits a bit more the relation between Pepper and Saffron. This episode clearly shows now the position of Pepper in regard to the the tactic used by Saffron (and undirectly Spirulina and Camomille) to win the magic challenge.

To compose this story, I worked around the theme of chance, success, effort and merit: how many opportunities did we missed because they did not conformed to our idea of effort? to our idea of merit? Does it make the actions "too obvious/easy path to success" the harder things to spot and grab? It is also a parody of the [Deus ex Machina](https://en.wikipedia.org/wiki/Deus_ex_machina) in ancient Greek tragedy, and a sweet arc to remotivate our Pepper to go further in her mastery of magic! 

From [Author's blog of episode 23](https://www.davidrevoy.com/article621/episode-23-take-a-chance/show#comments)
