# Transcript of Pepper&Carrot Episode 23 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 23: Zgrabi priložnost

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|1|False|Komonska tržnica, teden pozneje
Napis|2|False|Zvezda
Napis|3|False|50 000 kojev
Napis|4|False|Žafranka in njen uspeh
Napis|5|False|Moda
Napis|6|False|Žafranka - fenomen
Napis|7|False|Slog
Napis|8|False|Žafranka na vrhuncu
Zvok|9|False|PAF!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Hvala, Korenček.
Paprika|2|False|Vsak dan se na vse kriplje mučim, da bi postala dobra čarovnica.
Paprika|3|True|Spoštujem tradicije in pravila.
Paprika|4|False|Preprosto ni pravično, da je Žafranka požela slavo s takimi podlimi triki.
Paprika|5|False|Res ni pošteno.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Duh uspeha|2|True|Pozdravljeni!
Duh uspeha|3|False|Naj se predstavim:
Zvok|1|False|Bong!
Duh uspeha|4|False|Sem duh uspeha, na uslugo 24 lun na dan, 7 dni v tednu!*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Duh uspeha|1|False|*Razen vikendov in državnih praznikov. Ponudbe ni mogoče kombinirati. Na voljo, dokler ne poidejo zaloge.
Duh uspeha|2|False|V trenutku dobite svetovno marketinško kampanjo!
Duh uspeha|3|False|Mednarodni sloves!
Duh uspeha|4|False|Osebni stilist in frizer vključena!
Duh uspeha|5|False|Takojšnji rezultati zagotovljeni!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Duh uspeha|1|False|Potrebujem le droben podpis na dnu te pogodbe in vrata slave vam bodo odprta!
Paprika|2|False|Čakaj malo.
Paprika|4|True|Kaj stoji za to deus ex machino ?!
Paprika|3|False|Točno takrat, ko sem najbolj potrta, padeš z neba s čarobno rešitvijo za vse moje težave?!
Paprika|5|False|Tole je sumljivo, zelo sumljivo!
Paprika|6|True|Pridi Korenček.
Paprika|7|False|Taka zvijača bi nekoč mogoče še vžgala, vendar nisva več tako lahkoverna!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ptica|1|True|Čiiv!
Ptica|2|True|Gru!
Ptica|3|True|Gruu!
Ptica|4|False|Čiiv!
Paprika|5|False|Veš, kaj, Korenček? Mislim, da vem, kaj pomeni odrasti.
Paprika|6|False|Da ne iščeš več bližnjic, ampak uživaš v sadovih svojega truda …
Paprika|7|False|Zakaj le bi bila drugim nevoščljiva za uspeh?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|1|False|Zvezda
Napis|2|False|50 000 kojev
Napis|3|False|Žafranka in njen uspeh
Napis|4|False|Moda
Napis|5|False|Žafranka - fenomen
Paprika|8|False|Tudi nama se bo nekega dne nasmehnila sreča!
Napis|6|False|Slog
Napis|7|False|Žafranka na vrhuncu
Zvok|9|False|Bzeeuuu!
Napis|10|False|Zvezda
Napis|11|False|G. Golob kandidira za predsednika
Napis|12|False|Moda
Napis|13|False|G. Golob - lepši, kakor izgleda
Napis|14|False|Slog
Napis|15|False|G. Golob na vrhuncu
Pripovedovalec|16|False|- KONEC -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|September 2017 - www.peppercarrot.com - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Zasluge|2|False|Lektoriranje in izboljšave dialoga: Alex Gryson, Calimeroteknik, Nicolas Artance, Valvin in Craig Maloney.
Zasluge|4|False|Dogaja se v vesolju Hereve avtorja Davida Revoyja s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|5|False|Programska oprema: Krita 3.1.4 in Inkscape 0.92dev na Linux Mint 18.2 Cimetka (Cinnamon ;) )
Zasluge|6|False|Licenca: Creative Commons Priznanje avtorstva 4.0.
Zasluge|8|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
Zasluge|7|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 879 bralcev:
Zasluge|3|False|Pomoč pri oblikovanju zgodbe in uprizoritve: Calimeroteknik in Craig Maloney.
