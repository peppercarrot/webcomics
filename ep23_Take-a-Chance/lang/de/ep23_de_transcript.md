# Transcript of Pepper&Carrot Episode 23 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 23: Nach dem Glück greifen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Markt in Komona, eine Woche später
Schrift|2|False|Prominenz
Schrift|3|False|50 000 Ko
Schrift|4|False|Saffron und ihr Gewinn
Schrift|5|False|Mode
Schrift|6|False|Das Phänomen Saffron
Schrift|7|False|Stil
Schrift|8|False|Saffron Sonderheft
Geräusch|9|False|PLUMPS!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Danke, Carrot…
Pepper|2|False|Du weißt, dass ich jeden Tag hart daran arbeite, eine gute Hexe zu sein.
Pepper|3|True|… ich achte die Tradition, befolge die Regeln …
Pepper|4|False|… es ist einfach nicht fair, dass Saffron mit diesen Tricks so berühmt wird.
Pepper|5|False|Einfach ungerecht.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geist des Erfolgs|2|True|Seien Sie gegrüßt!
Geist des Erfolgs|3|False|Und erlauben Sie mir, mich vorzustellen:
Geräusch|1|False|Plong!
Geist des Erfolgs|4|False|Ich bin der Geist des Erfolgs, täglich rund um die Uhr stets zu Ihren Diensten*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geist des Erfolgs|1|False|*Ausgenommen an Wochenenden und Feiertagen. Nicht mit anderen Angeboten kombinierbar. Solange der Vorrat reicht.
Geist des Erfolgs|2|False|Inklusive sofortiger internationaler Markenkampagne!
Geist des Erfolgs|3|False|Weltbekannter Anbieter!
Geist des Erfolgs|4|False|Persönlicher Stylist und Frisör!
Geist des Erfolgs|5|False|Mit sofortiger Erfolgsgarantie!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geist des Erfolgs|1|False|Nur eine winzig kleine Unterschrift unter diesen Vertrag und die Tür zum Ruhm steht Ihnen offen!
Pepper|2|False|Moment.
Pepper|4|True|Was steckt hinter diesem Deus ex Machina?!
Pepper|3|False|Ausgerechnet jetzt, wo ich am Boden zerstört bin, fällst Du mit einer magischen Lösung für alle meine Probleme vom Himmel?
Pepper|5|False|Das kommt mir alles verdächtig vor, sehr verdächtig.
Pepper|6|True|Los komm, Carrot.
Pepper|7|False|Dieser Trick hat vielleicht früher mal funktioniert, aber jetzt sind wir nicht mehr so blöd, in so eine Falle zu laufen.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vogel|1|True|Zwitscher
Vogel|2|True|Gurr!
Vogel|3|True|Gurrr!
Vogel|4|False|Zwitscher!
Pepper|5|False|Siehst du Carrot, das heißt es, erwachsen zu werden
Pepper|6|False|Nicht den leichten Weg nehmen, keine Anstrengung scheuen …
Pepper|7|False|… kein Grund, neidisch auf den Erfolg anderer zu sein.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Schrift|1|False|Prominenz
Schrift|2|False|50 000 Ko
Schrift|3|False|Saffron und ihr Gewinn
Schrift|4|False|Mode
Schrift|5|False|Das Phänomen Saffron
Pepper|8|False|Irgendwann lacht auch uns das Glück!
Schrift|6|False|Stil
Schrift|7|False|Saffron Sonderheft
Geräusch|9|False|Bsiooo!
Schrift|10|False|Prominenz
Schrift|11|False|Das flotte Leben der Frau Taube
Schrift|12|False|Mode
Schrift|13|False|Das Phänomen Frau Taube
Schrift|14|False|Stil
Schrift|15|False|Frau Taube Sonderheft
Erzähler|16|False|- ENDE -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|08/2017 - www.peppercarrot.com - Illustration & Handlung: David Revoy - Deutsche Übersetzung: colognella
Impressum|2|False|Korrektorat & Dialogoptimierung: Alex Gryson, Calimeroteknik, Nicolas Artance und Valvin.
Impressum|4|False|Basierend auf der Hereva-Welt von David Revoy mit Beiträgen von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Impressum|5|False|Software: Krita 3.1.4, Inkscape 0.92dev auf Linux Mint 18.2 Cinnamon
Impressum|6|False|Lizenz: Creative Commons Namensnennung 4.0
Impressum|8|False|Auch Du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
Impressum|7|False|Pepper&Carrot ist komplett frei, Open Source und wird durch seine Leserinnen und Leser unterstützt und finanziert. Für diese Episode geht der Dank an 879 Förderer:
Impressum|3|False|Unterstützung bei Konzept & Inszenierung: Calimeroteknik und Craig Maloney.
