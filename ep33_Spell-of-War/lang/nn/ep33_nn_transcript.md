# Transcript of Pepper&Carrot Episode 33 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 33: Krigsmagi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fiende|1|False|huUVOOOOOOOOOOO!
Hær|2|False|Røøøl!
Hær|5|False|Grrråår!
Hær|4|False|Grrrr!
Hær|3|False|Jurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|True|Så, unge heks.
Konge|2|False|Har du ein trylleformel som kan hjelpa oss, er no rett tid å bruka han!
Pepar|3|True|Greitt!
Pepar|4|False|Førebu deg på å sjå ...
Lyd|5|False|Dzziii!!
Pepar|6|False|... MEISTER-VERKET MITT!
Lyd|7|False|Dzziiii!!
Pepar|8|False|Realitas Hackeris Pepperus!
Lyd|9|False|Dzziooo!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Fiizz!
Lyd|2|False|Dzii!
Lyd|3|False|Sjii!
Lyd|4|False|Ffhii!
Lyd|8|False|Dziing!
Lyd|7|False|Fiizz!
Lyd|6|False|Sjii!
Lyd|5|False|Ffhii!
Konge|9|True|Er det så at denne magien gjer sverda våre kraftigare ...
Konge|10|False|... og våpena deira svakare?
Lyd|11|False|Dzii...
Pepar|12|True|Heh.
Pepar|13|True|Vent og sjå!
Pepar|14|False|Men eg kan røpa at du ikkje kjem til å mista nokon soldatar i dag.
Pepar|15|False|Like fullt må de kjempa så godt de kan!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|False|Det er det som er planen ...
Konge|2|False|PÅ ORDRE FRÅ MEG ...
Hær|3|False|Jaaa!
Hær|4|False|Jaaa!
Hær|5|False|Jaaa!
Hær|6|False|Jaaa!
Hær|7|False|Jaaa!
Hær|8|False|Jaaa!
Hær|9|False|Jaaa!
Hær|10|False|Jaaa!
Hær|11|False|Jaaa!
Konge|12|False|TIL ÅÅÅTAAAK!!!
Hær|13|False|Jaaa!
Hær|14|False|Jaaa!
Hær|15|False|Jaaa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|False|Jaaah!!!
Fiende|2|False|Juurrr!!!
Lyd|3|False|Sjviiing!!!
Lyd|4|False|Svosj!!!
Skrift|5|False|12
Fiende|6|False|?!!
Skrift|7|False|8
Konge|8|False|?!!
Skrift|9|False|64
Skrift|10|False|32
Skrift|11|False|72
Skrift|12|False|0
Skrift|13|False|64
Skrift|14|False|0
Skrift|15|False|56
Hær|20|False|Yrrr!
Hær|17|False|Jurr!
Hær|19|False|Grrr!
Hær|21|False|Jaaah!
Hær|18|False|Jaaah!
Hær|16|False|Jaaååå!
Lyd|27|False|Ssjing
Lyd|25|False|Fffsj
Lyd|22|False|Visss
Lyd|23|False|Svosj
Lyd|24|False|Sjklong
Lyd|26|False|Sjkling

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Konge|1|False|?!
Skrift|2|False|3
Skrift|3|False|24
Skrift|4|False|38
Skrift|5|False|6
Skrift|6|False|12
Skrift|7|False|0
Skrift|8|False|5
Skrift|9|False|0
Skrift|10|False|37
Skrift|11|False|21
Skrift|12|False|62
Skrift|13|False|27
Skrift|14|False|4
Konge|15|False|!!
Konge|16|False|HEEEEEEEEEEKS!!! KVA ER DET HER?!!!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Ta-daaa!
Pepar|2|True|Det her
Pepar|3|False|... er meister-verket mitt!
Pepar|4|False|Det er ein innvikla trylleformel som endrar røynda og viser kor mange livspoeng som fienden har att.
Skrift|5|False|33
Pepar|6|True|Når ein slepp opp for livspoeng, må ein forlata slagmarka.
Pepar|7|True|Hæren som framleis har folk att på marka til slutt, vinn.
Pepar|8|False|Enkelt og greitt.
Skrift|9|False|0
Hær|10|False|?
Pepar|11|True|Er det ikkje fantastisk?!
Pepar|12|True|Ingen fleire drepne!
Pepar|13|True|Ingen fleire såra!
Pepar|14|False|Det vil revolusjonera all krig!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|True|Men no når me alle kjenner reglane, skal eg nullstilla livspoenga, slik at me kan starta på nytt.
Pepar|2|False|Då vert det meir rettferdig!
Skrift|3|False|64
Skrift|4|False|45
Skrift|5|False|6
Skrift|6|False|2
Skrift|7|False|0
Skrift|8|False|0
Skrift|9|False|0
Skrift|10|False|0
Skrift|11|False|9
Skrift|12|False|5
Skrift|13|False|0
Skrift|14|False|0
Skrift|15|False|0
Lyd|16|False|Ssjing!
Lyd|17|False|Zooo!
Lyd|19|False|Tsjakk!
Lyd|18|False|Pokk!
Pepar|20|True|Opp med farten, Gulrot!
Pepar|21|False|Trylleformelen varer ikkje evig!
Forteljar|22|False|– SLUTT –

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|29. juni 2020 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2 og Valvin. Omsetjing til nynorsk Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson. Programvare: Krita 4.3 og Inkscape 1.0 på Kubuntu 19.10. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 1 190 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
