# Transcript of Pepper&Carrot Episode 33 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 33: Hechizo de guerra

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Enemigo|1|False|¡baWOUUUUUUUUUU!
Ejército.|2|False|¡¡Roooooar!!
Ejército.|5|False|¡¡Grroooaarr!!
Ejército.|4|False|¡Grrrr!
Ejército.|3|False|¡Yuurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|True|Bien, joven bruja.
Rey|2|False|Si tienes un hechizo para ayudarnos, es el momento de usarlo, ¡ahora o nunca!
Pimienta|3|True|¡Entendido!
Pimienta|4|False|Preparaos para ser testigo de...
Sonido|5|False|¡¡Dzziii!!
Pimienta|6|False|...¡¡MI OBRA MAESTRA!!
Sonido|7|False|¡¡Dzziiii!!
Pimienta|8|False|¡Realitas Hackeris Pimientus!
Sonido|9|False|¡¡Dzziooo!!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|1|False|¡Fiizz!
Sonido|2|False|¡Dzii!
Sonido|3|False|¡Schii!
Sonido|4|False|¡Ffhii!
Sonido|8|False|¡Dziing!
Sonido|7|False|¡Fiizz!
Sonido|6|False|¡Schii!
Sonido|5|False|¡Ffhii!
Rey|9|True|¿Este hechizo es para dar más fuerza a nuestras espadas...
Rey|10|False|...y debilitar las armas de nuestros enemigos?
Sonido|11|False|Dzii...
Pimienta|12|True|Jeje.
Pimienta|13|True|¡Ya lo veréis!
Pimienta|14|False|Lo único que puedo adelantaros es que no vais a perder a ningún soldado hoy.
Pimienta|15|False|¡Pero aún así tendréis que luchar y dar lo mejor de vosotros mismos!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|False|¡Eso es para lo que estamos entrenados!
Rey|2|False|¡A MIS ÓRDENES!
Ejército.|3|False|!Yaaa!
Ejército.|4|False|¡Sí, señor!
Ejército.|5|False|¡Yaaa!
Ejército.|6|False|¡Yaa!
Ejército.|7|False|¡Síii!
Ejército.|8|False|¡Yaaah!
Ejército.|9|False|¡Yihaaa!
Ejército.|10|False|¡Yahii!
Ejército.|11|False|¡Yaa!
Rey|12|False|¡¡¡A LA CARGAAAAAA!!!
Ejército.|13|False|¡SÍIII!
Ejército.|14|False|¡Yaaa!
Ejército.|15|False|¡Yihaaa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|False|¡¡Yaaaa!!
Enemigo|2|False|¡¡Yuuurr!!
Sonido|3|False|¡¡Chuiiiing!!
Sonido|4|False|¡¡Swuush!!
Escritura|5|False|12
Enemigo|6|False|¡¿?!
Escritura|7|False|8
Rey|8|False|¡¿?!
Escritura|9|False|64
Escritura|10|False|32
Escritura|11|False|72
Escritura|12|False|0
Escritura|13|False|64
Escritura|14|False|0
Escritura|15|False|56
Ejército.|20|False|¡Yurrr!
Ejército.|17|False|¡Yurr!
Ejército.|19|False|¡Grrr!
Ejército.|21|False|¡Yaaa!
Ejército.|18|False|¡Síiii!
Ejército.|16|False|¡Yaaa!
Sonido|27|False|Sshing
Sonido|25|False|Fffshh
Sonido|22|False|wiizz
Sonido|23|False|Swuush
Sonido|24|False|Shklong
Sonido|26|False|Shkling

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rey|1|False|¡¿?!
Escritura|2|False|3
Escritura|3|False|24
Escritura|4|False|38
Escritura|5|False|6
Escritura|6|False|12
Escritura|7|False|0
Escritura|8|False|5
Escritura|9|False|0
Escritura|10|False|37
Escritura|11|False|21
Escritura|12|False|62
Escritura|13|False|27
Escritura|14|False|4
Rey|15|False|¡¡!!
Rey|16|False|¡BRUJAAAAAA! ¡¿QUÉ HAS HECHO?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Tachán!
Pimienta|2|True|«Esta»
Pimienta|3|False|...¡es mi obra maestra!
Pimienta|4|False|Es un hechizo complejo que altera la realidad y muestra los puntos de vida restantes de vuestro adversario.
Escritura|5|False|33
Pimienta|6|True|Cuando vos no tengáis más puntos de vida, abandonaréis el campo de batalla hasta el final del combate.
Pimienta|7|True|¡La primera armada que elimine todos los enemigos gana!
Pimienta|8|False|Simple.
Escritura|9|False|0
Ejército.|10|False|¿?
Pimienta|11|True|¿No es genial?
Pimienta|12|True|¡No más muertes!
Pimienta|13|True|¡Ningún soldado herido más!
Pimienta|14|False|¡Esto va a revolucionar la manera de hacer la guerra!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¡Ahora que ya conocéis las reglas, dejadme reiniciar los contadores para que podáis recomenzar!
Pimienta|2|False|Así será más justo.
Escritura|3|False|64
Escritura|4|False|45
Escritura|5|False|6
Escritura|6|False|2
Escritura|7|False|0
Escritura|8|False|0
Escritura|9|False|0
Escritura|10|False|0
Escritura|11|False|9
Escritura|12|False|5
Escritura|13|False|0
Escritura|14|False|0
Escritura|15|False|0
Sonido|16|False|¡Sshing!
Sonido|17|False|¡Souu!
Sonido|19|False|¡Chac!
Sonido|18|False|¡Poc!
Pimienta|20|True|¡Más rápido, Zanahoria!
Pimienta|21|False|¡El hechizo no va a durar mucho tiempo más!
Narrador|22|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|4|False|El 29 de junio 2020 Dibujo & Guion: David Revoy. Lectores de la versión beta: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Versión en castellano Traducción: TheFaico. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.3, Inkscape 1.0 en Kubuntu 19.10. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
Pimienta|5|True|¿Sabías qué?
Pimienta|6|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|7|False|¡Este episodio ha recibido el apoyo de 1190 mecenas!
Pimienta|8|True|¡Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|9|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|10|True|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|11|False|¡Gracias!
