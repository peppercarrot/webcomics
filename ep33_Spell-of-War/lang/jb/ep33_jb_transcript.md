# Transcript of Pepper&Carrot Episode 33 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 32 mo'o valsi lo ka jamna

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Enemy|1|False|.e'ei .e'ei .e'ei
Army|2|False|le'o
Army|5|False|Grrawwr!
Army|4|False|klama
Army|3|False|gunta

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|.i'a doi citno te makfa
King|2|False|ganai do se slabu lo valsi be lo ka sidju mi'a gi le cabna cu cedra ka bu
la .piper.|3|True|je'e do
la .piper.|4|False|.i e'e do do zgana
Sound|5|False|.sang.
la .piper.|6|False|le nu manci gau mi
Sound|7|False|.sang.
la .piper.|8|False|ca'e sampla le munje be la .piper.
Sound|9|False|.jbam.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fiizz!
Sound|2|False|Dzii!
Sound|3|False|Schii!
Sound|4|False|Ffhii!
Sound|8|False|Dziing!
Sound|7|False|Fiizz!
Sound|6|False|Schii!
Sound|5|False|Ffhii!
King|9|True|ja'o xu ri'a tu'a le valsi ge le mi'a cladakfu co'i zenba lo ka vlipa
King|10|False|gi le cladakfu pe le bradi co'i jdika lo ka vlipa
Sound|11|False|.sang.
la .piper.|12|True|u'i
la .piper.|13|True|za'aba'adai
la .piper.|14|False|.ai mi xusra le du'u ca le cabdei le do jenmi na zenba lo ka xo kau mei
la .piper.|15|False|.i ku'i .ei do damba tai lo zabna

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|ba'e la'e do'i se vlipa mi'a
King|2|False|ko mi minde
Army|3|False|Yahhhh!
Army|4|False|Yeaahh!
Army|5|False|Yaaah!
Army|6|False|Yaawww!
Army|7|False|Yeaaah!
Army|8|False|Yaaah!
Army|9|False|Yaeeh!
Army|10|False|Yaeeh!
Army|11|False|Yaahhh!
King|12|False|gunta
Army|13|False|Yeaahh!
Army|14|False|Yahhh!
Army|15|False|Yaeeh!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|e'e
Enemy|2|False|a'e
Sound|3|False|bu'o
Sound|4|False|le'o
Writing|5|False|12
Enemy|6|False|y
Writing|7|False|8
King|8|False|ua nai
Writing|9|False|64
Writing|10|False|32
Writing|11|False|72
Writing|12|False|0
Writing|13|False|64
Writing|14|False|0
Writing|15|False|56
Army|20|False|y
Army|17|False|a'a
Army|19|False|oi
Army|21|False|ia
Army|18|False|ie
Army|16|False|i'o!
Sound|27|False|mo
Sound|25|False|a'e
Sound|22|False|ui
Sound|23|False|ue
Sound|24|False|ia
Sound|26|False|uo

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|False|ua nai sai
Writing|2|False|3
Writing|3|False|24
Writing|4|False|38
Writing|5|False|6
Writing|6|False|12
Writing|7|False|0
Writing|8|False|5
Writing|9|False|0
Writing|10|False|37
Writing|11|False|21
Writing|12|False|62
Writing|13|False|27
Writing|14|False|4
King|15|False|!!
King|16|False|doi te makfa do zukte ma ge'e sai

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|False|uo cai
la .piper.|2|True|la'e do'i
la .piper.|3|False|traji fo le'i makfa be fi mi
la .piper.|4|False|i makfa fa le nu le munje cu cenba ja'e lo nu ro do ka'e viska lo tcita be lo se jivna be do bei lo du'u ma kau jei ri vlipa
Writing|5|False|33
la .piper.|6|True|i ganai li no jei do vlipa gi do co'u zvati le foldi
la .piper.|7|True|.i ganai ro sonci be lo drata co'u zvati gi do jinga
la .piper.|8|False|.i sampu
Writing|9|False|0
Army|10|False|y
la .piper.|11|True|zabna xu nai
la .piper.|12|True|i ba'o gau morsi
la .piper.|13|True|i ba'o se xrani sonci
la .piper.|14|False|i lo'e nu jamna ba ze'e drata

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|i nauku mi'o djuno fi le javni i ai gau mi le sonci cu se tcita le krasi i ai mi'o za'u re'u simxu lo ka damba
la .piper.|2|False|i ba vrude pajni
Writing|3|False|64
Writing|4|False|45
Writing|5|False|6
Writing|6|False|2
Writing|7|False|0
Writing|8|False|0
Writing|9|False|0
Writing|10|False|0
Writing|11|False|9
Writing|12|False|5
Writing|13|False|0
Writing|14|False|0
Writing|15|False|0
Sound|16|False|c.c.c.cin
Sound|17|False|j.j.j.
Sound|19|False|.darx.
Sound|18|False|.tunt.
la .piper.|20|True|ko bajra doi karot
la .piper.|21|False|le makfa pu'o fasnu
Narrator|22|False|- fanmo -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|4|False|la'e la .piper. joi la .karot. cu fingubni gi'e se sarji le tcidu
la .piper.|5|True|.i sarji le nu finti le dei lisri kei fa le 1121 da poi liste fa di'e
la .piper.|6|True|xu do djuno le du'u
la .piper.|7|False|.i do ji'a ka'e sidju .i je ba'e le cmene be do ba se ciska fi le tai papri
la .piper.|8|True|.i ko tcidu fi la'o .url. www.peppercarrot.com .url. ja'e le nu do djuno fi le tcila
la .piper.|9|True|.i mi'a pilno la'o gy.Patreon.gy. e la'o gy.Tipeee.gy. e la'o gy.PayPal.gy. e la'o gy.Liberapay.gy. e la'o gy. e lo drata
la .piper.|10|True|ki'e do
la .piper.|11|False|.i de'i li nanca bu 2020 masti bu 6 djedi bu 29 co'a gubni .i le pixra .e le lisri cu se finti la'o fy.David Revoy.fy. .i pu cipra tcidu lu'i le lisri poi pu nu'o bredi fa la'o gy.Arlo James Barnes.gy. e la'o gy.Craig Maloney.gy. e la'o gy.GunChleoc.gy. e la'o gy.Hyperbolic Pain.gy. e la'o gy.Karl Ove Hufthammer.gy. e la'o gy.Martin Disch.gy. e la'o gy.Nicolas Artance.gy. e la'o gy.Nicolas Artance.gy.e la'o gy.Valvin.gy. .i le lojbo xe fanva zo'u fanva fa la gleki .i skicu la .erevas. noi munje zi'e noi finti fa la'o.fy.David Revoy.fy. .i ralju sidju fa la'o gy.Craig Maloney.gy. .i finti lei lisri be la .erevas. fa la'o gy.Craig Maloney.gy. e la'o gy.Nartance.gy. e la'o gy.Scribblemaniac.gy. e la'o gy.Valvin.gy. .i cikre le xe fanva fa la'o gy.Willem Sonke.gy. e la'o gy.Moini.gy. e la'o gy.Hali.gy. e la'o gy.CGand.gy. e la'o gy.Alex Gryson.gy. .i pilno le proga poi du la'o py. Krita 4.3.py. jo'u la'o py. Inkscape 1.0.py. jo'u la'o py.Kubuntu 19.10.py. i finkrali javni fa la'o jy. Creative Commons Attribution 4.0 .jy .i zoi .urli. www.peppercarrot.com .urli. judri
