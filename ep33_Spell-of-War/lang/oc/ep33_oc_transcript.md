# Transcript of Pepper&Carrot Episode 33 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 33 : Lo Sortilègi de Guèrra

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Enemic|1|False|haoooooooooooo !
Armada|2|False|Roooooar !!
Armada|5|False|Grroooaarr !!
Armada|4|False|Grrrr !
Armada|3|False|Nhuurrr !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|True|OK, jova masca.
Rei|2|False|Se as un sortilègi per nos ajudar, ara es lo moment !
Pepper|3|True|Plan notat !
Pepper|4|False|Preparatz-vos a descobrir...
Son|5|False|Dzziii !!
Pepper|6|False|... MON CAP-D’ÒBRA !!
Son|7|False|Dzziiii !!
Pepper|8|False|Realitas Hackeris Pepperus !
Son|9|False|Dzziooo !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Fiizz !
Son|2|False|Dzii !
Son|3|False|Sshii !
Son|4|False|Ffhii !
Son|8|False|Dziing !
Son|7|False|Fiizz !
Son|6|False|Sshii !
Son|5|False|Ffhii !
Rei|9|True|Seriá un sortilègi per balhar mai de poténcia a nòstras espadas...
Rei|10|False|... e aflaquir las armas de nòstres enemics ?
Son|11|False|Dzii...
Pepper|12|True|Ha, ha.
Pepper|13|True|Veiretz !
Pepper|14|False|Tot çò que vos pòdi dire es que perdretz pas cap de soldat uèi.
Pepper|15|False|Çaquelà vos calrà batre e balhar tot çò que podètz !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|False|Es per aquò que sèm entraïnats !
Rei|2|False|QUAND O COMANDI !
Armada|3|False|Zooo !
Armada|4|False|Òc !
Armada|5|False|Anem !
Armada|6|False|Anem !
Armada|7|False|Fai tirar !
Armada|8|False|Zooo !
Armada|9|False|Hihaaa !
Armada|10|False|Prèst !
Armada|11|False|Zoo !
Rei|12|False|CARGAAAAAAAAAATZ !!!
Armada|13|False|Òòòòòc !
Armada|14|False|Zooooo !
Armada|15|False|Hihaaa !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|False|Haaai !!
Enemic|2|False|Nhuurr !!!
Son|3|False|Tchiiiing !!
Son|4|False|Flotch !!
Escritura|5|False|12
Enemic|6|False|?!!
Escritura|7|False|8
Rei|8|False|?!!
Escritura|9|False|64
Escritura|10|False|32
Escritura|11|False|72
Escritura|12|False|0
Escritura|13|False|64
Escritura|14|False|0
Escritura|15|False|56
Armada|20|False|Urrr !
Armada|17|False|Nhur !
Armada|19|False|Grrr !
Armada|21|False|Tèèè !
Armada|18|False|Òòòòc !
Armada|16|False|Zoo !
Son|27|False|Sshing
Son|25|False|Fffchh
Son|22|False|uiizz
Son|23|False|Floosh
Son|24|False|Chclong
Son|26|False|Chcling

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Rei|1|False|?!
Escritura|2|False|3
Escritura|3|False|24
Escritura|4|False|38
Escritura|5|False|6
Escritura|6|False|12
Escritura|7|False|0
Escritura|8|False|5
Escritura|9|False|0
Escritura|10|False|37
Escritura|11|False|21
Escritura|12|False|62
Escritura|13|False|27
Escritura|14|False|4
Rei|15|False|!!
Rei|16|False|MAAAAASCA ! MAS DE QU'AS FAIT ?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadààà !
Pepper|2|True|« Aquò »
Pepper|3|False|... es mon cap d'òbra !
Pepper|4|False|Es un sortilègi complèxe qu'altèra la realitat e mòstra los punts de vida que demòran a vòstre adversari.
Escritura|5|False|33
Pepper|6|True|Quand avètz pas pus de punts de vida, daissatz lo prat batalhièr fins a la fin del combat.
Pepper|7|True|La primièra armada qu'elimina totes sos opausants ganha !
Pepper|8|False|Es aisit !
Escritura|9|False|0
Armada|10|False|?
Pepper|11|True|Es pas genial ?
Pepper|12|True|Pas pus de mòrts !
Pepper|13|True|Pas pus de soldats nafrats !
Pepper|14|False|Aquò revolucionarà la faiçon de far la guèrra !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Ara que coneissètz las règlas, daissatz-me tornar totes los comptadors a zèro per que nos i posquèssem tornar !
Pepper|2|False|Serà mai equitable.
Escritura|3|False|64
Escritura|4|False|45
Escritura|5|False|6
Escritura|6|False|2
Escritura|7|False|0
Escritura|8|False|0
Escritura|9|False|0
Escritura|10|False|0
Escritura|11|False|9
Escritura|12|False|5
Escritura|13|False|0
Escritura|14|False|0
Escritura|15|False|0
Son|16|False|Sshing !
Son|17|False|Zooo !
Son|19|False|tchac !
Son|18|False|pòc !
Pepper|20|True|Cor mai lèu, Carròt !
Pepper|21|False|Lo sortilègi se va acabar pro lèu !
Narrator|22|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|4|False|Lo 29 de junh de 2020 Art & scenari : David Revoy. Lectors de la version bêta : Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Traduccion occitana : Aure Séguier Basat sus l'univèrs d'Hereva Creator : David Revoy. Manteneire màger : Craig Maloney. Redactors : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logicials : Krita 4.3, Inkscape 1.0 sus Kubuntu 19.10. Licéncia : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|O sabiatz ?
Pepper|6|True|Pepper&Carrot es completament liure, gratuit, open-source e esponsorizat amb lo mecenat de sos lectors.
Pepper|7|False|Aqueste episòdi a recebut lo sosten de 1190 mecènas !
Pepper|8|True|Podètz tanben venir mecèna de Pepper&Carrot e aver vòstre nom inscriut aquí !
Pepper|9|True|Sèm sus Patreon, Tipeee, PayPal, Liberapay ... e d'autres !
Pepper|10|True|Anatz sus www.peppercarrot.com per mai d'informacions !
Pepper|11|False|Mercés !
