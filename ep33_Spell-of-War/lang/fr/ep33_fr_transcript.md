# Transcript of Pepper&Carrot Episode 33 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 33 : Le Sortilège de Guerre

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ennemi|1|False|hôoWOUUUUUUUUUU !
Armée|2|False|Roooooar !!
Armée|5|False|Grroooaarr !!
Armée|4|False|Grrrr !
Armée|3|False|Yuurrr !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|True|OK, jeune sorcière.
Roi|2|False|Si tu as un sort pour nous aider, c'est maintenant ou jamais !
Pepper|3|True|Bien reçu !
Pepper|4|False|Préparez-vous à découvrir...
Son|5|False|Dzziii !!
Pepper|6|False|... MON CHEF-D’ŒUVRE !!
Son|7|False|Dzziiii !!
Pepper|8|False|Realitas Hackeris Pepperus !
Son|9|False|Dzziooo !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Fiizz !
Son|2|False|Dzii !
Son|3|False|Schii !
Son|4|False|Ffhii !
Son|8|False|Dziing !
Son|7|False|Fiizz !
Son|6|False|Schii !
Son|5|False|Ffhii !
Roi|9|True|Serait-ce un sort pour donner plus de puissance à nos épées...
Roi|10|False|... et affaiblir les armes de nos ennemis ?
Son|11|False|Dzii...
Pepper|12|True|Hé, hé.
Pepper|13|True|Vous verrez bien !
Pepper|14|False|Tout ce que je peux dire, c'est que vous n'allez perdre aucun soldat aujourd'hui.
Pepper|15|False|Mais il faudra quand même vous battre et donner le meilleur de vous-mêmes !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|False|C'est ce pour quoi nous nous sommes entraînés !
Roi|2|False|À MON COMMANDEMENT !
Armée|3|False|Yaaa !
Armée|4|False|Ouais !
Armée|5|False|Yaaa !
Armée|6|False|Yâaa !
Armée|7|False|Ouaiiiis !
Armée|8|False|Yaaah !
Armée|9|False|Yihaaa !
Armée|10|False|Yahii !
Armée|11|False|Yaa !
Roi|12|False|CHARGEEEEEEEEEEZ !!!
Armée|13|False|Ouaaais !
Armée|14|False|Yaaa !
Armée|15|False|Yihaaa !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|False|Yaaaa !!
Ennemi|2|False|Yuuurr !!!
Son|3|False|Tchiiiing !!
Son|4|False|Swoutch !!
Écriture|5|False|12
Ennemi|6|False|?!!
Écriture|7|False|8
Roi|8|False|?!!
Écriture|9|False|64
Écriture|10|False|32
Écriture|11|False|72
Écriture|12|False|0
Écriture|13|False|64
Écriture|14|False|0
Écriture|15|False|56
Armée|20|False|Yrrr !
Armée|17|False|Yurr !
Armée|19|False|Grrr !
Armée|21|False|Yaaa !
Armée|18|False|Ouaaais !
Armée|16|False|Yâaa !
Son|27|False|Sshing
Son|25|False|Fffchh
Son|22|False|wiizz
Son|23|False|Swoosh
Son|24|False|Chklong
Son|26|False|Chkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Roi|1|False|?!
Écriture|2|False|3
Écriture|3|False|24
Écriture|4|False|38
Écriture|5|False|6
Écriture|6|False|12
Écriture|7|False|0
Écriture|8|False|5
Écriture|9|False|0
Écriture|10|False|37
Écriture|11|False|21
Écriture|12|False|62
Écriture|13|False|27
Écriture|14|False|4
Roi|15|False|!!
Roi|16|False|SORCIÈÈÈÈRE ! QU'EST-CE QUE TU AS FAIT ?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa !
Pepper|2|True|« Ceci »
Pepper|3|False|... est mon chef-d’œuvre !
Pepper|4|False|C'est un sort complexe qui altère la réalité et montre les points de vie restant de votre adversaire.
Écriture|5|False|33
Pepper|6|True|Lorsque vous n'avez plus de point de vie, vous quittez le champ de bataille jusqu'à la fin du combat.
Pepper|7|True|La première armée qui élimine tous ses opposants gagne !
Pepper|8|False|Facile.
Écriture|9|False|0
Armée|10|False|?
Pepper|11|True|C'est pas génial ?
Pepper|12|True|Plus aucun mort !
Pepper|13|True|Plus aucun soldat blessé !
Pepper|14|False|Ça va révolutionner la façon de faire la guerre !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Maintenant que vous connaissez les règles, laissez-moi remettre les compteurs à zéro pour qu'on puisse recommencer !
Pepper|2|False|Ce sera plus équitable.
Écriture|3|False|64
Écriture|4|False|45
Écriture|5|False|6
Écriture|6|False|2
Écriture|7|False|0
Écriture|8|False|0
Écriture|9|False|0
Écriture|10|False|0
Écriture|11|False|9
Écriture|12|False|5
Écriture|13|False|0
Écriture|14|False|0
Écriture|15|False|0
Son|16|False|Sshing !
Son|17|False|Zouu !
Son|19|False|tchac !
Son|18|False|poc !
Pepper|20|True|Cours plus vite, Carrot !
Pepper|21|False|Le sort ne va plus durer très longtemps !
Narrateur|22|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|4|False|Le 29 Juin 2020 Art & scénario : David Revoy. Lecteurs de la version bêta : Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Traduction française : Nicolas Artance, David Revoy, Valvin Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 4.3, Inkscape 1.0 sur Kubuntu 19.10. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Le saviez-vous ?
Pepper|6|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|7|False|Cet épisode a reçu le soutien de 1190 mécènes !
Pepper|8|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|9|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|10|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|11|False|Merci !
