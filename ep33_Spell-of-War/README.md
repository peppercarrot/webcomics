# Episode 33: Spell of War

![cover of episode 33](https://www.peppercarrot.com/0_sources/ep33_Spell-of-War/low-res/Pepper-and-Carrot_by-David-Revoy_E33.jpg)

## Comments from the author

Hey, so here is the second and last part of the Battlefield arc. It's an obvious reference to the life points system used in many video games but also on Tabletop RPGs. It also was a necessary episode to demotivate Pepper to use her power for war or to help armies; a very important element into the larger big story I have in mind for Pepper&Carrot.

From [Author's blog of episode 33](https://www.davidrevoy.com/article780/episode-33-spell-of-war/show#comments)
