# Transcript of Pepper&Carrot Episode 35 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 35: Odsev

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|O, zbudil si se.
Paprika|2|False|Si dobro? Te ne zebe?
Paprika|3|True|Žal bo moj rês kmalu pošel.
Paprika|4|False|In moja avra je prešibka, da bi naju na tej višina varovala pred mrazom.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Kmalu bom izgubila hiperpogon
Paprika|2|True|in tista letalka z njenim zmajem sta nama že več ur za petami.
Paprika|3|False|Če se upočasniva, naju bosta ujela.
Paprika|4|True|Sem pa res trapa!
Paprika|5|False|Mislila sem, da bosta že zdavnaj odnehala.
Paprika|6|False|Ko bi jima vsaj lahko ušla.
Paprika|7|True|NE. Moram se ju znebiti!
Paprika|8|False|Preden naju ujameta!
Paprika|9|True|Ampak tako zelo …
Paprika|10|True|… sem …
Paprika|11|False|… utrujena …
Paprika|12|True|UPS!
Paprika|13|False|Spravi se k sebi, Paprika!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Toreja|1|False|Si videl tole?
Arra|2|True|Da. Tega pregona bo zdaj zdaj konec.
Arra|3|False|Kaj kmalu ji bodo pošle moči.
Toreja|4|False|Takrat bova napadla.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|1|False|BumM!
Paprika|2|False|KAJ?!
Paprika|3|False|Prebila sta zvočni zid?!
Paprika|4|False|!!!
Zvok|5|False|VUuš!!!
Paprika|6|True|Grrr!
Paprika|7|False|Drži se, Korenček!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|PRESNETO!
Paprika|2|False|KAJ NAJ STORIM?
Paprika|3|False|O!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Jama pri Rogaškem jezeru!
Paprika|2|False|Korenček, zdaj imava priložnost!
Paprika|3|False|To jamo poznam.
Paprika|4|True|Odpira se v dolg predor, ki je na koncu zrušen.
Paprika|5|True|Kapniki na vdrtem stropu tvorijo smrtonosno bodičasto steno!
Paprika|6|True|Pred pastjo je majhna odprtina, ravno pravšnje velikosti za naju.
Paprika|7|False|Toda zmaj se ne bo mogel pravočasno ustaviti in bo s polno hitrostjo treščil v steno! Muahahaha!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Odlično! Sledita nama!
Paprika|2|False|Kmalu se ju bova znebila.
Paprika|3|True|Komaj čakam!
Paprika|4|False|Muahahaha!
Paprika|5|False|!!!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Čakaj … V kaj sem se spremenila?
Paprika|2|True|V morilko?
Paprika|3|False|V zlobno čarovnico?
Paprika|4|False|!!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|4|False|VUuš!
Paprika|1|True|NE!
Paprika|2|False|To nisem jaz!
Paprika|3|False|STOOOJ!!!
Paprika|5|False|PREDAM SE!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Toreja|1|False|Menda že veš, da ti trda prede, če te predam gospodarici Vasabi?
Paprika|2|False|Vem, a kljub temu verjamem, da se lahko pogovoriva in pobotava.
Paprika|3|True|Konec koncev ni nič nepopravljivo.
Paprika|4|False|Tega me je naučila prijateljica.

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Hvala, da si Korenčku pustila, da mi prinese čista oblačila.
Toreja|2|True|Z veseljem.
Toreja|3|False|Olajšala si nama nalogo. Toliko ti lahko pustim v zahvalo.
Toreja|4|True|A vedi, da je Vasabi strašno besna.
Toreja|5|True|Zahteva popolnost in s svojim prihodom, opravo in nečistostjo si to popolnost skalila.
Toreja|6|False|Poleg tega si jo še napadla, česar ne bo kar tako odpustila.
Paprika|7|False|Vem.
Toreja|8|True|Kakorkoli že, odspala bom eno kitico.
Toreja|9|True|Ko se zbudim, gremo.
Toreja|10|False|Med tem delaj, kar hočeš, le ne hodi predaleč …
Zvok|11|False|Pljusk
Toreja|12|False|Kakšna zanimiva čarovnica …

### P12

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Od zdaj naprej si bom kljub tveganjem in nevarnostim
Paprika|2|True|ostala zvesta.
Paprika|3|False|Častna beseda!
Pripovedovalec|4|False|SE NADALJUJE …

### P13

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|18. junij 2021 Piše in riše: David Revoy. Testni bralci: Arlo James Barnes, Craig Maloney, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore in Valvin. Prevedla: Andrej Ficko in Gorazd Gorup. Dogaja se v vesolju Hereve Stvaritelj: David Revoy. Glavni vzdrževalec: Craig Maloney. Uredniki: Craig Maloney, Nartance, Scribblemaniac in Valvin. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson. Programska oprema: Krita 4.4.2 in Inkscape 1.1 na Kubuntu Linux 20.04 Licenca: Creative Commons Priznanje avtorstva 4.0. www.peppercarrot.com
Paprika|3|True|Že veš?
Paprika|4|False|Strip Paprika in Korenček je popolnoma prost in odprtokoden, podpirajo pa ga njegovi bralci!
Paprika|2|True|Za to epizodo je zaslužnih 1054 podpornikov!
Paprika|5|True|Tudi ti lahko podpreš izdelavo tega stripa in si zagotoviš prostor na zgornjem seznamu imen!
Paprika|7|True|Smo na Patreonu, Tipeeeju, PayPalu, Liberapayju … in drugod!
Paprika|6|True|Obišči www.peppercarrot.com za več informacij!
Paprika|8|False|Hvalaaaa!
