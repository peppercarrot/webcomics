# Transcript of Pepper&Carrot Episode 17 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium XVII: Novum initium

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cuminum|1|False|Sed Piper... ...Reveni...
Piper|2|False|NOLO ! DISCEDO !!
Piper|3|False|Vos numquam discitis veram magiam. Eo! ...in sectam magarum Ahis!
Sonus|4|False|Zuuush !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|True|Bene, eamus in patriam Lunarum occidentum.
Piper|2|False|Shichimi nobis consilium dabit ad sectam magarum Ahis accedendum.
Piper|3|False|Carota cape pyxin magneticam chartamque geographicam: sunt nimis nubes et nescio ubi eam.
Piper|5|True|MECASTOR !
Piper|6|True|Turbulentiae !
Piper|7|False|AD REM ADHAERESCE !
Piper|9|False|Eheu !!!
Sonus|8|False|BrrUuuuUuu !!|nowhitespace
Writing|4|False|N

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|1|False|CrrAsh !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|6|False|Kaïe !!
Monster|5|True|Kaïe !!
Piper|1|False|Noli solicitari, Carota...
Caienna|2|True|"Minacibus oculis potes vitare multa non necessaria fascinationes!...
Piper|7|False|Ego, maluissem discere nonnullas bonas fascinationes ad impugnandum; sed age...
Piper|8|True|Mecastor... jam nec scopas, nec res habemus.
Piper|9|False|Iter longum erit...
Caienna|3|False|...Frange eos. Domina eos !"

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cuminum|4|False|"“Esculentas plantas novisse? Est potiones contra famem paratas novisse."
Piper|6|False|Num me unam solam docuit?
Piper|2|True|Mihi quoque, a viribus deficior...
Piper|3|False|Multos per dies nihil edimus.
Carota|1|False|Groo
Piper|5|True|Sed veras potiones ?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymus|1|False|"Verae magae Chaosahis nec charta geographica nec pyxide magnetica opus est, si stellae lucent in caelo."
Piper|3|True|Macte virtute esto Carota!
Piper|4|False|Videsne? Hic est!
Piper|2|False|...ego maluissem veram scholam divinationis

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|7|False|- FINIS -
Credits|6|False|Nunc omnia novisti.
Narrator|4|False|... et mihi dicis te hic pervenisse quia illae nihil te docuerunt?
Credits|5|False|..ecce.
Piper|2|False|Licentia : Creative Commons Attribution 4.0, Instrumenta : Krita, Blender, G'MIC, Inkscape in Ubuntu
Sichimi|3|False|Fabula situa est in Herevae universo qui a David Revoy inventus est, Craig Maloney adiuvante. Correctiones: Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Piper|1|True|M. junii /A. MMXVI - www.peppercarrot.com - Designum & Fabula : David Revoy, Translatio: Benjamin Touati & Valentine Guillocheau

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Piper&Carota gratis constat, omnno apertum fontis est ac juvatur maecenatu lectorum . In hoc episodium, gratias ago illis 719 Maecenatibus :
Credits|2|False|Tu quoque, potes fieri maecenas Piperis&Carotae : www.patreon.com/davidrevoy
