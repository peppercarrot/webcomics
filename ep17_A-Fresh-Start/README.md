﻿# Episode 17: A Fresh Start

![cover of episode 17](https://www.peppercarrot.com/0_sources/ep17_A-Fresh-Start/low-res/Pepper-and-Carrot_by-David-Revoy_E17.jpg)

## Comments from the author

It's a longer episode than usual with 7 pages. The episode already has 16 translations ( at release time ) thanks to the super-translation-team. The production of all panels with this level of rendering was a real challenge. Wind, beam of light, a lot of different color palette... not easy to paint. I spent a lot of nights and days on polishing this one ;-) I'm even a little sad to leave this ep17 now: I had good moment painting the various scenes, expressions, places and characters of this story.

From [Author's blog of episode 17](https://www.davidrevoy.com/article577/episode-17-a-fresh-start/show#comments)
