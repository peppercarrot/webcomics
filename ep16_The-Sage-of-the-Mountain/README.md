﻿# Episode 16: The Sage of the Mountain

![cover of episode 16](https://www.peppercarrot.com/0_sources/ep16_The-Sage-of-the-Mountain/low-res/Pepper-and-Carrot_by-David-Revoy_E16.jpg)

## Comments from the author

In this new episode, I tried to produce a visual direction more looking like an animation ; with painted landscape and almost thin line cell-shaded characters. A challenge! The episode features Saffron, Thyme, Cayenne and of course Pepper and Carrot.

From [Author's blog of episode 16](https://www.davidrevoy.com/article569/episode-16-the-sage-of-the-mountain/show#comments)
