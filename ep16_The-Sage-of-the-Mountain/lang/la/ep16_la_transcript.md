# Transcript of Pepper&Carrot Episode 16 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium XVI : Sapiens Montis

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crocus|9|False|Minime, minime, minime, oh Piper.
Writing|1|True|Crocus
Writing|2|True|Magia
Writing|3|False|★★★
Writing|5|False|Lanius
Writing|6|False|15
Writing|4|False|13
Writing|7|False|via Stella
Writing|8|False|Tonsor
Sonus|10|True|Glou
Sonus|11|False|Glou

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crocus|2|False|Etiam si es amica mea, ego nolo mediatricem esse. istae res numquam succedunt
Crocus|1|True|Hoc faciendum est tibi soli.
Piper|3|False|Quaeso, Croce...
Piper|4|True|Nihil disco cum matrinis meis, adsunt tantum domi, semper querentes de veteribus ossibus earum, objurgantesque me simul ut quidquid faciam...
Crocus|6|True|Ecce, tu maga es, age sicut maga !
Crocus|8|False|Es magna puella mecastor!
Crocus|7|True|Dic eis directe !
Piper|5|False|... Nonne vero potes me adjuvare ad eis loquendum de isto?...
Crocus|9|False|Bene bene! Fortasse possum tecum venire eis locutum...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thymus|1|False|Quid? Nihil discis ?!
Thymus|12|False|Et non es tam fortis ut venias sola id dictum?!
Thymus|14|False|Si tergum non doleat, tibi dem bonam scholam! Et non solum de magia, si intellegis quid volo dicere!
Caienna|15|True|Thyme, desiste.
Caienna|16|False|Verae sunt.
Bird|2|True|pio
Bird|3|False|pio
Bird|4|True|pio
Bird|5|False|pio
Bird|6|True|pio
Bird|7|False|pio
Bird|8|True|pio
Bird|9|False|pio
Bird|10|True|pio
Bird|11|False|pio
Sonus|13|False|TAX !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Caienna|1|True|Nondum juvenes sumus et tempus est scholae dandae.
Caienna|2|True|Censeo ut occuramus
Thymus|5|True|He he !
Thymus|6|True|Ille Sapiens ?
Thymus|7|False|Nonne nimis iuvenes sunt? Fortasse non intellegent...
Piper|8|True|Nimis iuvenes ad quid faciendum?!
Piper|9|False|Nos eamus !
Crocus|10|False|"Nos" ?
Thymus|11|True|Ecce, ibi est.
Thymus|12|True|Consuetudinem iungite cum illo Sapiente quaeso.
Thymus|13|False|Manet sursum.
Thymus|14|True|Parataene ad accipiendam
Thymus|15|False|bonam scholam ?
Caienna|3|True|illum Sapientem montis
Caienna|4|False|Praecepta eius magnum momentum habent.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|False|Parata !
Crocus|2|False|Ego quoque!
Thymus|3|False|Euge, euge! Optimum animum!
Piper|9|False|OPPUGNEMUS !!!
Crocus|6|False|Est... est MONSTRUM !
Piper|7|True|Fallacia est!
Piper|8|True|ID SCIVI !!!
Piper|5|False|Eheu !
Thymus|4|False|Agete !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|1|False|𐌂𐌓𐌀𐌖𐌉𐌕𐌀𐌔 𐌔𐌐𐌉𐌓𐌀𐌋𐌉𐌔 !
Crocus|2|False|𐌁𐌓𐌀𐌔𐌄𐌓𐌏 𐌉𐌍𐌕𐌄𐌍𐌔𐌉𐌀 !
Sonus|3|False|Pluf !
Thymus|4|True|Ahh, vos juvenes, semper oppugnare vultis !
Piper|7|False|Quales sunt istae scholae ?!
Credits|11|False|Licentia : Creative Commons Attribution 4.0, Instrumenta : Krita, Blender, G'MIC, Inkscape in Ubuntu
Credits|10|False|Fabula situa est in Herevae universo qui a David Revoy inventus est, Craig Maloney adiuvante. Correctiones: Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Narrator|8|False|- FINIS -
Credits|9|False|M. Aprilis/A. MMXVI - www.peppercarrot.com - Designum & Fabula : David Revoy, Translatio: Benjamin Touati, Correctio: Valentine Guillocheau
Thymus|6|False|Quando res male se habent nihil melius est quam bonum balneum qui nervis rheumatismisque nostris bonus est!...
Thymus|5|True|Schola Sapientis est eum imitari !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Piper&Carota gratis constat, omnno apertum fontis est ac juvatur maecenatu lectorum ; in hoc episodium, gratias ago illis 671 Maecenatibus :
Credits|2|False|Tu quoque, potes fieri maecenas Piperis&Carotae : www.patreon.com/davidrevoy
