# Transcript of Pepper&Carrot Episode 16 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 16: Modrec na gori

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Žafranka|9|False|Ne, ne in še enkrat ne, Paprika.
Napis|2|True|Čarovništvo
Napis|3|False|Žafranka
Napis|1|False|★★★
Napis|5|False|MESAR
Napis|6|False|15
Napis|4|False|13
Napis|7|False|Ulica zvezd
Napis|8|False|Frizerstvo
Zvok|10|True|Ššš
Zvok|11|False|Ššš

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Žafranka|2|False|Nočem posredovati, niti za prijateljico. Takšne stvari se vedno slabo končajo.
Žafranka|1|True|To boš morala urediti kar sama.
Paprika|3|False|P-prosim, Žafranka …
Paprika|4|True|Moje botre me ničesar ne naučijo, samo po hiši se mi prestavljajo, nergajo čez svoje stare kosti in se derejo name, ne glede na to, kaj storim …
Žafranka|6|True|Saj si vendar čarovnica!
Žafranka|8|False|Naravnost jim povej, kot odrasla ženska!
Žafranka|7|True|Obnašaj se temu primerno!
Paprika|5|False|Kaj res ne moreš priti na pogovor z njimi in mi pomagati?
Žafranka|9|False|Že prav, no! Mogoče lahko grem s teboj na pogovor z njimi.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timijana|1|False|Kako to misliš, da se ničesar ne naučiš?!
Timijana|12|False|In te ni dovolj skupaj, da bi nam to sama povedala?!
Timijana|14|False|Če me ne bi dajal križ, bi ti napela pošteno lekcijo! Pa ne v copranju, da veš!
Kajenka|15|True|Timijana, pomirite se.
Kajenka|16|False|Prav imata …
Ptica|2|True|pi
Ptica|3|False|pi
Ptica|4|True|pi
Ptica|5|False|pi
Ptica|6|True|pi
Ptica|7|False|pi
Ptica|8|True|pi
Ptica|9|False|pi
Ptica|10|True|pi
Ptica|11|False|pi
Zvok|13|False|BAM!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenka|1|True|Nismo več tako mlade kot nekoč. Čas je za novo lekcijo.
Kajenka|2|True|Predlagam srečanje z
Timijana|5|True|He he!
Timijana|6|True|Modrec, praviš?
Timijana|7|False|Mar nista smrklji premladi, da bi razumeli njegov nauk?
Paprika|8|True|Premladi za nauk!?
Paprika|9|False|Medve že ne!
Žafranka|10|False|„Medve“?
Timijana|11|True|No, prispele smo.
Timijana|12|True|Sami se pojdita soočit z modrecem.
Timijana|13|False|Našli ga bosta na vrhu skalovja.
Timijana|14|True|Sta pripravljeni na
Timijana|15|False|pravo lekcijo?
Kajenka|3|True|modrecem na gori.
Kajenka|4|False|Dal nam bo pravi nasvet.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Pripravljena!
Žafranka|2|False|Jaz tudi!
Timijana|3|False|Imenitno! Korajža velja!
Paprika|9|False|V NAPAD!!!
Žafranka|6|False|PO… POŠAST!
Paprika|8|True|TO JE PAST!
Paprika|7|True|VEDELA SEM!!
Paprika|5|False|O, ne!
Timijana|4|False|Gremo!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|GRAVITAS SPIRALIS!
Žafranka|2|False|BRASERO INTENSIA!
Zvok|3|False|Štrbunk!
Timijana|4|True|Ta mladina. Samo spopadali bi se!
Paprika|7|False|Pa kaj so te nore lekcije?!
Zasluge|11|False|Licenca: Creative Commons Priznanje avtorstva 4.0 - Programska oprema: Krita, Blender, G'MIC, Inkscape na Ubuntuju
Zasluge|10|False|Dogaja se v vesolju Hereve avtorja Davida Revoyja s prispevki Craiga Maloneya. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Pripovedovalec|8|False|- KONEC -
Zasluge|9|False|April 2016 - www.peppercarrot.com - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Timijana|6|False|Kadar ti gre vse narobe, ni boljšega od vroče in sproščujoče kopeli! To pomaga pri najini revmi in vajinih živcih!
Timijana|5|True|Modrečeva lekcija tiči v njegovem oponašanju!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 671 bralcev:
Zasluge|2|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
