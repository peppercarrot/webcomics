# Transcript of Pepper&Carrot Episode 16 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 16: El Sabio de la Montaña

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Azafrán|9|False|¡No, no, no y no, Pimienta!
Escritura|2|True|Azafrán
Escritura|3|False|Brujería
Escritura|1|False|★★★
Escritura|5|False|Carnicería
Escritura|6|False|15
Escritura|4|False|13
Escritura|7|False|Calle Estrella
Escritura|8|False|Peluquería
Sonido|10|True|Glob
Sonido|11|False|Glob

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Azafrán|2|False|Yo nunca hago de mediadora, ni siquiera por una amiga. Siempre terminan mal estas historias.
Azafrán|1|True|Esto es un asunto que deberías resolver por ti misma.
Pimienta|3|False|Porfa, Azafrán...
Pimienta|4|True|No aprendo nada con mis madrinas, lo único que hacen es estar de okupas en mi casa, quejarse de que les duelen los huesos y gritarme sobre qué debo hacer...
Azafrán|6|True|Escucha, eres una bruja, ¡actúa como tal!
Azafrán|8|False|¡Como una mujer!
Azafrán|7|True|¡Díselo a la cara!
Pimienta|5|False|¿De verdad no puedes ayudarme a hablarlo con ellas?...
Azafrán|9|False|¡Vale, de acuerdo! Supongo que puedo ir contigo y hablarlo con ellas.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tomillo|1|False|¡¿Cómo que no aprendes nada?!
Tomillo|12|False|¡¿Y no tienes el valor siquiera para venir a decírnoslo tú sola?!
Tomillo|14|False|¡Si tuviera mejor la espalda, te daría una buena lección! ¡Y con lección no me refiero a un hechizo precisamente!
Cayena|15|True|Tomillo, cálmate.
Cayena|16|False|Tienen razón...
Ave|2|True|piou
Ave|3|False|piou
Ave|4|True|piou
Ave|5|False|piou
Ave|6|True|piou
Ave|7|False|piou
Ave|8|True|piou
Ave|9|False|piou
Ave|10|True|piou
Ave|11|False|piou
Sonido|13|False|¡PAM!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayena|1|True|Ya no somos tan jóvenes y es el momento ideal para una lección.
Cayena|2|True|Propongo un encuentro con
Tomillo|5|True|¡Jejeje!
Tomillo|6|True|¿El Sabio?
Tomillo|7|False|¿No serán todavía demasiado jóvenes para entenderlo?
Pimienta|8|True|¿¡Demasiado jóvenes para qué!?
Pimienta|9|False|¡Vamos a ir!
Azafrán|10|False|¿"Vamos"?
Tomillo|11|True|Bueno, aquí estamos,
Tomillo|12|True|os dejo para que conozcáis al Sabio.
Tomillo|13|False|Lo encontraréis en lo alto de esta pendiente.
Tomillo|14|True|¿Estáis listas para aprender
Tomillo|15|False|una verdadera lección?
Cayena|3|True|el Sabio de la montaña.
Cayena|4|False|Su enseñanza es esencial...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Preparada!
Azafrán|2|False|¡Yo también!
Tomillo|3|False|¡Bien, bien! ¡Esa es la actitud!
Pimienta|9|False|¡¡¡ AL ATAQUE !!!
Azafrán|6|False|¡Un... UN MONSTRUO!
Pimienta|8|True|¡¡ LO SABÍA !!
Pimienta|7|True|¡ES UNA TRAMPA!
Pimienta|5|False|¡Oh, no!
Tomillo|4|False|¡Vamos!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡GRAVITAS SPIRALIS!
Azafrán|2|False|¡BRASERO INTENSIA!
Sonido|3|False|¡Chof!
Tomillo|4|True|¡Ahh, estos jóvenes, siempre quieren atacar todo lo que ven!
Pimienta|7|False|¡Pero qué clase de lecciones son estas!
Créditos|11|False|Licencia: Creative Commons Attribution 4.0, Software: Krita, Blender, G'MIC, Inkscape en Ubuntu
Créditos|10|False|Basado en el universo de Hereva, creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Narrador|8|False|- FIN -
Créditos|9|False|Abril 2016 - www.peppercarrot.com - Dibujo & Guión: David Revoy - Traducción: TheFaico
Tomillo|6|False|¡Cuando nada sucede como quieres, nada mejor que un buen baño! ¡Es bueno para nuestros reumatismos y para vuestros nervios!...
Tomillo|5|True|¡La lección del Sabio consiste en imitarlo!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto, y patrocinado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 671 mecenas:
Créditos|2|False|Tú también puedes ser mecenas de Pepper&Carrot. www.patreon.com/davidrevoy
