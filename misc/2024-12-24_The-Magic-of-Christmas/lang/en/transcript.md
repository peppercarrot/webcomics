# The magic of Christmas

## Page 1

Panel 1: On a snowy night, probably in the middle of the night, a view of a chalet decorated for Christmas and covered in a thick layer of snow. An adult figure walks into the night, lit by a cold street lamp, while under the porch of the chalet, a warm light shines on a small figure wrapped in a thick blanket.

Panel 2: The little girl addresses the figure, now identified as a postman, sorting through his leather satchel to pull out a letter.

> Little girl: Hello.

> Little girl: Mail?

> Little girl: Today?

> Postman: Oh yes, progress never stops...

Panel 3: Portrait on the curious postman.

> Postman: But tell me, what are you doing outside your house in the middle of the night, little girl? And on Christmas?

> Postman: You're the one who should be in bed!

Panel 4: All embarrassed, the little girl grabs the letter with her mittens.

> Little girl: I'm waiting for Santa, have you seen him?

Panel 5: The postman closes his bag, taking care to put it back on his side.

> Postman: Nope, I haven't seen him around.

> Postman: Why are you so keen on finding him?

Panel 6: Close-up on the little girl, a bit downcast.

> Little girl: Well, I haven't been very good this year, so...

> Little girl: Well...

> Little girl: You see, I thought that if I spoke with him, I could maybe ask him to forgive me.

Panel 7: Close-up of the postman holding his chin to think.

> Postman: Hmm!


## Page 2

Panel 1: The postman is a bit amused and teasing, he leans towards the girl while readjusting his professional cap.

> Postman: And you don't think he's not going to come precisely because you've been naughty?

Panel 2: The little girl pauses, her eyes wide and her mouth agape as she realises her miscalculation.

Panel 3: The postman is sympathetic and starts thinking again.

> Postman: I see, you hadn't thought about that!

> Postman: Well, I suggest you tell me all about it, and if I see him on my delivery round, I'll repeat it all back to him.

Panel 4: The little girl suddenly stands up, her eyes full of hope, which surprises the postman.

> Little girl: You'd do that?!

Panel 5: The postman laughs at the girl's enthusiasm.

> Postman: Ha! Ha!

> Postier: Yes, you can trust me.

> Postman: Delivering messages is my job, after all.

Panel 6: The postman decides to sit next to the girl on the porch.

> Postman: So,

> Postman: tell me about it...


## Page 3

Panel 1: Flashback, the little girl is screaming and crying in her Daddy's arms: she is having a crisis in a pet shop to get a puppy. The lady in the shop is surprised by her behaviour.

> Little girl: Well, here it is: once, in a pet shop, I really wanted to adopt a puppy but my parents wouldn't let me.

Panel 2: Flashback, at the dinner table the little girl refuses to eat and her parents argue with her.

> Little girl: ‘You'll get one for Christmas if you're good,’ they told me. But it was too long a wait!

Panel 3: Flashback, the little girl is banging on a pan, screaming, while her parents are relaxing in front of the television in a dark living room. They are exasperated and cover their ears with their hands.

> Little girl: So I started bugging them every night...

Panel 4: Flashback, one morning in the golden light, the little girl is on the floor in her bedroom and has painted the walls with her hands. She smiles while her mother, who discovers the scene on the doorstep, is scandalised.

> Little girl: ... and did everything I could to be the opposite of a good girl.

Panel 5: Very close-up on the little girl's shining eyes, feeling sorry for herself.

> Little girl: But Christmas has finally come and and I really regret my behavior now...

Panel 6: the postman, a bit of a philosopher, explains.

> Postman: Well, if you ask me, you should tell your parents everything and ask them for forgiveness.

Panel 7: the little girl obviously hadn't thought of that; she wonders.

> Little girl: You think so?

Panel 8: The postman gets up and talks to the girl, who is delighted.

> Postman: Of course!

> Little girl: Then, that's what I'll do..

> Postman: Promise?

> Little girl: Promise!


## Page 4

Panel 1: Now standing tall, the postman is about to set off again.

> Postman: Alright! It's about time I get back to work...

> Sound from his bag: WOOF!

Panel 2: Amused, the postman takes a cute, happy little puppy out of his bag with his big mittens.

> Postman: Oh yes, this morning a colleague gave me this puppy...

> Postman: So I accepted, but I haven't got time to look after him with my work.

> Postman: I haven't even found a name for him yet!

Panel 3: He holds out the puppy to the little girl, the puppy and the little girl look at each other and it's the beginning of a great friendship.

> Postman: Would you like to become his new owner?

Panel 4: Overall view of a happy scene in the warm light of the porch. The postman is happy, the little girl is cuddling the puppy, who is licking her cheek in a friendly way. 

Panel 5: The postman walks away from the house, which can be seen in the distance, and turns round one last time to wave to the little girl. The little silhouette finally goes inside and greets him back.

> Postman: ... and Merry Christmas!

> Little girl: Merry Christmas, Sir!

Panel 6: Plot twist! Now well away from the house, with a snap of his fingers the postman turns back into Santa.

> title: THE END
