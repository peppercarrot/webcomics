# Die Magie von Weihnachten

## Seite 1

Panel 1: In einer verschneiten Nacht, wahrscheinlich mitten in der Nacht, ein Blick auf ein weihnachtlich geschmücktes Chalet, das mit einer dicken Schneeschicht bedeckt ist. Eine erwachsene Figur geht in die Nacht hinein, beleuchtet von einer kalten Straßenlaterne, während unter der Veranda des Chalets ein warmes Licht auf eine kleine, in eine dicke Decke gehüllte Figur scheint.

Panel 2: Das kleine Mädchen wendet sich an die Gestalt, die sich nun als Postbote zu erkennen gibt, der in seiner Ledertasche wühlt und einen Brief herauszieht.

> kleines Mädchen: Hallo.

> kleines Mädchen: Post?

> kleines Mädchen: Heute?

> Postbote: Ja, der Fortschitt hört nicht auf...

Panel 3: Portrait auf dem neugierigen Postboten.

> Postbote: Was tust du hier mitten in der Nacht und das an Weihnachten?

> Postbote: Du solltest im Bett liegen.

Panel 4: Vor lauter Verlegenheit greift das kleine Mädchen mit ihren Fäusten nach dem Brief.

> kleines Mädchen: Ich warte auf den Weihnachtsmann, haben Sie ihn gesehen?

Panel 5: Der Postbote schließt seine Tasche und legt sie wieder auf seine Seite.

> Postbote: Nein, den habe ich hier nicht gesehen.

> Postbote: Warum ist dir das so wichtig?

Panel 6: Nahaufnahme des kleinen Mädchens, etwas niedergeschlagen.

> kleines Mädchen: Nun, ich war dieses Jahr nicht sehr brav, also...

> kleines Mädchen: Also...

> kleines Mädchen: Sehen Sie, ich dachte, wenn ich mit ihm spreche, könnte ich ihn um Verzeihung bitten.

Panel 7: Nahaufnahme des Postboten, der sich nachdenklich an sein Kinn fasst.

> Postbote: Hmm!


## Seit 2

Panel 1: Der Postbote ist ein amüsiert und scherzhaft, er beugt sich zu dem Mädchen, während er seine Kappe zurechtrückt.

> Postbote: Und du denkst, dass er nicht kommt, weil du nicht brav warst?

Panel 2: Das kleine Mädchen hält inne, die Augen weit aufgerissen und den Mund offen, als es seinen Rechenfehler bemerkt.

Panel 3: Der Postbote ist verständnisvoll und beginnt wieder zu denken.

> Postbote: Oh, darüber hast du nicht nachgedacht..

> Postbote: Ok, dann schlage ich vor, dass du mir alles erzählst und ich schaue, ob ich ihn auf meiner Route sehe..

Panel 4: Das kleine Mädchen steht plötzlich auf, ihre Augen sind voller Hoffnung, was den Postboten überrascht.

> kleines Mädchen: Das würden Sie tun?

Panel 5: Der Postbote lacht über die Begeisterung des Mädchens.

> Postbote: Ha! Ha!

> Postier: Ja, du kannst mir vertrauen!

> Postbote: Nachrichten zu überbringen ist schlließlich meiin Job!

Panel 6: Der Postbote beschließt, sich neben das Mädchen auf die Veranda zu setzen.

> Postbote: Also...

> Postbote: erzähl mir alles...


## Seite 3

Panel 1: Rückblende: Das kleine Mädchen liegt schreiend und weinend in den Armen ihres Vaters: Sie möchte einen Welpen bekommen. Die Verkäuferin ist von ihrem Verhalten überrascht.

> kleines Mädchen: Also: Einmal waren wir in der Tierhandlung. Ich wollte ein Hündchen adoptieren, aber meine Eltern ließen mich nicht...

Panel 2: Rückblende: Am Abendbrottisch weigert sich das kleine Mädchen zu essen und ihre Eltern streiten mit ihr.

> kleines Mädchen: "Du bekommst einen zu Weihnachten, wenn du brav bist.", sagten sie mir. Aber das dauerte zu lange.

Panel 3: Rückblende: Das kleine Mädchen schlägt schreiend auf eine Pfanne, während sich die Eltern in einem dunklen Wohnzimmer vor dem Fernseher entspannen. Sie sind verärgert und halten sich die Ohren mit den Händen zu.

> kleines Mädchen: Also fing ich an sie jeden Abend zu nerven...

Panel 4: Rückblende: Eines Morgens, im goldenen Licht, liegt das kleine Mädchen in ihrem Zimmer auf dem Boden und hat die Wände mit ihren Händen bemalt. Sie lächelt, während ihre Mutter, die die Szene auf der Türschwelle entdeckt, empört ist.

> kleines Mädchen: ...und tat alles, um das Gegenteil von einem braven Mädchen zu sein.

Panel 5: Großaufnahme der leuchtenden Augen des kleinen Mädchens, das sich selbst bemitleidet.

> kleines Mädchen: Aber Weihnachten ist nun endlich da und mir tut mein Verhalten nun sehr leid...

Panel 6: der Postbote, der ein bisschen ein Philosoph ist, erklärt

> Postbote: Meiner Meinung nach solltest du das deinen Eltern erzählen und sie um Verzeihung bitten.

Panel 7: Daran hat das kleine Mädchen offensichtlich nicht gedacht; sie wundert sich.

> kleines Mädchen: Sind Sie sicher?

Panel 8: Der Postbote steht auf und spricht mit dem Mädchen, das sich sehr darüber freut.

> Postbote: Na, klar!

> kleines Mädchen: Dann mache ich das!

> Postbote: Verspochen?

> kleines Mädchen: Verspochen!


## Seite 4

Panel 1: Der Postbote steht nun aufrecht und macht sich wieder auf den Weg.

> Postbote: Ok, es wird Zeit für mich weiterzuarbeiten...

> Geräusch aus seiner Tasche: WUFF!

Panel 2: Amüsiert holt der Postbote mit seinen großen Handschuhen ein süßes, fröhliches Hündchen aus seiner Tasche.

> Postbote: Ach ja, heute Morgen gab mir ein Kollege dieses Hündchen..

> Postbote: Ich habe es angenommen, aber keine Zeit mich darum zu kümmern.

> Postbote: Er hat noch keinen Namen.

Panel 3: Er hält dem kleinen Mädchen den Welpen hin, der Welpe und das kleine Mädchen schauen sich an und es ist der Beginn einer großen Freundschaft.

> Postbote: Möchtest du sein neues Frauchen sein?

Panel 4: Gesamtansicht einer fröhlichen Szene im warmen Licht der Veranda. Der Postbote ist glücklich, das kleine Mädchen kuschelt mit dem Welpen, der ihr freundlich die Wange leckt. 

Panel 5: Der Postbote entfernt sich von dem Haus, das in der Ferne zu sehen ist, und dreht sich ein letztes Mal um, um dem kleinen Mädchen zuzuwinken. Die kleine Silhouette geht schließlich hinein und grüßt ihn zurück.

> Postbote: ... und Frohe Weihnachten!

> kleines Mädchen: Frohe Weihnachten!

Panel 6: Plottwist! Der Postbote ist nun weit weg vom Haus und verwandelt sich mit einem Fingerschnippen wieder in den Weihnachtsmann.

> title: ENDE
