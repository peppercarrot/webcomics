# La magie de Noël

## Page 1

Case 1 : Dans une nuit neigeuse, probablement au milieu de la nuit, vue sur un chalet décoré pour noël et recouvert d'une épaisse couche de neige. Une silhouette adulte s'avance dans la nuit, éclairée par un réverbère à la lumière froide, tandis que sous le porche du chalet, une lumière chaude éclaire une petite silhouette emmailloté dans une épaisse couverture.

Case 2 : La fillette s'adresse à la silhouette, maintenant identifié comme un postier, triant dans sa sacoche en cuir pour y sortir une lettre.

> Filette : Bonjour.

> Filette : Du courrier ?

> Filette : Aujourd'hui ?

> Postier : Et oui, on n'arrête pas le progrès...

Case 3 : Portrait sur le postier, curieux.

> Postier : Mais dis donc, qu'est-ce que tu fais devant chez toi en pleine nuit, ma petite ? Et à Noël ?

> Postier : C'est surtout toi qui devrais être au lit !

Case 4 : Toute peneaude, la filette se saisi de la lettre avec ses moufles.

> Fillette : J'attends le père Noël, vous l'avez vu ?

Case 5 : Le postier referme sa sacoche en prenant soin de la remettre sur son flanc.

> Postier : Ha non, ça, je l'ai pas vu dans le coin.

> Postier : Pourquoi tu tiens autant à le voir ?

Case 6 : Gros plan sur la filette, un peu abattue.

> Filette : Eh bien, je n'ai pas été très sage cette année, alors...

> Filette : Enfin...

> Filette : Vous voyez, j'me suis dit que si je lui parlais, je pourrais sans doute lui demander pardon.

Case 7 : Gros plan sur le postier se tenant le menton pour réfléchir.

> Postier : Hmm !

## Page 2

Case 1 : Le postier est un peu amusé et taquin, il se penche vers la filette en se réajustant sa casquette professionnelle.

> Postier : Et tu ne crois pas que justement, il ne viendra pas, car tu n'as pas été sage ?

Case 2 : La fillette marque un temps d'arrêt, les yeux écarquillés, bouche bée, elle se rend compte de son erreur de calcul.

Case 3 : Le postier compatit et se remet à réfléchir, la fillette est de nouveau abattue.

> Postier : Je vois, tu n'y avais pas pensé !

> Postier : Eh bien je te propose de tout me raconter, et si je le vois dans ma tournée, je lui répéterai tout.

Case 4 : La fillette se relève brusquement, les yeux remplis d'espoir, se qui surprend le postier.

> Fillette : Vous feriez ça ?!

Case 5 : L'enthousiasme de la fillette fait rire le postier.

> Postier : Ha ! Ha !

> Postier : Oui, tu peux me faire confiance.

> Postier : Délivrer des messages, c'est mon travail, après tout.

Case 6 : Le postier décide de s'assoir à côté de la fillette sur le porche.

> Postier : Allez,

>  Postier : raconte-moi tout...

## Page 3

Case 1 : Flashback, la fillette hurle en pleurant dans les bras de son Papa : elle fait une crise dans une animalerie pour avoir un chiot. La vendeuse est surprise par son comportement.

> Fillette : Eh bien voilà : une fois, dans une animalerie, j'ai vraiment voulu qu'on adopte un chiot mais mes parents n'étaient pas d'accord.

Case 2 : Flashback, à table la fillette refuse de manger et ses parents la disputent.

> Fillette : "Tu en auras un à Noël si tu es sage", m'ont-ils dit. Mais ça faisait vraiment trop longtemps à attendre !

Case 3 : Flashback, la fillette tape sur une casserole en hurlant alors que ses parents se détendent devant la télévision dans un salon sombre. Ils sont exaspérés et se bouchent les oreilles avec les mains.

> Fillette : Alors je me suis mise à leur casser les pieds tous les soirs...

Case 4 : Flashback, un matin à la lumière dorée, la filette est au sol dans sa chambre et à mis de la peinture sur les murs avec ses mains. Elle sourie tandis que sa mère qui découvre la scène sur le pas de la porte est scandalisée.

> Fillette : ... et faire tout mon possible pour être l'inverse d'une petite fille sage.

Case 5 : Très gros plan sur les yeux brillants de la fillette, se sentant désolée.

> Fillette : Cependant, Noël est finalement arrivé et je regrette beaucoup mon attitude à présent...

Case 6 : le postier, un peu philosophe, argumente.

> Postier : Eh bien, à mon avis, c'est surtout à tes parents que tu devrais raconter tout ça et demander pardon.

Case 7 : la fillette n'avait visiblement pas envisagé ça ; elle est se questionne.

> Fillette : Vous croyez ?

Case 8 : Le postier se relève tout en dialoguant avec la fillette, qui est ravie.

> Postier : Bien sûr !

> Fillette : Alors oui, c'est ce que je ferai.

> Postier : Promis ?

> Fillette : Promis !


## Page 4

Case 1 : À présent debout, avec fière allure le postier est sur le point de reprendre sa route.

> Postier : Bien ! Il est grand temps pour moi que je retourne au trav...

> Son venant de sa besace : OUAF !

Case 2 : Amusé, le postier sort de sa besace avec ses grosses moufles un petit chiot tout mignon et heureux.

> Postier : Ha oui, ce matin un collègue m'a donné ce chiot...

> Postier : Alors j'ai accepté, mais je n'ai pas le temps de m'en occuper avec mon travail.

> Postier : Je ne lui ai même pas encore trouvé un nom !...

Case 3 : Il tend le chiot vers la fillette, le chiot et la fillette se regardent et c'est le regard d'une grande amitié qui commence là.

> Postier : Ça te dirait d'en devenir la maîtresse ?

Case 4 : Vue d'ensemble sur une scène heureuse sous la lumière chaleureuse du porche. Le postier est heureux, la fillette caline le chiot, celui ci lui lèche la joue amicalement. 

Case 5 : Le postier distance la maison qu'on voir au loin et se retourne une dernière fois pour saluer la filette de la main. Celles ci, en petite silhouette rentre enfin à l'intérieur et le salue en retour.

> Postier : ... et Joyeux Noël !

> Filette : Joyeux Noël Monsieur !

Case 6 : Plot twist ! À présent bien éloigné de la maison, d'un claquement de doigts le postier se retransforme en Père Noël.

> titre : FIN


