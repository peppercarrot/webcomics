# Julemagi

## Side 1

Rute 1: Eit snødekt landskap seint om kvelden. I framgrunnen ser me eit lite hus pynta til jul og med snø på taket. Ein vaksen person kjem gåande, så vidt opplyst av eit gatelys. På trammen utanfor det vesle huset sit ei lita jente inntulla i eit tjukt pledd.

Rute 2: Jenta helsar på den vaksne, som me no ser er ein postmann. Han blar gjennom postveska si og hentar ut eit brev.

> Lita jente: Hei sann!

> Lita jente: Post?

> Lita jente: På kvelden?

> Postmann: Ja, posten må jo fram …

Rute 3: Nærbilete av postmannen, som interessert spør:

> Postmann: Men sei meg, vesle jente: Kva gjer du her ute så seint? Og på sjølvaste julaftan?

> Postmann: Du burde vel vore i seng no?

Rute 4: Jenta vert litt brydd, men tek imot brevet med vottar på hendene.

> Lita jente: Eg ventar på julenissen. Har du sett han?

Rute 5: Postmannen lukkar postveska.

> Postmann: Nei, ikkje i dag.

> Postmann: Er det nokon spesiell grunn til at du er så ivrig etter å treffa han?

Rute 6: Nærbilete av den vesle jenta, som verkar litt lei seg.

> Lita jente: Vel, eg har ikkje vore heilt snill i år, så …

> Lita jente: Vel …

> Lita jente: … eg tenkte at om eg fekk snakka med han, kunne eg kanskje be han om tilgjeving.

Rute 7: Nærbilete av postmannen som held handa opp til haka for å tenkja.

> Postmann: Hm!


## Side 2

Rute 1: Postmannen lenar seg mot jenta, rettar på hatten og seier litt ertande:

> Postmann: Men kanskje han ikkje kjem i kveld, sidan du har vore slem?

Rute 2: Jenta stoppar opp, med vidopne auge og open munn. Ho innser at ho har tabba seg ut.

Rute 3: Postmannen ser venleg på jenta og grunnar litt.

> Postmann: Hm, det hadde du visst ikkje tenkt på!

> Postmann: Kan du ikkje fortelja meg alt? Viss eg møter julenissen på postrunden min, skal eg fortelja det vidare til han.

Rute 4: Jenta reiser seg med håp i auga. Postmannen vert litt overraska.

> Lita jente: Vil du gjera det for meg?!

Rute 5: Postmannen ler høgt over entusiasmen til jenta.

> Postmann: Ha-ha!

> Postier: Ja, meg kan du stola på!

> Postmann: Å levera meldingar er trass alt jobben min.

Rute 6: Postmannen set seg ned ved sida av jenta på trammen.

> Postmann: Så,

> Postmann: fortel det heile, no …


## Side 3

Rute 1: Tilbakeblikk frå ein dyrebutikk. Den vesle jenta skrik og ber seg i armane til faren. Ho ser desperat mot ein hundekvalp. Ekspeditrisa verkar forskrekka over korleis jenta oppfører seg.

> Lita jente: Jo, då me var i dyrebutikken, fekk eg så lyst på ein kvalp. Men mamma og pappa sa nei.

Rute 2: Tilbakeblikk til ein middag med den vesle jenta og foreldra. Jenta ser furten ut og nektar å eta opp maten. Foreldra kjeftar på henne.

> Lita jente: «Men om du er snill i år, så kan du kanskje få ein til jul», sa dei. Men det var altfor lenge å venta!

Rute 3: Tilbakeblikk til ei stove om kvelden. Jenta hamrar på ein kasserolle og ropar, medan foreldra prøver å sjå på fjernsyn. Dei er tydeleg fortvila og held hendene over øyra.

> Lita jente: Så eg masa på dei dag og natt …

Rute 4: Tilbakeblikk til ein morgon på soverommet til den vesle jenta. Sollyset strøymer inn. Jenta sit på golvet og har måla på veggen med hendene. Ho smiler, men mor hennar i dørkarmen ser forferda ut.

> Lita jente: … og gjorde det eg kunne for å vera alt anna enn snill jente.

Rute 5: Ultranærbilete av dei tindrande auga til jenta. Ho ser lei seg ut.

> Lita jente: Men no er det jul, og eg er så lei meg for korleis eg oppførte meg …

Rute 6: Postmannen, ein tenksam kar, forklarar:

> Postmann: Slik eg ser det, er det foreldra dine du bør snakka med om dette – og be dei tilgje deg.

Rute 7: Jenta hadde tydelegvis ikkje tenkt på dette. Ho undrar seg:

> Lita jente: Synest du det?

Rute 8: Postmannen reiser seg og snakkar til jenta, som ser oppglødd ut.

> Postmann: Klårt!

> Lita jente: Ja, då gjer eg det!

> Postmann: Lovar du det?

> Lita jente: Eg lovar!


## Side 4

Rute 1: Postmannen har reist seg og er klar til å gå.

> Postmann: Greitt! Men no får eg koma meg tilbake til arbeidet …

> Lyd frå postveska hans: Voff!

Rute 2: Postmannen smiler og løftar ut ein liten hundekvalp av postveska med dei store vottane sine.

> Postmann: Jo, forresten, i dag fekk eg denne hundekvalpen av ein som eg jobbar med …

> Postmann: Eg tok imot, men har eigentleg ikkje tid til å ta meg av han med alt arbeidet mitt.

> Postmann: Eg har ikkje kome på noko namn til han enno!

Rute 3: Han overrekkjer kvalpen til jenta. Jenta og kvalpen ser på og smiler mot kvarandre. Det verkar som starten på eit vakkert venskap.

> Postmann: Kanskje du har lyst til å overta han?

Rute 4: Oversiktsbilete med varmt lys over trammen. Postmannen smiler, og den vesle jenta kosar med kvalpen, som slikkar henne i ansiktet.

Rute 5: Postmannen på veg vekk frå huset. Han snur seg ein siste gong for å vinka til jenta. Ho har opna døra for å gå inn i huset, og helsar tilbake til postmannen.

> Postmann: … og god jul!

> Lita jente: God jul til deg òg!

Rute 6: Overrasking! Postmannen er no langt frå huset. Han knipsar med fingrane, og vips så er han forvandla til julenissen!

> Tittel: SLUTT
