#!/bin/bash

# User configuration:
export defaultLang="en"
export projectName="Heritage en Couleur"
export creditSuffix="_Heritage-en-Couleur_by-David-Revoy"
export defaultAttribution="David Revoy <https://framagit.org/Deevad>"
# An absolute path to a directory (ending by /) to save copyt of Krita and SVG files at each rendering.
# (eg. here the path of my external "Virgo" disk)
export backupPath="/media/deevad/virgo/roaming/"
# Export:
export lowResSize="1140x" # width in px for the resulting JPG in low-res directory
export lowResPpi="96" # in ppi for the JPG outputs Classic values: 72
export hiResPpi="180" # in ppi for your Krita files in order to get real size in mm
export multipage=1
