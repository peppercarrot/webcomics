## L'héritage en couleur

Work in progress: no transcript available yet.

If you want to contribute writing it, the source file should be in a `lang/en/transcript.md` [somewhere down here](https://framagit.org/peppercarrot/webcomics/-/tree/master/misc?ref_type=heads). Thank you.
