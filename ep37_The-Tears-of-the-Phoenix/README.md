# Episode 37: The Tears of the Phoenix

![cover of episode 37](https://www.peppercarrot.com/0_sources/ep37_The-Tears-of-the-Phoenix/low-res/Pepper-and-Carrot_by-David-Revoy_E37.jpg)

## Comments from the author

This episode introduces a giant Phoenix and also sets what it costs to use the super powerful Rea of Dragon.

I knew when I wrote last episode I'll have to setup later a strong counterpart sideeffect to protect the Lore. Arra the dragon tells Pepper it is taboo, "forbidden", "at her own risk". I still haven't decided if it should also affect Arra. I'll see what I can do within the very restricted page count I have per episodes.

From [Author's blog of episode 37](https://www.davidrevoy.com/article920/episode-37-the-tears-of-the-phoenix/show#comments)
