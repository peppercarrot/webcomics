# Transcript of Pepper&Carrot Episode 37 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 37: Die Tränen des Phönix

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Puuh...
Pepper|2|False|Endlich angekommen!
Pepper|3|False|Oh, es ist Markttag.
Pepper|4|False|Wir sollten uns etwas zu essen schnappen, bevor wir den Vulkan ersteigen.
Pepper|5|False|Ich wusste, dass dir das gefallen würde!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Schrift|1|False|GESUCHT
Schrift|2|False|Torreya
Schrift|3|False|100 000Ko
Schrift|4|False|Shichimi
Schrift|5|False|250 000Ko
Schrift|6|False|Pepper
Schrift|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Die haben das wirklich überall aufgehangen.
Pepper|2|False|Selbst an den entferntesten Orten.
Pepper|3|False|Komm, lass uns abhauen, bevor wir erkannt werden.
Pepper|4|False|Wir haben gerade größere Probleme...
Pepper|5|False|Dieses Licht...
Pepper|6|False|Wir müssen in der Nähe des Nests sein.
Pepper|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phönix|1|False|Warum störst du mich, Mensch?
Pepper|2|False|Sei gegrüßt, oh großer Phönix!
Pepper|3|False|Mein Name ist Pepper, ich bin eine Hexe von Chaosāh.
Pepper|4|False|Ich habe kürzlich eine hohe Dosis Drachen-Rea erhalten. Seitdem ist das hier aufgetaucht und wächst jeden Tag mehr...
Pepper|5|False|Es hebt alle meine Kräfte auf und könnte mich auf Dauer töten.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phönix|1|True|Hmm...
Phönix|2|False|Verstehe...
Phönix|3|False|...und nun bist du hier, um dich mit Phönixtränen zu heilen, stimmt's?
Pepper|4|False|Ja, es ist meine einzige Wahl.
Phönix|5|False|*seufz*
Phönix|6|False|...nun, worauf wartest du?
Phönix|7|False|Versuch mich zum Weinen zu bringen.
Phönix|8|False|Ich gebe dir eine Minute!
Pepper|9|False|Was?!
Pepper|10|False|Dich zum Weinen bringen?!
Pepper|11|False|Aber ich wusste nicht, dass...
Pepper|12|False|Und zwar... in nur einer Minute?!
Pepper|13|True|Ähm...
Pepper|14|True|OK!
Pepper|15|False|Mal sehen.
Pepper|16|True|Hmm... Stell dir Hungersnöte vor.
Pepper|17|True|Äh, wartewartewarte!
Pepper|18|True|Ich weiß was besseres:
Pepper|19|False|Denk an die, die von dir gegangen sind.
Pepper|20|True|Immer noch nicht?
Pepper|21|True|Ausgesetzte Haustiere?!
Pepper|22|False|Ausgesetzte Tiere sind so traurig...
Phönix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|So, was meinst du?
Pepper|2|False|Bringt dich das nicht zum Weinen?
Phönix|3|False|DEIN ERNST?!
Phönix|4|False|IST DAS ETWA ALLES?!
Phönix|5|True|Die da haben es wenigstens mit Poesie versucht!
Phönix|6|True|Tragödien geschrieben!
Phönix|7|True|KUNST!
Phönix|8|False|DRAMA!
Phönix|9|True|ABER DU?!
Phönix|10|False|DU KAMST UNVORBEREITET!
Phönix|11|True|JAWOHL, SO IST'S RECHT; HUSCH-HUSCH!
Phönix|12|False|GEH NACH HAUSE UND KOMM ZURÜCK, WENN DU BEREIT BIST!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grr! Komm schon, Pepper! Denk dir etwas Trauriges aus.
Pepper|2|True|Du schaffst das.
Pepper|3|False|Du schaffst das.
Pepper|4|True|Nein...
Pepper|5|False|Du schaffst es nicht.
Verkäufer|6|False|Hey! Oh! Geh weg!
Pepper|7|False|!!
Verkäufer|8|False|Lass die Pfoten von meinem Zeug, wenn du nicht bezahlen kannst!
Pepper|9|True|Oh nein, bloß nicht...
Pepper|10|True|CARROT!
Pepper|11|False|Du könntest mir helfen, statt an deinen Magen zu denken!
Pepper|12|False|Oh?!
Pepper|13|True|Oh, ja...
Pepper|14|False|Das könnte klappen.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phönix|1|True|Ah! Du bist wieder da.
Phönix|2|False|So schnell...
Phönix|3|False|Eine Waffe aus Metall?!
Phönix|4|False|Ernsthaft?!
Phönix|5|False|Weißt du denn nicht, dass ich jedes Metall schmelzen kann und...
Geräusch|6|False|Plopp
Geräusch|7|False|Plopp
Geräusch|8|False|Plopp
Phönix|9|True|OH NEIN!
Phönix|10|False|BLOSS DAS NICHT!
Phönix|11|False|DAS IST NICHT FAIR!
Pepper|12|True|Schnell, Carrot!
Pepper|13|False|Fang so viele Tränen auf, wie du kannst!
Geräusch|14|False|Hack!
Geräusch|15|False|Hack!
Geräusch|16|False|Hack!
Titel|17|False|- ENDE -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|3. August 2022 Illustration & Handlung: David Revoy. Beta-Leser: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Deutsche Übersetzung: Ret Samys. Basierend auf der Hereva-Welt Erstellung: David Revoy. Hauptbetreuer: Craig Maloney. Redakteure: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Korrektur: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 auf Fedora 36 KDE Spin. Lizenz: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Wusstest du schon?
Pepper|3|False|Pepper&Carrot ist vollständig frei(libre), Open Source und finanziert durch Spenden von Lesern.
Pepper|4|False|Für diese Episode danken wir 1058 Gönnern!
Pepper|5|False|Du kannst auch Gönner von Pepper& Carrot werden und deinen Namen hier lesen!
Pepper|6|False|Wir sind auf Patreon, Tipeee, PayPal, Liberapay ...und mehr!
Pepper|7|False|Geh auf www.peppercarrot.com für mehr Informationen!
Pepper|8|False|Dankeschön!
