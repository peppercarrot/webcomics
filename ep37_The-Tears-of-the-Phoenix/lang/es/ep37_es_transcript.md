# Transcript of Pepper&Carrot Episode 37 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 37: Las lágrimas del Fénix

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|Uf...
Pimienta|2|False|¡Qué bien estar ya aquí!
Pimienta|3|False|Oh, es día de mercadillo.
Pimienta|4|False|Busquemos algo de comer antes de subir al volcán.
Pimienta|5|False|¡Sabía que te gustaría mi idea!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|1|False|SE BUSCAN
Escritura|2|False|Torreya
Escritura|3|False|100 000Ko
Escritura|4|False|Shichimi
Escritura|5|False|250 000Ko
Escritura|6|False|Pimienta
Escritura|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|Han puesto carteles por todos lados.
Pimienta|2|False|Hasta en la esquina más remota.
Pimienta|3|False|Venga, vámonos de aquí antes de que nos reconozcan.
Pimienta|4|False|Tenemos problemas más importantes ahora mismo...
Pimienta|5|False|Esa luz...
Pimienta|6|False|Tenemos que estar cerca de su nido.
Pimienta|7|False|¡Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fénix|1|False|¿Por qué me molestas. humana?
Pimienta|2|False|¡Saludos, Oh gran Fénix!
Pimienta|3|False|Me llamo Pimienta, y soy una bruja de Chaosah.
Pimienta|4|False|Hace poco recibí una gran dosis de Rea de Dragón y, desde entonces, tengo esta marca que va creciendo cada día...
Pimienta|5|False|Ha neutralizado mis poderes y puede que termine por matarme.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fénix|1|True|Hmm...
Fénix|2|False|Ya veo...
Fénix|3|False|...y ahora estás aquí porque quieres las lágrimas de un Fénix para curarte, ¿verdad?
Pimienta|4|False|Sí, es mi única opción.
Fénix|5|False|*suspira*
Fénix|6|False|...bueno, ¿A qué estás esperando?
Fénix|7|False|Intenta hacerme llorar.
Fénix|8|False|¡Te daré un minuto!
Pimienta|9|False|¡¿Qué?!
Pimienta|10|False|¡¿Hacerte llorar?!
Pimienta|11|False|Pero no sabía nada de eso...
Pimienta|12|False|Osea... ¡¿Solo un minuto?!
Pimienta|13|True|Uh...
Pimienta|14|True|¡OK!
Pimienta|15|False|Veamos.
Pimienta|16|True|Hmm... Piensa en el hambre mundial.
Pimienta|17|True|¡Err, esperaespera!
Pimienta|18|True|Tengo algo mejor:
Pimienta|19|False|Piensa en los que ya no están.
Pimienta|20|True|¿Todavía nada?
Pimienta|21|True|¡¿Mascotas abandonadas?!
Pimienta|22|False|El abandono es tan triste...
Fénix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|True|¿Y bien?
Pimienta|2|False|¿No te hace llorar?
Fénix|3|False|¡¿ESTÁS DE BROMA?!
Fénix|4|False|¡¿ES TODO LO QUE TIENES?!
Fénix|5|True|¡Ellos al menos lo intentaron con poesía!
Fénix|6|True|¡Escribiendo tragedias!
Fénix|7|True|¡ARTE!
Fénix|8|False|¡DRAMA!
Fénix|9|True|¡¿PERO TÚ?!
Fénix|10|False|¡NO TE HAS PREPARADO NADA!
Fénix|11|True|LARGO, ESO ES; ¡FUS!
Fénix|12|False|¡VETE A CASA Y VUELVE CUANDO ESTÉS PREPARADA!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|1|False|¡Grr! ¡Vamos, Pimienta! Encuentra algo triste.
Pimienta|2|True|Puedes hacerlo.
Pimienta|3|False|Puedes hacerlo.
Pimienta|4|True|No...
Pimienta|5|False|No puedes.
Vendedor|6|False|¡Ey! Oh! ¡Vete!
Pimienta|7|False|¡!
Vendedor|8|False|¡Quita tus patas de mis cosas si no lo puedes pagar!
Pimienta|9|True|Oh no...
Pimienta|10|True|¡ZANAHORIA!
Pimienta|11|False|¡Podrías ayudarme en vez de pensar en tu barriga!
Pimienta|12|False|¡¿Oh?!
Pimienta|13|True|Oh sí...
Pimienta|14|False|Podría funcionar.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fénix|1|True|¡Ah! Has vuelto.
Fénix|2|False|Que pronto...
Fénix|3|False|¿¡ Un arma de metal?!
Fénix|4|False|¡¿En serio?!
Fénix|5|False|No sabes que puedo derretir cualquier metal y...
Sonido|6|False|Plop
Sonido|7|False|Plop
Sonido|8|False|Plop
Fénix|9|True|¡OH NO!
Fénix|10|False|¡ESO NO!
Fénix|11|False|¡NO ES JUSTO!
Pimienta|12|True|¡Rápido Zanahoria!
Pimienta|13|False|¡Coge tantas lágrimas como puedas!
Sonido|14|False|¡Chop!
Sonido|15|False|¡Chop!
Sonido|16|False|¡Chop!
Título|17|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|2022-08-03 Dibujo y guion: David Revoy. Lectores de la versión beta: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Versión en castellano Traducción: Raúl G Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Redactores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 5.0.5, Inkscape 1.2 en Fedora 36 KDE Spin. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
Pimienta|2|False|¿Sabías qué?
Pimienta|3|False|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pimienta|4|False|Este episodio ha recibido el apoyo de 1058 mecenas!
Pimienta|5|False|Tú también puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pimienta|6|False|Estamos en Patreon, Tipeee, PayPal, Liberapay ...¡y más sitios!
Pimienta|7|False|¡Visítanos en www.peppercarrot.com para más información!
Pimienta|8|False|¡Gracias!
