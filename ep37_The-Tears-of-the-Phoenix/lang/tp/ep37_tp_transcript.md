# Transcript of Pepper&Carrot Episode 37 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka luka luka tu: telo tan oko pi waso seli

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|aaaa...
jan Pepa|2|False|mi kama lon ma pona!
jan Pepa|3|False|a ni li tenpo suno esun.
jan Pepa|4|False|mi wile tawa sewi pi nena seli la mi o esun e moku.
jan Pepa|5|False|ni li pona tawa sina. mi sona pona a a a!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen|1|False|O ALASA E ONA
sitelen|2|False|jan Toleja
sitelen|3|False|100 000Ko
sitelen|4|False|jan Sisimi
sitelen|5|False|250 000Ko
sitelen|6|False|jan Pepa
sitelen|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|jan ike li pana e lipu alasa lon ma ale a.
jan Pepa|2|False|lon ma weka ni kin.
jan Pepa|3|False|mi o awen tawa. ante la jan alasa li ken sona e mi.
jan Pepa|4|False|ike ante li lon li suli tawa mi...
jan Pepa|5|False|suno ni a...
jan Pepa|6|False|pilin mi la mi lon poka pi tomo ona.
jan Pepa|7|False|sona pona!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
waso seli|1|False|jan o! sina tawa mi tan seme a?
jan Pepa|2|False|toki, waso seli suli o!
jan Pepa|3|False|mi jan Pepa pi kulupu Pakalaa.
jan Pepa|4|False|tenpo pini lili la mi kama jo e wawa Lonaa mute tan akesi wawa. tan tenpo ni la ijo nasa suno ni li kama lon sijelo mi li awen kama suli...
jan Pepa|5|False|ona li anpa e wawa ale mi li ken moli e mi lon tenpo.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
waso seli|1|True|a...
waso seli|2|False|mi sona...
waso seli|3|False|...ni la sina wile pona e sijelo sina kepeken telo oko pi waso seli, anu seme?
jan Pepa|4|False|wile. ni taso li ken.
waso seli|5|False|*Pilin*
waso seli|6|False|...ni la o pali
waso seli|7|False|o lukin kama e telo tan oko mi.
waso seli|8|False|o kepeken tenpo sama tenpo moku pi kili wan!
jan Pepa|9|False|seme?!
jan Pepa|10|False|mi o kama e telo a?!
jan Pepa|11|False|taso tenpo pini la mi sona ala e ni...
jan Pepa|12|False|kepeken... tenpo lili anu seme?!
jan Pepa|13|True|a...
jan Pepa|14|True|MI PALI!
jan Pepa|15|False|seme li pona.
jan Pepa|16|True|aaa... o kute: ma ale la jan li ken ala moku sama wile ona.
jan Pepa|17|True|a, o awen o awen o awen!
jan Pepa|18|True|ijo ante li wawa:
jan Pepa|19|False|waso olin sina li moli.
jan Pepa|20|True|a, ni kin li pona ala
jan Pepa|21|True|soweli suwi li wan taso!
jan Pepa|22|False|soweli suwi li wan taso la ni li ike...
waso seli|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|ni la sina pilin seme?
jan Pepa|2|False|sina pana ala pana e telo tan oko?
waso seli|3|False|NI LI PAKALA SEME?!
waso seli|4|False|NI LI ALE PI KEN SINA ANU SEME?!
waso seli|5|True|jan ante li pana e toki suwi!
waso seli|6|True|ona li sitelen e kama wawa ike!
waso seli|7|True|MUSI A!
waso seli|8|False|PILIN A!
waso seli|9|True|TASO SINA A
waso seli|10|False|SINA KAMA KEPEKEN ALA SONA PALI!
waso seli|11|True|NI A, NI LI PONA; O TAWA!
waso seli|12|False|O WEKA! O KAMA SIN LON TENPO PI SONA SIN!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|aaa! o pali, jan Pepa o! toki pi pilin ike o lon.
jan Pepa|2|True|sina ken pali.
jan Pepa|3|False|sina ken pali.
jan Pepa|4|True|ala...
jan Pepa|5|False|sina ken ala.
jan esun|6|False|a! sina o! o kama weka!
jan Pepa|7|False|!!
jan esun|8|False|sina ken ala pana e mani la o luka ala e ijo esun mi!
jan Pepa|9|True|ike a. o ni ala...
jan Pepa|10|True|SOWELI KAWA O!
jan Pepa|11|False|sina ken pana e pona tawa pali mi. taso sina wile moku taso!
jan Pepa|12|False|aa?!
jan Pepa|13|True|a, pona...
jan Pepa|14|False|ni li ken.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
waso seli|1|True|a! sina kama sin.
waso seli|2|False|lon tenpo lili...
waso seli|3|False|ilo utala anu seme?!
waso seli|4|False|sina sona ala sona?!
waso seli|5|False|mi seli e kiwen ilo la kiwen ale li kama ko li...
kalama|6|False|Anpa
kalama|7|False|Anpa
kalama|8|False|Anpa
waso seli|9|True|PAKALA!
waso seli|10|False|O NI ALA!
waso seli|11|False|NASIN NI LI PONA ALA!
jan Pepa|12|True|o pali, soweli Kawa o!
jan Pepa|13|False|o poki e telo oko mute kepeken ken ale sina!
kalama|14|False|Utala!
kalama|15|False|Utala!
kalama|16|False|Utala!
nimi|17|False|- PINI -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|tenpo 2022-08-03 musi sitelen & toki: David Revoy. jan ni li pona e toki: jan Arlo James Barnes en jan Benjamin Loubet en jan Bobby Hiltz en jan Chloé en janCraig Maloney en jan Estefania de Vasconcellos Guimaraes en jan GunChleoc en jan Nicolas Artance en jan Olivier Jolly en jan Rhombihexahedron en jan Valvin. toki pona: jan Ke Tami. jan Tepo en jan Juli en jan Tamalu li lukin li pona e ona. jan ni li pona e toki nimi: jan Craig Maloney. musi ni li tan pali pi ma Elewa mama: David Revoy. jan awen nanpa wan: Craig Maloney. jan pi toki sitelen: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson. ilo: ilo Krita 5.0.5 en ilo Inkscape 1.2 li kepeken ilo Fedora 36 KDE Spin. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
jan Pepa|2|False|sina sona ala sona?
jan Pepa|3|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free (libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 1058 li pana e mani la, lipu ni li lon!
jan Pepa|5|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|6|False|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|7|False|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|8|False|sina pona!
