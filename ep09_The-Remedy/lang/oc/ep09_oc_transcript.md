# Transcript of Pepper&Carrot Episode 09 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 9 : Lo remèdi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|L'endeman de la fèsta ...
Pepper|3|True|Tè, lo senhal de las montanhas, de la sèlva e de las nívols ?
Pepper|4|False|Mas perqué ?
Pepper|5|False|Òi, non !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Ai completament doblidat de desactivar mon « sistèma de securitat »...
Pepper|5|False|Un pauc plujosa, cresi, è ?...
Pepper|4|True|Sinon, cossí s'es passada la nuèit ?
Pepper|6|True|...Soi tan desolada, mas sens rencura, è ?
Pepper|7|False|Ai preparat de tè per que nos remontem totas après aquela petita malparada.
Pepper|1|True|Perdon...
Pepper|3|True|...mas soi tan contenta que siatz totas vengudas ! ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Pepper, pegassa ! Ara es tròp tard, me detèstan...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Yuzu|11|True|Glap
Yuzu|12|False|Glap
Carròt|1|False|Miaaaauuu !
Trufèl|16|True|Miii
Trufèl|17|False|Miii
Manga|14|True|Còt
Manga|15|False|Cot
Safran|19|False|Al laboratòri, que pòt èstre mortal !
Safran|18|True|Aviat !
Yuzu|2|True|Glap
Yuzu|3|True|Glap
Carròt|13|False|Miaaaauuu
Trufèl|5|True|Miii
Trufèl|6|True|Miii
Manga|8|True|Còt
Manga|9|True|Còt
Trufèl|7|False|Miii !
Manga|10|False|Còt !
Yuzu|4|False|Glap !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|False|...ça que la, es damatge qu'ajam pas poscut empachar los efièits segondaris de la pocion.
Shichimi|6|False|Te'n fagas pas ; la pèrdia dels pels e de las plumas serà pas que temporària. Tornarà créisser.
Narrator|7|False|- FIN -
Crèdits|8|False|Julhet de 2015 - Dessenh e scenari : David Revoy
Safran e Pepper|1|True|Extrà !
Shichimi e Coriandre|3|True|Yihaaa !
Safran e Pepper|2|False|Sèm las melhoras !
Shichimi e Coriandre|4|False|Son garits !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot est completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 406 mecènas :
Crèdits|7|False|https://www.patreon.com/davidrevoy
Crèdits|6|True|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent sus
Crèdits|8|False|Licéncia : Creative Commons Attribution 4.0 Sorsas : disponiblas sus www.peppercarrot.com Logicials : aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita 2.9.6, Inkscape 0.91 sus Linux Mint 17
Crèdits|4|False|Глеб Бузало ★ 无名 ★ 獨孤欣 & 獨弧悦 ★ Adam Mathena ★ Addison Lewis A Distinguished Robot ★ Aina Reich ★ Alan Hardman ★ Albert Westra ★ Alex ★ AlexanderKennedy Alexander Lust ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex V ★ Alfredo Alien Green ★ Allan Zieser ★ Alok Baikadi ★ Amic ★ Andreas Rieger ★ Andreas Ulmer ★ Andrej Kwadrin Andrew Godfrey ★ Andrey Alekseenko ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Ardash Crowfoot Arjun Chennu ★ Arnulf ★ Arturo J. Pérez ★ Austin Knowles ★ Axel Bordelon ★ Bastian Hougaard ★ Ben Evans blacksheep33512 ★ Boonsak Watanavisit ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Bryan Butler ★ BS Bui Dang Hai Trieu ★ carlos levischi ★ carolin der keks ★ Cedric Wohlleber ★ Chance Millar ★ Charles★ Chaz Straney Chris ★ Chris Sakkas ★ Christian Howe ★ Christophe Carré ★ Christopher Bates ★ Christopher Rodriguez Christopher Vollick ★ Colby Driedger ★ Damien ★ Daniel ★ Daniel Lynn ★ Danijel ★ David Brennan David Kerdudo ★ David Tang ★ DecMoon ★ Derek Zan ★ Dio Fantasma ★ Dmitry ★ Doug Moen ★ douze12 ★ Drew Fisher Durand D’souza ★ Elijah Brown ★ Elisha Condon ★-epsilon-★ Eric Schulz ★ Faolan Grady ★ francisco dario aviltis Francois Schnell ★ Francou ★ Garret Patterson ★ Gary Thomas ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov Grzegorz Wozniak ★ G. S. Davis ★ Guillaume ★ Gustav Strömbom ★ happy2ice ★ Happy Mimic ★ Helmar Suschka Henning Döscher ★ Henry Ståhle ★ HobbyThor ★ Ilyas ★ Irina Rempt ★ Jacob ★ James Frazier ★ Jamie Sutherland ★ Janusz Jason ★ Jeffrey Schneider ★ Jessey Wright ★ Jessica Gadling ★ Joao Luiz ★ John ★ John Gholson ★ John Urquhart Ferguson Jonas Peter ★ Jonathan Leroy ★ Jonathan Walsh ★ Justus Kat ★ Kailyce ★ Kai-Ting (Danil) Ko ★ Kari Lehto ★ Kathryn Wuerstl kazakirinyancat ★ Ken Mingyuan Xia ★ Kevin Estalella ★ Kevin Trévien ★ Kingsquee ★ Kurain ★ La Plume ★ Lenod ★ Liang Lise-Lotte Pesonen ★ Lloyd Ash Pyne ★ Lorentz Grip ★ Lorenzo Leonini ★ Magnus Kronnäs ★ Marc et Rick ★ Marco ★ marcus Matt Lichtenwalner ★ Michael F. Schönitzer ★ Michael Gill ★ Michael Polushkin ★ Mike Mosher ★ Mohamed El Banna ★ Nabispace Nazhif ★ Nicholas DeLateur ★ Nicola Angel ★ Oleg Schelykalnov ★ Olga Bikmullina ★ Olivier Amrein ★ Olivier Gavrois ★ Omar Willey Oscar Moreno ★ Pato Acevedo ★ Patrick Dezothez ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Peter Moonen ★ Petr Vlašic Pierre Vuillemin ★ Pranab Shenoy ★ Pummie ★ Raghavendra Kamath ★ Rajul Gupta ★ Ramel Hill ★ Ray Brown ★ Rebecca Morris ResidentEvilArtist ★ Reuben Tracey ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Ryan ★ Sally Bridgewater Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Scott Smiesko ★ ShadowMist ★ shafak ★ Shawn Meyer ★ Soriac ★ Stanislav ★ Stephan Theelke Stephen Bates ★ Steven Bennett ★ Stuart Dickson ★ surt ★ Takao Yamada ★ TamaskanLEM ★ tar8156 ★ TheFaico ★ thibhul ★ Thomas Schwery T.H. Porter ★ Tim Burbank ★ Tim J. ★ Tom Savage ★ Travis Humble ★ Tristy ★ Tyson Tan ★ Venus ★ Vera Vukovic ★ Victoria ★ Victoria White WakoTabacco ★ Wei-Ching Shyu ★ Westen Curry ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Yasmin ★ Zeni Pong
Crèdits|5|False|Adrian Lord ★ Ahmad Ali ★ al ★ Alandran ★ Alcide ★ Alexander Bülow Tomassen ★ Alexander Kashev ★ Alex Bradaric ★ Alex Cochrane ★ Alexey Golubev Ali Poulton (Aunty Pol) ★ Amy ★ Andrew ★ Andy Gelme ★ Angelica Beltran ★ anonymous ★ Antoine ★ Antonio Mendoza ★ Antonio Parisi ★ Axel Philipsenburg barbix ★ BataMoth ★ Bela Bargel ★ Bernd ★ Bernhard Saumweber ★ Betsy Luntao ★ Birger Tuer Thorvaldsen ★ blueswag ★ Boris Fauret ★ Brett Bryan Rosander ★ BXS ★ Chris Kastorff ★ Chris Radcliff ★ Christian Gruenwaldner ★ Clara Dexter ★ codl ★ Comics by Shoonyah Studio ★ Conway Scott Smith Coppin Olivier ★ Craig Bogun ★ Crystal Bollinger ★ Cuthbert Williams ★ Cyol ★ Cyrille Largillier ★ Cyril Paciullo ★ Daniel Björkman ★ Dan Norder Dan Stolyarov ★ David ★ Davi Na ★ Dawn Blair ★ Deanna ★ Denis Bolkovskis ★ Dezponia Veil ★ DiCola Jamn ★ Dmitriy Yakimov ★ Donald Hayward Douglas Oliveira Pessoa ★ Duke ★ Eitan Goldshtrom ★ Emery Schulz ★ Enrico Billich ★ Erik Moeller ★ Esteban Manchado Velázquez ★ Fen Yun Fat Fernando Nunes ★ ida nilsen ★ Igor ★ Ivan Korotkov ★ Jamie Hunter ★ Jason Baldus ★ Jazyl Homavazir ★ JDB ★ Jean-Baptiste Hebbrecht Jean-Gabriel LOQUET ★ Jhonny Rosa ★ Jim ★ Jim Street ★ Joerg Raidt ★ Joern Konopka ★ joe rutledge ★ John ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman Josh ★ Josh Cavalier ★ Juju Mendivil ★ Julian Dauner ★ Julia Velkova ★ Kate ★ Kroet ★ Lars Ivar Igesund ★ Liselle ★ Liska Myers ★ Louis Yung Luc Stepniewski ★ Luke Hochrein ★ Mahwiii ★ Mancy S ★ Manu Järvinen ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Megan Sandiford ★ Michael Michael Pureka ★ Michelle Pereira Garcia ★ Miroslav ★ mjkj ★ Moonsia ★ Moritz Fuchs ★ Muriah Summer ★ Mylène Cassen ★ Nicholas Terranova Nicole Heersema ★ Nielas Sinclair ★ Nikita Stafeev ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ No Reward ★ Nyx ★ Öykü Su Gürler Ozone S. ★ Pat David ★ Patrick Kennedy ★ Paul ★ Pet0r ★ Peter ★ Peter ★ Pierre Geier ★ Pyves & Ran ★ Ray Cruz ★ Raymond Fullon ★ Ray Powell Rebecca ★ Rebekah Hopper ★ Rei ★ Reorx Meng ★ Ret Samys ★ rictic ★ Robin Moussu ★ Sean Adams ★ Sebastien ★ Sevag Bakalian ★ Shadefalcon Simon Isenberg ★ Simon Moffitt ★ Siora ★ Sonja Reimann-Klieber ★ Sonny W. ★ Stanislav German-Evtushenko ★ Stephen Smoogen ★ Surabhi Gaur Taedirk ★ Tas Kartas ★ Terry Hancock (Personal) ★ Thomas Citharel ★ Thomas Werner ★ Thor Galle ★ Thornae ★ Timothy Boersma ★ Tomas Hajek Tomáš Slapnička ★ Tom Dickson ★ tree ★ uglyheroes ★ Umbra Draconis ★ Vitaly Tokarenko ★ Vladislav Kurdyukov ★ Wander ★ Wilhelmine Faust William Crumpler ★ Źmicier Kušnaroŭ ★ zubr kabbi
Crèdits|3|False|Arne Brix ★ Boudewijn Rempt ★ Brent Houghton ★ Casey Tatum Davis Aites ★ Ejner Fergo ★ Enrique Lopez ★ Francois Didier freecultureftw ★ Jonathan Ringstad ★ Julio Avila ★ Levi Kornelsen Matthew Reynolds ★ Mefflin Ross Bullis-bates ★ Michael Oliveira Nguyen Minh Trung ★ Nicki Aya ★ NinjaKnight Comics ★ Olivier Brun Praveen Bhamidipati ★ Ricardo Muggli ★ RJ van der Weide Roman Burdun ★ Urm
Crèdits|2|False|Jónatan Nilsson Alex Kotenko ★ Philippe Jean Edward Bateman
