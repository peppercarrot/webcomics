# Transcript of Pepper&Carrot Episode 30 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 30: Knuffel nodig?

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|- EINDE -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|3 september 2019 Tekeningen & verhaal: David Revoy. Bèta-feedback: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Nederlandstalige versie Vertaling: Midgard. Proeflezing: Marno van der Maas. Gebaseerd op het universum van Hereva Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita/4.2~git branch, Inkscape 0.92.3 op Kubuntu 18.04.2. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|3|True|Pepper&Carrot is helemaal vrij, open-bron en gesponsord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik de 973 patronen!
Pepper|7|True|Wist je dat?
Pepper|6|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|8|False|Kijk op www.peppercarrot.com voor alle info!
Pepper|2|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay … en meer!
Aftiteling|1|False|Dank je!
