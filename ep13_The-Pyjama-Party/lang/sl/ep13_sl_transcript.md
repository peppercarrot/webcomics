# Transcript of Pepper&Carrot Episode 13 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 13: Zabava v pižamah

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|4|False|Tukaj je res lepo!
Paprika|5|False|Hvala, da si naju povabila k sebi, Koriandrika!
Paprika|3|False|… vseh časov!
Paprika|1|True|Najboljše…
Paprika|2|True|… počitnice …

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandrika|1|True|Malenkost!
Koriandrika|2|False|Upam samo, da vaju moje osebje ne plaši preveč.
Paprika|11|False|O, kje pa. Zelo so gostoljubni. Prav domače se počutiva.
Šičimi|13|False|Starinsko in domačno hkrati!
Paprika|14|False|Res je!
Šičimi|12|True|Všeč mi je, kako imaš opremljeno.
Pošast|9|False|BuuuUuu ! ! !|nowhitespace
Pošast|10|False|BuUu ! ! !|nowhitespace
Pošast|8|False|BuuuUuu ! ! !|nowhitespace
Zvok|7|False|švvvvviiing!
Zvok|6|False|Klaf!
Zvok|5|False|Klukf!
Zvok|3|False|Dzzziiip ! ! !|nowhitespace
Zvok|4|False|Škak ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandrika|2|True|Včasih se mi zdi tako …
Šičimi|5|True|Jaz bi z veseljem živela v takem udobju,
Zvok|7|False|Šiingz ! ! !|nowhitespace
Zvok|4|False|bam!
Šičimi|8|False|„Prava-aška-čarovnica-ne-sme-živeti-na-tak-način.“
Paprika|9|True|Ha ha!
Paprika|10|True|Moč tradicije …
Paprika|11|False|Nekaj takega bi rekle tudi moje botre!
Šičimi|12|False|Aja?
Koriandrika|1|True|Lepo, da me spomnita.
Koriandrika|3|False|… „plehko“?
Šičimi|6|False|ampak …

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriandrika|2|False|Dajmo, dekleti, zberita se! Skoraj smo že na cilju!
Paprika in Šičimi|1|False|Hi hi hi!
Paprika|3|True|No, ni tako hudo!
Paprika|5|False|Niti začele še nismo…
Zvok|6|False|PRUuuuUuuuum ! ! !|nowhitespace
Zvok|9|False|Ssssss
Šičimi|8|False|?!!
Koriandrika|7|False|PAPRIKA!!!
Paprika|4|True|Ta naloga je res lahka.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zvok|6|False|ŽBAM ! !|nowhitespace
Šičimi|8|False|O, NE!
Koriandrika|9|False|Ne že spet!
Paprika|11|False|Daj to iz ust! Takoj!
Koriandrika|1|False|O, ne! Prepozno je! Je že m-m…
Šičimi|2|False|Neeeeee!!!
Pošast|3|False|MUAH HAHA HAHA !!!|nowhitespace
Šičimi|4|False|GRR!!!
Koriandrika|5|False|ZA TO BOŠ ŠE PLAČAL!
Zvok|7|False|ŽVENK ! !|nowhitespace
Zvok|13|False|PLONK!!
Zvok|12|False|PAF!!
Paprika|10|True|KORENČEK!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|10|False|November 2015 - Piše in riše David Revoy - Prevedla Andrej Ficko in Gorazd Gorup
Zvok|1|False|Tof
Paprika|4|True|Saj vem, da si me hotel zaščititi,
Paprika|3|True|Ne kujaj se, no!
Paprika|5|False|ampak samo igramo se!
Korenček|8|False|Grrrr
Napis|6|False|Utrdbe in feniksi
Napis|2|False|Princesa Koriandrika
Pripovedovalec|9|False|- KONEC -
Napis|7|False|Utrdbe in feniksi

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|1|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek sta podprla 602 bralca:
Zasluge|2|True|Tudi ti lahko postaneš denarni/a podpornik/ca za naslednjo epizodo stripa:
Zasluge|3|False|https://www.patreon.com/davidrevoy
Zasluge|4|False|Licenca: Creative Commons Priznanje avtorstva 4.0. Izvorne datoteke na voljo na www.peppercarrot.com Programska oprema: ta epizoda je nastala s prostim programjem Krita 2.9.9 in Inkscape 0.91 na Linux Mint 17
