# Transcript of Pepper&Carrot Episode 13 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titulus|1|False|Episodium XIII : Vestium dormitoriarum convivium

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Piper|4|False|Longe bellissimae sunt !
Piper|5|False|Gratias tibi Coriandro quod nos domo invitavisti !
Piper|3|False|... mundi !
Piper|1|True|Sunt optimae....
Piper|2|True|... feriae...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandrum|1|True|Nil est !
Coriandrum|2|False|Spero vos non nimis commoveri ab ministerium meum.
Piper|11|False|Noli sollicitari; valde adfabiles sunt, bene nos habemus cum eis!
Sichimi|13|False|...tam elegante et simul amicabili !
Piper|14|False|Ita profecto est !
Sichimi|12|True|Ego commoveor ornatione...
Monster|9|False|UoooOoh ! ! !|nowhitespace
Monster|10|False|UoOoh ! ! !|nowhitespace
Monster|8|False|UoooOoh ! ! !|nowhitespace
Sonus|7|False|chuuuuuuiiing !|nowhitespace
Sonus|6|False|Klaf !|nowhitespace
Sonus|5|False|Klukf !|nowhitespace
Sonus|3|False|Dzzziii ! ! !|nowhitespace
Sonus|4|False|Shcac ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandrum|2|True|Non numquam, censeo haec omnia esse nimis
Sichimi|5|True|Ego si haberem tam commoditatem vitae, mihi magno gaudio esset
Sonus|7|False|Shiingz ! ! !|nowhitespace
Sonus|4|False|bim !|nowhitespace
Sichimi|8|False|...“Non-oportet-vera-maga-Ahis-hoc-modo-vivere”
Piper|9|True|Ha ha !...
Piper|10|True|Onus morum...
Piper|11|False|Matrinae meae talem rem dicere possunt !
Sichimi|12|False|Vere ?
Coriandrum|1|True|Bella est quae hoc dixisti.
Coriandrum|3|False|... "comunia" ...
Sichimi|6|False|sed...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriandrum|2|False|Agete, puellae, animadvertite, tandem ad finem pervenimus!
Piper et Sichimi|1|False|Hi hi hi !
Piper|3|True|Oh, recte...!
Piper|5|False|...et nondum incepimus...
Sonus|6|False|VRoooOvvovv ! ! !|nowhitespace
Sonus|9|False|Shhshh|nowhitespace
Sichimi|8|False|?!!
Coriandrum|7|False|PIPER !!!|nowhitespace
Piper|4|True|Iste labor nimis facilis est...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonus|6|False|CRAC ! !|nowhitespace
Sichimi|8|False|HEU !
Coriandrum|9|False|Non rursus !
Piper|11|False|Hoc remitte ! Statim!
Coriandrum|1|False|...Eheu ! Serius est ! Illa est... est ...
Sichimi|2|False|Vae nobis !!!|nowhitespace
Monster|3|False|MUAH HAHA HAHA !!!|nowhitespace
Sichimi|4|False|GRR !!!|nowhitespace
Coriandrum|5|False|ISTUD VINDICABIMUS !
Sonus|7|False|KLING ! !|nowhitespace
Sonus|13|False|PLONK !!|nowhitespace
Sonus|12|False|TAX !!|nowhitespace
Piper|10|True|CAROTA !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|8|False|- FINIS -
Credits|9|False|Mense novembre A. MMXV - Designum et fabula : David Revoy, lingua latina translatio: Benjamin Touati, correctio: Valentine Guillocheau
Sonus|1|False|Pam
Piper|4|True|Scio te istud facere quia vis me defendere ...
Piper|3|True|Age, noli iratum esse !
Piper|5|False|... Sed nobis ludendis, frustra agis !
Carota|7|False|Grrrr
Writing|6|False|Citadelles & Phénix
Writing|2|False|Principissa Coriandrum

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Piper&Carota gratis constat, omnno apertum fontis est ac juvatur maecenatu lectorum ; in hoc episodium, gratias ago illis 602 Maecenatibus :
Credits|2|True|Tu quoque, potes fieri maecenas Piperis&Carotae sequenti episodio :
Credits|4|False|Licentia : Creative Commons Attribution 4.0 Fontes : inveniuntur apud www.peppercarrot.com Instrumenta : Hoc episodium omnino designatum est liberis programmatibus Krita 2.9.9, Inkscape 0.91 in Linux Mint 17
Credits|3|False|https://www.patreon.com/davidrevoy
