# Transcript of Pepper&Carrot Episode 13 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 13: La fiesta del pijama

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pimienta|4|False|¡Esto es genial!
Pimienta|5|False|¡Muchas gracias por invitarnos, Celandria!
Pimienta|3|False|...¡de mi vida!
Pimienta|1|True|Las mejores...
Pimienta|2|True|...vacaciones...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Celandria|1|True|¡Gracias a vosotras!
Celandria|2|False|Espero que no os sintáis muy incómodas por la presencia de mi personal.
Pimienta|11|False|No, no te preocupes, son majísimos. Nos hacen sentir como en casa.
Shichimi|13|False|...es tan refinada y, a la vez, tan acogedora...!
Pimienta|14|False|¡Muy cierto eso!
Shichimi|12|True|¡Yo, con lo que estoy impresionada, es con la decoración...
Monstruo|9|False|¡ ¡ ¡ Buuuuuuh ! ! !|nowhitespace
Monstruo|10|False|¡ ¡ ¡ Buuuh ! ! !|nowhitespace
Monstruo|8|False|¡ ¡ ¡ Buuuuuh ! ! !|nowhitespace
Sonido|7|False|¡shwwwwwwiiing!
Sonido|6|False|¡Klaf!
Sonido|5|False|¡Klukf!
Sonido|3|False|¡ ¡ ¡ Dzzziii ! ! !|nowhitespace
Sonido|4|False|¡ ¡ ¡ Shkak ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Celandria|2|True|a veces lo veo todo tan...
Shichimi|5|True|A mi me encantaría vivir con estas comodidades,
Sonido|7|False|¡ ¡ ¡ Shiingz ! ! !|nowhitespace
Sonido|4|False|¡ bam !
Shichimi|8|False|..."Una-verdadera-bruja-de-Ah-no-debe-vivir-de-esa-manera".
Pimienta|9|True|¡Jajaja!...
Pimienta|10|True|El peso de las tradiciones...
Pimienta|11|False|Algo así es lo que dirían mis madrinas.
Shichimi|12|False|¿En serio?
Celandria|1|True|Es agradable que me lo recordéis...
Celandria|3|False|...no sé, "banal"...
Shichimi|6|False|pero...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Celandria|2|False|¡Vamos, chicas, concentrémonos, estamos llegando al último objetivo!
Pimienta y Shichimi|1|False|¡Jijijiji!
Pimienta|3|True|¡Oh, está bien...!
Pimienta|5|False|...y ni siquiera hemos empezado a...
Sonido|6|False|¡ ¡ ¡ WRoooOwwoww ! ! !|nowhitespace
Sonido|9|False|Shhshh
Shichimi|8|False|¡¡¿?!!
Celandria|7|False|¡¡¡PIMIENTA!!!
Pimienta|4|True|De todas formas esta misión está chupada...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sonido|6|False|¡ ¡ CRACK ! !|nowhitespace
Shichimi|8|False|¡Oh, NO!
Celandria|9|False|¡Otra vez no!
Pimienta|11|False|¡Suelta eso ahora mismo!
Celandria|1|False|...¡Oh no! ¡Demasiado tarde! Ella... ...está...
Shichimi|2|False|¡¡¡ Nooooo !!!
Monstruo|3|False|MUAH HAHA HAHA !!!|nowhitespace
Shichimi|4|False|¡¡¡GRR!!!
Celandria|5|False|¡NOS LAS VAS A PAGAR!...
Sonido|7|False|¡ ¡ KLING ! !|nowhitespace
Sonido|13|False|¡¡ PLONK !!
Sonido|12|False|¡¡ PAF !!
Pimienta|10|True|¡ZANAHORIA!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|10|False|Noviembre 2015 - Dibujo & Guión: David Revoy - Traducción: TheFaico
Sonido|1|False|Pam
Pimienta|4|True|Sé que lo haces para defenderme...
Pimienta|3|True|¡Vamos, deja de gruñir!
Pimienta|5|False|...pero cuando se trata de un juego... ¡no sirve de mucho!
Zanahoria|8|False|Grrrr
Escritura|6|False|Ciudadelas & Fénix
Escritura|2|False|Princesa Celandria
Narrador|9|False|- FIN -
Escritura|7|False|Ciudadelas & Fénix

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 602 mecenas:
Créditos|2|True|Tú también puedes ser mecenas de Pepper&Carrot para el próximo episodio
Créditos|3|False|https://www.patreon.com/davidrevoy
Créditos|4|False|Licencia: Creative Commons Attribution 4.0 Ficheros originales disponibles en www.peppercarrot.com Herramientas: Este episodio ha sido creado al 100% con software libre Krita 2.9.9, Inkscape 0.91 en Linux Mint 17
