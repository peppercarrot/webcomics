# Transcript of Pepper&Carrot Episode 25 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 25: No hay atajos

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|1|False|CROQUETAS

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrador|13|False|- FIN -
Pimienta|1|True|?
Pimienta|2|True|?
Pimienta|3|True|?
Pimienta|4|False|?
Pimienta|5|True|?
Pimienta|6|True|?
Pimienta|7|True|?
Pimienta|8|False|?
Pimienta|9|True|?
Pimienta|10|True|?
Pimienta|11|True|?
Pimienta|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|7|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
Créditos|6|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 909 mecenas:
Créditos|1|False|05/2018 - www.peppercarrot.com - Dibujo & Guion: David Revoy - Traducción: TheFaico
Créditos|3|False|Basado en el universo de Hereva creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Créditos|4|False|Software: Krita 4.0.0, Inkscape 0.92.3 en Kubuntu 17.10
Créditos|5|False|Licencia: Creative Commons Attribution 4.0
Créditos|2|False|Relectura durante la versión beta: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire y Zveryok.
