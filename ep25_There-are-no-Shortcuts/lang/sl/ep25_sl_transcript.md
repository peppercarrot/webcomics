# Transcript of Pepper&Carrot Episode 25 [sl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Naslov|1|False|Epizoda 25: Ni bližnjic

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Napis|1|False|BRIKETI

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pripovedovalec|13|False|- KONEC -
Paprika|1|True|?
Paprika|2|True|?
Paprika|3|True|?
Paprika|4|False|?
Paprika|5|True|?
Paprika|6|True|?
Paprika|7|True|?
Paprika|8|False|?
Paprika|9|True|?
Paprika|10|True|?
Paprika|11|True|?
Paprika|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zasluge|7|False|Tudi ti lahko denarno podpreš izdelavo stripa na www.patreon.com/davidrevoy
Zasluge|6|False|Strip Paprika in Korenček (Pepper&Carrot) je povsem prost in odprtokoden. Njegov nastanek je podprlo 909 bralcev:
Zasluge|1|False|Maj 2018 - www.peppercarrot.com - Piše in riše David Revoy
Zasluge|3|False|Dogaja se v vesolju Hereve avtorja Davida Revoyja s prispevki Craiga Maloneyja. Popravki: Willem Sonke, Moini, Hali, CGand in Alex Gryson.
Zasluge|4|False|Programska oprema: Krita 4.0.0 in Inkscape 0.92.3 na Kubuntu 17.10
Zasluge|5|False|Licenca: Creative Commons Priznanje avtorstva 4.0.
Zasluge|2|False|Beta povratne informacije: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire in Zveryok.
