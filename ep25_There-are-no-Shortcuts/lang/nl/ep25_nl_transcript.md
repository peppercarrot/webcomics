# Transcript of Pepper&Carrot Episode 25 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 25: Werken loont

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geschrift|1|False|BROKJES

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|13|False|- EINDE -
Pepper|1|True|?
Pepper|2|True|?
Pepper|3|True|?
Pepper|4|False|?
Pepper|5|True|?
Pepper|6|True|?
Pepper|7|True|?
Pepper|8|False|?
Pepper|9|True|?
Pepper|10|True|?
Pepper|11|True|?
Pepper|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|7|False|Jij kan ook een patroon van Pepper&Carrot worden op www.patreon.com/davidrevoy
Aftiteling|6|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 909 patronen:
Aftiteling|1|False|05/2018 - www.peppercarrot.com - Tekeningen en verhaal: David Revoy - Vertaling: Midgard, Willem Sonke en Marno van der Maas.
Aftiteling|3|False|Gebaseerd op het universum van Hereva gecreëerd door David Revoy met bijdragen van Craig Maloney. Verbeteringen door Willem Sonke, Moini, Hali, CGand en Alex Gryson.
Aftiteling|4|False|Software: Krita 4.0.0, Inkscape 0.92.3 op Kubuntu 17.10
Aftiteling|5|False|Licentie: Creative Commons Naamsvermelding 4.0
Aftiteling|2|False|Beta-feedback: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, XHire en Zveryok.
