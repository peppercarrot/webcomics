# Transcript of Pepper&Carrot Episode 25 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tίτλος|1|False|Επεισόδιο 25: Δεν υπάρχουν Σύντομοι Δρόμοι

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Κείμενο|1|False|CAT FOOD

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Αφηγητής|13|False|- ΤΕΛΟΣ -
Πιπερούλα|1|True|?
Πιπερούλα|2|True|?
Πιπερούλα|3|True|?
Πιπερούλα|4|False|?
Πιπερούλα|5|True|?
Πιπερούλα|6|True|?
Πιπερούλα|7|True|?
Πιπερούλα|8|False|?
Πιπερούλα|9|True|?
Πιπερούλα|10|True|?
Πιπερούλα|11|True|?
Πιπερούλα|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Συντελεστές|7|False|Μπορείτε και εσεις να συνεισφέρετε στο έργο Η Πιπερούλα και Ο Καροτούλης στο www.patreon.com/davidrevoy
Συντελεστές|6|False|Η Πιπερούλα και Ο Καροτούλης είναι εντελώς δωρεάν (ελεύθερο), ανοιχτού κώδικα και υπάρχει χάρη στην υποστήριξη των αναγνωστών του. Για αυτό το επεισόδιο, ευχαριστούμε τους 909 συνεισφέροντες:
Συντελεστές|1|False|05/2018 - www.peppercarrot.com - ΣΧΕΔΙΟ & ΣΕΝΑΡΙΟ: David Revoy
Συντελεστές|3|False|Βασισμένο στο σύμπαν του Έρεβα που δημιουργήθηκε από τον David Revoy με συνεισφορές του Craig Maloney. Διορθώσεις από τους Willem Sonke, Moini, Hali, CGand και Alex Gryson.
Συντελεστές|4|False|Λογισμικό: Krita 4.0.0, Inkscape 0.92.3 σε Kubuntu 17.10
Συντελεστές|5|False|Άδεια: Creative Commons Attribution 4.0
Συντελεστές|2|False|Έλεγχος προέκδοσης: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire και Zveryok.
