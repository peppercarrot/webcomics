# Transcript of Pepper&Carrot Episode 22 [el]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tίτλος|1|False|Επεισόδιο 22: Το Σύστημα Ψήφου

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|1|False|Ο ΜΕΓΑΛΟΣ ΔΙΑΓΩΝΙΣΜΟΣ ΜΑΓΕΙΑΣ ΜΠΟΡΕΙ ΕΠΙΤΕΛΟΥΣ ΝΑ ΑΡΧΙΣΕΙ
Κείμενο|2|False|Magic Contest

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|1|False|ΚΑΙ ΕΥΧΑΡΙΣΤΟΥΜΕ ΤΟΥΣ ΛΑΜΠΡΟΥΣ ΜΗΧΑΝΙΚΟΥΣ ΜΑΣ, ΟΠΟΥ ΚΑΙ ΕΣΕΙΣ ΜΠΟΡΕΙΤΕ ΝΑ ΛΑΒΕΤΕ ΜΕΡΟΣ
ο Δήμαρχος της Κομόνα|2|False|ΝΑΙ, ΝΑΙ ΦΙΛΟΙ ΜΟΥ, ΠΑΝΗΓΥΡΙΣΤΕ ΓΙΑ ΑΥΤΑ ΤΑ ΤΕΧΝΟΛΟΓΙΚΑ ΘΑΥΜΑΤΑ ΠΟΥ ΟΙ ΟΙΚΟΔΕΣΠΟΤΕΣ ΚΑΙ ΟΙΚΟΔΕΣΠΟΙΝΕΣ ΘΑ ΣΑΣ ΔΩΣΟΥΝ!
ο Δήμαρχος της Κομόνα|3|False|ΤΟ ΠΡΑΣΙΝΟ ΚΟΥΜΠΙ ΔΙΝΕΙ ΕΝΑ ΒΑΘΜΟ ΣΕ ΕΝΑΝ ΥΠΟΨΗΦΙΟ, ΤΟ ΚΟΚΚΙΝΟ ΚΟΥΜΠΙ ΑΦΑΙΡΕΙ ΕΝΑ ΒΑΘΜΟ. ΕΣΕΙΣ ΑΠΟΦΑΣΙΖΕΤΕ!
ο Δήμαρχος της Κομόνα|4|True|"ΚΑΙ Η ΚΡΙΤΙΚΗ ΕΠΙΤΡΟΠΗ;", ΣΕ ΑΚΟΥΩ ΝΑ ΡΩΤΑΣ.
ο Δήμαρχος της Κομόνα|5|False|ΜΗΝ ΑΝΗΣΥΧΕΙΣ, ΕΧΟΥΜΕ ΣΚΕΦΤΕΙ ΤΑ ΠΑΝΤΑ!
ο Δήμαρχος της Κομόνα|6|False|ΚΑΘΕ ΚΡΙΤΗΣ ΕΧΕΙ ΕΝΑ ΕΙΔΙΚΟ ΤΗΛΕΧΕΙΡΙΣΤΗΡΙΟ, ΙΚΑΝΟ ΝΑ ΔΩΣΕΙ Η ΝΑ ΠΑΡΕΙ ΕΚΑΤΟ ΠΟΝΤΟΥΣ ΜΕ ΕΝΑ ΠΑΤΗΜΑ!
Πιπεριά|7|False|ΟΥΑΟΥ

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|5|False|…50,000 Ko!
ο Δήμαρχος της Κομόνα|1|False|ΚΑΙ ΤΟ ΚΕΡΑΣΑΚΙ ΣΤΗΝ ΤΟΥΡΤΑ-ΟΙ ΒΑΘΜΟΛΟΓΙΕΣ ΘΑ ΕΜΦΑΝΙΣΤΟΥΝ ΑΚΡΙΒΩΣ ΠΑΝΩ ΑΠΟ ΤΟ ΚΕΦΑΛΙ ΤΟΥΣ!
ο Δήμαρχος της Κομόνα|3|False|ΟΙ ΤΡΕΙΣ ΜΑΓΙΣΣΕΣ ΠΟΥ ΘΑ ΛΑΒΟΥΝ ΤΙΣ ΚΑΛΥΤΕΡΕΣ ΒΑΘΜΟΛΟΓΙΕΣ ΘΑ ΠΡΟΧΩΡΗΣΟΥΝ ΣΤΟΝ ΤΕΛΙΚΟ!
ο Δήμαρχος της Κομόνα|4|True|ΜΗΝ ΞΕΧΝΑΤΕ, Ο ΝΙΚΗΤΗΣ ΤΟΥ ΜΕΓΑΛΟΥ ΒΡΑΒΕΙΟΥ ΘΑ ΠΑΡΕΙ ΣΠΙΤΙ ΤΟΥ ΤΟ ΕΚΛΗΚΤΙΚΟ ΠΟΣΟ…
Κείμενο|2|False|1337
Ακροατήριο|6|False|Clap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Καροτούλης|2|False|ΤΑΠ
Πιπεριά|3|False|ΔΕΝ ΕΙΝΑΙ ΜΙΑ ΘΑΥΜΑΣΙΑ ΙΔΕΑ ΚΑΡΟΤΟΥΛΗ;
Πιπεριά|4|True|ΚΑΙΝΟΤΟΜΑ…
Πιπεριά|5|True|ΔΙΑΣΚΕΔΑΣΤΙΚΗ…
Πιπεριά|6|True|ΔΗΜΟΚΡΑΤΙΚΗ…
Πιπεριά|7|False|.. ΤΟ ΤΕΛΕΙΟ ΣΥΣΤΗΜΑ!
Πιπεριά|8|True|ΔΕΝ ΧΡΕΙΑΖΟΜΑΣΤΕ ΠΛΕΟΝ ΕΙΔΙΚΟΥΣ ΝΑ ΚΡΙΝΟΥΝ ΤΟ ΑΠΟΤΕΛΕΣΜΑ
Πιπεριά|9|False|ΤΙ ΠΡΟΟΔΟΣ! ΖΟΥΜΕ ΣΕ ΜΙΑ ΦΑΝΤΑΣΤΙΚΗ ΕΠΟΧΗ!
ο Δήμαρχος της Κομόνα|10|False|ΟΛΟΙ ΕΧΕΤΕ ΤΟ ΤΗΛΕΚΟΝΤΡΟΛ;
Ακροατήριο|11|False|ΤΕΛΕΙΑ!
Ακροατήριο|12|False|ΝΑΙ!
Ακροατήριο|13|False|ΝΑΙ!
Ακροατήριο|14|False|ΝΑΙ!
Ακροατήριο|15|False|ΝΑΙ!
ο Δήμαρχος της Κομόνα|16|True|ΥΠΕΡΟΧΑ!
ο Δήμαρχος της Κομόνα|17|True|Ο ΔΙΑΓΩΝΙΣΜΟΣ...
ο Δήμαρχος της Κομόνα|18|False|ΑΡΧΙΖΕΙ!!
Ακροατήριο|1|False|Κλαπ

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|1|False|ΚΑΙ ΜΕ ΤΗ ΧΑΜΟΜΗΛΙΑ ΑΡΧΙΖΕΙ Ο ΔΙΑΓΩΝΙΣΜΟΣ
Ήχος|2|False|Dgiiioo…
Χαμομηλιά|3|False|SYLVESTRIS!
Ήχος|4|False|ΜΠΑΜ!
Ακροατήριο|5|True|ΤΑΠ
Ακροατήριο|6|True|ΤΑΠ
Ακροατήριο|7|True|ΤΑΠ
Ακροατήριο|8|True|ΤΑΠ
Ακροατήριο|9|True|ΤΑΠ
Ακροατήριο|10|True|ΤΑΠ
Ακροατήριο|11|False|ΤΑΠ

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|2|False|Η ΧΑΜΟΜΗΛΙΑ ΕΛΑΒΕ ΕΝΑ ΠΟΛΥ ΚΑΛΟ ΣΚΟΡ! ΤΩΡΑ Η ΣΕΙΡΑ ΤΗΣ ΣΙΣΣΙΜΙ!
Κείμενο|1|False|5861
Σίσσιμι|4|False|ΛΟΥΞ…
Ήχος|5|False|Φιιιιιιζζζ!!
Σίσσιμι|6|False|ΜΑΞΙΜΑ!
Ακροατήριο|7|False|AAAX!!
Ακροατήριο|8|False|OOY!!
Ακροατήριο|9|False|TA MATIA MOY!!!
Πιπεριά|12|False|ΚΑΡΟΤΟΥΛΗ… ΔΕΝ ΠΕΙΡΑΖΕΙ, ΔΩΣ ΤΗΣ ΒΑΘΜΟ, ΑΦΟΥ ΕΙΝΑΙ ΦΙΛΗ ΜΑΣ…
Καροτούλης|13|False|ΤΑΠ
Ακροατήριο|10|True|ΟΥΣΤ!
Ακροατήριο|11|False|ΟΥΣΤ!
Ακροατήριο|14|True|ΜΠΟΥΟΥ!
Ακροατήριο|15|False|ΕΞΩ!
ο Δήμαρχος της Κομόνα|17|False|Α, ΦΑΙΝΕΤΑΙ ΟΤΙ ΤΟ ΠΛΗΘΟΣ ΔΕΝ ΕΜΕΙΝΕ ΕΚΘΑΜΒΟ ΑΠΟ ΑΥΤΗ ΤΗ "ΠΑΝΕΞΥΠΝΗ" ΠΑΡΟΥΣΙΑΣΗ. ΤΩΡΑ ΣΕΙΡΑ ΕΧΕΙ Η ΣΠΙΡΟΥΛΙΝΑ!
Κείμενο|16|False|-42
Ακροατήριο|18|True|ΕΞΩΩΩΩ!
Ακροατήριο|19|True|ΦΥΓΕΕΕ!
Ακροατήριο|20|True|ΦΥΓΕ!
Ακροατήριο|21|False|ΕΞΩΩΩΩ!
Ακροατήριο|3|False|ΚΛΑΠ

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Σπιρουλίνα|1|False|ΕΛΕΥΘΕΡΙΟΥΣ ΤΕΡΑΤΙΟΥΣ!
Ήχος|2|False|ΓΙΟΟΟΟΟ!
Ήχος|3|False|ΣΠΛΑΑΤΣ!
ο Δήμαρχος της Κομόνα|5|False|ΠΑΝΕΜΟΡΦΑ! ΔΥΝΑΤΑ! Η ΣΠΙΡΟΥΛΙΝΑ ΠΡΟΗΓΕΙΤΑΙ! ΚΟΛΙΑΝΤΡΑ, ΣΕΙΡΑ ΣΟΥ!
Κείμενο|4|False|6225
Σπιρουλίνα & Ντούριαν|6|False|ΤΑΠ
Κολιάντρα|8|False|ΝΕΚΡΙΟΥΣ ΣΗΚΩΣΙΟΥΣ!
Ήχος|9|False|ΓΚΡΟΥΟΥ!
ο Δήμαρχος της Κομόνα|11|False|Ο, ΦΑΙΝΕΤΑΙ ΌΤΙ ΟΙ ΣΚΕΛΕΤΟΙ ΕΊΝΑΙ ΕΚΤΟΣ ΜΟΔΑΣ… ΤΩΡΑ Ο ΑΓΑΠΗΤΟΣ ΣΑΦΡΟΝ!
Κείμενο|10|False|2023
Ακροατήριο|7|False|ΚΛΑΠ

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Σάφρον|1|False|ΓΚΡΡ! ΔΕΝ ΘΑ ΑΦΗΣΩ ΤΟΝ ΕΑΥΤΟ ΜΟΥ ΝΑ ΤΑ ΠΑΕΙ ΧΕΙΡΟΤΕΡΑ ΑΠΟ ΤΗ ΣΠΙΡΟΥΛΙΝΑ! ΘΑ ΤΑ ΔΩΣΩ ΟΛΑ ΤΩΡΑ!
Σάφρον|2|False|ΚΑΝΕ ΠΙΣΩ ΤΡΟΥΦΑ!
Ήχος|3|False|ΦΣΣΣΣ!
Ήχος|4|False|ΦΡΡΡΡ!
Ήχος|5|False|ΚΡΡΡ!
Τρούφα|6|False|ΝΙΑΟΥ!
Σάφρον|7|False|ΠΕΡΙΣΤΡΟΦΙΚΟΥΣ
Σάφρον|8|False|ΦΩΤΩΔΙΑ!
Ήχος|9|False|ΦΣΣΣΣΣΣΣΣ!
Ήχος|10|False|ΣΟΥΙΙΙΙ!
Ήχος|11|False|Fsssh!
Ήχος|12|False|ΠΑΦ!
Σάφρον|13|False|?!!
Ήχος|14|False|φσσσσ!
Ήχος|15|False|ΦΣΣΣ!
Σάφρον|16|False|ΑΊΊ!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Σάφρον|1|False|Ο, OΧΙ... ΠOΣΗ ΝΤΡΟΠΗ!
Σάφρον|3|False|?!
Κείμενο|4|True|14
Κείμενο|5|False|849
Πιπεριά|6|False|ΤΑΠ
Λόρδος Αζίρφ|7|True|ΤΑΠ
Λόρδος Αζίρφ|8|True|ΤΑΠ
Λόρδος Αζίρφ|9|False|ΤΑΠ
Κείμενο|10|True|18
Κείμενο|11|False|231
ο Δήμαρχος της Κομόνα|12|True|ΠΑΡΑΚΑΛΩ... ΠΑΡΑΚΑΛΩ! ΛΙΓΟ ΕΥΠΡΕΠΕΙΑ!
ο Δήμαρχος της Κομόνα|13|False|Η ΣΙΣΙΜΙ ΚΑΙ Η ΚΟΛΙΑΝΤΡΑ ΗΤΤΗΘΗΚΑΝ!
ο Δήμαρχος της Κομόνα|14|False|Η ΧΑΜΟΜΗΛΙΑ, Η ΣΠΙΡΟΥΛΙΝΑ ΚΑΙ Η ΣΑΦΡΟΝ ΠΡΟΚΡΙΘΗΚΑΝ ΣΤΟΝ ΤΕΛΙΚΟ!
Ακροατήριο|2|False|ΚΛΑΠ

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ο Δήμαρχος της Κομόνα|1|False|ΚΥΡΙΕΣ ΕΙΣΤΕ ΕΤΟΙΜΕΣ ΓΙΑ ΑΚΟΜΗ ΜΙΑ ΔΟΚΙΜΑΣΙΑ;
ο Δήμαρχος της Κομόνα|2|False|ΑΜΕ
Κείμενο|3|True|15
Κείμενο|4|False|703
Κείμενο|5|True|19
Κείμενο|6|False|863
Κείμενο|7|True|13
Κείμενο|8|False|614
Ήχος|10|False|ΠΛΑΝΚ!
Πιπεριά|11|False|ΠΑΙΡΝΩ ΠΙΣΩ ΟΤΙ ΕΙΠΑ ΓΙΑ ΑΥΤΟ ΤΟ ΣΥΣΤΗΜΑ...
Λόρδος Αζίρφ|12|True|ΤΑΠ
Λόρδος Αζίρφ|13|True|ΤΑΠ
Λόρδος Αζίρφ|14|False|ΤΑΠ
Αφηγητής|15|False|- ΤΕΛΟΣ -
Ακροατήριο|9|False|Κλαπ!

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Συντελεστές|1|False|05/2017 - www.peppercarrot.com - Artwork & Scenario: David Revoy - Translation: Alex Gryson
Συντελεστές|2|False|Dialogue Improvements: Nicolas Artance, Valvin, Craig Maloney.
Συντελεστές|3|False|Based on the Hereva universe created by David Revoy with contributions from Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Συντελεστές|4|False|Software: Krita 3.1.3, Inkscape 0.92.1 on Linux Mint 18.1
Συντελεστές|5|False|License: Creative Commons Attribution 4.0
Συντελεστές|6|False|Pepper&Carrot is entirely free(libre), open-source, and sponsored thanks to the patronage of its readers. For this episode, thanks go to the 864 Patrons:
Συντελεστές|7|False|You too can become a patron of Pepper&Carrot on www.patreon.com/davidrevoy
