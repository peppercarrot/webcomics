# Transcript of Pepper&Carrot Episode 22 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episodio 22: El sistema de voto

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|1|False|¡Nuestro gran concurso de magia está listo para comenzar!
Escritura|2|False|Concurso de Magia

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|1|False|Y gracias a nuestros magníficos ingenieros, ¡vosotros también podréis participar!
Alcalde de Komona|2|False|En efecto, queridos amigos, ¡podéis admirar esta pequeña maravilla tecnológica que reparten nuestras azafatas!
Alcalde de Komona|3|False|El botón verde otorga un punto a la candidata, el botón rojo le resta uno: ¡vosotros decidís!
Alcalde de Komona|4|True|«¿Y el jurado, entonces?», os preguntaréis.
Alcalde de Komona|5|False|Tranquilos, ¡está todo bien pensado!
Alcalde de Komona|6|False|Cada miembro del jurado recibirá un mando a distancia especial, ¡capaz de dar o quitar cien puntos de golpe!
Pimienta|7|False|¡Hala!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|5|False|…¡50 000 Ko!
Alcalde de Komona|1|False|Y la guinda del pastel, las puntuaciones aparecerán directamente... ¡sobre las cabezas de nuestras participantes!
Alcalde de Komona|3|False|¡Las tres brujas que obtengan la mejor puntuación accederán a la gran final!
Alcalde de Komona|4|True|Y recordamos que la candidata que resulte la gran ganadora del concurso... se llevará la sustanciosa suma de...
Escritura|2|False|1337
Público|6|False|Clap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zanahoria|2|False|Tap
Pimienta|3|False|¡Genial idea! ¿Verdad, Zanahoria?
Pimienta|4|True|Innovadora…
Pimienta|5|True|Divertida…
Pimienta|6|True|Democrática…
Pimienta|7|False|…¡El sistema perfecto, vaya!
Pimienta|8|True|¡Ya no hacen falta expertos para juzgar la calidad!
Pimienta|9|False|¡Esto sí es progreso! ¡Vivimos en una época realmente fantástica!
Alcalde de Komona|10|False|¿Todo el mundo tiene su mando?
Público|11|False|¡Bieeen!
Público|12|False|¡Síiii!
Público|13|False|¡Síiiii!
Público|14|False|¡Síiii!
Público|15|False|¡Síii!
Alcalde de Komona|16|True|¡Bien!
Alcalde de Komona|17|True|Que comience…
Alcalde de Komona|18|False|¡¡EL ESPECTÁCULO!!
Público|1|False|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|1|False|¡Da comienzo el concurso! ¡Es el turno de Manzanilla!
Sonido|2|False|Dziiioo…
Manzanilla|3|False|¡SILVESTRIS!
Sonido|4|False|¡Bam!
Público|5|True|Tap
Público|6|True|Tap
Público|7|True|Tap
Público|8|True|Tap
Público|9|True|Tap
Público|10|True|Tap
Público|11|False|Tap

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|2|False|¡Manzanilla ha conseguido una muy buena puntuación! ¡Turno ahora para Shichimi!
Escritura|1|False|5861
Shichimi|4|False|LUX…
Sonido|5|False|¡¡Fiiiiiiizz!!
Shichimi|6|False|¡MÁXIMA!
Público|7|False|¡¡Aah!!
Público|8|False|¡¡Aagh!!
Público|9|False|¡¡¡Mis ojos!!!
Pimienta|12|False|Zanahoria… Dale de todas formas un pulgar verde, es nuestra amiga…
Zanahoria|13|False|Tap
Público|10|True|¡Uuuuuh!
Público|11|False|¡Buuuh!
Público|14|True|¡Huuuu!
Público|15|False|¡Buuh!
Alcalde de Komona|17|False|Ah, parece que el público no ha quedado seducido con esta «brillante» demostración. ¡Es el turno de Espirulina!
Escritura|16|False|-42
Público|18|True|¡Uuuh!
Público|19|True|¡Buuh!
Público|20|True|¡Huuu!
Público|21|False|¡Huu!
Público|3|False|Clap

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Espirulina|1|False|¡LIBERATE KRAKENIS!
Sonido|2|False|¡Wwwloo!
Sonido|3|False|¡Splaashh!
Alcalde de Komona|5|False|¡Precioso! ¡Potente! ¡Espirulina lidera ahora la clasificación! ¡Celandria, te toca!
Escritura|4|False|6225
Espirulina y Durian|6|False|Tap
Celandria|8|False|¡MORTUS REDITUS!
Sonido|9|False|¡Groowooo!
Alcalde de Komona|11|False|¡Ah! Parece que para nuestro público los esqueletos están ya pasados de moda… Ahora, ¡nuestra querida Azafrán!
Escritura|10|False|2023
Público|7|False|Clap

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Azafrán|1|False|¡Grrr! ¡No permitiré obtener menos puntuación que Espirulina! ¡Voy a dar el todo por el todo!
Azafrán|2|False|¡Apártate, Trufa!
Sonido|3|False|¡Frrrshh!
Sonido|4|False|¡Frrrshh!
Sonido|5|False|¡Krchh!
Trufa|6|False|¡Miau!
Azafrán|7|False|¡SPIRALIS
Azafrán|8|False|FLAMAaaaaaaah!
Sonido|9|False|¡Frrrooooshh!
Sonido|10|False|¡Swwwiiip!
Sonido|11|False|¡Frrrh!
Sonido|12|False|¡Paf!
Azafrán|13|False|?!!
Sonido|14|False|¡Frrrh!
Sonido|15|False|¡Frrrh!
Azafrán|16|False|¡¡Hiiiiiiiii!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Azafrán|1|False|¡Oh, no!… ¡Qué vergüenza!
Azafrán|3|False|?!
Escritura|4|True|14
Escritura|5|False|849
Pimienta|6|False|Tap
Lord Azeirf|7|True|Tap
Lord Azeirf|8|True|Tap
Lord Azeirf|9|False|Tap
Escritura|10|True|18
Escritura|11|False|231
Alcalde de Komona|12|True|¡Por favor! ¡Ruego un poco de seriedad!
Alcalde de Komona|13|False|¡Shichimi y Celandria quedan eliminadas!
Alcalde de Komona|14|False|Manzanilla, Espirulina y Azafrán están, por lo tanto, ¡clasificadas para la final!
Público|2|False|Clap

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Alcalde de Komona|1|False|Señoritas, ¿estáis listas para la última prueba?
Alcalde de Komona|2|False|¡Adelante!
Escritura|3|True|15
Escritura|4|False|703
Escritura|5|True|19
Escritura|6|False|863
Escritura|7|True|13
Escritura|8|False|614
Sonido|10|False|¡Plonk!
Pimienta|11|False|Retiro todo lo bueno que haya dicho sobre este sistema…
Lord Azeirf|12|True|Tap
Lord Azeirf|13|True|Tap
Lord Azeirf|14|False|Tap
Narrador|15|False|- FIN -
Público|9|False|Clap

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|05/2017 - www.peppercarrot.com - Dibujo & Guion: David Revoy - Traducción: TheFaico
Créditos|2|False|Mejora de los diálogos: Nicolas Artance, Valvin, Craig Maloney
Créditos|3|False|Basado en el universo de Hereva, creado por David Revoy con las contribuciones de Craig Maloney. Correcciones de Willem Sonke, Moini, Hali, CGand y Alex Gryson.
Créditos|4|False|Software: Krita 3.1.3, Inkscape 0.92.1 en Linux Mint 18.1
Créditos|5|False|Licencia: Creative Commons Attribution 4.0
Créditos|6|False|Pepper&Carrot es completamente gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 864 mecenas:
Créditos|7|False|Tú también puedes ser mecenas de Pepper&Carrot en www.patreon.com/davidrevoy
