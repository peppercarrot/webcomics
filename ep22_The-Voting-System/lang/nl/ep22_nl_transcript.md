# Transcript of Pepper&Carrot Episode 22 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 22: De stemkastjes

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Burgemeester van Komona|1|False|Onze grote magiewedstrijd kan eindelijk van start gaan!
Geschrift|2|False|Magiewedstrijd

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Burgemeester van Komona|1|False|En met dank aan onze briljante technici kunt u allen ook meedoen!
Burgemeester van Komona|2|False|Ja, ja, vrienden, kijk maar eens goed naar al die technische hoogstandjes die uw gaststad ter beschikking stelt!
Burgemeester van Komona|3|False|De groene knop kent een punt toe aan een kandidaat, de rode knop trekt er eentje af. U bepaalt!
Burgemeester van Komona|4|True|U vraagt natuurlijk, “En de jury dan?”
Burgemeester van Komona|5|False|Geen zorgen, we hebben overal aan gedacht!
Burgemeester van Komona|6|False|Juryleden hebben een speciaal stemkastje waarmee ze met één druk op de knop honderd punten kunnen toekennen!
Pepper|7|False|Wauw!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Burgemeester van Komona|5|False|… 50 000 Ko!
Burgemeester van Komona|1|False|En de kers op de taart: de scores verschijnen direct boven het hoofd van iedere deelnemer!
Burgemeester van Komona|3|False|De drie heksen met de hoogste scores gaan door naar de finale!
Burgemeester van Komona|4|True|En vergeet niet, de grote winnaar gaat naar huis met het enorme bedrag van …
Geschrift|2|False|1337
Publiek|6|False|Klap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carrot|2|False|Tik
Pepper|3|False|Wat een mooi concept hè, Carrot?
Pepper|4|True|Innovatief …
Pepper|5|True|Grappig …
Pepper|6|True|Democratisch …
Pepper|7|False|… het perfecte systeem!
Pepper|8|True|We hebben geen experts meer nodig om kwaliteit te bepalen!
Pepper|9|False|Wat een vooruitgang! In wat een geweldige tijd leven we toch …
Burgemeester van Komona|10|False|Heeft iedereen zijn stemkastje klaar?
Publiek|11|False|Wauw!
Publiek|12|False|Ja!
Publiek|13|False|Hoera!
Publiek|14|False|Ja!
Publiek|15|False|Ja!
Burgemeester van Komona|16|True|Goed!
Burgemeester van Komona|17|True|Laat de wedstrijd …
Burgemeester van Komona|18|False|BEGINNEN!
Publiek|1|False|Klap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Burgemeester van Komona|1|False|En we beginnen met Kamille!
Geluid|2|False|Dgiiioo…
Kamille|3|False|SYLVESTRIS!
Geluid|4|False|Bam!
Publiek|5|True|Klik
Publiek|6|True|Klik
Publiek|7|True|Klik
Publiek|8|True|Klik
Publiek|9|True|Klik
Publiek|10|True|Klik
Publiek|11|False|Klik

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Burgemeester van Komona|2|False|Kamille behaalt een uitstekende score! Dan is het nu aan Shichimi!
Geschrift|1|False|5861
Shichimi|4|False|LUX …
Geluid|5|False|Fiiiiiiizz!!
Shichimi|6|False|MAXIMA!
Publiek|7|False|Aah!!
Publiek|8|False|Auw!!
Publiek|9|False|Mijn ogen!!!
Pepper|12|False|Carrot … Geef haar toch maar een groene duim; ze is onze vriend …
Carrot|13|False|Klik
Publiek|10|True|Boe!
Publiek|11|False|Boee!
Publiek|14|True|Boee!
Publiek|15|False|Boee!
Burgemeester van Komona|17|False|Oei, het lijkt erop dat het publiek dit ‘schitterende’ staaltje toverkunst niet waardeert! Spirulina's beurt!
Geschrift|16|False|-42
Publiek|18|True|Boeee!
Publiek|19|True|Boee!
Publiek|20|True|Boee!
Publiek|21|False|Boe!
Publiek|3|False|Klap

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Spirulina|1|False|RELEASUS KRAKENIS!
Geluid|2|False|Wwwloo!
Geluid|3|False|Splonss!
Burgemeester van Komona|5|False|Geweldig! Magisch! Spirulina neemt de leiding! Koriander, jouw beurt!
Geschrift|4|False|6225
Spirulina & Durian|6|False|Pets
Koriander|8|False|MORTUS REDITUS!
Geluid|9|False|Groewooe!
Burgemeester van Komona|11|False|Volgens mij zijn skeletten uit de mode … Nu onze geliefde Saffraan!
Geschrift|10|False|2023
Publiek|7|False|Klap

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffraan|1|False|Grrr! Niks ervan dat Spirulina me verslaat! Ik ga alles geven!
Saffraan|2|False|Aan de kant, Truffel!
Geluid|3|False|Frrrsjj!
Geluid|4|False|Frrrsjj!
Geluid|5|False|Kratsj!
Truffel|6|False|Miauw!
Saffraan|7|False|SPIRALIS
Saffraan|8|False|VLAMAaaaaaaah!
Geluid|9|False|Vwwwoeeesjj!
Geluid|10|False|Glllijjj!
Geluid|11|False|Fsssj!
Geluid|12|False|Paf!
Saffraan|13|False|?!!
Geluid|14|False|Fsssj!
Geluid|15|False|Fsssj!
Saffraan|16|False|Aiiiiiee!!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Saffraan|1|False|O nee … Wat een schaamte!
Saffraan|3|False|?!
Geschrift|4|True|14
Geschrift|5|False|849
Pepper|6|False|Klik
Heer Azeirf|7|True|Klik
Heer Azeirf|8|True|Klik
Heer Azeirf|9|False|Klik
Geschrift|10|True|18
Geschrift|11|False|231
Burgemeester van Komona|12|True|Alstublieft! Houdt u allemaal uw fatsoen!
Burgemeester van Komona|13|False|Shichimi en Koriander zijn uitgeschakeld!
Burgemeester van Komona|14|False|Kamille, Spirulina en Saffraan hebben zich geplaatst voor de finale!
Publiek|2|False|Klap

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Burgemeester van Komona|1|False|Zijn jullie klaar voor de laatste uitdaging?
Burgemeester van Komona|2|False|Daar gaan we!
Geschrift|3|True|15
Geschrift|4|False|703
Geschrift|5|True|19
Geschrift|6|False|863
Geschrift|7|True|13
Geschrift|8|False|614
Geluid|10|False|Paf!
Pepper|11|False|Vergeet alles wat ik over dit systeem gezegd heb …
Heer Azeirf|12|True|Klik
Heer Azeirf|13|True|Klik
Heer Azeirf|14|False|Klik
Verteller|15|False|- EINDE -
Publiek|9|False|Klap

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Aftiteling|1|False|05/2017 - www.peppercarrot.com - Tekeningen en verhaal: David Revoy - Vertaling: Willem Sonke, Midgard, Marno van der Maas.
Aftiteling|2|False|Dialoogverbeteringen: Nicolas Artance, Valvin, Craig Maloney.
Aftiteling|3|False|Gebaseerd op het universum van Hereva gecreëerd door David Revoy met bijdragen van Craig Maloney. Verbeteringen door Willem Sonke, Moini, Hali, CGand, Alex Gryson.
Aftiteling|4|False|Software: Krita 3.1.3, Inkscape 0.91 op Linux Mint 18.1
Aftiteling|5|False|Licentie: Creative Commons Naamsvermelding 4.0
Aftiteling|6|False|Pepper&Carrot is geheel vrij, open-bron en gesponsord door de giften van haar lezers. Voor deze aflevering bedank ik 864 patronen:
Aftiteling|7|False|Jij kan ook een patroon van Pepper&Carrot worden op www.patreon.com/davidrevoy
